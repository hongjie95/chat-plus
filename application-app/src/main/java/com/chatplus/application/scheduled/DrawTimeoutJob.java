package com.chatplus.application.scheduled;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.domain.entity.draw.MjJobEntity;
import com.chatplus.application.domain.entity.draw.SdJobEntity;
import com.chatplus.application.service.draw.MjJobService;
import com.chatplus.application.service.draw.SdJobService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * 绘画超时定时任务
 */
@Component
public class DrawTimeoutJob {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(DrawTimeoutJob.class);

    private final MjJobService mjJobService;

    private final SdJobService sdJobService;

    public DrawTimeoutJob(MjJobService mjJobService, SdJobService sdJobService) {
        this.mjJobService = mjJobService;
        this.sdJobService = sdJobService;
    }

    /**
     * mj 任务超时定时任务
     */
    @XxlJob(value = "DrawTimeoutJob#mjTimeoutJob")
    public void mjTimeoutJob() {
        // 获取参数
        String param = XxlJobHelper.getJobParam();
        int minute = NumberUtil.parseInt(param);
        if (minute <= 0) {
            minute = 10;
        }
        Date requestAtGt = DateUtil.offset(new Date(), DateField.MINUTE, -minute);
        LOGGER.message("开始执行定时查询MJ绘画任务结果").context("开始查询时间", requestAtGt).debug();
        LambdaQueryWrapper<MjJobEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.isNull(MjJobEntity::getImgUrl);
        queryWrapper.ne(MjJobEntity::getProgress, -1);
        queryWrapper.lt(MjJobEntity::getCreatedAt, requestAtGt);
        List<MjJobEntity> list = mjJobService.list(queryWrapper);
        if (CollectionUtils.isNotEmpty(list)) {
            LOGGER.message("开始执行定时查询MJ绘画任务结果").context("开始查询时间", requestAtGt).context("笔数", list.size()).info();
            list.forEach(item -> {
                item.setProgress(-1);
                item.setErrMsg("任务超时");
                mjJobService.updateById(item);
            });
        }
    }

    /**
     * sd 任务超时定时任务
     */
    @XxlJob(value = "DrawTimeoutJob#sdTimeoutJob")
    public void sdTimeoutJob() {
        // 获取参数
        String param = XxlJobHelper.getJobParam();
        int minute = NumberUtil.parseInt(param);
        if (minute <= 0) {
            minute = 15;
        }
        Date requestAtGt = DateUtil.offset(new Date(), DateField.MINUTE, -minute);
        LOGGER.message("开始执行定时查询SD绘画任务结果").context("开始查询时间", requestAtGt).debug();
        LambdaQueryWrapper<SdJobEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.isNull(SdJobEntity::getImgUrl);
        queryWrapper.ne(SdJobEntity::getProgress, -1);
        queryWrapper.lt(SdJobEntity::getCreatedAt, requestAtGt);
        List<SdJobEntity> list = sdJobService.list(queryWrapper);
        if (CollectionUtils.isNotEmpty(list)) {
            LOGGER.message("开始执行定时查询SD绘画任务结果").context("开始查询时间", requestAtGt).context("笔数", list.size()).info();
            list.forEach(item -> {
                item.setProgress(-1);
                sdJobService.updateById(item);
            });
        }
    }
}
