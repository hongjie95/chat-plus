package com.chatplus.application.listener.pay;

import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.common.util.PlusJsonUtils;
import com.chatplus.application.domain.entity.pay.PayRequestEntity;
import com.chatplus.application.enumeration.OrderStatusEnum;
import com.chatplus.application.enumeration.PayStatusEnum;
import com.chatplus.application.event.GenericEvent;
import com.chatplus.application.event.order.OrderUpdateEvent;
import com.chatplus.application.event.order.dto.OrderRequestEvent;
import com.chatplus.application.event.pay.PayFailEvent;
import com.chatplus.application.event.pay.PaySuccessEvent;
import com.chatplus.application.event.pay.dto.PayRequestEvent;
import com.chatplus.application.service.pay.IPayRequestService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * 支付结果事件监听
 */
@Component
public class PayResultEventListener {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(PayResultEventListener.class);
    private final IPayRequestService payRequestService;
    private final ApplicationContext applicationContext;
    public PayResultEventListener(ApplicationContext applicationContext,
                                  IPayRequestService payRequestService) {
        this.payRequestService = payRequestService;
        this.applicationContext = applicationContext;
    }
    /**
     * 支付成功后的操作
     */
    @EventListener(classes = PaySuccessEvent.class)
    @Async
    public void paySuccessEventListener(GenericEvent<PayRequestEvent> event) {
        LOGGER.message("收到支付成功事件").context("event", "CHAT").context("data", PlusJsonUtils.toJsonString(event)).info();

        PayRequestEvent payRequestEvent = event.getData();
        if (payRequestEvent == null) {
            return;
        }
        Long orderId = payRequestEvent.getOrderId();
        PayRequestEntity payRequestEntity = payRequestService.findByAppBusinessId(orderId, PayStatusEnum.SUCCESS);
        // 订单更新成功后发送订单成功事件，并给用户添上
        OrderRequestEvent orderRequestEvent = OrderRequestEvent.builder()
                .orderId(orderId)
                .successAt(payRequestEntity.getSuccessAt())
                .tradeTransactionId(payRequestEntity.getTradeTransactionId())
                .status(OrderStatusEnum.SUCCESS)
                .build();
        applicationContext.publishEvent(new OrderUpdateEvent(orderRequestEvent));
    }

    /**
     * 支付失败后的操作
     */
    @EventListener(classes = PayFailEvent.class)
    @Async
    public void payFailEventListener(GenericEvent<PayRequestEvent> event) {
        LOGGER.message("收到支付失败事件").context("event", "CHAT").context("data", PlusJsonUtils.toJsonString(event)).info();
        PayRequestEvent payRequestEvent = event.getData();
        if (payRequestEvent == null) {
            return;
        }
        Long orderId = payRequestEvent.getOrderId();
        // 订单更新成功后发送订单成功事件，并给用户添上
        OrderRequestEvent orderRequestEvent = OrderRequestEvent.builder()
                .orderId(orderId)
                .status(OrderStatusEnum.NOT)
                .build();
        applicationContext.publishEvent(new OrderUpdateEvent(orderRequestEvent));

    }
}
