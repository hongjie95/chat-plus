package com.chatplus.application.listener.sensitiveword;

import com.chatplus.application.domain.notification.ReloadSensitiveWordsEvent;
import com.chatplus.application.service.basedata.SensitiveWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class SensitiveWordListener {


    private final SensitiveWordService sensitiveWordService;

    @Autowired
    public SensitiveWordListener(SensitiveWordService sensitiveWordService) {
        this.sensitiveWordService = sensitiveWordService;
    }

    @EventListener
    public void reloadSensitiveWords(ReloadSensitiveWordsEvent event) {
        sensitiveWordService.reloadAllWords();
    }

}

