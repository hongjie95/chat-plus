package com.chatplus.application.listener;

import cn.bugstack.openai.session.Configuration;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.chatplus.application.aiprocessor.platform.image.sd.SdImageProcessor;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.common.util.OkHttpClientUtil;
import com.chatplus.application.common.util.PlusJsonUtils;
import com.chatplus.application.domain.dto.ApiKeyDto;
import com.chatplus.application.domain.dto.SdJobResult;
import com.chatplus.application.domain.entity.draw.SdJobEntity;
import com.chatplus.application.domain.notification.SdStabilityJobNotification;
import com.chatplus.application.enumeration.AiPlatformEnum;
import com.chatplus.application.service.draw.SdJobService;
import com.chatplus.application.web.notification.NotificationListener;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Sd 任务监听
 * API 购买地址：https://platform.stability.ai
 */
@Component
public class SdStabilityJobNotificationListener {

    private final SdImageProcessor sdImageProcessor;
    private final SdJobService sdJobService;
    private final SdJobProcess sdJobProcess;

    public SdStabilityJobNotificationListener(SdImageProcessor sdImageProcessor,
                                              SdJobService sdJobService,
                                              SdJobProcess sdJobProcess) {
        this.sdImageProcessor = sdImageProcessor;
        this.sdJobService = sdJobService;
        this.sdJobProcess = sdJobProcess;
    }

    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(SdStabilityJobNotificationListener.class);

    @NotificationListener(notification = SdStabilityJobNotification.class)
    @Async("getAsyncExecutor")
    public void handleSdStabilityJobNotification(SdStabilityJobNotification notification) {
        ApiKeyDto apiKeyDto = notification.getApiKeyDto();
        SdJobEntity sdJobEntity = sdJobService.getById(notification.getSdJobId());
        if (sdJobEntity == null) {
            return;
        }
        OkHttpClient okHttpClient = OkHttpClientUtil.getOkHttpClient(apiKeyDto.getProxyUrl(), true);
        Request.Builder builder = new Request.Builder()
                .addHeader("Content-Type", Configuration.APPLICATION_JSON)
                .addHeader("Accept", Configuration.APPLICATION_JSON)
                .addHeader("Authorization", "Bearer " + apiKeyDto.getApiKey())
                .addHeader("Stability-Client-ID", sdJobEntity.getId() + "")
                .addHeader("Stability-Client-Version", "v1")
                .url(apiKeyDto.getUrl() + "/v1/generation/stable-diffusion-v1-6/text-to-image")
                .post(RequestBody.create(PlusJsonUtils.toJsonString(notification.getRequest()), MediaType.parse(Configuration.APPLICATION_JSON)));
        Request request = builder.build();
        Call call = okHttpClient.newCall(request);
        try (Response response = call.execute()) {
            if (response.isSuccessful() && response.body() != null) {
                String body = response.body().string();
                SdJobResult result = PlusJsonUtils.parseObject(body, SdJobResult.class);
                // ----------- 检查返回是否成功 ------------
                if (result == null || !result.isSuccess(AiPlatformEnum.SD_STABILITY)) {
                    LOGGER.message("SD Stability请求返回为空").context("response", response).error();
                    sdJobProcess.handlerError(sdJobEntity, AiPlatformEnum.SD_STABILITY);
                    return;
                }
                // ----------上传base64图片------------
                SdJobResult.ArtifactsBean artifactsBean = result.getArtifacts().getFirst();
                String base64Image = artifactsBean.getBase64();
                String url = sdImageProcessor.saveToOss(null, base64Image);
                if (StringUtils.isEmpty(url)) {
                    LOGGER.message("SD StabilityBase64图片上传失败").context("response", body).error();
                    sdJobProcess.handlerError(sdJobEntity, AiPlatformEnum.SD_STABILITY);
                    return;
                }
                sdJobEntity.setImgUrl(url);
                sdJobEntity.setProgress(100);
            } else {
                LOGGER.message("SD Stability图片请求失败").context("response", response).error();
                sdJobProcess.handlerError(sdJobEntity, AiPlatformEnum.SD_STABILITY);
            }
        } catch (IOException e) {
            LOGGER.message("SD Stability图片请求失败").context("exception", e).error();
            sdJobProcess.handlerError(sdJobEntity, AiPlatformEnum.SD_STABILITY);
        } finally {
            LambdaUpdateWrapper<SdJobEntity> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.eq(SdJobEntity::getId, sdJobEntity.getId());
            updateWrapper.set(SdJobEntity::getProgress, sdJobEntity.getProgress());
            updateWrapper.set(StringUtils.isNotEmpty(sdJobEntity.getImgUrl()), SdJobEntity::getImgUrl, sdJobEntity.getImgUrl());
            sdJobService.update(updateWrapper);
            sdImageProcessor.notifyUpdateTask(sdJobEntity.getId());
        }
    }
}
