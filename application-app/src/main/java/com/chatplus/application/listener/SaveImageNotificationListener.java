package com.chatplus.application.listener;

import com.chatplus.application.aiprocessor.platform.image.ImgAiProcessorService;
import com.chatplus.application.aiprocessor.provider.ImgAiProcessorServiceProvider;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.domain.entity.draw.MjJobEntity;
import com.chatplus.application.domain.entity.draw.SdJobEntity;
import com.chatplus.application.domain.notification.SaveImageNotification;
import com.chatplus.application.enumeration.AiPlatformEnum;
import com.chatplus.application.service.draw.MjJobService;
import com.chatplus.application.service.draw.SdJobService;
import com.chatplus.application.web.notification.NotificationListener;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 图片文件保存
 */
@Component
public class SaveImageNotificationListener {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(SaveImageNotificationListener.class);
    private final MjJobService mjJobService;
    private final SdJobService sdJobService;
    private final ImgAiProcessorServiceProvider imgAiProcessorServiceProvider;


    public SaveImageNotificationListener(MjJobService mjJobService,
                                         SdJobService sdJobService,
                                         ImgAiProcessorServiceProvider imgAiProcessorServiceProvider) {
        this.mjJobService = mjJobService;
        this.sdJobService = sdJobService;
        this.imgAiProcessorServiceProvider = imgAiProcessorServiceProvider;
    }

    @NotificationListener(notification = SaveImageNotification.class)
    public void handleSaveImageNotification(SaveImageNotification notification) throws IOException {
        if (notification == null) {
            return;
        }
        if (notification.getBizType() == null) {
            LOGGER.message("业务类型为空").context("notification", notification).error();
            return;
        }
        if (notification.getBizId() == null) {
            LOGGER.message("业务ID为空").context("notification", notification).error();
            return;
        }
        if (StringUtils.isEmpty(notification.getNetImgUrl()) && StringUtils.isEmpty(notification.getBase64ImgUrl())) {
            LOGGER.message("图片地址为空").context("notification", notification).error();
            return;
        }
        try {
            AiPlatformEnum bizType = notification.getBizType();
            ImgAiProcessorService imgAiProcessorService = imgAiProcessorServiceProvider.getAiProcessorService(bizType);
            String imgUrl = imgAiProcessorService.saveToOss(notification.getNetImgUrl(), notification.getBase64ImgUrl());
            if (StringUtils.isEmpty(imgUrl)) {
                LOGGER.message("图片上传失败").context("notification", notification).error();
                return;
            }
            switch (bizType) {
                case MJ:
                    MjJobEntity mjJobEntity = mjJobService.getById(notification.getBizId());
                    if (mjJobEntity == null) {
                        LOGGER.message("MJ任务不存在").context("bizId", notification.getBizId()).error();
                        return;
                    }
                    mjJobEntity.setImgUrl(imgUrl);
                    mjJobService.updateById(mjJobEntity);
                    break;
                case SD_LOCAL:
                    SdJobEntity sdJobEntity = sdJobService.getById(notification.getBizId());
                    if (sdJobEntity == null) {
                        LOGGER.message("SD任务不存在").context("bizId", notification.getBizId()).error();
                        return;
                    }
                    sdJobEntity.setImgUrl(imgUrl);
                    sdJobService.updateById(sdJobEntity);
                    break;
                default:
                    LOGGER.message("未知的业务类型").context("bizType", notification.getBizType()).error();
            }
            LOGGER.message("图片存储成功").context("notification", notification).info();
        } catch (Exception e) {
            LOGGER.message("处理图片失败").context("notification", notification).exception(e).error();
            throw e;
        }
    }

}
