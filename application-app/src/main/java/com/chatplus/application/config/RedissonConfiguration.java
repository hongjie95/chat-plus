package com.chatplus.application.config;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.config.Config;
import org.redisson.spring.starter.RedissonAutoConfigurationCustomizer;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedissonConfiguration  implements RedissonAutoConfigurationCustomizer {

    private final ObjectMapper objectMapper;

    public RedissonConfiguration(ObjectMapper objectMapper) {this.objectMapper = objectMapper;}

    @Override
    public void customize(Config configuration) {
        configuration.setCodec(new JsonJacksonCodec(objectMapper));
    }

    @Bean
    public CacheManager cacheManager() {
        return new PlusSpringCacheManager();
    }
}
