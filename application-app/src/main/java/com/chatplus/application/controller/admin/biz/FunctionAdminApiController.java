package com.chatplus.application.controller.admin.biz;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.chatplus.application.domain.entity.functions.FunctionEntity;
import com.chatplus.application.domain.request.FiledUpdateRequest;
import com.chatplus.application.domain.request.admin.FunctionSaveAdminApiRequest;
import com.chatplus.application.domain.response.admin.FunctionDetailApiResponse;
import com.chatplus.application.service.functions.FunctionService;
import com.chatplus.application.web.basecontroller.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/admin/function")
@Tag(name = "后台操作函数 API", description = "后台操作函数 API")
public class FunctionAdminApiController extends BaseController {

    private final FunctionService functionService;

    public FunctionAdminApiController(FunctionService functionService) {
        this.functionService = functionService;
    }

    @GetMapping("/list")
    @Operation(summary = "后台获取函数列表")
    public List<FunctionDetailApiResponse> list() {
        LambdaQueryWrapper<FunctionEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByAsc(FunctionEntity::getCreatedAt);
        List<FunctionEntity> list = functionService.list(queryWrapper);
        return list.stream().map(FunctionDetailApiResponse::build).toList();
    }

    @PostMapping("/save")
    @Operation(summary = "后台保存更新函数")
    public FunctionDetailApiResponse save(@RequestBody FunctionSaveAdminApiRequest request) {
        FunctionEntity functionEntity = functionService.getById(request.getId());
        if (functionEntity == null) {
            functionEntity = new FunctionEntity();
        }
        functionEntity.setName(request.getName());
        functionEntity.setLabel(request.getLabel());
        functionEntity.setDescription(request.getDescription());
        functionEntity.setParameters(request.getParameters());
        functionEntity.setAction(request.getAction());
        functionEntity.setEnabled(request.isEnabled());
        functionEntity.setToken(request.getToken());
        functionService.saveOrUpdate(functionEntity);
        return FunctionDetailApiResponse.build(functionEntity);
    }

    @GetMapping("/remove")
    @Operation(summary = "管理员删除函数")
    public void remove(@RequestParam(value = "id") Long id) {
        functionService.removeById(id);
    }

    @GetMapping("/token")
    @Operation(summary = "自动生成访问token,内部接口才能用")
    public String token() {
       return StpUtil.getTokenValue();
    }

    @PostMapping("/set")
    @Operation(summary = "管理员设置函数状态")
    public void set(@RequestBody FiledUpdateRequest request) {
        functionService.update(new UpdateWrapper<FunctionEntity>()
                .set(request.getFiled(), Boolean.valueOf(request.getValue()))
                .eq("id", Long.valueOf(request.getId())));
    }
}
