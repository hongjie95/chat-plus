package com.chatplus.application.controller.api;

import com.chatplus.application.aiprocessor.handler.PromptHandler;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.domain.request.PromptApiRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 翻译相关控制机器
 */
@RestController
@RequestMapping("/api/prompt")
public class PromptApiController {

    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(PromptApiController.class);

    private final PromptHandler promptHandler;

    public PromptApiController(PromptHandler promptHandler) {
        this.promptHandler = promptHandler;
    }

    @PostMapping("/translate")
    public String translate(@RequestBody PromptApiRequest request) {
        return promptHandler.translateToEN(request.getPrompt());
    }

    @PostMapping("/translateCn")
    public String translateCn(@RequestBody PromptApiRequest request) {
        return promptHandler.translateToCN(request.getPrompt());
    }
    @PostMapping("/rewrite")
    public String rewrite(@RequestBody PromptApiRequest request) {
        return promptHandler.rewrite(request.getPrompt());

    }
}
