package com.chatplus.application.controller.api;

import cn.dev33.satoken.annotation.SaIgnore;
import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.chatplus.application.domain.entity.chat.ChatModelEntity;
import com.chatplus.application.domain.response.ChatModelDetailResponse;
import com.chatplus.application.service.chat.ChatModelService;
import com.chatplus.application.web.basecontroller.BaseController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/model")
public class ChatModelApiController extends BaseController {
    private final ChatModelService chatModelService;


    public ChatModelApiController(ChatModelService chatModelService) {
        this.chatModelService = chatModelService;
    }
    @GetMapping("/list")
    @SaIgnore
    public List<ChatModelDetailResponse> list(@RequestParam(value = "enable", defaultValue = "1") Integer enable) {
        List<ChatModelEntity> list = chatModelService.list(new LambdaQueryWrapper<ChatModelEntity>()
                .eq(ChatModelEntity::getEnabled, enable)
                .orderByAsc(ChatModelEntity::getSortNum));
        List<ChatModelDetailResponse> responseList = new ArrayList<>();
        for (ChatModelEntity entity : list) {
            ChatModelDetailResponse response = BeanUtil.copyProperties(entity, ChatModelDetailResponse.class,"createdAt", "updatedAt");
            response.setCreatedAt(entity.getCreatedAt().getEpochSecond());
            response.setUpdatedAt(entity.getUpdatedAt().getEpochSecond());
            responseList.add(response);
        }
        return responseList;
    }
}
