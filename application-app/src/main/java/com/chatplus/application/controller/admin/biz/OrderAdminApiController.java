package com.chatplus.application.controller.admin.biz;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chatplus.application.common.exception.BadRequestException;
import com.chatplus.application.common.page.PageParam;
import com.chatplus.application.domain.entity.orders.OrderEntity;
import com.chatplus.application.domain.request.admin.OrderAdminApiRequest;
import com.chatplus.application.domain.response.OrderDetailApiResponse;
import com.chatplus.application.domain.response.PlusPageResponse;
import com.chatplus.application.enumeration.OrderStatusEnum;
import com.chatplus.application.service.orders.OrderService;
import com.chatplus.application.web.basecontroller.BaseController;
import com.google.common.collect.Lists;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/order")
@Tag(name = "后台订单API", description = "后台订单API")
public class OrderAdminApiController extends BaseController {

    private final OrderService orderService;

    public OrderAdminApiController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/list")
    @Operation(summary = "后台获取订单列表")
    public PlusPageResponse<OrderDetailApiResponse> list(@RequestBody OrderAdminApiRequest plusPageRequest) {
        PageParam pageParam = new PageParam(plusPageRequest.getPage(), plusPageRequest.getPageSize());
        List<String> payTime = plusPageRequest.getPayTime();
        LambdaQueryWrapper<OrderEntity> queryWrapper = Wrappers.<OrderEntity>lambdaQuery()
                .eq(plusPageRequest.getOrderId() != null, OrderEntity::getId, plusPageRequest.getOrderId()).orderByDesc(OrderEntity::getCreatedAt);

        if (CollUtil.isNotEmpty(payTime) && payTime.size() == 2) {
            queryWrapper.ge(OrderEntity::getCreatedAt, payTime.get(0)).le(OrderEntity::getCreatedAt, payTime.get(1));
        }
        Page<OrderEntity> pageDTO = orderService.page(pageParam.toPage(), queryWrapper);
        List<OrderEntity> orderEntityList = pageDTO.getRecords();
        if (CollUtil.isEmpty(orderEntityList)) {
            return new PlusPageResponse<>(plusPageRequest.getPage(), plusPageRequest.getPageSize(), 0, 0, Lists.newArrayList());
        }
        List<OrderDetailApiResponse> responseList = orderEntityList.stream().map(OrderDetailApiResponse::build).toList();
        return new PlusPageResponse<>(pageParam.getCurrent(), pageParam.getSize()
                , pageDTO.getPages(), pageDTO.getTotal(), responseList);
    }

    @GetMapping("/remove")
    @Operation(summary = "管理员删除订单")
    public void remove(@RequestParam(value = "id") Long id) {
        OrderEntity orderEntity = orderService.getById(id);
        if (orderEntity.getStatus() == OrderStatusEnum.SUCCESS) {
            throw new BadRequestException("已支付订单不允许删除！");
        }
        orderService.removeById(id);
    }
}
