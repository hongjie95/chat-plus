package com.chatplus.application.controller.api;

import cn.dev33.satoken.annotation.SaIgnore;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.domain.entity.basedata.ProductEntity;
import com.chatplus.application.domain.response.ProductDetailApiResponse;
import com.chatplus.application.service.basedata.ProductService;
import com.chatplus.application.web.basecontroller.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 产品API
 */
@Validated
@RestController
@RequestMapping("/api/product")
@Tag(name = "产品API", description = "产品API")
public class ProductApiController extends BaseController {

    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(ProductApiController.class);

    private final ProductService productService;

    @Autowired
    public ProductApiController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/list")
    @Operation(summary = "产品列表")
    @SaIgnore
    public List<ProductDetailApiResponse> list() {
        List<ProductEntity> productsList = productService.list(new LambdaQueryWrapper<ProductEntity>()
                .eq(ProductEntity::getEnabled, Boolean.TRUE)
                .orderByAsc(ProductEntity::getSortNum));
        return productsList.stream().map(ProductDetailApiResponse::build).toList();
    }

}
