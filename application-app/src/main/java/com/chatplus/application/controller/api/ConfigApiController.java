package com.chatplus.application.controller.api;

import cn.dev33.satoken.annotation.SaIgnore;
import com.chatplus.application.common.exception.BadRequestException;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.domain.dto.AdminConfigDto;
import com.chatplus.application.domain.entity.account.UserEntity;
import com.chatplus.application.service.account.UserService;
import com.chatplus.application.service.basedata.ConfigService;
import com.chatplus.application.web.basecontroller.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static cn.dev33.satoken.stp.StpUtil.isLogin;

/**
 * 系统配置
 */
@Validated
@RestController
@RequestMapping("/api/config")
@Tag(name = "配置API", description = "配置API")
public class ConfigApiController extends BaseController {
    static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(ConfigApiController.class);
    private final ConfigService configService;
    private final UserService userService;

    public ConfigApiController(ConfigService configService, UserService userService) {
        this.configService = configService;
        this.userService = userService;
    }

    // TODO 按需返回配置信息
    @GetMapping("get")
    @Operation(summary = "获取后台配置")
    @SaIgnore
    public Object get(@RequestParam(value = "key") String key) {
        // 登录判断,除了系统配置，其他配置需要登录
        if (!key.equals("system") && !key.equals("notice") && !isLogin()) {
            throw new BadRequestException("未登录");
        }
        switch (key) {
            case "system":
                AdminConfigDto systemConfig = configService.getSystemConfig();
                if (isLogin()) {
                    UserEntity userEntity = userService.getById(getUserId());
                    if (userEntity != null) {
                        if (userEntity.getSdPower() != null) {
                            systemConfig.setSdPower(userEntity.getSdPower());
                        }
                        if (userEntity.getMjPower() != null) {
                            systemConfig.setMjPower(userEntity.getMjPower());
                        }
                        if (userEntity.getDallPower() != null) {
                            systemConfig.setDallPower(userEntity.getDallPower());
                        }
                    }
                }
                return systemConfig;
            case "notice":
                return configService.getNoticeConfig();
            case "file":
                return configService.getFileConfig();
            default:
                return null;
        }
    }
}
