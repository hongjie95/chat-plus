package com.chatplus.application.controller.api;

import cn.dev33.satoken.annotation.SaIgnore;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.domain.entity.basedata.MenuEntity;
import com.chatplus.application.domain.response.MenuDetailResponse;
import com.chatplus.application.service.basedata.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 前端菜单表视图控制器
 *
 * <p>Table: t_menu - 前端菜单表</p>
 *
 * @author developer
 * @see MenuEntity
 * @since 2024-04-08 10:17:35
 */
@RestController
@RequestMapping("/api/menu")
public class MenuApiController {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(MenuApiController.class);

    private MenuService menuService;

    @Autowired
    public void setMenuService(MenuService menuService) {
        this.menuService = menuService;
    }

    @GetMapping("/list")
    @SaIgnore
    public List<MenuDetailResponse> list() {
        List<MenuEntity> list = menuService.list(new LambdaQueryWrapper<MenuEntity>()
                .eq(MenuEntity::getEnabled, true)
                .orderByAsc(MenuEntity::getSortNum));
        return list.stream().map(MenuDetailResponse::build).toList();
    }


}
