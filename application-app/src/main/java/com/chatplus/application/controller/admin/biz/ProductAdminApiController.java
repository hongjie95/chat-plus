package com.chatplus.application.controller.admin.biz;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.chatplus.application.domain.entity.basedata.ProductEntity;
import com.chatplus.application.domain.request.FiledUpdateRequest;
import com.chatplus.application.domain.request.ProductSaveRequest;
import com.chatplus.application.domain.response.ProductDetailApiResponse;
import com.chatplus.application.service.basedata.ProductService;
import com.chatplus.application.web.basecontroller.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin/product")
@Tag(name = "充值产品API", description = "充值产品API")
public class ProductAdminApiController extends BaseController {

    private final ProductService productService;

    public ProductAdminApiController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/list")
    @Operation(summary = "后台获取充值产品列表")
    public List<ProductDetailApiResponse> list() {
        LambdaQueryWrapper<ProductEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByAsc(ProductEntity::getSortNum);
        List<ProductEntity> list = productService.list(queryWrapper);
        return list.stream().map(ProductDetailApiResponse::build).toList();
    }

    @PostMapping("/save")
    @Operation(summary = "后台保存更新充值产品")
    public ProductDetailApiResponse save(@RequestBody ProductSaveRequest productSaveRequest) {
        ProductEntity productEntity = productService.getById(productSaveRequest.getId());
        if (productEntity == null) {
            productEntity = new ProductEntity();
        }
        productEntity.setId(productSaveRequest.getId());
        productEntity.setName(productSaveRequest.getName());
        productEntity.setPrice(productSaveRequest.getPrice());
        productEntity.setDiscount(productSaveRequest.getDiscount());
        productEntity.setPower(productSaveRequest.getPower());
        productEntity.setVipDayPower(productSaveRequest.getVipDayPower());
        productEntity.setDays(productSaveRequest.getDays());
        productEntity.setSortNum(productSaveRequest.getSortNum());
        productEntity.setEnabled(productSaveRequest.isEnabled());
        productEntity.setSales(productSaveRequest.getSales());
        productEntity.setSortNum(productService.getMaxSortNum() + 1);
        productEntity.setProductType(productSaveRequest.getProductType());
        productService.saveOrUpdate(productEntity);
        return ProductDetailApiResponse.build(productEntity);
    }

    @GetMapping("/remove")
    @Operation(summary = "管理员删除充值产品")
    public void remove(@RequestParam(value = "id") Long id) {
        productService.removeById(id);
    }

    @PostMapping("/set")
    @Operation(summary = "管理员设置充值产品状态")
    public void set(@RequestBody FiledUpdateRequest request) {
        productService.update(new UpdateWrapper<ProductEntity>()
                .set(request.getFiled(), Boolean.valueOf(request.getValue()))
                .eq("id", Long.valueOf(request.getId())));
    }
}
