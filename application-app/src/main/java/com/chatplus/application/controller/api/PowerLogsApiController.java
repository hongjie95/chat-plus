package com.chatplus.application.controller.api;

import cn.dev33.satoken.annotation.SaIgnore;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.common.page.PageParam;
import com.chatplus.application.domain.entity.basedata.PowerLogsEntity;
import com.chatplus.application.domain.request.PowerLogsRequest;
import com.chatplus.application.domain.response.PlusPageResponse;
import com.chatplus.application.domain.response.PowerLogsResponse;
import com.chatplus.application.service.basedata.PowerLogsService;
import com.chatplus.application.web.basecontroller.BaseController;
import com.google.common.collect.Lists;
import io.swagger.v3.oas.annotations.Operation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.List;

import static cn.dev33.satoken.stp.StpUtil.isLogin;

/**
 * 用户算力消费日志视图控制器
 *
 * <p>Table: t_power_logs - 用户算力消费日志</p>
 *
 * @author developer
 * @see PowerLogsEntity
 * @since 2024-03-30 12:44:51
 */
@RestController
@RequestMapping("/api/powerLog")
public class PowerLogsApiController extends BaseController {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(PowerLogsApiController.class);

    private PowerLogsService powerLogsService;

    @Autowired
    public void setPowerLogsService(PowerLogsService powerLogsService) {
        this.powerLogsService = powerLogsService;
    }

    @PostMapping("/list")
    @Operation(summary = "获取用户消费记录")
    @SaIgnore
    public PlusPageResponse<PowerLogsResponse> list(@RequestBody PowerLogsRequest plusPageRequest) {
        if (!isLogin()) {
            return new PlusPageResponse<>(plusPageRequest.getPage(), 0, 0, 0, Lists.newArrayList());
        }
        PageParam pageParam = new PageParam(plusPageRequest.getPage(), plusPageRequest.getPageSize());
        LambdaQueryWrapper<PowerLogsEntity> queryWrapper = Wrappers.lambdaQuery();
        List<String> date = plusPageRequest.getDate();
        if (CollUtil.isNotEmpty(date) && date.size() == 2) {
            Instant startTime = DateUtil.beginOfDay(DateUtil.parse(date.get(0), DatePattern.NORM_DATE_PATTERN)).toInstant();
            Instant endTime = DateUtil.endOfDay(DateUtil.parse(date.get(1), DatePattern.NORM_DATE_PATTERN)).toInstant();
            queryWrapper.ge(PowerLogsEntity::getCreatedAt, startTime).le(PowerLogsEntity::getCreatedAt, endTime);
        }
        Page<PowerLogsEntity> pageDTO = powerLogsService.page(pageParam.toPage(),
                queryWrapper.eq(PowerLogsEntity::getUserId, getUserId())
                        .eq(StringUtils.isNotEmpty(plusPageRequest.getModel()), PowerLogsEntity::getModel, plusPageRequest.getModel())
                        .orderByDesc(PowerLogsEntity::getCreatedAt));
        List<PowerLogsEntity> records = pageDTO.getRecords();
        if (CollUtil.isEmpty(records)) {
            return new PlusPageResponse<>(plusPageRequest.getPage(), 0, 0, 0, Lists.newArrayList());
        }
        List<PowerLogsResponse> responseList = records.stream().map(PowerLogsResponse::build).toList();
        return new PlusPageResponse<>(pageParam.getCurrent(), pageParam.getSize()
                , pageDTO.getPages(), pageDTO.getTotal(), responseList);
    }

}
