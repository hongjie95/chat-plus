package com.chatplus.application.controller.admin.biz;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.chatplus.application.common.exception.BadRequestException;
import com.chatplus.application.domain.entity.chat.ChatRoleEntity;
import com.chatplus.application.domain.request.ChatRoleSaveRequest;
import com.chatplus.application.domain.request.FiledUpdateRequest;
import com.chatplus.application.domain.response.ChatRoleDetailResponse;
import com.chatplus.application.service.chat.ChatRoleService;
import com.chatplus.application.web.basecontroller.BaseController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/admin/role")
@Tag(name = "后台操作角色 API", description = "后台操作角色 API")
public class ChatRoleAdminApiController extends BaseController {

    private final ChatRoleService chatRoleService;

    public ChatRoleAdminApiController(ChatRoleService chatRoleService) {
        this.chatRoleService = chatRoleService;
    }

    @GetMapping("/list")
    @Operation(summary = "后台获取角色列表")
    public List<ChatRoleDetailResponse> list() {
        LambdaQueryWrapper<ChatRoleEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByAsc(ChatRoleEntity::getSortNum);
        List<ChatRoleEntity> list = chatRoleService.list(queryWrapper);
        List<ChatRoleDetailResponse> responseList = new ArrayList<>();
        for (ChatRoleEntity entity : list) {
            ChatRoleDetailResponse response = BeanUtil.copyProperties(entity, ChatRoleDetailResponse.class, "createdAt", "updatedAt");
            response.setKey(entity.getMarker());
            response.setCreatedAt(entity.getCreatedAt().getEpochSecond());
            response.setUpdatedAt(entity.getUpdatedAt().getEpochSecond());
            responseList.add(response);
        }
        return responseList;
    }

    @PostMapping("/save")
    @Operation(summary = "管理员保存更新角色")
    public ChatRoleDetailResponse save(@RequestBody ChatRoleSaveRequest chatRoleSaveRequest) {
        ChatRoleEntity chatRoleEntity = new ChatRoleEntity();
        chatRoleEntity.setSortNum(chatRoleService.getMaxSortNum() + 1);
        if (chatRoleSaveRequest.getId() != null) {
            chatRoleEntity = chatRoleService.getById(chatRoleSaveRequest.getId());
        }
        if (chatRoleEntity == null) {
            throw new BadRequestException("角色不存在");
        }
        chatRoleEntity.setName(chatRoleSaveRequest.getName());
        chatRoleEntity.setMarker(chatRoleSaveRequest.getKey());
        chatRoleEntity.setIcon(chatRoleSaveRequest.getIcon());
        chatRoleEntity.setHelloMsg(chatRoleSaveRequest.getHelloMsg());
        chatRoleEntity.setEnable(chatRoleSaveRequest.isEnable());
        chatRoleEntity.setContext(chatRoleSaveRequest.getContext());
        chatRoleService.saveOrUpdate(chatRoleEntity);
        ChatRoleDetailResponse response = BeanUtil.copyProperties(chatRoleEntity, ChatRoleDetailResponse.class, "createdAt", "updatedAt");
        response.setKey(chatRoleEntity.getMarker());
        response.setCreatedAt(chatRoleEntity.getCreatedAt().getEpochSecond());
        response.setUpdatedAt(chatRoleEntity.getUpdatedAt().getEpochSecond());
        return response;
    }

    @GetMapping("/remove")
    @Operation(summary = "管理员删除角色")
    public void remove(@RequestParam(value = "id") Long id) {
        chatRoleService.removeById(id);
    }

    @PostMapping("/set")
    @Operation(summary = "管理员设置角色状态")
    public void set(@RequestBody FiledUpdateRequest request) {
        chatRoleService.update(new UpdateWrapper<ChatRoleEntity>()
                .set(request.getFiled(), Boolean.valueOf(request.getValue()))
                .eq("id", Long.valueOf(request.getId())));
    }
}
