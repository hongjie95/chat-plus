package com.chatplus.application.controller.admin.devops;

import cn.dev33.satoken.annotation.SaIgnore;
import com.chatplus.application.aiprocessor.handler.MJWebSocketHandler;
import com.chatplus.application.aiprocessor.platform.image.ImgAiProcessorService;
import com.chatplus.application.aiprocessor.provider.ImgAiProcessorServiceProvider;
import com.chatplus.application.domain.entity.draw.MjJobEntity;
import com.chatplus.application.enumeration.AiPlatformEnum;
import com.chatplus.application.service.draw.MjJobService;
import com.chatplus.application.web.notification.NotificationPublisher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Chat
 *
 * @author chj
 * @date 2023/12/28
 **/
@RestController
@RequestMapping("/api/admin/")
public class TestController {

    private final ImgAiProcessorServiceProvider imgAiProcessorServiceProvider;
    private final MJWebSocketHandler webSocketHandler;
    private final NotificationPublisher notificationPublisher;
    private final MjJobService mjJobService;

    public TestController(ImgAiProcessorServiceProvider imgAiProcessorServiceProvider
            , MJWebSocketHandler webSocketHandler
            , NotificationPublisher notificationPublisher,
                          MjJobService mjJobService) {
        this.imgAiProcessorServiceProvider = imgAiProcessorServiceProvider;
        this.webSocketHandler = webSocketHandler;
        this.notificationPublisher = notificationPublisher;
        this.mjJobService = mjJobService;
    }

    @GetMapping("/getMjProcess")
    @SaIgnore
    public Integer getMjProcess(@RequestParam Long processId) {
        ImgAiProcessorService imgAiProcessorService = imgAiProcessorServiceProvider.getAiProcessorService(AiPlatformEnum.MJ);
        return imgAiProcessorService.getImageProgress(processId);

    }

    @GetMapping("/getMjAllProcess")
    @SaIgnore
    public void getMjAllProcess() {
        ImgAiProcessorService imgAiProcessorService = imgAiProcessorServiceProvider.getAiProcessorService(AiPlatformEnum.MJ);
        List<MjJobEntity> mjJobEntities = mjJobService.list();
        mjJobEntities.forEach(mjJobEntity -> {
            imgAiProcessorService.getImageProgress(mjJobEntity.getId());
        });
    }

}
