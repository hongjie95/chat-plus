package com.chatplus.application.controller.api.extend;

import cn.dev33.satoken.annotation.SaIgnore;
import com.chatplus.application.aiprocessor.platform.image.ImgAiProcessorService;
import com.chatplus.application.aiprocessor.provider.ImgAiProcessorServiceProvider;
import com.chatplus.application.common.constant.RedisPrefix;
import com.chatplus.application.common.exception.BadRequestException;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.domain.dto.extend.ImgResultDto;
import com.chatplus.application.domain.request.extend.Dalle3Request;
import com.chatplus.application.enumeration.AiPlatformEnum;
import com.chatplus.application.util.BaiduTransUtil;
import com.xingyuv.captcha.util.MD5Util;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.redisson.codec.SerializationCodec;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
/**
 * Chat GPT dalle3 画图
 */
@Validated
@RestController
@RequestMapping("/api/extend/")
@Tag(name = "微博第三方API", description = "微博第三方API")
public class GptDalle3ExtendController {
    static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(GptDalle3ExtendController.class);
    private final ImgAiProcessorServiceProvider imgAiProcessorServiceProvider;
    private final RedissonClient redissonClient;

    public GptDalle3ExtendController(ImgAiProcessorServiceProvider imgAiProcessorServiceProvider,
                                     RedissonClient redissonClient) {
        this.imgAiProcessorServiceProvider = imgAiProcessorServiceProvider;
        this.redissonClient = redissonClient;
    }

    @PostMapping("/dalle3")
    @Operation(summary = "获取dalle3画图")
    @SaIgnore
    public ImgResultDto dalle3(@RequestBody Dalle3Request request) {
        String prompt = request.getPrompt();
        String redisKey = RedisPrefix.EXTEND_REDIS_PREFIX_TEST + "dalle3:" + MD5Util.md5(prompt);
        RBucket<ImgResultDto> rBucket = redissonClient.getBucket(redisKey, new SerializationCodec());
        ImgResultDto cache = rBucket.isExists() ? rBucket.get() : null;
        if (cache != null) {
            return cache;
        }
        ImgAiProcessorService imgAiProcessorService = imgAiProcessorServiceProvider.getAiProcessorService(AiPlatformEnum.OPEN_AI);
        cache = imgAiProcessorService.process(prompt);
        if (cache == null) {
            throw new BadRequestException("获取dalle3画图失败");
        }
        if (StringUtils.isNotEmpty(cache.getRevisedPrompt())) {
            cache.setRevisedPrompt(BaiduTransUtil.getTransResult(cache.getRevisedPrompt(), null, null));
        }
        rBucket.set(cache, Duration.ofMinutes(30));
        return cache;
    }
}
