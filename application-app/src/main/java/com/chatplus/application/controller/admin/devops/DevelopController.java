package com.chatplus.application.controller.admin.devops;

import cn.dev33.satoken.annotation.SaIgnore;
import com.chatplus.application.aiprocessor.platform.image.chatgpt.ChatGPTImageProcessor;
import com.chatplus.application.common.annotation.RateLimiter;
import com.chatplus.application.common.annotation.RepeatSubmit;
import com.chatplus.application.common.enumeration.LimitType;
import com.chatplus.application.domain.request.AccountLoginRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * Chat
 *
 * @author chj
 * @date 2023/12/28
 **/
@RestController
@RequestMapping("/develop")
public class DevelopController {

    @Autowired
    private ChatGPTImageProcessor chatGPTImageProcessor;

    @GetMapping("/genImg")
    @SaIgnore
    public String list(@RequestParam(name = "prompt") String prompt,
                       @RequestParam(name = "modelName") String modelName) {
        return "";
    }

    /**
     * 测试请求IP限流(key基于参数获取)
     * 同一IP请求受影响
     */
    @RateLimiter(count = 1, time = 5, limitType = LimitType.IP)
    @GetMapping("/testObj")
    @SaIgnore
    public String testObj() {
        return "操作成功";
    }

    @RepeatSubmit(interval = 2, timeUnit = TimeUnit.SECONDS)
    @PostMapping("/testObj")
    @SaIgnore
    public String testSubmit(@RequestBody AccountLoginRequest request) {
        return "操作成功";
    }
}
