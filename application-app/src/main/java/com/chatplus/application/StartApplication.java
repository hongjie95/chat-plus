package com.chatplus.application;

import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.config.extend.ExtendConfigContextInitializer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 启动类
 */
@SpringBootApplication(exclude = ErrorMvcAutoConfiguration.class)
@EnableAspectJAutoProxy(proxyTargetClass = true, exposeProxy = true)
public class StartApplication {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(StartApplication.class);

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(StartApplication.class);
        ApplicationPidFileWriter pidFileWriter = new ApplicationPidFileWriter();
        pidFileWriter.setTriggerEventType(ApplicationReadyEvent.class);
        app.addListeners(pidFileWriter);
        app.addInitializers(new ExtendConfigContextInitializer());
        app.run(args);
        LOGGER.message(">>>应用启动成功<<<").info();
    }

}
