management:
  endpoints:
    enabled-by-default: true
    web:
      exposure:
        include: health
  metrics:
    tags:
      application: ${spring.application.name}
  endpoint:
    health:
      show-details: never
      cache:
        time-to-live: 1m
      status:
        http-mapping:
          up: 200
          down: 503
  health:
    redis:
      enabled: true
    db:
      enabled: true
    elasticsearch:
      enabled: ${elasticsearch.connection.enabled:false}

mybatis-plus:
  mapper-locations: classpath*:mapper/*.xml, classpath*:mapper/**/*.xml
  configuration:
    call-setters-on-nulls: on
    default-enum-type-handler: com.chatplus.application.datasource.handler.NameValueEnumTypeHandler
  global-config:
    db-config:
      id-type: assign_id
      update-strategy: ignored
  type-aliases-package: com.chatplus.application.domain.entity

spring:
  main:
    banner-mode: "console"
    allow-bean-definition-overriding: true
  servlet:
    multipart:
      max-file-size: 100MB
      max-request-size: 100MB
  application:
    name: chatplus
  profiles:
    group:
      default:
        - dev
      local:
        - local
      prod:
        - prod
    active: local
  jackson:
    serialization:
      write_dates_as_timestamps: false
      write_date_timestamps_as_nanoseconds: false
    deserialization:
      read_date_timestamps_as_nanoseconds: false
    mapper:
      accept_case_insensitive_properties: true
  pid:
    fail-on-write-error: true
  task:
    execution:
      pool:
        queue-capacity: 640
    scheduling:
      pool:
        size: 64
  aop:
    # 使用cglib动态代理
    proxy-target-class: true

logging:
  config: classpath:logback-spring.xml
  level:
    EVENT: info
    NOTIFICATION: info
    JOB: info
    com.chatplus: info
    org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver: info

server:
  servlet:
    context-path: /
  forward-headers-strategy: native
  undertow:
    # HTTP post内容的最大大小。当值为-1时，默认值为大小是无限的
    max-http-post-size: -1
    # 以下的配置会影响buffer,这些buffer会用于服务器连接的IO操作,有点类似netty的池化内存管理
    # 每块buffer的空间大小,越小的空间被利用越充分
    buffer-size: 512
    # 是否分配的直接内存
    direct-buffers: true
    threads:
      # 设置IO线程数, 它主要执行非阻塞的任务,它们会负责多个连接, 默认设置每个CPU核心一个线程
      io: 8
      # 阻塞任务线程池, 当执行类似servlet请求阻塞操作, undertow会从这个线程池中取得线程,它的值设置取决于系统的负载
      worker: 256
xss:
  filter:
    enabled: true
    excludes:
async:
  executor:
    thread:
      core_pool_size: 10
      max_pool_size: 50
      queue_capacity: 1000
      keep_alive_seconds: 300

springdoc:
  api-docs:
    # 是否开启接口文档
    enabled: true

# Sa-Token配置
sa-token:
  # token名称 (同时也是cookie名称)
  token-name: Authorization
  # token 有效期（单位：秒），默认30天，-1代表永不过期
  timeout: 2592000
  # 多端不同 token 有效期 可查看 LoginHelper.loginByDevice 方法自定义
  # token最低活跃时间 (指定时间无操作就过期) 单位: 秒
  active-timeout: -1
  # 允许动态设置 token 有效期
  dynamic-active-timeout: true
  # 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录)
  is-concurrent: true
  # 在多人登录同一账号时，是否共用一个token (为true时所有登录共用一个token, 为false时每次登录新建一个token)
  is-share: false
  # 是否尝试从header里读取token
  is-read-header: true
  # 是否尝试从cookie里读取token
  is-read-cookie: false
  # token前缀
  # token-prefix: "Bearer"
  # jwt秘钥
  jwt-secret-key: 3sriwLUplXoIQDLZJAufenz5fHlvBn

# security配置
security:
  # 排除路径
  excludes:
    - /error
    # 静态资源
    - /*.html
    - /**/*.html
    - /**/*.css
    - /**/*.js
    # 公共路径
    - /favicon.ico
    - /error
    # swagger 文档配置
    - /*/api-docs
    - /*/api-docs/**
    # actuator 监控配置
    - /actuator
    - /actuator/**
    # 运维接口
    - /devops
    - /devops/**
    # 消息推送
    - /ws/**