package cn.bugstack.openai.session.defaults;

import cn.bugstack.openai.exception.OpenAiSdkException;
import cn.bugstack.openai.executor.Executor;
import cn.bugstack.openai.executor.parameter.ChatChoice;
import cn.bugstack.openai.executor.parameter.CompletionRequest;
import cn.bugstack.openai.executor.parameter.CompletionResponse;
import cn.bugstack.openai.executor.parameter.ImageRequest;
import cn.bugstack.openai.executor.parameter.ImageResponse;
import cn.bugstack.openai.executor.parameter.Message;
import cn.bugstack.openai.executor.parameter.PictureRequest;
import cn.bugstack.openai.executor.parameter.RequestChannel;
import cn.bugstack.openai.session.Configuration;
import cn.bugstack.openai.session.OpenAiSession;
import com.chatplus.application.common.util.PlusJsonUtils;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.sse.EventSource;
import okhttp3.sse.EventSourceListener;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class DefaultOpenAiSession implements OpenAiSession {

    private final Configuration configuration;
    private final Map<RequestChannel, Executor> executorGroup;

    public DefaultOpenAiSession(Configuration configuration, Map<RequestChannel, Executor> executorGroup) {
        this.configuration = configuration;
        this.executorGroup = executorGroup;
    }

    @Override
    public EventSource completions(CompletionRequest completionRequest, EventSourceListener eventSourceListener) throws Exception {
        // 选择执行器；model -> ChatGLM/ChatGPT
        Executor executor = executorGroup.get(completionRequest.getChannel());
        if (null == executor) throw new OpenAiSdkException(completionRequest.getChannel() + " 渠道尚未实现！");
        return executor.completions(completionRequest, eventSourceListener);
    }

    @Override
    public EventSource completions(String apiHostByUser, String apiKeyByUser, CompletionRequest completionRequest, EventSourceListener eventSourceListener) throws Exception {
        // 选择执行器；model -> ChatGLM/ChatGPT
        Executor executor = executorGroup.get(completionRequest.getChannel());
        if (null == executor) throw new OpenAiSdkException(completionRequest.getChannel() + " 渠道尚未实现！");
        // 执行结果
        return executor.completions(apiHostByUser, apiKeyByUser, completionRequest, eventSourceListener);
    }

    @Override
    public CompletableFuture<String> completions(CompletionRequest completionRequest) throws Exception {
        return completions(null, null, completionRequest);
    }

    @Override
    public CompletableFuture<String> completions(String apiHostByUser, String apiKeyByUser, CompletionRequest completionRequest) throws Exception {
        // 用于执行异步任务并获取结果
        CompletableFuture<String> future = new CompletableFuture<>();
        StringBuilder dataBuffer = new StringBuilder();

        completions(apiHostByUser, apiKeyByUser, completionRequest, new EventSourceListener() {
            @Override
            public void onEvent(EventSource eventSource, @Nullable String id, @Nullable String type, String data) {
                if ("[DONE]".equalsIgnoreCase(data)) {
                    future.complete(dataBuffer.toString());
                    return;
                }

                CompletionResponse chatCompletionResponse = PlusJsonUtils.parseObject(data, CompletionResponse.class);
                List<ChatChoice> choices = chatCompletionResponse.getChoices();
                for (ChatChoice chatChoice : choices) {
                    Message delta = chatChoice.getDelta();
                    if (CompletionRequest.Role.ASSISTANT.getCode().equals(delta.getRole())) continue;

                    // 应答完成
                    String finishReason = chatChoice.getFinishReason();
                    if (StringUtils.isNoneBlank(finishReason) && "stop".equalsIgnoreCase(finishReason)) {
                        future.complete(dataBuffer.toString());
                        return;
                    }

                    // 填充数据
                    dataBuffer.append(delta.getContent());
                }
            }

            @Override
            public void onClosed(EventSource eventSource) {
                future.completeExceptionally(new OpenAiSdkException("Request closed before completion"));
            }

            @Override
            public void onFailure(EventSource eventSource, @Nullable Throwable t, @Nullable Response response) {
                future.completeExceptionally(new OpenAiSdkException("Request closed before completion"));
            }
        });

        return future;
    }

    @Override
    public ImageResponse genImages(ImageRequest imageRequest) throws Exception {
        // 选择执行器；model -> ChatGLM/ChatGPT
        return executorGroup.get(imageRequest.getChannel()).genImages(imageRequest);
    }

    @Override
    public ImageResponse genImages(String apiHostByUser, String apiKeyByUser, ImageRequest imageRequest) throws Exception {
        // 选择执行器；model -> ChatGLM/ChatGPT
        return executorGroup.get(imageRequest.getChannel()).genImages(apiHostByUser, apiKeyByUser, imageRequest);
    }

    @Override
    public OkHttpClient getClient() {
        return configuration.getOkHttpClient();
    }

    @Override
    public EventSource pictureUnderstanding(PictureRequest pictureRequest, EventSourceListener eventSourceListener) throws Exception {
        return pictureUnderstanding(null, null, pictureRequest, eventSourceListener);
    }

    @Override
    public EventSource pictureUnderstanding(String apiHostByUser, String apiKeyByUser, PictureRequest pictureRequest, EventSourceListener eventSourceListener) throws Exception {
        // 选择执行器；model -> ChatGLM/ChatGPT
        Executor executor = executorGroup.get(pictureRequest.getChannel());
        if (null == executor) {
            throw new OpenAiSdkException(pictureRequest.getChannel() + " 执行器尚未实现！");
        }
        // 执行结果
        return executor.pictureUnderstanding(apiHostByUser, apiKeyByUser, pictureRequest, eventSourceListener);
    }

    @Override
    public void cancelRequest(Object tag) {
        if (tag == null || getClient() == null) {
            return;
        }
        for (Call call : getClient().dispatcher().queuedCalls()) {
            Object callTag = call.request().tag();
            if (callTag != null && callTag.toString().contains(tag.toString())) {
                call.cancel();
                getClient().dispatcher().executorService().shutdown();
            }
        }
        for (Call call : getClient().dispatcher().runningCalls()) {
            Object callTag = call.request().tag();
            if (callTag != null && callTag.toString().contains(tag.toString())) {
                call.cancel();
                getClient().dispatcher().executorService().shutdown();
            }
        }
    }
}
