package cn.bugstack.openai.session;

/**
 * 工厂接口
 *
 * @author 小傅哥，微信：fustack
 */
public interface OpenAiSessionFactory {

    OpenAiSession openSession();

}
