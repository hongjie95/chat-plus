package cn.bugstack.openai.session;

import cn.bugstack.openai.executor.Executor;
import cn.bugstack.openai.executor.model.aliyun.AliModelExecutor;
import cn.bugstack.openai.executor.model.aliyun.config.AliModelConfig;
import cn.bugstack.openai.executor.model.baidu.BaiduModelExecutor;
import cn.bugstack.openai.executor.model.baidu.config.BaiduConfig;
import cn.bugstack.openai.executor.model.brain360.Brain360ModelExecutor;
import cn.bugstack.openai.executor.model.brain360.config.Brain360Config;
import cn.bugstack.openai.executor.model.chatglm.ChatGLMModelExecutor;
import cn.bugstack.openai.executor.model.chatglm.config.ChatGLMConfig;
import cn.bugstack.openai.executor.model.chatgpt.ChatGPTModelExecutor;
import cn.bugstack.openai.executor.model.claude.config.ClaudeConfig;
import cn.bugstack.openai.executor.model.gemini.GeminiProModelExecutor;
import cn.bugstack.openai.executor.model.gemini.config.GeminiProConfig;
import cn.bugstack.openai.executor.model.google.PalmChatModelExecutor;
import cn.bugstack.openai.executor.model.google.config.PalmConfig;
import cn.bugstack.openai.executor.model.tencent.TencentModelExecutor;
import cn.bugstack.openai.executor.model.tencent.config.TencentConfig;
import cn.bugstack.openai.executor.model.xunfei.XunFeiModelExecutor;
import cn.bugstack.openai.executor.model.xunfei.config.XunFeiConfig;
import cn.bugstack.openai.executor.parameter.RequestChannel;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.sse.EventSource;
import okhttp3.sse.EventSources;

import java.util.EnumMap;
import java.util.Map;

/**
 * 配置文件
 *
 * @author 小傅哥，微信：fustack
 */
@Slf4j
@Data
public class Configuration {
    /**
     * 智谱Ai ChatGLM Config
     */
    private ChatGLMConfig chatGLMConfig;

    /**
     * 讯飞
     */
    private XunFeiConfig xunFeiConfig;

    /**
     * 阿里通义千问
     */
    private AliModelConfig aliModelConfig;

    /**
     * 百度文心一言
     */
    private BaiduConfig baiduConfig;

    /**
     * 腾讯混元
     */
    private TencentConfig tencentConfig;

    /**
     * Google Palm2
     */
    private PalmConfig palmConfig;

    /**
     * 360智脑
     */
    private Brain360Config brain360Config;

    /**
     * claudeConfig
     */
    private ClaudeConfig claudeConfig;

    /**
     * GeminiProConfig
     */
    private GeminiProConfig geminiProConfig;

    /**
     * OkHttpClient
     */
    private OkHttpClient okHttpClient;

    private Map<RequestChannel, Executor> executorGroup;

    public EventSource.Factory createRequestFactory() {
        return EventSources.createFactory(okHttpClient);
    }

    // OkHttp 配置信息
    private HttpLoggingInterceptor.Level level = HttpLoggingInterceptor.Level.HEADERS;
    private long connectTimeout = 4500;
    private long writeTimeout = 4500;
    private long readTimeout = 4500;

    // http keywords
    public static final String SSE_CONTENT_TYPE = "text/event-stream";
    public static final String DEFAULT_USER_AGENT = "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)";
    public static final String APPLICATION_JSON = "application/json";
    public static final String JSON_CONTENT_TYPE = APPLICATION_JSON + "; charset=utf-8";

    public Map<RequestChannel, Executor> newExecutorGroup() {
        this.executorGroup = new EnumMap<>(RequestChannel.class);
        // 经过测试--------------------------------
        executorGroup.put(RequestChannel.CHAT_GLM, new ChatGLMModelExecutor(this));
        executorGroup.put(RequestChannel.OPEN_AI, new ChatGPTModelExecutor(this));
        executorGroup.put(RequestChannel.XUN_FEI, new XunFeiModelExecutor(this));
        executorGroup.put(RequestChannel.ALI_YUN_Q_WEN, new AliModelExecutor(this));
        executorGroup.put(RequestChannel.BAI_DU, new BaiduModelExecutor(this));
        // 未经过测试--------------------------------
        executorGroup.put(RequestChannel.GOOGLE, new PalmChatModelExecutor(this));
        executorGroup.put(RequestChannel.ZHI_NAO_360, new Brain360ModelExecutor(this));
        executorGroup.put(RequestChannel.TECENT_HUNYUAN, new TencentModelExecutor(this));
        executorGroup.put(RequestChannel.GEMINI, new GeminiProModelExecutor(this));
        return executorGroup;
    }
//    public Map<String, Executor> newExecutorGroup() {
//        this.executorGroup = new HashMap<>();
//        // ChatGLM 类型执行器填充
//        Executor chatGLMModelExecutor = new ChatGLMModelExecutor(this);
//        Executor charGLMModelExecutor = new CharGLMModelExecutor(this);
//        executorGroup.put(CompletionRequest.Model.GLM_4.getCode(), chatGLMModelExecutor);
//        executorGroup.put(CompletionRequest.Model.GLM_4V.getCode(), chatGLMModelExecutor);
//        executorGroup.put(CompletionRequest.Model.GLM_3_5_TURBO.getCode(), chatGLMModelExecutor);
//        executorGroup.put(CompletionRequest.Model.CHARGLM_3.getCode(), charGLMModelExecutor);
//        // ChatGPT 类型执行器填充
//        Executor chatGPTModelExecutor = new ChatGPTModelExecutor(this);
//        executorGroup.put(CompletionRequest.Model.GPT_3_5_TURBO.getCode(), chatGPTModelExecutor);
//        executorGroup.put(CompletionRequest.Model.GPT_3_5_TURBO_16K_0613.getCode(), chatGPTModelExecutor);
//        executorGroup.put(CompletionRequest.Model.GPT_3_5_TURBO_0125.getCode(), chatGPTModelExecutor);
//        executorGroup.put(CompletionRequest.Model.GPT_3_5_TURBO_1106.getCode(), chatGPTModelExecutor);
//        executorGroup.put(CompletionRequest.Model.GPT_3_5_TURBO_16K.getCode(), chatGPTModelExecutor);
//        executorGroup.put(CompletionRequest.Model.GPT_4.getCode(), chatGPTModelExecutor);
//        executorGroup.put(CompletionRequest.Model.GPT_4_32K.getCode(), chatGPTModelExecutor);
//        executorGroup.put(CompletionRequest.Model.GPT_4_1106_PREVIEW.getCode(), chatGPTModelExecutor);
//        executorGroup.put(CompletionRequest.Model.GPT_4_ALL.getCode(), chatGPTModelExecutor);
//
//        executorGroup.put(CompletionRequest.Model.DALL_E_2.getCode(), chatGPTModelExecutor);
//        executorGroup.put(CompletionRequest.Model.DALL_E_3.getCode(), chatGPTModelExecutor);
//        // XUNFEI
//        Executor xunfeiModelExecutor = new XunFeiModelExecutor(this);
//        executorGroup.put(CompletionRequest.Model.XUNFEI_V1.getCode(), xunfeiModelExecutor);
//        executorGroup.put(CompletionRequest.Model.XUNFEI_V2.getCode(), xunfeiModelExecutor);
//        executorGroup.put(CompletionRequest.Model.XUNFEI_V3.getCode(), xunfeiModelExecutor);
//        executorGroup.put(CompletionRequest.Model.XUNFEI_V35.getCode(), xunfeiModelExecutor);
//        // 阿里通义千问
//        Executor aliModelExecutor = new AliModelExecutor(this);
//        executorGroup.put(CompletionRequest.Model.QWEN_TURBO.getCode(), aliModelExecutor);
//        executorGroup.put(CompletionRequest.Model.QWEN_PLUS.getCode(), aliModelExecutor);
//        executorGroup.put(CompletionRequest.Model.QWEN_MAX.getCode(), aliModelExecutor);
//        executorGroup.put(CompletionRequest.Model.QWEN_MAX_1201.getCode(), aliModelExecutor);
//        executorGroup.put(CompletionRequest.Model.QWEN_MAX_LONGCONTEXT.getCode(), aliModelExecutor);
//        // 百度文心一言
//        Executor wenXinModelExecutor = new BaiduModelExecutor(this);
//        executorGroup.put(CompletionRequest.Model.ERNIE_BOT_TURBO.getCode(), wenXinModelExecutor);
//        executorGroup.put(CompletionRequest.Model.ERNIE_BOT.getCode(), wenXinModelExecutor);
//        executorGroup.put(CompletionRequest.Model.ERNIE_Bot_4.getCode(), wenXinModelExecutor);
//        executorGroup.put(CompletionRequest.Model.ERNIE_Bot_8K.getCode(), wenXinModelExecutor);
//        executorGroup.put(CompletionRequest.Model.STABLE_DIFFUSION_XL.getCode(), wenXinModelExecutor);
//        // 腾讯混元
//        Executor tencentExecutor = new TencentModelExecutor(this);
//        executorGroup.put(CompletionRequest.Model.HUNYUAN_CHATSTD.getCode(), tencentExecutor);
//        executorGroup.put(CompletionRequest.Model.HUNYUAN_CHATPRO.getCode(), tencentExecutor);
//
//        // 360智脑
//        Executor brain360Executor = new Brain360ModelExecutor(this);
//        executorGroup.put(CompletionRequest.Model.Brain_360GPT_S2_V9.getCode(), brain360Executor);
//
//        // Google Palm
//        executorGroup.put(CompletionRequest.Model.PALM_CHAT.getCode(), new PalmChatModelExecutor(this));
//        executorGroup.put(CompletionRequest.Model.PALM_TEXT.getCode(), new PalmTextModelExecutor(this));
//
//        // Claude
//        // TODO: 2023/12/14 未实现
//
//        // GeminiPro
//        Executor geminiProExecutor = new GeminiProModelExecutor(this);
//        executorGroup.put(CompletionRequest.Model.GEMINI_PRO.getCode(), geminiProExecutor);
//        executorGroup.put(CompletionRequest.Model.GEMINI_PRO_VERSION.getCode(), geminiProExecutor);
//
//        return this.executorGroup;
    //   }

}
