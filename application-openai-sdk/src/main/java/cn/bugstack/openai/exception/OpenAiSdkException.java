package cn.bugstack.openai.exception;

import lombok.Getter;

import java.io.Serial;

/**
 * SDK异常
 *
 * @author ruoyi
 */
@Getter
public final class OpenAiSdkException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 错误码
     */
    private final Integer code;
    /**
     * 错误提示
     */
    private final String message;
    public OpenAiSdkException() {
        this.code = 500;
        this.message = "SDK服务器异常";
    }
    public OpenAiSdkException(String message) {
        this.code = 400;
        this.message = message;
    }
    public OpenAiSdkException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
