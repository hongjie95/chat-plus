package cn.bugstack.openai.executor.model.chatgpt.valobj;

import cn.bugstack.openai.executor.parameter.ImageEnum;
import cn.bugstack.openai.executor.parameter.ImageRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * 图片请求
 *
 * @author 小傅哥，微信：fustack
 */
@Slf4j
@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class ChatGPTImageRequest extends ChatGPTImageEnum implements Serializable {

    /** 模型 */
    @Builder.Default
    private String model = Model.DALL_E_3.code;
    /** 问题描述 */
    @NonNull
    private String prompt;
    /** 为每个提示生成的完成次数 */
    @Builder.Default
    private Integer n = 1;
    /** 图片大小 */
    @Builder.Default
    private String size = Size.size_1024.getCode();
    @JsonProperty("response_format")
    @Builder.Default
    private String responseFormat = ImageEnum.ResponseFormat.URL.getCode();
    /**
     @Setter
    private String user; */
    /**
     * 此参数仅仅dall-e-3,默认值：standard
     */
    @Builder.Default
    private String quality = ImageRequest.Quality.STANDARD.getCode();

    /**
     * 此参数仅仅dall-e-3,取值范围：vivid、natural
     * 默认值：vivid
     *
     * @see ImageRequest.Style
     */
    @Builder.Default
    private String style = ImageRequest.Style.VIVID.getCode();
    @Getter
    @AllArgsConstructor
    public enum Model {
        DALL_E_2("dall-e-2"),
        DALL_E_3("dall-e-3"),
        ;
        private final String code;
    }

}
