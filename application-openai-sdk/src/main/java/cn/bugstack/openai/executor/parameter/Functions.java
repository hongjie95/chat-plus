package cn.bugstack.openai.executor.parameter;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
/**
 * 函数调用对象
 *
 * @author chj
 * @date 2024/1/30
 **/
@Data
@Builder
public class Functions implements Serializable{
    private String name;
    private String description;
    private Parameters parameters;

    @Data
    @Builder
    public static class Parameters implements Serializable {
        private String type;
        private Object properties;
        private List<String> required;
    }
}
