package cn.bugstack.openai.executor.model.aliyun.config;

/**
 * 通义千问配置信息
 *
 * @author Angus
 */
public class AliModelConfig {

    private AliModelConfig() {
    }

    public static final String API_HOST = "https://dashscope.aliyuncs.com";

    public static final String V1_COMPLETIONS = "/api/v1/services/aigc/text-generation/generation";
}
