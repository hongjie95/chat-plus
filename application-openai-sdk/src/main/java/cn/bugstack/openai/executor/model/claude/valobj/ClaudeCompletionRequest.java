package cn.bugstack.openai.executor.model.claude.valobj;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * ChatGLM 请求参数
 *
 * @author 小傅哥，微信：fustack, fy
 */
@Slf4j
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
//@NoArgsConstructor
//@AllArgsConstructor
public class ClaudeCompletionRequest {


}
