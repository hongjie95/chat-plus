package cn.bugstack.openai.executor.parameter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 函数返回
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FunctionCall implements Serializable {
    /**
     * 方法名
     */
    private String name;
    /**
     * 方法参数
     */
    private String arguments;
}
