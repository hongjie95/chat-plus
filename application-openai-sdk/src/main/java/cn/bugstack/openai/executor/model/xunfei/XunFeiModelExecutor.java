package cn.bugstack.openai.executor.model.xunfei;

import cn.bugstack.openai.executor.Executor;
import cn.bugstack.openai.executor.model.xunfei.config.XunFeiConfig;
import cn.bugstack.openai.executor.model.xunfei.valobj.Usage;
import cn.bugstack.openai.executor.model.xunfei.valobj.*;
import cn.bugstack.openai.executor.parameter.Message;
import cn.bugstack.openai.executor.parameter.*;
import cn.bugstack.openai.session.Configuration;
import com.chatplus.application.common.exception.BadRequestException;
import com.chatplus.application.common.util.PlusJsonUtils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import okhttp3.sse.EventSource;
import okhttp3.sse.EventSourceListener;
import org.apache.commons.collections4.CollectionUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.concurrent.CountDownLatch;

/**
 * 讯飞大模型 https://console.xfyun.cn/services/bm3
 *
 * @author 小傅哥，微信：fustack
 */
@Slf4j
public class XunFeiModelExecutor implements Executor, ParameterHandler<XunFeiCompletionRequest> {
    /**
     * 客户端
     */
    private final OkHttpClient okHttpClient;
    public XunFeiModelExecutor(Configuration configuration) {
        this.okHttpClient = configuration.getOkHttpClient();
    }

    @Override
    public EventSource completions(CompletionRequest completionRequest, EventSourceListener eventSourceListener) throws Exception {
        return completions(null, null, completionRequest, eventSourceListener);
    }

    @Override
    public EventSource completions(String apiHostByUser, String apiKeyByUser, CompletionRequest completionRequest, EventSourceListener eventSourceListener) throws Exception {
        // 1. 核心参数校验；不对用户的传参做更改，只返回错误信息。
        if (!completionRequest.isStream()) {
            throw new BadRequestException("illegal parameter stream is false!");
        }

        // 2. 动态设置 Host、Key，便于用户传递自己的信息
        String apiHost = null == apiHostByUser ? XunFeiConfig.API_HOST : apiHostByUser;
//        String authURL = getAuthURL(apiKeyByUser, apiHost);
        // 1. 转换参数信息
        XunFeiCompletionRequest xunFeiCompletionRequest = getParameterObject(completionRequest);
        // 2. 构建请求信息
        Request request = new Request.Builder()
                .url(apiHost)
                .tag(completionRequest.getTag())
                .build();
        // 3. 调用请求
        WebSocket webSocket = okHttpClient.newWebSocket(request, new BigModelWebSocketListener(xunFeiCompletionRequest, eventSourceListener));
        // 4. 封装结果
        return new EventSource() {

            @NotNull
            @Override
            public Request request() {
                return request;
            }

            @Override
            public void cancel() {
                webSocket.cancel();
            }
        };
    }

    @Override
    public ImageResponse genImages(ImageRequest imageRequest) throws Exception {
        return genImages(null, null, imageRequest);
    }

    @Override
    public ImageResponse genImages(String apiHostByUser, String apiKeyByUser, ImageRequest imageRequest) throws Exception {

        // 1. 动态设置 Host、Key，便于用户传递自己的信息
        String apiHost = null == apiHostByUser ? XunFeiConfig.API_TTI_HOST : apiHostByUser;
        //String authURL = getAuthURL(apiKeyByUser, apiHost, "POST", Boolean.FALSE);
        CompletionRequest completionRequest = CompletionRequest.builder()
                .messages(Collections.singletonList(Message.builder().role(CompletionRequest.Role.USER).content(imageRequest.getPrompt()).build()))
                .temperature(0.5f)
                .build();
        // 1. 转换参数信息
        XunFeiCompletionRequest xunFeiCompletionRequest = getParameterObject(completionRequest, "general");
        // 2. 构建请求信息
        Request request = new Request.Builder()
                .url(apiHost)

                .post(RequestBody.create(PlusJsonUtils.toJsonString(xunFeiCompletionRequest), MediaType.parse(Configuration.APPLICATION_JSON)))
                .build();
        //3. 执行请求
        try (Response execute = okHttpClient.newCall(request).execute()) {
            if (execute.isSuccessful() && execute.body() != null) {
                XunFeiCompletionResponse xunFeiGenImageResponse = PlusJsonUtils.parseObject(execute.body().string(), XunFeiCompletionResponse.class);
                if (xunFeiGenImageResponse == null) {
                    log.error("生成图片失败");
                    return null;
                }
                if (xunFeiGenImageResponse.getHeader().getCode() == XunFeiCompletionResponse.Header.Code.SUCCESS.getValue()) {
                    XunFeiCompletionResponse.Payload payload = xunFeiGenImageResponse.getPayload();

                    List<Text> texts = payload.getChoices().getText();
                    ImageResponse imageResponse = new ImageResponse();
                    imageResponse.setCreated(System.currentTimeMillis());
                    imageResponse.setData(new ArrayList<>());
                    for (Text text : texts) {
                        Item item = new Item();
                        item.setUrl(base64ToImageUrl(text.getContent()));
                        imageResponse.getData().add(item);
                    }
                    return imageResponse;

                } else {
                    log.error("生成图片失败，code:{},message:{}", xunFeiGenImageResponse.getHeader().getCode(), xunFeiGenImageResponse.getHeader().getMessage());
                }
            }
        }
        return null;
    }

    @Override
    public EventSource pictureUnderstanding(PictureRequest pictureRequest, EventSourceListener eventSourceListener) throws Exception {
        return pictureUnderstanding(null, null, pictureRequest, eventSourceListener);
    }

    @Override
    public EventSource pictureUnderstanding(String apiHostByUser, String apiKeyByUser, PictureRequest pictureRequest, EventSourceListener eventSourceListener) throws Exception {

        String apiHost = null == apiHostByUser ? XunFeiConfig.API_PICTURE_HOST : apiHostByUser;
        //String authURL = getAuthURL(apiKeyByUser, apiHost);
        // 1. 转换参数信息
        XunFeiCompletionRequest xunFeiCompletionRequest = getParameterObject(pictureRequest);
        // 2. 构建请求信息
        Request request = new Request.Builder()
                .url(apiHost)
                .build();

        WebSocket webSocket = okHttpClient.newWebSocket(request, new BigModelWebSocketListener(xunFeiCompletionRequest, eventSourceListener));

        return new EventSource() {
            @Override
            public Request request() {
                return request;
            }

            @Override
            public void cancel() {
                webSocket.cancel();
            }
        };
    }

//    private String getAuthURL(String apiKeyByUser, String apiHost) throws Exception {
//
//        return getAuthURL(apiKeyByUser, apiHost, "GET", Boolean.TRUE);
//    }
//
//    private String getAuthURL(String apiKeyByUser, String apiHost, String httpMethod, Boolean websocket) throws Exception {
//        String authURL;
//        if (apiKeyByUser == null) {
//            authURL = URLAuthUtils.getAuthURl(apiHost, xunFeiConfig.getApiKey(), xunFeiConfig.getApiSecret(), httpMethod, websocket);
//            appid = xunFeiConfig.getAppid();
//        } else {
//            // 拆解 879d40fc.fe81b961ccb561c404f844838fa09876.MjUzYTdhMWEyNThiZDBhMTE1NmRjZTk3
//            String[] configs = apiKeyByUser.split("\\.");
//            appid = configs[0];
//            authURL = URLAuthUtils.getAuthURl(apiHost, configs[1], configs[2], httpMethod, websocket);
//        }
//        return authURL;
//    }

    private String base64ToImageUrl(String base64String) {
        // 将Base64编码字符串转换为字节数组
        byte[] imageBytes = Base64.getDecoder().decode(base64String.split(",")[1]);

        // 将字节数组转换为图像URL
        return "data:image/jpeg;base64," + Base64.getEncoder().encodeToString(imageBytes);
    }

    protected static class BigModelWebSocketListener extends WebSocketListener {

        private final XunFeiCompletionRequest request;
        private final EventSourceListener eventSourceListener;
        private final CountDownLatch countDownLatch = new CountDownLatch(1);
        private final EventSource eventSource;

        public BigModelWebSocketListener(XunFeiCompletionRequest request, EventSourceListener eventSourceListener) {
            this.request = request;
            this.eventSourceListener = eventSourceListener;
            this.eventSource = new EventSource() {
                @NotNull
                @Override
                public Request request() {
                    // 创建一个虚拟的连接
                    return new Request.Builder().url(XunFeiConfig.API_HOST).build();
                }

                @Override
                public void cancel() {
                    //this.cancel();
                }
            };
        }

        @Override
        public void onOpen(@NotNull WebSocket webSocket, @NotNull Response response) {
            String appid = response.request().header(XunFeiConfig.XUN_FEI_APP_ID);
            eventSourceListener.onOpen(eventSource, response);
            new Thread(() -> {
                try {
                    // 头信息
                    request.setHeader(getHeader(appid));
                    String content = PlusJsonUtils.toJsonString(request);
                    if (content == null) {
                        content = "";
                    }
                    webSocket.send(content);
                    // 等待服务端返回完毕后关闭
                    countDownLatch.await();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                webSocket.close(1000, "");
            }).start();
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            XunFeiCompletionResponse response = PlusJsonUtils.parseObject(text, XunFeiCompletionResponse.class);
            XunFeiCompletionResponse.Header header = response.getHeader();
            int code = header.getCode();

            // 反馈失败
            if (XunFeiCompletionResponse.Header.Code.SUCCESS.getValue() != code) {
                log.error("讯飞大模型应答失败，code:{},message:{}", code, header.getMessage());
                countDownLatch.countDown();
                return;
            }
            // 封装参数
            CompletionResponse completionResponse = new CompletionResponse();
            List<ChatChoice> chatChoices = new ArrayList<>();
            ChatChoice chatChoice = new ChatChoice();

            XunFeiCompletionResponse.Payload payload = response.getPayload();
            Choices choices = payload.getChoices();
            List<Text> texts = choices.getText();

            for (Text t : texts) {
                chatChoice.setDelta(cn.bugstack.openai.executor.parameter.Message.builder()
                        .name("")
                        .role(CompletionRequest.Role.SYSTEM)
                        .content(t.getContent())
                        .functionCall(t.getFunctionCall())
                        .build());
                chatChoices.add(chatChoice);
            }
            completionResponse.setChoices(chatChoices);

            int status = header.getStatus();
            if (XunFeiCompletionResponse.Header.Status.START.getValue() == status) {
                eventSourceListener.onEvent(eventSource, null, null, PlusJsonUtils.toJsonString(completionResponse));
            } else if (XunFeiCompletionResponse.Header.Status.ING.getValue() == status) {
                eventSourceListener.onEvent(eventSource, null, null, PlusJsonUtils.toJsonString(completionResponse));
            } else if (XunFeiCompletionResponse.Header.Status.END.getValue() == status) {
                Usage usage = payload.getUsage();
                Usage.Text usageText = usage.getText();
                cn.bugstack.openai.executor.parameter.Usage openaiUsage = new cn.bugstack.openai.executor.parameter.Usage();
                openaiUsage.setPromptTokens(usageText.getPromptTokens());
                openaiUsage.setCompletionTokens(usageText.getCompletionTokens());
                openaiUsage.setTotalTokens(usageText.getTotalTokens());
                completionResponse.setUsage(openaiUsage);
                completionResponse.setCreated(System.currentTimeMillis());
                chatChoice.setFinishReason("stop");
                chatChoices.add(chatChoice);
                eventSourceListener.onEvent(eventSource, null, null, PlusJsonUtils.toJsonString(completionResponse));
                countDownLatch.countDown();
            }

        }

        @Override
        public void onClosed(WebSocket webSocket, int code, String reason) {
            eventSourceListener.onClosed(eventSource);
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, @Nullable Response response) {
            eventSourceListener.onFailure(eventSource, t, response);
        }
    }

    private XunFeiCompletionRequest getParameterObject(CompletionRequest completionRequest, String domain) {
        // 模型
        XunFeiCompletionRequest.Parameter parameter = XunFeiCompletionRequest.Parameter.builder().chat(Chat.builder()
                .domain(domain)
                .temperature(completionRequest.getTemperature())
                .maxTokens(completionRequest.getMaxTokens())
                .build()).build();
        // 内容
        List<Text> texts = new ArrayList<>();
        List<Message> messages = completionRequest.getMessages();
        for (Message message : messages) {
            if (CompletionRequest.Role.SYSTEM.getCode().equals(message.getRole())) {
                texts.add(Text.builder()
                        .role(Text.Role.SYSTEM.getName())
                        .content(message.getContent())
                        .build());
            }
            if (CompletionRequest.Role.ASSISTANT.getCode().equals(message.getRole())) {
                texts.add(Text.builder()
                        .role(Text.Role.ASSISTANT.getName())
                        .content(message.getContent())
                        .build());
            }
            if (CompletionRequest.Role.USER.getCode().equals(message.getRole())) {
                texts.add(Text.builder()
                        .role(Text.Role.USER.getName())
                        .content(message.getContent())
                        .build());
            }
        }
        XunFeiCompletionRequest.Payload payload = XunFeiCompletionRequest.Payload.builder()
                .message(cn.bugstack.openai.executor.model.xunfei.valobj.Message.builder().text(texts).build())
                .build();
        List<Functions> functions = completionRequest.getFunctions();
        if (CollectionUtils.isNotEmpty(functions) && supportFunction(completionRequest.getModel())) {
            payload.setFunctions(XunFeiCompletionRequest.XunFeiFunctions.builder()
                    .text(functions)
                    .build());
        }
        return XunFeiCompletionRequest.builder()
                .parameter(parameter)
                .payload(payload)
                .build();
    }

    @Override
    public XunFeiCompletionRequest getParameterObject(CompletionRequest completionRequest) {
        return getParameterObject(completionRequest, completionRequest.getModel());
    }

    @Override
    public boolean supportFunction(String model) {
        List<String> models = List.of("generalv3", "generalv3.5");
        return models.contains(model);
    }

    private XunFeiCompletionRequest getParameterObject(PictureRequest pictureRequest) {
        // 模型
        XunFeiCompletionRequest.Parameter parameter = XunFeiCompletionRequest.Parameter.builder().chat(Chat.builder()
                .domain("general")
                .temperature(pictureRequest.getTemperature())
                .maxTokens(pictureRequest.getMaxTokens())
                .auditing("default")
                .build()).build();
        // 内容
        List<Text> texts = new ArrayList<>();
        List<PictureRequest.Text> messages = pictureRequest.getMessages();
        for (PictureRequest.Text message : messages) {
            texts.add(Text.builder()
                    .role(Text.Role.USER.getName())
                    .content(message.getContent())
                    .contentType(message.getContentType())
                    .build());
        }

        XunFeiCompletionRequest.Payload payload = XunFeiCompletionRequest.Payload.builder()
                .message(cn.bugstack.openai.executor.model.xunfei.valobj.Message.builder().text(texts).build())
                .build();

        return XunFeiCompletionRequest.builder()
                .parameter(parameter)
                .payload(payload)
                .build();
    }

    private static XunFeiCompletionRequest.Header getHeader(String appid) {
        return XunFeiCompletionRequest.Header.builder()
                .appid(appid)
                .uid(UUID.randomUUID().toString().substring(0, 10))
                .build();
    }

}
