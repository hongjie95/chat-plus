package cn.bugstack.openai.executor.parameter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 描述信息
 *
 * @author 小傅哥，微信：fustack
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Message implements Serializable {

    @Serial
    private static final long serialVersionUID = -2171962932857512264L;

    private String role;
    private String content;
    private String name;
    @JsonProperty("function_call")
    private FunctionCall functionCall;
    public Message() {
    }

    private Message(Builder builder) {
        this.role = builder.role;
        this.content = builder.content;
        this.name = builder.name;
        this.functionCall = builder.functionCall;
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * 建造者模式
     */
    public static final class Builder {

        private String role;
        private String content;
        private String name;
        @JsonProperty("function_call")
        private FunctionCall functionCall;

        public Builder() {
        }

        public Builder role(CompletionRequest.Role role) {
            this.role = role.getCode();
            return this;
        }

        public Builder content(String content) {
            this.content = content;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }
        public Builder functionCall(FunctionCall functionCall) {
            this.functionCall = functionCall;
            return this;
        }
        public Message build() {
            return new Message(this);
        }
    }


}
