package cn.bugstack.openai.executor.parameter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

/**
 * 图片请求
 *
 * @author 小傅哥，微信：fustack
 */
@Slf4j
@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class ImageRequest extends ImageEnum implements Serializable {

    /** 模型 */
    @Builder.Default
    private String model = Model.DALL_E_3.code;
    @Builder.Default
    private RequestChannel channel = RequestChannel.OPEN_AI;
    /** 问题描述
     *  提示词：dall-e-2支持1000字符、dall-e-3支持4000字符
     * */
    @NonNull
    private String prompt;
    /** 为每个提示生成的完成次数
     * 为每个提示生成的个数，dall-e-3只能为1。
     * */
    @Builder.Default
    private Integer n = 1;
    /**
     * 此参数仅仅dall-e-3,默认值：standard
     */
    @Builder.Default
    private String quality = Quality.STANDARD.getCode();

    /**
     * 此参数仅仅dall-e-3,取值范围：vivid、natural
     * 默认值：vivid
     *
     * @see Style
     */
    @Builder.Default
    private String style = Style.VIVID.getCode();
    /** 图片大小
     * 图片尺寸，默认值：1024x1024
     * dall-e-2支持：256x256, 512x512, or 1024x1024
     * dall-e-3支持：1024x1024, 1792x1024, or 1024x1792
     * */
    @Builder.Default
    private String size = Size.size_1024.getCode();
    /**
     * 生成图片格式：url、b64_json
     *
     * @see ResponseFormat
     */
    @JsonProperty("response_format")
    @Builder.Default
    private String responseFormat = ResponseFormat.URL.getCode();
    /**
    @Setter
    private String user; */

    @Getter
    @AllArgsConstructor
    public enum Model {
        DALL_E_2("dall-e-2"),
        DALL_E_3("dall-e-3"),
        STABLE_DIFFUSION_XL("Stable_Diffusion_XL"),
        ;
        private final String code;
    }
    /**
     * 生成图片质量
     */
    @Getter
    @AllArgsConstructor
    public enum Quality {
        STANDARD("standard"),
        HD("hd"),
        ;
        private final String code;
    }

    /**
     * 生成图片风格
     */
    @Getter
    @AllArgsConstructor
    public enum Style {
        VIVID("vivid"),
        NATURAL("natural"),
        ;
        private final String code;
    }
}
