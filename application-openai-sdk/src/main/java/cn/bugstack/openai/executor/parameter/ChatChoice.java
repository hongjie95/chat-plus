package cn.bugstack.openai.executor.parameter;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 对话信息
 *
 * @author 小傅哥，微信：fustack
 */
@Data
public class ChatChoice implements Serializable {

    @Serial
    private static final long serialVersionUID = 802469482706186701L;

    private long index;
    /**
     * stream = true 请求参数里返回的属性是 delta
     */
    @JsonProperty("delta")
    private Message delta;
    /**
     * stream = false 请求参数里返回的属性是 delta
     */
    @JsonProperty("message")
    private Message message;
    @JsonProperty("finish_reason")
    private String finishReason;
}
