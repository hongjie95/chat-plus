package cn.bugstack.openai.executor.model.google.valobj.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PalmTextRequest {
    private TextPrompt prompt;
    @Builder.Default
    private float temperature = 0.1f;
    @JsonProperty("candidate_count")
    @Builder.Default
    private Integer candidateCount = 1;
    @Builder.Default
    private float topP = 0.8f;
    @Builder.Default
    private Integer topK = 10;
}
