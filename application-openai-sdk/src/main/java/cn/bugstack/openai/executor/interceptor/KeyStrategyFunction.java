package cn.bugstack.openai.executor.interceptor;

/**
 * key 的获取策略
 *
 * @author chj
 * @date 2024/3/28
 **/
@FunctionalInterface
public interface KeyStrategyFunction<T, R> {

    /**
     * Applies this function to the given argument.
     *
     * @param t the function argument
     * @return the function result
     */
    R apply(T t);

}