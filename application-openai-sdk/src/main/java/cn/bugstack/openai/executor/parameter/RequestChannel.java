package cn.bugstack.openai.executor.parameter;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 请求渠道
 *
 * @author chj
 * @date 2024/3/28
 **/
@Getter
@AllArgsConstructor
public enum RequestChannel {
    OPEN_AI, // 已完成
    CHAT_GLM,// 已完成
    XUN_FEI,// 已完成
    ALI_YUN_Q_WEN,// 已完成
    BAI_DU,// 已完成
    GOOGLE,
    ZHI_NAO_360,
    TECENT_HUNYUAN,
    GEMINI
}
