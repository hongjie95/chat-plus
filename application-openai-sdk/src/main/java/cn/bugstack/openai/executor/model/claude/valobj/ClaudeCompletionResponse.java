package cn.bugstack.openai.executor.model.claude.valobj;

import lombok.Data;

/**
 * Claude 应答参数
 *
 * @author 小傅哥，微信：fustack, fy
 */
@Data
public class ClaudeCompletionResponse {

}
