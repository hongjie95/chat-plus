package cn.bugstack.openai.executor.model.chatgpt.config;

/**
 * OpenAi 配置信息
 */
public class ChatGPTConfig {
    private ChatGPTConfig() {
    }

    public static final String API_HOST = "https://api.openai.com";
    public static final String V1_CHAT_COMPLETIONS = "/v1/chat/completions";
    public static final String V1_IMAGES_COMPLETIONS = "/v1/images/generations";

}
