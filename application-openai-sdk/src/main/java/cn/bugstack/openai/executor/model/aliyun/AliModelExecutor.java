package cn.bugstack.openai.executor.model.aliyun;

import cn.bugstack.openai.executor.Executor;
import cn.bugstack.openai.executor.model.aliyun.config.AliModelConfig;
import cn.bugstack.openai.executor.model.aliyun.valobj.AliModelCompletionRequest;
import cn.bugstack.openai.executor.model.aliyun.valobj.AliModelCompletionResponse;
import cn.bugstack.openai.executor.model.aliyun.valobj.FinishReason;
import cn.bugstack.openai.executor.parameter.*;
import cn.bugstack.openai.executor.result.ResultHandler;
import cn.bugstack.openai.session.Configuration;
import com.chatplus.application.common.util.PlusJsonUtils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.sse.EventSource;
import okhttp3.sse.EventSourceListener;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * 通义千问 执行器
 *
 * @author Angus
 */
@Slf4j
public class AliModelExecutor implements Executor, ResultHandler, ParameterHandler<AliModelCompletionRequest> {
    private final EventSource.Factory factory;

    public AliModelExecutor(Configuration configuration) {
        this.factory = configuration.createRequestFactory();
    }

    @Override
    public EventSource completions(CompletionRequest completionRequest, EventSourceListener eventSourceListener) throws Exception {
        // 1. 转换参数信息
        AliModelCompletionRequest aliModelCompletionRequest = getParameterObject(completionRequest);

        // 2. 构建请求信息

        Request request = new Request.Builder()
                .tag(completionRequest.getTag())
                .header("Accept", "text/event-stream")
                .url(AliModelConfig.API_HOST.concat(AliModelConfig.V1_COMPLETIONS))
                .post(RequestBody.create(PlusJsonUtils.toJsonString(aliModelCompletionRequest),MediaType.parse(Configuration.APPLICATION_JSON)))
                .build();
        // 3. 返回事件结果
        return factory.newEventSource(request, eventSourceListener(eventSourceListener));
    }

    @Override
    public EventSource completions(String apiHostByUser, String apiKeyByUser, CompletionRequest completionRequest, EventSourceListener eventSourceListener) throws Exception {
        return null;
    }

    @Override
    public ImageResponse genImages(ImageRequest imageRequest) {
        return null;
    }

    @Override
    public ImageResponse genImages(String apiHostByUser, String apiKeyByUser, ImageRequest imageRequest) {
        return null;
    }

    @Override
    public EventSource pictureUnderstanding(PictureRequest pictureRequest, EventSourceListener eventSourceListener) throws Exception {
        return null;
    }

    @Override
    public EventSource pictureUnderstanding(String apiHostByUser, String apiKeyByUser, PictureRequest pictureRequest, EventSourceListener eventSourceListener) throws Exception {
        return null;
    }

    @Override
    public AliModelCompletionRequest getParameterObject(CompletionRequest completionRequest) {
        AliModelCompletionRequest aliModelCompletionRequest = new AliModelCompletionRequest();
        aliModelCompletionRequest.setModel(completionRequest.getModel());
        AliModelCompletionRequest.Input input = new AliModelCompletionRequest.Input();
        List<Message> messages = completionRequest.getMessages();
        List<cn.bugstack.openai.executor.model.aliyun.valobj.Message> aliMessages = new ArrayList<>();
        for (Message message : messages) {
            aliMessages.add(cn.bugstack.openai.executor.model.aliyun.valobj.Message.builder()
                    .role(message.getRole())
                    .content(message.getContent())
                    .build());
        }
        input.setMessages(aliMessages);
        aliModelCompletionRequest.setInput(input);
        aliModelCompletionRequest.setParameters(AliModelCompletionRequest.Parameters.builder()
                .incrementalOutput(true)
                .build());
        return aliModelCompletionRequest;
    }

    @Override
    public boolean supportFunction(String model) {
        return false;
    }


    @Override
    public EventSourceListener eventSourceListener(EventSourceListener eventSourceListener) {
        return new EventSourceListener() {
            @Override
            public void onEvent(EventSource eventSource, @Nullable String id, @Nullable String type, String data) {
                AliModelCompletionResponse response = PlusJsonUtils.parseObject(data, AliModelCompletionResponse.class);
                if (FinishReason.CONTINUE.getCode().equals(response.getOutput().getFinish_reason())) {
                    CompletionResponse completionResponse = new CompletionResponse();
                    List<ChatChoice> choices = new ArrayList<>();
                    ChatChoice chatChoice = new ChatChoice();
                    chatChoice.setDelta(Message.builder()
                            .role(CompletionRequest.Role.SYSTEM)
                            .name("")
                            .content(response.getOutput().getText())
                            .build());
                    choices.add(chatChoice);
                    completionResponse.setChoices(choices);
                    eventSourceListener.onEvent(eventSource, id, type, PlusJsonUtils.toJsonString(completionResponse));
                } else if (FinishReason.STOP.getCode().equals(response.getOutput().getFinish_reason())) {
                    AliModelCompletionResponse.Usage aliUsage = response.getUsage();
                    Usage usage = new Usage();
                    usage.setPromptTokens(aliUsage.getInput_tokens());
                    usage.setCompletionTokens(aliUsage.getOutput_tokens());
                    usage.setTotalTokens(aliUsage.getTotal_tokens());
                    List<ChatChoice> choices = new ArrayList<>();
                    ChatChoice chatChoice = new ChatChoice();
                    chatChoice.setFinishReason("stop");
                    chatChoice.setDelta(Message.builder()
                            .name("")
                            .role(CompletionRequest.Role.SYSTEM)
                            .content(response.getOutput().getText())
                            .build());
                    choices.add(chatChoice);
                    // 构建结果
                    CompletionResponse completionResponse = new CompletionResponse();
                    completionResponse.setChoices(choices);
                    completionResponse.setUsage(usage);
                    completionResponse.setCreated(System.currentTimeMillis());
                    // 返回数据
                    eventSourceListener.onEvent(eventSource, id, type, PlusJsonUtils.toJsonString(completionResponse));
                } else {
                    onClosed(eventSource);
                }
            }

            @Override
            public void onClosed(EventSource eventSource) {
                eventSourceListener.onClosed(eventSource);
            }

            @Override
            public void onOpen(EventSource eventSource, Response response) {
                eventSourceListener.onOpen(eventSource, response);
            }

            @Override
            public void onFailure(EventSource eventSource, @javax.annotation.Nullable Throwable t, @javax.annotation.Nullable Response response) {
                eventSourceListener.onFailure(eventSource, t, response);
            }

        };
    }
}
