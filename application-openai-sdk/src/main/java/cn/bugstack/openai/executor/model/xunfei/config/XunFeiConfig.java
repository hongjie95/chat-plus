package cn.bugstack.openai.executor.model.xunfei.config;

/**
 * 讯飞 配置信息
 *
 * @author 小傅哥，微信：fustack
 */
public class XunFeiConfig {

    private XunFeiConfig() {
    }

    public static final String API_HOST = "https://spark-api.xf-yun.com";
    public static final String API_TTI_HOST = "https://spark-api.cn-huabei-1.xf-yun.com/v2.1/tti";
    public static final String API_PICTURE_HOST = "https://spark-api.cn-huabei-1.xf-yun.com/v2.1/image";
    public static final String XUN_FEI_APP_ID = "XUN_FEI_APP_ID";
}
