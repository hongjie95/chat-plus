package cn.bugstack.openai.executor.model.chatglm.valobj;

import cn.bugstack.openai.executor.parameter.ChatChoice;
import cn.bugstack.openai.executor.parameter.Usage;
import lombok.Data;

import java.util.List;

/**
 * ChatGLM 应答参数
 *
 * @author 小傅哥，微信：fustack
 */
@Data
public class ChatGLMCompletionResponse {
    // 24年1月发布的 GLM_3_5_TURBO、GLM_4 模型时新增
    private String id;
    private Long created;
    private String model;
    private List<ChatChoice> choices;
    private Usage usage;
}
