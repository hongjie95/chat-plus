package cn.bugstack.openai.executor.model.chatglm.valobj;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * ChatGLM 消息模型
 *
 * @author 小傅哥，微信：fustack
 */
@Getter
@AllArgsConstructor
public enum Model {

    /** 智谱AI 24年01月发布 */
    GLM_3_TURBO("glm-3-turbo","适用于对知识量、推理能力、创造力要求较高的场景"),
    GLM_4("glm-4","适用于复杂的对话交互和深度内容创作设计的场景"),
    GLM_4V("glm-4v","根据输入的自然语言指令和图像信息完成任务，推荐使用 SSE 或同步调用方式请求接口"),
    COGVIEW_3("cogview-3","根据用户的文字描述生成图像,使用同步调用方式请求接口"),

    ;
    private final String code;
    private final String info;

    public static String getModel(String code) {
        for (Model model : Model.values()) {
            if (model.getCode().equals(code)) {
                return model.code;
            }
        }
        return null;
    }
}
