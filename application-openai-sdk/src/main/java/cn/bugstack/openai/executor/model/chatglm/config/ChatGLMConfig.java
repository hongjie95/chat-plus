package cn.bugstack.openai.executor.model.chatglm.config;

/**
 * 智谱Ai 配置信息
 *
 * @author Angus
 */
public class ChatGLMConfig {

    private ChatGLMConfig() {
    }
    // 智普Ai ChatGlM 请求地址
    public static final String API_HOST = "https://open.bigmodel.cn";

    public static final String V3_COMPLETIONS = "/api/paas/v3/model-api/{model}/sse-invoke";

    public static final String V4_COMPLETIONS = "/api/paas/v4/chat/completions";
}
