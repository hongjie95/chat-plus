package cn.bugstack.openai.executor.model.baidu.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 百度配置信息
 */
public class BaiduConfig {


    public static final String API_HOST = "https://aip.baidubce.com";

    /**
     * token 授权地址
     */
    public static final String AUTH_HOST = "https://aip.baidubce.com/oauth/2.0/token";
    @Getter
    @AllArgsConstructor
    public enum CompletionsUrl {
        ERNIE_Bot_turbo("eb-instant", "/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/eb-instant"),
        ERNIE_Bot("completions", "/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions"),
        ERNIE_Bot_4("completions_pro", "/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/completions_pro"),
        ERNIE_Bot_8K("ernie_bot_8k", "/rpc/2.0/ai_custom/v1/wenxinworkshop/chat/ernie_bot_8k"),
        Stable_Diffusion_XL("sd_xl", "/rpc/2.0/ai_custom/v1/wenxinworkshop/text2image/sd_xl");
        private final String code;
        private final String url;

        public static BaiduConfig.CompletionsUrl fromCode(String code) {
            for (BaiduConfig.CompletionsUrl value : BaiduConfig.CompletionsUrl.values()) {
                if (value.code.equals(code)) {
                    return value;
                }
            }
            throw new IllegalArgumentException("Unknown code: " + code);
        }
    }
}
