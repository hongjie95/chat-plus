package cn.bugstack.openai.executor.model.xunfei.valobj;

import cn.bugstack.openai.executor.parameter.Functions;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class XunFeiCompletionRequest {

    private Header header;
    private Parameter parameter;
    private Payload payload;


    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Header {
        @JsonProperty("app_id")
        private String appid;
        private String uid;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Parameter {
        private Chat chat;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Payload {
        private Message message;
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private XunFeiFunctions functions;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class XunFeiFunctions {
        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private List<Functions> text;
    }

}
