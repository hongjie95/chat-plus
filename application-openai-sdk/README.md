## 项目介绍
该SDK项目依赖于
https://bugstack.cn/md/zsxq/project/openai-sdk-java.html

因为部分代码不符合本项目需求，所以复制源码到该项目进行修改
后续会时不时对比两个仓库的代码差异进行同步
## 原SDK地址
https://gitcode.net/KnowledgePlanet/open-hub/openai-sdk-java
