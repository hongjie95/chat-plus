package com.chatplus.application.util;

import cn.hutool.extra.spring.SpringUtil;
import com.chatplus.application.aiprocessor.constant.AIConstants;
import com.chatplus.application.common.constant.GroupCacheNames;
import com.chatplus.application.common.util.CacheGroupUtils;
import com.chatplus.application.common.util.PlusJsonUtils;
import com.chatplus.application.domain.dto.AdminConfigDto;
import com.chatplus.application.domain.dto.ApiKeyDto;
import com.chatplus.application.domain.dto.DevopsConfigDto;
import com.chatplus.application.domain.dto.ExtendConfigDto;
import com.chatplus.application.domain.entity.account.UserEntity;
import com.chatplus.application.domain.entity.basedata.ApiKeyEntity;
import com.chatplus.application.enumeration.AiPlatformEnum;
import com.chatplus.application.enumeration.ImageTaskTypeEnum;
import com.chatplus.application.service.account.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 配置工具类
 *
 * @author chj
 * @date 2024/3/28
 **/
public class ConfigUtil {
    private ConfigUtil() {
    }

    private static final UserService userService = SpringUtil.getBean(UserService.class);

    public static String getWebsiteName() {
        AdminConfigDto adminConfigDto = getAdminConfig();
        return StringUtils.isNotEmpty(adminConfigDto.getTitle()) ? adminConfigDto.getTitle() : "";
    }

    /**
     * 获取图像算力
     *
     * @param userId       用户ID
     * @param platformEnum 平台
     * @return 算力
     */
    public static Integer getImagePower(Long userId, AiPlatformEnum platformEnum, ImageTaskTypeEnum taskType) {
        if (userId == null) {
            return getImagePower(platformEnum);
        }
        UserEntity userEntity = userService.getById(userId);
        if (userEntity == null) {
            return getImagePower(platformEnum);
        }
        switch (platformEnum) {
            case SD_LOCAL, SD_STABILITY:
                return userEntity.getSdPower() != null ? userEntity.getSdPower() : getImagePower(platformEnum);
            case MJ:
                int power = userEntity.getMjPower() != null ? userEntity.getMjPower() : getImagePower(platformEnum);
                List<ImageTaskTypeEnum> action = List.of(ImageTaskTypeEnum.ACTION, ImageTaskTypeEnum.UPSCALE, ImageTaskTypeEnum.VARIATION);
                if (taskType != null && action.contains(taskType)) {
                    AdminConfigDto adminConfigDto = getAdminConfig();
                    if (adminConfigDto.getMjActionPower() != null) {
                        return adminConfigDto.getMjActionPower();
                    }
                }
                return power;
            case DALL:
                return userEntity.getDallPower() != null ? userEntity.getDallPower() : getImagePower(platformEnum);
            default:
                return 0;
        }
    }

    /**
     * 获取图像算力
     *
     * @param platformEnum 平台
     * @return 算力
     */
    public static Integer getImagePower(AiPlatformEnum platformEnum) {
        AdminConfigDto adminConfigDto = getAdminConfig();
        return switch (platformEnum) {
            case SD_LOCAL, SD_STABILITY -> adminConfigDto.getSdPower();
            case MJ -> adminConfigDto.getMjPower();
            case DALL -> adminConfigDto.getDallPower();
            default -> 0;
        };
    }

    /**
     * 获取邀请的算力
     */
    public static Integer getInvitePower() {
        AdminConfigDto adminConfigDto = getAdminConfig();
        return adminConfigDto.getInvitePower() != null ? adminConfigDto.getInvitePower() : 0;
    }

    /**
     * 获取每日赠送算力
     */
    public static Integer getDailyPower() {
        AdminConfigDto adminConfigDto = getAdminConfig();
        return adminConfigDto.getDailyPower() != null ? adminConfigDto.getDailyPower() : 0;
    }

    /**
     * 获取会员订阅卡每日赠送算力
     */
    public static Integer getVipDefaultDayPower() {
        AdminConfigDto adminConfigDto = getAdminConfig();
        return adminConfigDto.getVipDefaultDayPower() != null ? adminConfigDto.getVipDefaultDayPower() : 0;
    }

    /**
     * 获取初始用户赠送算力
     */
    public static Integer getInitPower() {
        AdminConfigDto adminConfigDto = getAdminConfig();
        return adminConfigDto.getInitPower() != null ? adminConfigDto.getInitPower() : 0;
    }

    public static AdminConfigDto getAdminConfig() {
        AdminConfigDto adminConfigDto = CacheGroupUtils.get(GroupCacheNames.SYS_AI_SETTING, AIConstants.SYSTEM_CONFIG_REDIS_KEY);
        return adminConfigDto != null ? adminConfigDto : new AdminConfigDto();
    }

    public static ExtendConfigDto getExtendConfig() {
        ExtendConfigDto extendConfigDto = CacheGroupUtils.get(GroupCacheNames.SYS_AI_SETTING, AIConstants.SYSTEM_EXTEND_REDIS_KEY);
        return extendConfigDto != null ? extendConfigDto : new ExtendConfigDto();
    }

    public static List<ApiKeyDto> getImageApiKey(AiPlatformEnum channel) {
        String json = CacheGroupUtils.get(GroupCacheNames.SYS_AI_IMAGE_API_KEY, channel);
        List<ApiKeyEntity> apiKeys = PlusJsonUtils.parseArray(json, ApiKeyEntity.class);
        if (CollectionUtils.isEmpty(apiKeys)) {
            return Collections.emptyList();
        }
        List<ApiKeyDto> apiKeyDtoList = new ArrayList<>();
        apiKeys.forEach(apiKeyEntity -> {
            String value = apiKeyEntity.getValue();
            String apiUrl = apiKeyEntity.getApiUrl();
            if (StringUtils.isEmpty(apiUrl)) {
                apiUrl = AIConstants.DefaultBaseUrl.getDefaultBaseUrlByChannel(channel);
            }
            if (StringUtils.isNotEmpty(value) && StringUtils.isNotEmpty(apiUrl)) {
                ApiKeyDto apiKeyDto = new ApiKeyDto();
                apiKeyDto.setUrl(apiUrl);
                apiKeyDto.setApiKey(apiKeyEntity.getValue());
                apiKeyDto.setNotifyUrl(apiKeyEntity.getNotifyUrl());
                apiKeyDto.setProxyUrl(apiKeyEntity.getProxyUrl());
                apiKeyDtoList.add(apiKeyDto);
            }
        });
        return apiKeyDtoList;
    }

    public static List<ApiKeyDto> getChatApiKey(AiPlatformEnum platform, String channel) {
        String json = CacheGroupUtils.get(GroupCacheNames.SYS_AI_CHAT_API_KEY, channel);
        List<ApiKeyEntity> apiKeys = PlusJsonUtils.parseArray(json, ApiKeyEntity.class);
        if (CollectionUtils.isEmpty(apiKeys)) {
            return Collections.emptyList();
        }
        List<ApiKeyDto> apiKeyDtoList = new ArrayList<>();
        apiKeys.forEach(apiKeyEntity -> {
            String value = apiKeyEntity.getValue();
            String apiUrl = apiKeyEntity.getApiUrl();
            if (StringUtils.isEmpty(apiUrl)) {
                apiUrl = AIConstants.DefaultBaseUrl.getDefaultBaseUrlByChannel(platform);
            }
            if (StringUtils.isNotEmpty(value) && StringUtils.isNotEmpty(apiUrl)) {
                ApiKeyDto apiKeyDto = new ApiKeyDto();
                apiKeyDto.setUrl(apiUrl);
                apiKeyDto.setUrl(apiUrl);
                apiKeyDto.setApiKey(apiKeyEntity.getValue());
                apiKeyDto.setNotifyUrl(apiKeyEntity.getNotifyUrl());
                apiKeyDto.setProxyUrl(apiKeyEntity.getProxyUrl());
                apiKeyDtoList.add(apiKeyDto);
            }
        });
        return apiKeyDtoList;
    }

    public static DevopsConfigDto getDevopsConfig() {
        DevopsConfigDto devopsConfigDto = CacheGroupUtils.get(GroupCacheNames.SYS_AI_SETTING, AIConstants.SYSTEM_DEVOPS_REDIS_KEY);
        return devopsConfigDto != null ? devopsConfigDto : new DevopsConfigDto();
    }
}
