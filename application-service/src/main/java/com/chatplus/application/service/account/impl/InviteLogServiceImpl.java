package com.chatplus.application.service.account.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.dao.account.InviteLogDao;
import com.chatplus.application.domain.entity.account.InviteLogEntity;
import com.chatplus.application.service.account.InviteLogService;
import org.springframework.stereotype.Service;

/**
 * 邀请注册日志业务逻辑实现
 *
 * <p>Table: t_invite_log - 邀请注册日志</p>
 *
 * @author developer
 * @see InviteLogEntity
 */
@Service
public class InviteLogServiceImpl extends ServiceImpl<InviteLogDao, InviteLogEntity> implements InviteLogService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(InviteLogServiceImpl.class);

}
