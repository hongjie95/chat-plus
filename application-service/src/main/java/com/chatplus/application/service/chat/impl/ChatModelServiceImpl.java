package com.chatplus.application.service.chat.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.dao.chat.ChatModelDao;
import com.chatplus.application.domain.entity.chat.ChatModelEntity;
import com.chatplus.application.service.chat.ChatModelService;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import org.springframework.stereotype.Service;

/**
 * AI 模型表业务逻辑实现
 *
 * <p>Table: t_chat_model - AI 模型表</p>
 *
 * @author developer
 * @see ChatModelEntity
 */
@Service
public class ChatModelServiceImpl extends ServiceImpl<ChatModelDao, ChatModelEntity> implements ChatModelService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(ChatModelServiceImpl.class);

    @Override
    public Integer getMaxSortNum() {
        ChatModelEntity chatModelEntity = baseMapper.selectOne(Wrappers.<ChatModelEntity>lambdaQuery()
                .orderByDesc(ChatModelEntity::getSortNum)
                .last("LIMIT 1"), false);
        return chatModelEntity != null ? chatModelEntity.getSortNum() : 0;
    }
}
