package com.chatplus.application.service.auth.request;


import com.chatplus.application.service.auth.authentication.PasswordAuthenticationToken;
import com.chatplus.application.service.auth.authentication.SmsCaptchaAuthenticationToken;

import java.io.Serial;
import java.io.Serializable;

/**
 * 登录请求信息
 * @author weird
 */
public class AccountAuthenticationRequest implements
        PasswordAuthenticationToken,
        SmsCaptchaAuthenticationToken,
    Serializable {
    @Serial
    private static final long serialVersionUID = 4881520454357753200L;
    /**
     * 登录账号
     */
    private String username;
    /**
     * 短信验证码
     */
    private String verifyCode;
    /**
     * 登录密码
     */
    private String password;

    private String remoteIp;

    @Override
    public Object getUserPrincipal() {
        return username;
    }

    @Override
    public String getRemoteIp() {
        return remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
