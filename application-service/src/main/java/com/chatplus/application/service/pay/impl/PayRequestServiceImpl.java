package com.chatplus.application.service.pay.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.dao.pay.PayRequestDao;
import com.chatplus.application.domain.entity.pay.PayRequestEntity;
import com.chatplus.application.enumeration.PayStatusEnum;
import com.chatplus.application.service.pay.IPayRequestService;
import org.springframework.stereotype.Service;


/**
 * 支付请求记录（账单）业务逻辑实现
 *
 * <p>Table: t_pay_request - 支付请求记录（账单）</p>
 *
 * @author developer
 * @see PayRequestEntity
 */
@Service
public class PayRequestServiceImpl extends ServiceImpl<PayRequestDao, PayRequestEntity> implements IPayRequestService {


    @Override
    public PayRequestEntity findByPayTransactionId(String payTransactionId) {
        return getOne(new LambdaQueryWrapper<PayRequestEntity>()
            .eq(PayRequestEntity::getPayTransactionId, payTransactionId)
            .orderByDesc(PayRequestEntity::getId)
            .last("LIMIT 1"));
    }

    /**
     *  根据业务ID获取数据
     * @param appBusinessId 业务订单ID
     * @param payStatusEnum 支付状态
     */
    @Override
    public PayRequestEntity findByAppBusinessId(Long appBusinessId, PayStatusEnum payStatusEnum) {
        return getOne(new LambdaQueryWrapper<PayRequestEntity>()
            .eq(PayRequestEntity::getBizId, appBusinessId)
            .eq(payStatusEnum != null, PayRequestEntity::getPayStatus, payStatusEnum)
            .orderByDesc(PayRequestEntity::getRequestAt)
            .last("LIMIT 1"));
    }
}
