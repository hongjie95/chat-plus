package com.chatplus.application.service.basedata;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.common.domain.response.SensitiveWordFilterResultResponse;
import com.chatplus.application.common.validation.SensitiveWordValidationService;
import com.chatplus.application.domain.entity.basedata.SensitiveWordEntity;

/**
 * 敏感词表业务逻辑接口
 *
 * <p>Table: t_sensitive_word - 敏感词表</p>
 *
 * @author developer
 * @see SensitiveWordEntity
 */
public interface SensitiveWordService extends IService<SensitiveWordEntity>, SensitiveWordValidationService {

    void reloadAllWords();

    SensitiveWordFilterResultResponse filter(String input);

}
