package com.chatplus.application.service.chat.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.dao.chat.ChatRoleDao;
import com.chatplus.application.domain.entity.chat.ChatRoleEntity;
import com.chatplus.application.service.chat.ChatRoleService;
import org.springframework.stereotype.Service;

/**
 * 聊天角色表业务逻辑实现
 *
 * <p>Table: t_chat_role - 聊天角色表</p>
 *
 * @author developer
 * @see ChatRoleEntity
 */
@Service
public class ChatRoleServiceImpl extends ServiceImpl<ChatRoleDao, ChatRoleEntity> implements ChatRoleService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(ChatRoleServiceImpl.class);

    @Override
    public Integer getMaxSortNum() {
        ChatRoleEntity chatRoleEntity = baseMapper.selectOne(Wrappers.<ChatRoleEntity>lambdaQuery()
                .orderByDesc(ChatRoleEntity::getSortNum)
                .last("LIMIT 1"), false);
        return chatRoleEntity != null ? chatRoleEntity.getSortNum() : 0;
    }
}
