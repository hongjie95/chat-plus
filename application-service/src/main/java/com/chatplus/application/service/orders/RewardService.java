package com.chatplus.application.service.orders;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.orders.RewardEntity;

/**
 * 用户打赏业务逻辑接口
 *
 * <p>Table: t_reward - 用户打赏</p>
 *
 * @author developer
 * @see RewardEntity
 */
public interface RewardService extends IService<RewardEntity> {

}
