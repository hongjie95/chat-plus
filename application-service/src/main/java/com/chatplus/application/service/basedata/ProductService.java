package com.chatplus.application.service.basedata;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.basedata.ProductEntity;

/**
 * 会员套餐表业务逻辑接口
 *
 * <p>Table: t_product - 会员套餐表</p>
 *
 * @author developer
 * @see ProductEntity
 */
public interface ProductService extends IService<ProductEntity> {

    Integer getMaxSortNum();
}
