package com.chatplus.application.service.framework.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.dao.framework.PendingNotificationDao;
import com.chatplus.application.domain.entity.framework.PendingNotificationEntity;
import com.chatplus.application.service.framework.PendingNotificationService;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 待执行队列通知业务逻辑实现
 *
 * <p>Table: t_pending_notification - 待执行队列通知</p>
 *
 * @author developer
 * @see PendingNotificationEntity
 */
@Service
public class PendingNotificationServiceImpl extends ServiceImpl<PendingNotificationDao, PendingNotificationEntity> implements PendingNotificationService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(PendingNotificationServiceImpl.class);

}
