package com.chatplus.application.service.verification.impl;

import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.service.verification.PlusCaptchaService;
import com.chatplus.application.util.ConfigUtil;
import com.chatplus.application.util.email.MailUtils;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;

@Service(value = "emailCaptchaService")
public class EmailCaptchaServiceImpl extends PlusCaptchaService {
    static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(EmailCaptchaServiceImpl.class);

    public EmailCaptchaServiceImpl(RedissonClient redissonClient) {
        super(redissonClient);
    }

    @Override
    public boolean sendNotice(String receiver, String noticeMsg) {
        return send(receiver, String.format("%s通知", ConfigUtil.getWebsiteName()), noticeMsg);
    }

    @Override
    public boolean doSendCaptcha(String email, String captcha) {
        return send(email, String.format("%s验证码", ConfigUtil.getWebsiteName()), "您本次验证码为：" + captcha + "，有效性为" + getCaptchaValidateTime().toMinutes() + "分钟，请尽快填写。");
    }

    private boolean send(String email, String subject, String msg) {
        try {
            MailUtils.sendText(email, subject, msg);
            return true;
        } catch (Exception e) {
            LOGGER.message("验证码邮件发送异常").exception(e).error();
        }
        return false;
    }
}
