package com.chatplus.application.service.draw;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.common.page.PageParam;
import com.chatplus.application.domain.entity.draw.SdJobEntity;

/**
 * Stable Diffusion 任务表业务逻辑接口
 *
 * <p>Table: t_sd_job - Stable Diffusion 任务表</p>
 *
 * @author developer
 * @see SdJobEntity
 */
public interface SdJobService extends IService<SdJobEntity> {



    Page<SdJobEntity> getSdJobPage(PageParam pageParam, Boolean finish, Long userId, Boolean publish);

    /**
     * 获取用户正在进行中的任务数量
     * @param userId 用户ID
     * @return 正在进行中的任务数量
     */
    long getRunningJobCount(Long userId);
}
