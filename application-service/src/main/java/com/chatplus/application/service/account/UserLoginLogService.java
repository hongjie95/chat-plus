package com.chatplus.application.service.account;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.account.UserLoginLogEntity;

/**
 * 用户登录日志业务逻辑接口
 *
 * <p>Table: t_user_login_log - 用户登录日志</p>
 *
 * @author developer
 * @see UserLoginLogEntity
 */
public interface UserLoginLogService extends IService<UserLoginLogEntity> {

}
