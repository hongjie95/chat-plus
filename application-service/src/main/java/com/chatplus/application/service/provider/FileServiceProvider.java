package com.chatplus.application.service.provider;

import com.chatplus.application.common.exception.BadRequestException;
import com.chatplus.application.enumeration.FileChannelEnum;
import com.chatplus.application.file.dto.FileConfigDto;
import com.chatplus.application.service.basedata.ConfigService;
import com.chatplus.application.service.file.FileService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * 文件服务提供者
 **/
@Service
public class FileServiceProvider {
    private final Map<FileChannelEnum, FileService> fileServiceMap = new EnumMap<>(FileChannelEnum.class);

    private ConfigService configService;

    public FileServiceProvider(List<FileService> fileServiceList, ConfigService configService) {
        for (FileService fileService : fileServiceList) {
            fileServiceMap.put(fileService.getUploadChannel(), fileService);
        }
        this.configService = configService;
    }

    /**
     * 获取AI渠道处理器
     *
     * @param channel 支付渠道
     * @return PayChannelService
     */
    public FileService getFileService(FileChannelEnum channel) {
        return fileServiceMap.get(channel);
    }

    public FileService getFileService(String channel) {
        FileChannelEnum fileChannelEnum = FileChannelEnum.get(channel);
        return fileServiceMap.get(fileChannelEnum);
    }

    public FileChannelEnum getActiveFileDriver() {
        FileConfigDto fileConfigDto = configService.getFileConfig();
        if (fileConfigDto == null || StringUtils.isEmpty(fileConfigDto.getConfigKey())) {
            throw new BadRequestException("请配置文件服务");
        }
        String active = fileConfigDto.getConfigKey();
        if ("OSS".equals(active)) {
            return FileChannelEnum.OSS;
        }
        if ("LOCAL".equals(active)) {
            return FileChannelEnum.LOCAL;
        }
        throw new BadRequestException("请配置文件服务");
    }
}
