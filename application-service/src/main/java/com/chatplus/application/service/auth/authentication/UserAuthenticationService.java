package com.chatplus.application.service.auth.authentication;

import com.chatplus.application.domain.response.AccountLoginResponse;

public interface UserAuthenticationService {
    /**
     * 使用AuthenticationToken处理认证请求
     * @param authenticationToken 认证信息
     * @return Authentication
     */
    AccountLoginResponse authenticate(AuthenticationToken authenticationToken);
}
