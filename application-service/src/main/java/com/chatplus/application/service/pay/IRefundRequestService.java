package com.chatplus.application.service.pay;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.pay.RefundRequestEntity;

import java.util.List;

/**
 * 退款请求表业务逻辑接口
 *
 * <p>Table: t_refund_request - 退款请求表</p>
 *
 * @author developer
 * @see RefundRequestEntity
 */
public interface IRefundRequestService extends IService<RefundRequestEntity> {
    RefundRequestEntity findByRefundApplySeqNo(String refundApplySeqNo);
    RefundRequestEntity findLastRefundByPayRequestId(Long payRequestId);
    List<RefundRequestEntity> findRefundByPayRequestId(Long payRequestId);
    Long sumRefundedMoney(Long payRequestId);
}
