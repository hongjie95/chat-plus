package com.chatplus.application.service.pay.impl.alipay;

import com.chatplus.application.client.pay.AliPayClient;
import com.chatplus.application.client.pay.domain.request.alipay.AlipayOrderPayRequest;
import com.chatplus.application.client.pay.domain.response.alipay.AlipayOrderPayResponse;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 支付宝支付-PC支付
 */
@Service
public class AliPayWebPayService extends AliPayApiService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(AliPayWebPayService.class);
    @Autowired
    public AliPayWebPayService(AliPayClient aliPayServiceClient) {
        super(aliPayServiceClient);
    }
    @Override
    public AlipayOrderPayResponse orderPay(AlipayOrderPayRequest request) {
        try {
            return aliPayServiceClient.wapPay(request);
        } catch (Exception e) {
            LOGGER.message("支付宝支付-PC支付调用失败")
                    .context("request", request)
                    .exception(e).error();
        }
        return null;
    }
}
