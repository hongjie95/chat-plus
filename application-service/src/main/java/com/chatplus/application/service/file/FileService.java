package com.chatplus.application.service.file;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chatplus.application.dao.file.FileDao;
import com.chatplus.application.domain.entity.file.FileEntity;
import com.chatplus.application.domain.vo.file.UpLoadFileVo;
import com.chatplus.application.enumeration.FileChannelEnum;
import com.chatplus.application.file.dto.FileConfigDto;
import com.chatplus.application.service.basedata.ConfigService;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.Tika;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.Collection;

/**
 * 文件服务接口
 */
public abstract class FileService {
    private final ConfigService configService;

    protected final FileDao fileDao;

    protected final Tika tika;
    public static final String FILE_SEPARATOR = "/";

    protected FileService(ConfigService configService,
                          FileDao fileDao) {
        this.configService = configService;
        this.fileDao = fileDao;
        tika = new Tika();
    }


    public FileConfigDto getFileConfig() {
        return configService.getFileConfig();
    }

    /**
     * 文件上传
     */
    public abstract UpLoadFileVo uploadFile(MultipartFile scrFile, Long userId);

    public abstract InputStream getInputStream(Long fileId);

    public FileEntity getByHashId(String hashId) {
        if (StringUtils.isEmpty(hashId)) {
            return null;
        }
        LambdaQueryWrapper<FileEntity> lqw = Wrappers.lambdaQuery();
        lqw.eq(FileEntity::getHashId, hashId);
        lqw.orderByDesc(FileEntity::getUpdatedAt);
        lqw.last("LIMIT 1");
        return fileDao.selectOne(lqw, false);
    }

    ;

    public abstract FileChannelEnum getUploadChannel();

    public abstract void deleteByIds(Collection<Long> ids);

    public abstract void download(Long fileId, String fileName, HttpServletResponse response);

}
