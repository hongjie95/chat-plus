package com.chatplus.application.service.chat.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.dao.chat.ChatItemDao;
import com.chatplus.application.domain.entity.chat.ChatItemEntity;
import com.chatplus.application.service.chat.ChatItemService;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户会话列表业务逻辑实现
 *
 * <p>Table: t_chat_item - 用户会话列表</p>
 *
 * @author developer
 * @see ChatItemEntity
 */
@Service
public class ChatItemServiceImpl extends ServiceImpl<ChatItemDao, ChatItemEntity> implements ChatItemService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(ChatItemServiceImpl.class);

    @Override
    public List<ChatItemEntity> getChatItemByUserId(Long userId) {
        return this.list(new LambdaQueryWrapper<ChatItemEntity>()
                .eq(ChatItemEntity::getUserId, userId)
                .orderByDesc(ChatItemEntity::getId)
        );
    }

    @Override
    public ChatItemEntity getChatItemByChatId(String chatId) {
        return getOne(new LambdaQueryWrapper<ChatItemEntity>()
                .eq(ChatItemEntity::getChatId, chatId).last("limit 1"));
    }
}
