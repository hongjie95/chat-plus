package com.chatplus.application.service.wxmp.handler;

import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.service.account.UserService;
import com.chatplus.application.service.account.WechatUserService;
import com.chatplus.application.service.wxmp.builder.TextBuilder;
import com.chatplus.application.service.wxmp.impl.WeChatMpService;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 消息处理器
 *
 * @author chj
 */
@Component
public class MsgHandler extends AbstractHandler {

    static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(MsgHandler.class);

    public MsgHandler(WeChatMpService weChatMpService,
                      WechatUserService wechatUserService,
                      UserService userService) {
        super(weChatMpService, wechatUserService, userService);
    }

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
                                    Map<String, Object> context, WxMpService wxMpService,
                                    WxSessionManager sessionManager) {

        WeChatMpService weixinService = (WeChatMpService) wxMpService;

        if (!wxMessage.getMsgType().equals(WxConsts.XmlMsgType.EVENT)) {
            //TODO 可以选择将消息保存到本地
        }
        LOGGER.message("收到消息")
                .context("FromUser", wxMessage.getFromUser())
                .context("Content", wxMessage.getContent())
                .info();
        //TODO 组装回复消息
        String content = "你好，如有疑问，请留言，我们会尽快回复您";
        return new TextBuilder().build(content, wxMessage, weixinService);

    }

}
