package com.chatplus.application.service.auth.authentication;

public interface PasswordAuthenticationToken extends AuthenticationToken{
    /**
     * 登录密码
     */
    String getPassword();
}
