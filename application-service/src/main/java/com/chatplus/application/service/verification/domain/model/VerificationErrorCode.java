package com.chatplus.application.service.verification.domain.model;

import com.chatplus.application.common.exception.BadRequestException;
import com.chatplus.application.common.exception.TurnRightException;
import com.chatplus.application.common.exception.status.Code;

import java.util.Objects;

/**
 * @author
 */

public enum VerificationErrorCode implements Code {

    SEND_SMS_CAPTCHA_OVER_LIMIT(400, "请求过于频繁"),
    BOT_CHECK_REQUIRED(400, "需要执行人机验证"),
    VERIFY_OPERATION_FAILED(400, "人机验证不通过"),
    CAPTCHA_INVALID(400, "验证码错误，请重试"),
    MOBILE_PHONE_REQUIRED(400, "请输入手机号"),
    MOBILE_PHONE_INVALID(400, "请输入正确的手机号");

    private final Integer code;
    private final String message;
    private final Object payload;

    VerificationErrorCode(Integer code, String message) {
        this.code = code;
        this.message = message;
        this.payload = null;
    }

    VerificationErrorCode(Integer code, Object payload) {
        this.code = code;
        this.message = null;
        this.payload = payload;
    }

    VerificationErrorCode(Integer code, String message, Object payload) {
        this.code = code;
        this.message = message;
        this.payload = payload;
    }

    @Override
    public RuntimeException get() {
        if (Objects.equals(400, getCode())) {
            return new BadRequestException(getMsg(), getPayload());
        }
        return new TurnRightException(this, null, getPayload());
    }

    public RuntimeException toException() {
        return get();
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return message;
    }

    public Object getPayload() {
        return payload;
    }
}
