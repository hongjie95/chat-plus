package com.chatplus.application.service.basedata.impl;

import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.aiprocessor.constant.AIConstants;
import com.chatplus.application.common.constant.GroupCacheNames;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.common.util.CacheGroupUtils;
import com.chatplus.application.common.util.PlusJsonUtils;
import com.chatplus.application.config.extend.ExtendConfigContext;
import com.chatplus.application.dao.basedata.ConfigDao;
import com.chatplus.application.domain.dto.AdminConfigDto;
import com.chatplus.application.domain.dto.DevopsConfigDto;
import com.chatplus.application.domain.dto.ExtendConfigDto;
import com.chatplus.application.domain.dto.NoticeConfigDto;
import com.chatplus.application.domain.entity.basedata.ConfigEntity;
import com.chatplus.application.file.dto.FileConfigDto;
import com.chatplus.application.service.basedata.ConfigService;
import com.chatplus.application.service.wxmp.impl.WeChatMpService;
import com.thoughtworks.xstream.InitializationException;
import org.dromara.sms4j.aliyun.config.AlibabaConfig;
import org.dromara.sms4j.core.config.SupplierFactory;
import org.dromara.sms4j.provider.enumerate.SupplierType;
import org.dromara.sms4j.tencent.config.TencentConfig;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 第三方AI配置表业务逻辑实现
 *
 * <p>Table: t_config - 第三方AI配置表</p>
 *
 * @author developer
 * @see ConfigEntity
 */
@Service
public class ConfigServiceImpl extends ServiceImpl<ConfigDao, ConfigEntity> implements ConfigService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(ConfigServiceImpl.class);

    @Override
    public void init() {
        CacheGroupUtils.clear(GroupCacheNames.SYS_AI_SETTING);
        LOGGER.message("开始加载Chat Plus 系统配置到缓存").info();
        List<ConfigEntity> list = this.list();
        if (list == null || list.isEmpty()) {
            throw new InitializationException("加载Chat Plus 系统配置到缓存失败");
        }
        ExtendConfigContext extendConfigContext = ExtendConfigContext.getInstance();
        list.forEach(configEntity -> {
            String marker = configEntity.getMarker();
            String value = configEntity.getConfig();
            switch (marker) {
                case "system":
                    AdminConfigDto adminConfigDto = PlusJsonUtils.parseObject(value, AdminConfigDto.class);
                    if (adminConfigDto == null) {
                        LOGGER.message("adminConfigDto加载系统参数配置失败").error();
                    } else {
                        CacheGroupUtils.put(GroupCacheNames.SYS_AI_SETTING, AIConstants.SYSTEM_CONFIG_REDIS_KEY, adminConfigDto);
                    }
                    break;
                case "notice":
                    NoticeConfigDto noticeConfigDto = PlusJsonUtils.parseObject(value, NoticeConfigDto.class);
                    if (noticeConfigDto == null) {
                        LOGGER.message("noticeConfigDto加载系统参数配置失败").error();
                    } else {
                        CacheGroupUtils.put(GroupCacheNames.SYS_AI_SETTING, AIConstants.SYSTEM_NOTICE_REDIS_KEY, noticeConfigDto);
                    }
                    break;
                case "file":
                    FileConfigDto fileConfigDto = PlusJsonUtils.parseObject(value, FileConfigDto.class);
                    if (fileConfigDto == null) {
                        LOGGER.message("fileConfigDto加载系统参数配置失败").error();
                    } else {
                        CacheGroupUtils.put(GroupCacheNames.SYS_AI_SETTING, AIConstants.SYSTEM_FILE_REDIS_KEY, fileConfigDto);
                    }
                    break;
                case "devops":
                    DevopsConfigDto devopsConfigDto = PlusJsonUtils.parseObject(value, DevopsConfigDto.class);
                    if (devopsConfigDto == null) {
                        LOGGER.message("devopsConfigDto加载系统参数配置失败").error();
                        break;
                    } else {
                        CacheGroupUtils.put(GroupCacheNames.SYS_AI_SETTING, AIConstants.SYSTEM_DEVOPS_REDIS_KEY, devopsConfigDto);
                    }
                    extendConfigContext.updateConfig("chatplus.audit-logger.enable", devopsConfigDto.getEnableAuditLogger());
                    break;
                case "extend":
                    ExtendConfigDto extendConfigDto = PlusJsonUtils.parseObject(value, ExtendConfigDto.class);
                    if (extendConfigDto == null) {
                        LOGGER.message("extendConfigDto加载系统参数配置失败").error();
                        break;
                    } else {
                        CacheGroupUtils.put(GroupCacheNames.SYS_AI_SETTING, AIConstants.SYSTEM_EXTEND_REDIS_KEY, extendConfigDto);
                    }
                    // ------------------------短信配置------------------------
                    boolean enableSms = extendConfigDto.getEnableSms() != null && extendConfigDto.getEnableSms();
                    if (enableSms) {
                        try {
                            String supplier = extendConfigDto.getSmsSupplier();
                            SupplierType supplierType = SupplierType.valueOf(supplier);
                            switch (supplierType) {
                                case ALIBABA:
                                    // 阿里云短信
                                    AlibabaConfig alibabaConfig = AlibabaConfig.builder()
                                            .accessKeyId(extendConfigDto.getSmsAccessKeyId())
                                            .templateId(extendConfigDto.getSmsTemplateId())
                                            .signature(extendConfigDto.getSmsSignature())
                                            .accessKeySecret(extendConfigDto.getSmsAccessKeySecret())
                                            .requestUrl(extendConfigDto.getSmsRequestUrl())
                                            .build();
                                    SupplierFactory.setAlibabaConfig(alibabaConfig);
                                    break;
                                case TENCENT:
                                    // 腾讯云短信
                                    TencentConfig tencentConfig = TencentConfig.builder()
                                            .accessKeyId(extendConfigDto.getSmsAccessKeyId())
                                            .templateId(extendConfigDto.getSmsTemplateId())
                                            .signature(extendConfigDto.getSmsSignature())
                                            .accessKeySecret(extendConfigDto.getSmsAccessKeySecret())
                                            .sdkAppId(extendConfigDto.getSmsSdkAppId())
                                            .territory(extendConfigDto.getSmsTerritory())
                                            .requestUrl(extendConfigDto.getSmsRequestUrl())
                                            .build();
                                    SupplierFactory.setTencentConfig(tencentConfig);
                                    break;
                                default:
                                    break;

                            }
                        } catch (Exception e) {
                            LOGGER.message("短信配置加载失败").exception(e).error();
                        }
                    }
                    // ------------------------邮件配置------------------------
                    boolean enableMail = extendConfigDto.getEnableMail() != null && extendConfigDto.getEnableMail();
                    extendConfigContext.updateConfig("mail.enable", enableMail);
                    if (enableMail) {
                        extendConfigContext.updateConfig("mail.mock", extendConfigDto.getMailMock());
                        extendConfigContext.updateConfig("mail.host", extendConfigDto.getMailHost());
                        extendConfigContext.updateConfig("mail.port", extendConfigDto.getMailPort());
                        extendConfigContext.updateConfig("mail.auth", extendConfigDto.getMailEnableAuth());
                        extendConfigContext.updateConfig("mail.from", extendConfigDto.getMailFrom());
                        extendConfigContext.updateConfig("mail.user", extendConfigDto.getMailUser());
                        extendConfigContext.updateConfig("mail.pass", extendConfigDto.getMailPassword());
                        extendConfigContext.updateConfig("mail.starttls-enable", extendConfigDto.getMailEnableStarttls());
                        extendConfigContext.updateConfig("mail.ssl-enable", extendConfigDto.getMailEnableSsl());
                        extendConfigContext.updateConfig("mail.timeout", extendConfigDto.getMailTimeout());
                        extendConfigContext.updateConfig("mail.connection-timeout", extendConfigDto.getMailConnectionTimeout());
                    }
                    // ------------------------支付宝支付配置------------------------
                    boolean enableAliPay = extendConfigDto.getEnableAlipay() != null && extendConfigDto.getEnableAlipay();
                    extendConfigContext.updateConfig("chatplus.pay.alipay.enable", enableAliPay);
                    if (enableAliPay) {
                        extendConfigContext.updateConfig("chatplus.pay.alipay.protocol", extendConfigDto.getAlipayProtocol());
                        extendConfigContext.updateConfig("chatplus.pay.alipay.gateway-host", extendConfigDto.getAlipayGatewayHost());
                        extendConfigContext.updateConfig("chatplus.pay.alipay.app-id", extendConfigDto.getAlipayAppId());
                        extendConfigContext.updateConfig("chatplus.pay.alipay.sign-type", extendConfigDto.getAlipaySignType());
                        extendConfigContext.updateConfig("chatplus.pay.alipay.merchant-private-key", extendConfigDto.getAlipayMerchantPrivateKey());
                        extendConfigContext.updateConfig("chatplus.pay.alipay.merchant-cert-path", extendConfigDto.getAlipayMerchantCertPath());
                        extendConfigContext.updateConfig("chatplus.pay.alipay.alipay-cert-path", extendConfigDto.getAlipayAlipayCertPath());
                        extendConfigContext.updateConfig("chatplus.pay.alipay.alipay-root-cert-path", extendConfigDto.getAlipayAlipayRootCertPath());
                        extendConfigContext.updateConfig("chatplus.pay.alipay.notify-url", extendConfigDto.getAlipayNotifyUrl());
                    }
                    // ------------------------微信支付配置------------------------
                    boolean enableWechatPay = extendConfigDto.getEnableWechatPay() != null && extendConfigDto.getEnableWechatPay();
                    extendConfigContext.updateConfig("chatplus.pay.wechat.enable", enableWechatPay);
                    if (enableWechatPay) {
                        extendConfigContext.updateConfig("chatplus.pay.wechat.app-id", extendConfigDto.getWechatPayAppId());
                        extendConfigContext.updateConfig("chatplus.pay.wechat.mch-id", extendConfigDto.getWechatPayMchId());
                        extendConfigContext.updateConfig("chatplus.pay.wechat.mch-key", extendConfigDto.getWechatPayMchKey());
                        extendConfigContext.updateConfig("chatplus.pay.wechat.api-v3-key", extendConfigDto.getWechatPayApiV3Key());
                        extendConfigContext.updateConfig("chatplus.pay.wechat.private-key-path", extendConfigDto.getWechatPayPrivateKey());
                        extendConfigContext.updateConfig("chatplus.pay.wechat.private-cert-path", extendConfigDto.getWechatPayPrivateCert());
                        extendConfigContext.updateConfig("chatplus.pay.wechat.notify-url", extendConfigDto.getWechatPayNotifyUrl());
                    }
                    //-----------------------微信公众号配置------------------------
                    boolean enableWechatMp = extendConfigDto.getEnableWechatMp() != null && extendConfigDto.getEnableWechatMp();
                    extendConfigContext.updateConfig("chatplus.wechat.mp.enable", enableWechatMp);
                    if (enableWechatMp) {
                        extendConfigContext.updateConfig("chatplus.wechat.mp.app-id", extendConfigDto.getWechatMpAppId());
                        extendConfigContext.updateConfig("chatplus.wechat.mp.secret", extendConfigDto.getWechatMpAppSecret());
                        extendConfigContext.updateConfig("chatplus.wechat.mp.token", extendConfigDto.getWechatMpToken());
                        extendConfigContext.updateConfig("chatplus.wechat.mp.aes-key", extendConfigDto.getWechatMpAesKey());
                        extendConfigContext.updateConfig("chatplus.wechat.mp.notify-url", extendConfigDto.getWechatMpNotifyUrl());
                        // 初始化微信公众号配置，这是目前暂时能想到最好的方法了，如果你有更好的方式请和我说
                        SpringUtil.getBean(WeChatMpService.class).init();
                    }
                    break;
                default:
                    LOGGER.message("未知配置KEY").context("marker", marker).warn();
                    break;
            }
        });
        LOGGER.message("结束加载Chat Plus 系统配置到缓存").info();
    }

    @Override
    public ConfigEntity getByMarker(String marker) {
        return this.getOne(new LambdaQueryWrapper<ConfigEntity>().eq(ConfigEntity::getMarker, marker).last("limit 1"));
    }

    @Override
    public AdminConfigDto getSystemConfig() {
        return CacheGroupUtils.get(GroupCacheNames.SYS_AI_SETTING, AIConstants.SYSTEM_CONFIG_REDIS_KEY);

    }

    @Override
    public NoticeConfigDto getNoticeConfig() {
        return CacheGroupUtils.get(GroupCacheNames.SYS_AI_SETTING, AIConstants.SYSTEM_NOTICE_REDIS_KEY);
    }

    @Override
    public FileConfigDto getFileConfig() {
        return CacheGroupUtils.get(GroupCacheNames.SYS_AI_SETTING, AIConstants.SYSTEM_FILE_REDIS_KEY);
    }

    @Override
    public ExtendConfigDto getExtendConfig() {
        return CacheGroupUtils.get(GroupCacheNames.SYS_AI_SETTING, AIConstants.SYSTEM_EXTEND_REDIS_KEY);
    }

    @Override
    public DevopsConfigDto getDevopsConfig() {
        return CacheGroupUtils.get(GroupCacheNames.SYS_AI_SETTING, AIConstants.SYSTEM_DEVOPS_REDIS_KEY);

    }
}
