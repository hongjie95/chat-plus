package com.chatplus.application.service.functions;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.functions.FunctionEntity;

/**
 * 函数插件表业务逻辑接口
 *
 * <p>Table: t_function - 函数插件表</p>
 *
 * @author developer
 * @see FunctionEntity
 */
public interface FunctionService extends IService<FunctionEntity> {

}
