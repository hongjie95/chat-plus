package com.chatplus.application.service.account.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.dao.account.InviteCodeDao;
import com.chatplus.application.domain.entity.account.InviteCodeEntity;
import com.chatplus.application.service.account.InviteCodeService;
import org.springframework.stereotype.Service;

/**
 * 用户邀请码业务逻辑实现
 *
 * <p>Table: t_invite_code - 用户邀请码</p>
 *
 * @author developer
 * @see InviteCodeEntity
 */
@Service
public class InviteCodeServiceImpl extends ServiceImpl<InviteCodeDao, InviteCodeEntity> implements InviteCodeService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(InviteCodeServiceImpl.class);

    @Override
    public InviteCodeEntity getByCode(String code) {
        return getOne(new LambdaQueryWrapper<InviteCodeEntity>().eq(InviteCodeEntity::getCode, code).last("limit 1"));
    }

    @Override
    public InviteCodeEntity getByUserId(Long userId) {
        return getOne(new LambdaQueryWrapper<InviteCodeEntity>().eq(InviteCodeEntity::getUserId, userId).last("limit 1"));
    }
}
