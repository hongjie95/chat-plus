package com.chatplus.application.service.basedata.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.dao.basedata.PowerLogsDao;
import com.chatplus.application.domain.entity.basedata.PowerLogsEntity;
import com.chatplus.application.service.basedata.PowerLogsService;
import org.springframework.stereotype.Service;

/**
 * 用户算力消费日志业务逻辑实现
 *
 * <p>Table: t_power_logs - 用户算力消费日志</p>
 *
 * @author developer
 * @see PowerLogsEntity
 */
@Service
public class PowerLogsServiceImpl extends ServiceImpl<PowerLogsDao, PowerLogsEntity> implements PowerLogsService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(PowerLogsServiceImpl.class);

}
