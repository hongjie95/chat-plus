package com.chatplus.application.service.account;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.account.UserEntity;

/**
 * 用户表业务逻辑接口
 *
 * <p>Table: t_users - 用户表</p>
 *
 * @author developer
 * @see UserEntity
 */
public interface UserService extends IService<UserEntity> {

    UserEntity getByUsername(String username);

    UserEntity wxOneClickRegister();

}
