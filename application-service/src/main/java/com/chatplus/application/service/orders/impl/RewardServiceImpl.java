package com.chatplus.application.service.orders.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.dao.orders.RewardDao;
import com.chatplus.application.domain.entity.orders.RewardEntity;
import com.chatplus.application.service.orders.RewardService;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 用户打赏业务逻辑实现
 *
 * <p>Table: t_reward - 用户打赏</p>
 *
 * @author developer
 * @see RewardEntity
 */
@Service
public class RewardServiceImpl extends ServiceImpl<RewardDao, RewardEntity> implements RewardService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(RewardServiceImpl.class);

}
