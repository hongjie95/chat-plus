package com.chatplus.application.service.draw;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.common.page.PageParam;
import com.chatplus.application.domain.entity.draw.MjJobEntity;
import com.chatplus.application.domain.request.MjCallbackNotifyRequest;

/**
 * MidJourney 任务表业务逻辑接口
 *
 * <p>Table: t_mj_job - MidJourney 任务表</p>
 *
 * @author developer
 * @see MjJobEntity
 */
public interface MjJobService extends IService<MjJobEntity> {
    MjJobEntity getByTaskId(String taskId);

    Page<MjJobEntity> getMjJobPage(PageParam pageParam, Boolean finish, Long userId, Boolean publish);

    /**
     * 获取用户正在进行中的任务数量
     *
     * @param userId 用户ID
     * @return 正在进行中的任务数量
     */
    long getRunningJobCount(Long userId);


    void handleNotify(MjJobEntity mjJobEntity, MjCallbackNotifyRequest request);
}
