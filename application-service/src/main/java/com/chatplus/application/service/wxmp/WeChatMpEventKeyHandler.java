package com.chatplus.application.service.wxmp;

import com.chatplus.application.domain.entity.account.WechatUserEntity;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;

/**
 * 微信公众号事件处理器
 *
 * @author chj
 * @date 2024/4/1
 **/
public interface WeChatMpEventKeyHandler {
    /**
     * 处理器，处理微信扫码后的回调
     *
     * @param wxMpXmlMessage 微信公众号的推送过来的消息
     * @param weChatUser     发送的公众号
     * @return 向微信用户发送消息
     */
    WxMpXmlOutMessage handle(WxMpXmlMessage wxMpXmlMessage, WechatUserEntity weChatUser);
}
