package com.chatplus.application.service.basedata;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.basedata.MenuEntity;

/**
 * 前端菜单表业务逻辑接口
 *
 * <p>Table: t_menu - 前端菜单表</p>
 *
 * @author developer
 * @see MenuEntity
 */
public interface MenuService extends IService<MenuEntity> {

}
