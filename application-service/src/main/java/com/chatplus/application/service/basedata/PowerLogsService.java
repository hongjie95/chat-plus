package com.chatplus.application.service.basedata;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.basedata.PowerLogsEntity;

/**
 * 用户算力消费日志业务逻辑接口
 *
 * <p>Table: t_power_logs - 用户算力消费日志</p>
 *
 * @author developer
 * @see PowerLogsEntity
 */
public interface PowerLogsService extends IService<PowerLogsEntity> {

}
