package com.chatplus.application.service.account;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.account.InviteCodeEntity;

/**
 * 用户邀请码业务逻辑接口
 *
 * <p>Table: t_invite_code - 用户邀请码</p>
 *
 * @author developer
 * @see InviteCodeEntity
 */
public interface InviteCodeService extends IService<InviteCodeEntity> {

    InviteCodeEntity getByCode(String code);

    InviteCodeEntity getByUserId(Long userId);

}
