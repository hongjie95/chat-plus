package com.chatplus.application.service.pay.impl;

import com.chatplus.application.client.pay.domain.response.InternalPayQueryResponse;
import com.chatplus.application.domain.entity.pay.PayRequestEntity;
import com.chatplus.application.client.pay.domain.response.InternalPayResponse;
import com.chatplus.application.domain.response.PaymentQueryResponse;
import com.chatplus.application.enumeration.PayChannelEnum;

import java.util.Map;

public abstract class PayChannelService {
    public abstract InternalPayResponse orderPay(PayRequestEntity payRequestEntity, String subject);

    public abstract InternalPayQueryResponse queryOrderPay(PayRequestEntity payRequestEntity);

    public abstract boolean verifySign(Map<String, String> params);

    public abstract PayChannelEnum getChannel();

    public abstract String getLogo();
}
