package com.chatplus.application.service.account.impl;

import cn.dev33.satoken.secure.BCrypt;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.common.constant.RedisPrefix;
import com.chatplus.application.common.enumeration.UserStatusEnum;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.dao.account.UserDao;
import com.chatplus.application.domain.dto.AdminConfigDto;
import com.chatplus.application.domain.dto.UserCustomChatConfigDto;
import com.chatplus.application.domain.entity.account.UserEntity;
import com.chatplus.application.service.account.UserProductLogService;
import com.chatplus.application.service.account.UserService;
import com.chatplus.application.util.ConfigUtil;
import org.redisson.api.RedissonClient;
import org.redisson.codec.SerializationCodec;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户表业务逻辑实现
 *
 * <p>Table: t_users - 用户表</p>
 *
 * @author developer
 * @see UserEntity
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(UserServiceImpl.class);
    private final RedissonClient redissonClient;
    private final UserProductLogService userProductLogService;

    public UserServiceImpl(RedissonClient redissonClient, UserProductLogService userProductLogService) {
        this.redissonClient = redissonClient;
        this.userProductLogService = userProductLogService;
    }

    @Override
    public UserEntity getByUsername(String username) {
        return this.getOne(new LambdaQueryWrapper<>(UserEntity.class)
                .eq(UserEntity::getUsername, username).last("limit 1"));
    }

    @Override
    public UserEntity wxOneClickRegister() {
        LOGGER.message("用户发起一键注册请求").info();
        AdminConfigDto adminConfigDto = ConfigUtil.getAdminConfig();
        UserEntity accountEntity = new UserEntity();
        String username = RandomUtil.randomString(5);
        String password = RandomUtil.randomString(8);
        accountEntity.setUsername(username);
        accountEntity.setAdmin(false);
        accountEntity.setNickname("wx" + username);
        accountEntity.setUsername("wx" + username);
        accountEntity.setPassword(BCrypt.hashpw(password));
        // 设置其他必填信息,初始化用户的其他信息
        accountEntity.setAvatar("/images/avatar/user.png");
        accountEntity.setStatus(UserStatusEnum.OK);
        accountEntity.setChatRoles(List.of("gpt"));
        accountEntity.setChatConfig(new UserCustomChatConfigDto());
        accountEntity.setChatModels(adminConfigDto.getDefaultModels());
        super.save(accountEntity);
        // 注册成功后给与用户一定的免费次数
        userProductLogService.adminAddPowerToUser(accountEntity.getId(), adminConfigDto.getInitPower(), "微信扫码注册赠送");
        return accountEntity;
    }

    @Override
    public boolean updateById(UserEntity entity) {
        redissonClient.getBucket(RedisPrefix.SESSION_PREFIX + entity.getId(), new SerializationCodec()).delete();
        return baseMapper.updateById(entity) > 0;
    }

}
