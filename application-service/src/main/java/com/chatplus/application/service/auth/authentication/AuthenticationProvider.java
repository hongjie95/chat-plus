package com.chatplus.application.service.auth.authentication;

import cn.dev33.satoken.stp.StpUtil;
import com.chatplus.application.common.enumeration.AccountErrorCode;
import com.chatplus.application.common.enumeration.DeviceTypeEnum;
import com.chatplus.application.common.enumeration.UserStatusEnum;
import com.chatplus.application.domain.response.AccountLoginResponse;
import com.chatplus.application.web.satoken.helper.LoginHelper;
import com.chatplus.application.web.satoken.model.LoginUser;

import static cn.hutool.core.lang.Assert.isTrue;

/**
 * 认证器接口定义
 *
 * @author weird
 */
public interface AuthenticationProvider {

    /**
     * 获取认证器名称
     *
     * @return 认证器名称
     */
    String getName();

    /**
     * 使用AuthenticationToken进行用户认证
     *
     * @param authentication 认证信息
     * @return Authentication
     */
    AccountLoginResponse authenticate(AuthenticationToken authentication);

    /**
     * 判断是否支持本认证方式
     *
     * @param authentication 认证信息
     * @return true or false
     */
    boolean supports(AuthenticationToken authentication);

    /**
     * 认证成功回调
     *
     * @param authentication 认证成功信息
     */
    default void onAuthenticationSuccess(AccountLoginResponse authentication) {
        // 此处可根据登录用户的数据不同 自行创建 loginUser
        LoginUser loginUser = new LoginUser();
        loginUser.setUserId(authentication.getId());
        loginUser.setUsername(authentication.getUsername());
        loginUser.setUserType(authentication.getUserType().getUserType());
        loginUser.setLoginTime(System.currentTimeMillis());
        // 生成token
        LoginHelper.loginByDevice(loginUser, DeviceTypeEnum.PC);
        String token = StpUtil.getTokenValue();
        authentication.setAuthorizationToken(token);
        //TODO 登录成功后，赠送每日免费额度

    }

    /**
     * 验证账号是否被锁定
     *
     * @param account 账号信息
     */
    static void checkAccountHasLocked(AccountLoginResponse account) {
        isTrue(account.getStatus() == UserStatusEnum.OK, AccountErrorCode.ACCOUNT_HAS_LOCKED);
    }
}
