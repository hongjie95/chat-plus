package com.chatplus.application.service.basedata;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.basedata.ApiKeyEntity;

import java.util.List;

/**
 * OpenAI API 业务逻辑接口
 *
 * <p>Table: t_api_key - OpenAI API </p>
 *
 * @author developer
 * @see ApiKeyEntity
 */
public interface ApiKeyService extends IService<ApiKeyEntity> {
    /**
     * 初始化ApiKey配置
     */
    void init();
    List<ApiKeyEntity> getEnableApiKeys(String platform, String type);
}
