package com.chatplus.application.service.orders;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.orders.OrderEntity;

/**
 * 充值订单表业务逻辑接口
 *
 * <p>Table: t_order - 充值订单表</p>
 *
 * @author developer
 * @see OrderEntity
 */
public interface OrderService extends IService<OrderEntity> {

}
