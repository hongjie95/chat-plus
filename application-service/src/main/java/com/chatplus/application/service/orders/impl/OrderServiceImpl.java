package com.chatplus.application.service.orders.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.dao.orders.OrderDao;
import com.chatplus.application.domain.entity.orders.OrderEntity;
import com.chatplus.application.service.orders.OrderService;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 充值订单表业务逻辑实现
 *
 * <p>Table: t_order - 充值订单表</p>
 *
 * @author developer
 * @see OrderEntity
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderDao, OrderEntity> implements OrderService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(OrderServiceImpl.class);

}
