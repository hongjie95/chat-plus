package com.chatplus.application.service.pay.impl.wechat;

import com.chatplus.application.common.exception.BadRequestException;
import com.chatplus.application.enumeration.SubModeEnum;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class WeChatApiServiceProvider {

    private final ApplicationContext applicationContext;

    public WeChatApiServiceProvider(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public WeChatPayApiService getWeChatPayApiService(SubModeEnum subModeEnum) {
        try {
            String beanName = "WeChatApiService_" + subModeEnum.name();
            return applicationContext.getBean(beanName, WeChatPayApiService.class);
        } catch (Exception e) {
            throw new BadRequestException("微信支付无该子支付渠道");
        }
    }

    public WeChatPayApiService getWeChatPayApiService(String subMode) {
        SubModeEnum subModeEnum = SubModeEnum.get(subMode);
        return getWeChatPayApiService(subModeEnum);
    }


}
