package com.chatplus.application.service.account.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.dao.account.UserLoginLogDao;
import com.chatplus.application.domain.entity.account.UserLoginLogEntity;
import com.chatplus.application.service.account.UserLoginLogService;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 用户登录日志业务逻辑实现
 *
 * <p>Table: t_user_login_log - 用户登录日志</p>
 *
 * @author developer
 * @see UserLoginLogEntity
 */
@Service
public class UserLoginLogServiceImpl extends ServiceImpl<UserLoginLogDao, UserLoginLogEntity> implements UserLoginLogService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(UserLoginLogServiceImpl.class);

}
