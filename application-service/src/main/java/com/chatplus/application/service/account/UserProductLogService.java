package com.chatplus.application.service.account;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.account.UserProductLogEntity;
import com.chatplus.application.enumeration.AiPlatformEnum;

import java.time.Instant;
import java.util.List;

/**
 * 用户套餐记录业务逻辑接口
 *
 * <p>Table: t_user_product_log - 用户套餐记录</p>
 *
 * @author developer
 * @see UserProductLogEntity
 */
public interface UserProductLogService extends IService<UserProductLogEntity> {

    /**
     * 添加用户套餐记录
     *
     * @param userId    用户id
     * @param productId 套餐id
     * @param orderId   订单id
     */
    void addProductToUser(Long userId, Long productId, Long orderId);

    /**
     * 后台动态给用户添加次数
     *
     * @param userId 用户id
     * @param power  算力
     * @param remark 备注
     */
    void adminAddPowerToUser(Long userId, Integer power, String remark);

    /**
     * 后台操作会员到期时间
     *
     * @param userId         用户id
     * @param oldExpiredTime 旧的会员最新有效期
     * @param newExpiredTime 新的会员最新有效期
     */
    void adminUpdateVidDateToUser(Long userId, Instant oldExpiredTime, Instant newExpiredTime);

    /**
     * 给VIP用户增加每日使用额度
     *
     * @param userId      用户id
     * @param vipDayPower 会员每日使用额度
     */
    void resetSubscribeToUserByDaily(Long userId, Integer vipDayPower);

    /**
     * 获取有效期内的套餐记录
     *
     * @param userId 用户id
     * @return 套餐记录
     */
    List<UserProductLogEntity> getExpirationCardList(Long userId);

    /**
     * 获取用户可用会话余额
     *
     * @param userId 用户id
     * @return 余额
     */
    int getUserChatPower(Long userId);

    /**
     * 会话套餐扣减
     *
     * @param userId 用户id
     * @param power  算力
     */
    void reducePower(Long userId, Integer power, AiPlatformEnum aiPlatformEnum, Long historyItemId);

    /**
     * 退费给用户
     */
    void refundProductToUser(Long userId, Integer power, AiPlatformEnum aiPlatformEnum, Long historyItemId);
}
