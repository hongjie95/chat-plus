package com.chatplus.application.service.basedata.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.dao.basedata.MenuDao;
import com.chatplus.application.domain.entity.basedata.MenuEntity;
import com.chatplus.application.service.basedata.MenuService;
import org.springframework.stereotype.Service;

/**
 * 前端菜单表业务逻辑实现
 *
 * <p>Table: t_menu - 前端菜单表</p>
 *
 * @author developer
 * @see MenuEntity
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuDao, MenuEntity> implements MenuService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(MenuServiceImpl.class);

}
