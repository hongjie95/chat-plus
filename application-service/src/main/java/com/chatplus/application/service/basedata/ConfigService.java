package com.chatplus.application.service.basedata;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.dto.AdminConfigDto;
import com.chatplus.application.domain.dto.DevopsConfigDto;
import com.chatplus.application.domain.dto.ExtendConfigDto;
import com.chatplus.application.domain.dto.NoticeConfigDto;
import com.chatplus.application.domain.entity.basedata.ConfigEntity;
import com.chatplus.application.file.dto.FileConfigDto;

/**
 * 第三方AI配置表业务逻辑接口
 *
 * <p>Table: t_config - 第三方AI配置表</p>
 *
 * @author developer
 * @see ConfigEntity
 */
public interface ConfigService extends IService<ConfigEntity> {

    void init();
    ConfigEntity getByMarker(String marker);

    AdminConfigDto getSystemConfig();
    NoticeConfigDto getNoticeConfig();

    FileConfigDto getFileConfig();

    ExtendConfigDto getExtendConfig();

    DevopsConfigDto getDevopsConfig();
}
