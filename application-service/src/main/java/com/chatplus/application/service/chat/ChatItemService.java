package com.chatplus.application.service.chat;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.chat.ChatItemEntity;

import java.util.List;

/**
 * 用户会话列表业务逻辑接口
 *
 * <p>Table: t_chat_item - 用户会话列表</p>
 *
 * @author developer
 * @see ChatItemEntity
 */
public interface ChatItemService extends IService<ChatItemEntity> {


    List<ChatItemEntity> getChatItemByUserId(Long userId);

    ChatItemEntity getChatItemByChatId(String chatId);
}
