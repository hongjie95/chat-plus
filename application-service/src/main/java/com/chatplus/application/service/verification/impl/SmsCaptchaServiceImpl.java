package com.chatplus.application.service.verification.impl;

import com.chatplus.application.common.exception.BadRequestException;
import com.chatplus.application.domain.dto.ExtendConfigDto;
import com.chatplus.application.service.verification.PlusCaptchaService;
import com.chatplus.application.util.ConfigUtil;
import org.dromara.sms4j.api.SmsBlend;
import org.dromara.sms4j.api.entity.SmsResponse;
import org.dromara.sms4j.core.factory.SmsFactory;
import org.dromara.sms4j.provider.enumerate.SupplierType;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service(value = "smsCaptchaService")
public class SmsCaptchaServiceImpl extends PlusCaptchaService {
    public SmsCaptchaServiceImpl(RedissonClient redissonClient) {
        super(redissonClient);
    }

    @Override
    public boolean sendNotice(String receiver, String noticeMsg) {
        return false;
    }

    @Override
    public boolean doSendCaptcha(String phoneNumber, String captcha) {
        ExtendConfigDto extendConfig = ConfigUtil.getExtendConfig();
        if (Boolean.FALSE.equals(extendConfig.getEnableSms())) {
            throw new BadRequestException("短信供应商未配置");
        }
        if (Boolean.TRUE.equals(extendConfig.getSmsMock())) {
            return true;
        }
        SupplierType supplierType = SupplierType.valueOf(extendConfig.getSmsSupplier());
        SmsBlend smsBlend = SmsFactory.createSmsBlend(supplierType);
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        map.put("code", captcha);
        SmsResponse response = smsBlend.sendMessage(phoneNumber, extendConfig.getSmsTemplateId(), map);
        return response.isSuccess();
    }

}
