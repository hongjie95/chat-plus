package com.chatplus.application.service.pay.impl.wechat;

import com.chatplus.application.client.pay.WeChatPayClient;
import com.chatplus.application.client.pay.domain.request.wechat.WeChatOrderPayRequest;
import com.chatplus.application.client.pay.domain.response.wechat.WechatOrderPayResponse;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.enumeration.SubModeEnum;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderV3Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 微信支付-PC支付
 */
@Service("WeChatApiService_WECHAT_NATIVE")
public class WeChatPayWebPayService extends WeChatPayApiService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(WeChatPayWebPayService.class);

    @Autowired
    public WeChatPayWebPayService(WeChatPayClient weChatPayClient) {
        super(weChatPayClient);
    }

    @Override
    public WechatOrderPayResponse orderPay(WeChatOrderPayRequest param) {
        try {
            WxPayUnifiedOrderV3Request.Amount amount = new WxPayUnifiedOrderV3Request.Amount();
            WxPayUnifiedOrderV3Request request = new WxPayUnifiedOrderV3Request();
            request.setMchid(param.getMchId());
            request.setNotifyUrl(param.getNotifyUrl());
            // 通知地址
            request.setTimeExpire(param.getExpireTime());
            amount.setTotal(param.getMoney().intValue());
            // 商户订单号
            request.setOutTradeNo(param.getPayTransactionId());
            request.setAmount(amount);
            // 商品描述
            request.setDescription(param.getSubject());
            //  附加数据
            request.setAttach(SubModeEnum.WECHAT_NATIVE.getValue());
            return weChatPayClient.webPay(request);
        } catch (Exception e) {
            LOGGER.message("微信支付-PC支付调用失败")
                    .context("request", param)
                    .exception(e).error();
        }
        return null;
    }
}
