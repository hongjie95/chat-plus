package com.chatplus.application.service.verification.domain.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class CaptchaSendInfo implements Serializable {
    private static final long serialVersionUID = 5083203938470333988L;
    private Boolean success;
    private String captcha;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime sendDateTime;
    /**
     * 是否已验证
     */
    private Boolean hasVerified;
    /**
     * 验证码提供者名
     */
    private String providerName;
    private String serviceProviderId;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public LocalDateTime getSendDateTime() {
        return sendDateTime;
    }

    public void setSendDateTime(LocalDateTime sendDateTime) {
        this.sendDateTime = sendDateTime;
    }

    public Boolean getHasVerified() {
        return hasVerified;
    }

    public void setHasVerified(Boolean hasVerified) {
        this.hasVerified = hasVerified;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(String serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

    public boolean captchaIsExpire(long expireInSecond) {
        return Boolean.TRUE.equals(getSuccess())
                && Objects.nonNull(getSendDateTime())
                && getSendDateTime().plusSeconds(expireInSecond).isBefore(LocalDateTime.now());
    }
}
