package com.chatplus.application.service.chat;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.chat.ChatRoleEntity;

/**
 * 聊天角色表业务逻辑接口
 *
 * <p>Table: t_chat_role - 聊天角色表</p>
 *
 * @author developer
 * @see ChatRoleEntity
 */
public interface ChatRoleService extends IService<ChatRoleEntity> {

    Integer getMaxSortNum();

}
