package com.chatplus.application.service.chat;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.chat.ChatHistoryEntity;

import java.util.List;

/**
 * 聊天历史记录业务逻辑接口
 *
 * <p>Table: t_chat_history - 聊天历史记录</p>
 *
 * @author developer
 * @see ChatHistoryEntity
 */
public interface ChatHistoryService extends IService<ChatHistoryEntity> {
    /**
     * 获取用户最后一条回复历史记录
     *
     * @param userId 用户id
     * @param chatId 会话id
     * @return 历史记录
     */
    ChatHistoryEntity getLastReplyHistoryByUserId(Long userId, String chatId);

    /**
     * 获取聊天上下文历史记录
     *
     * @param userId 用户id
     * @param chatId 会话id
     * @return 聊天历史记录
     */
    List<ChatHistoryEntity> getChatContextHistoryList(Long userId, String chatId);

    /**
     * 获取问题和回答两条数据
     *
     * @param historyItemId 问题和回答的关联Id
     * @return 聊天历史记录
     */
    List<ChatHistoryEntity> getHistoryByItemId(Long historyItemId);
    /**
     * 获取AI回复历史记录
     *
     * @param userId 用户id
     * @return 历史记录
     */
    List<ChatHistoryEntity> getReplyHistoryList(Long userId);
    /**
     * 获取本月的token
     */
    long getThisMonthToken(List<ChatHistoryEntity> replyHistoryList);
    /**
     * 获取总的token
     */
    long getTotalToken(List<ChatHistoryEntity> replyHistoryList);
}
