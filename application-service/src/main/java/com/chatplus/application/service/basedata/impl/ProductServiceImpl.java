package com.chatplus.application.service.basedata.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.dao.basedata.ProductDao;
import com.chatplus.application.domain.entity.basedata.ProductEntity;
import com.chatplus.application.service.basedata.ProductService;
import org.springframework.stereotype.Service;

/**
 * 会员套餐表业务逻辑实现
 *
 * <p>Table: t_product - 会员套餐表</p>
 *
 * @author developer
 * @see ProductEntity
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductDao, ProductEntity> implements ProductService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(ProductServiceImpl.class);

    @Override
    public Integer getMaxSortNum() {
        ProductEntity productEntity = baseMapper.selectOne(Wrappers.<ProductEntity>lambdaQuery()
                .orderByDesc(ProductEntity::getSortNum)
                .last("LIMIT 1"), false);
        return productEntity != null ? productEntity.getSortNum() : 0;

    }
}
