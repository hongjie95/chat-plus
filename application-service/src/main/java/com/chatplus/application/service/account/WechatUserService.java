package com.chatplus.application.service.account;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.account.WechatUserEntity;
import com.chatplus.application.domain.response.WechatLoginQrResponse;

/**
 * 微信小程序用户业务逻辑接口
 *
 * <p>Table: t_wechat_user - 微信小程序用户</p>
 *
 * @author developer
 * @see WechatUserEntity
 */
public interface WechatUserService extends IService<WechatUserEntity> {
    /**
     * 获取获取的二维码
     *
     * @return 二维码图片地址
     */
    WechatLoginQrResponse getLoginQrCode();

    WechatUserEntity getOneByOpenidAndAppId(String openId, String appId);

    WechatLoginQrResponse generateBindQrCode(Long userId);

    WechatUserEntity getByUserId(Long userId);
}
