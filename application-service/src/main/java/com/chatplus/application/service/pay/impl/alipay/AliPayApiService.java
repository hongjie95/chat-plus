package com.chatplus.application.service.pay.impl.alipay;

import com.alipay.easysdk.payment.common.models.AlipayTradeFastpayRefundQueryResponse;
import com.alipay.easysdk.payment.common.models.AlipayTradeQueryResponse;
import com.alipay.easysdk.payment.common.models.AlipayTradeRefundResponse;
import com.chatplus.application.client.pay.AliPayClient;
import com.chatplus.application.client.pay.domain.request.alipay.AlipayOrderPayRequest;
import com.chatplus.application.client.pay.domain.request.alipay.AlipayRefundRequest;
import com.chatplus.application.client.pay.domain.response.alipay.AlipayOrderPayResponse;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.domain.entity.pay.PayRequestEntity;
public abstract class AliPayApiService {

    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(AliPayApiService.class);

    protected AliPayClient aliPayServiceClient;

    protected AliPayApiService(AliPayClient aliPayServiceClient) {
        this.aliPayServiceClient = aliPayServiceClient;
    }
    /**
     * 支付接口抽象方法
     *
     * @param request 参数
     * @return AlipayOrderPayResponse
     */
    public abstract AlipayOrderPayResponse orderPay(AlipayOrderPayRequest request);

    public AlipayTradeQueryResponse queryOrderPay(PayRequestEntity payRequestEntity) {
        try {
            return aliPayServiceClient.queryOrderPay(payRequestEntity);
        } catch (Exception e) {
            LOGGER.message("支付宝支付-支付结果查询调用失败")
                    .context("request", payRequestEntity)
                    .exception(e).error();
        }
        return null;
    }
}
