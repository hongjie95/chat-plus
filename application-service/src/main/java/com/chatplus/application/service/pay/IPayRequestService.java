package com.chatplus.application.service.pay;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.pay.PayRequestEntity;
import com.chatplus.application.enumeration.PayStatusEnum;

/**
 * 支付请求记录（账单）业务逻辑接口
 *
 * <p>Table: t_pay_request - 支付请求记录（账单）</p>
 *
 * @author developer
 * @see PayRequestEntity
 */
public interface IPayRequestService extends IService<PayRequestEntity> {

    PayRequestEntity findByPayTransactionId(String payTransactionId);
    PayRequestEntity findByAppBusinessId(Long appBusinessId, PayStatusEnum payStatusEnum);
}
