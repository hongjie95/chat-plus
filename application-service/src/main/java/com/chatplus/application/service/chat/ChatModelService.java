package com.chatplus.application.service.chat;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.chat.ChatModelEntity;

/**
 * AI 模型表业务逻辑接口
 *
 * <p>Table: t_chat_model - AI 模型表</p>
 *
 * @author developer
 * @see ChatModelEntity
 */
public interface ChatModelService extends IService<ChatModelEntity> {
    Integer getMaxSortNum();
}
