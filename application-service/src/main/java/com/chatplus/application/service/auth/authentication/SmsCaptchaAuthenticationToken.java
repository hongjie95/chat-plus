package com.chatplus.application.service.auth.authentication;

public interface SmsCaptchaAuthenticationToken extends AuthenticationToken{
    /**
     * 短信验证码
     */
    String getVerifyCode();
}
