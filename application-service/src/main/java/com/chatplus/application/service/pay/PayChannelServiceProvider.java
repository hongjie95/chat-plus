package com.chatplus.application.service.pay;

import com.chatplus.application.enumeration.PayChannelEnum;
import com.chatplus.application.service.pay.impl.PayChannelService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 支付渠道处理器提供者
 *
 * @author liexuan
 * @date 2022-08-16 14:00
 **/
@Service
public class PayChannelServiceProvider {

    public static final String SERVICE_NAME_PRE = "PayChannelService_";

    private final Map<PayChannelEnum, PayChannelService> payChannelServiceMap = new HashMap<>();

    public PayChannelServiceProvider(List<PayChannelService> payChannelServices) {
        for (PayChannelService payChannelService : payChannelServices) {
            payChannelServiceMap.put(payChannelService.getChannel(), payChannelService);
        }
    }

    /**
     * 获取支付渠道处理器
     *
     * @param channel 支付渠道
     * @return PayChannelService
     */
    public PayChannelService getPayChannelService(PayChannelEnum channel) {
        return payChannelServiceMap.get(channel);
    }

}
