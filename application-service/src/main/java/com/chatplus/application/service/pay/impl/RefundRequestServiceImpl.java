package com.chatplus.application.service.pay.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.dao.pay.RefundRequestDao;
import com.chatplus.application.domain.entity.pay.RefundRequestEntity;
import com.chatplus.application.service.pay.IRefundRequestService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 退款请求表业务逻辑实现
 *
 * <p>Table: t_refund_request - 退款请求表</p>
 *
 * @author developer
 * @see RefundRequestEntity
 */
@Service
public class RefundRequestServiceImpl extends ServiceImpl<RefundRequestDao, RefundRequestEntity> implements IRefundRequestService {
    private final RefundRequestDao refundRequestDao;

    public RefundRequestServiceImpl(RefundRequestDao refundRequestDao) {
        this.refundRequestDao = refundRequestDao;
    }

    @Override
    public RefundRequestEntity findByRefundApplySeqNo(String refundApplySeqNo) {
        return getOne(new LambdaQueryWrapper<RefundRequestEntity>()
            .eq(RefundRequestEntity::getRefundApplyNo, refundApplySeqNo)
            .orderByDesc(RefundRequestEntity::getId)
            .last("LIMIT 1"));
    }

    @Override
    public RefundRequestEntity findLastRefundByPayRequestId(Long payRequestId) {
        return getOne(new LambdaQueryWrapper<RefundRequestEntity>()
            .eq(RefundRequestEntity::getPayRequestId, payRequestId)
            .orderByDesc(RefundRequestEntity::getApplyAt)
            .last("LIMIT 1"));
    }

    @Override
    public List<RefundRequestEntity> findRefundByPayRequestId(Long payRequestId) {
        return list(new LambdaQueryWrapper<RefundRequestEntity>()
            .eq(RefundRequestEntity::getPayRequestId, payRequestId)
            .orderByDesc(RefundRequestEntity::getApplyAt));
    }

    @Override
    public Long sumRefundedMoney(Long payRequestId) {
        return refundRequestDao.sumRefundedMoney(payRequestId);
    }
}
