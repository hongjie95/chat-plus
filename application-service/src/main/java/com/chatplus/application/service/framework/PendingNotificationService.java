package com.chatplus.application.service.framework;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.framework.PendingNotificationEntity;

/**
 * 待执行队列通知业务逻辑接口
 *
 * <p>Table: t_pending_notification - 待执行队列通知</p>
 *
 * @author developer
 * @see PendingNotificationEntity
 */
public interface PendingNotificationService extends IService<PendingNotificationEntity> {

}
