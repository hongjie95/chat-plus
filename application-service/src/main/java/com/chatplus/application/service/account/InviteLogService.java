package com.chatplus.application.service.account;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chatplus.application.domain.entity.account.InviteLogEntity;

/**
 * 邀请注册日志业务逻辑接口
 *
 * <p>Table: t_invite_log - 邀请注册日志</p>
 *
 * @author developer
 * @see InviteLogEntity
 */
public interface InviteLogService extends IService<InviteLogEntity> {

}
