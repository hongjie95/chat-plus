package com.chatplus.application.service.auth.authentication;

/**
 * 账号登录认证信息
 * @author weird
 */
public interface AuthenticationToken {
    /**
     * 被认证主体的身份标识，如用户名，手机号等。
     * @return 被认证主体的身份标识
     */
    Object getUserPrincipal();

    String getRemoteIp();
}
