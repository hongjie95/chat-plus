package com.chatplus.application.service.functions.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chatplus.application.dao.functions.FunctionDao;
import com.chatplus.application.domain.entity.functions.FunctionEntity;
import com.chatplus.application.service.functions.FunctionService;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 函数插件表业务逻辑实现
 *
 * <p>Table: t_function - 函数插件表</p>
 *
 * @author developer
 * @see FunctionEntity
 */
@Service
public class FunctionServiceImpl extends ServiceImpl<FunctionDao, FunctionEntity> implements FunctionService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(FunctionServiceImpl.class);

}
