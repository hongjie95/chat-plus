package com.chatplus.application.client.pay.domain.request.wechat;

/**
 * 微信退款需要到的参数封装
 **/
public class WeChatRefundRequest {
    /**
     * 退款回调地址，可为空
     */
    private String notifyUrl;

    private String mchId;
    /**
     * 金额：单位分
     */
    private Long refundMoney;
    /**
     * 金额：单位分
     */
    private Long totalMoney;
    /**
     * 退款备注
     */
    private String refundRemark;
    /**
     * 退款流水号，收银台这边生成的
     */
    private String refundApplySeqNo;

    /////////////// 以下两个参数二选一 //////////////////////

    /**
     *  支付流水号，收银台生成的
     */
    private String payTransactionId;

    /**
     * 第三方交易ID;微信/支付宝生成的支付流水号
     */
    private String tradeTransactionId;


    public Long getRefundMoney() {
        return refundMoney;
    }

    public void setRefundMoney(Long refundMoney) {
        this.refundMoney = refundMoney;
    }


    public String getRefundRemark() {
        return refundRemark;
    }

    public void setRefundRemark(String refundRemark) {
        this.refundRemark = refundRemark;
    }

    public String getRefundApplySeqNo() {
        return refundApplySeqNo;
    }

    public void setRefundApplySeqNo(String refundApplySeqNo) {
        this.refundApplySeqNo = refundApplySeqNo;
    }

    public String getPayTransactionId() {
        return payTransactionId;
    }

    public void setPayTransactionId(String payTransactionId) {
        this.payTransactionId = payTransactionId;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public Long getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Long totalMoney) {
        this.totalMoney = totalMoney;
    }

    public String getTradeTransactionId() {
        return tradeTransactionId;
    }

    public void setTradeTransactionId(String tradeTransactionId) {
        this.tradeTransactionId = tradeTransactionId;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }
}
