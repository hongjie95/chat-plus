package com.chatplus.application.client.pay.domain.response.wechat;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * description: 微信统一下单返回参数封装
 *
 * @author Angus
 * @date 2021-02-04 11:30
 **/
@Data
public class WechatOrderPayResponse implements Serializable {
    @Serial
    private static final long serialVersionUID = -2491424243434627075L;


    @Schema(description = "签名")
    private String sign;

    @Schema(description = "预支付交易会话ID")
    private String prepayId;

    @Schema(description = "商户号")
    private String partnerId;

    @Schema(description = "appid")
    private String appId;

    @Schema(description = "mchid")
    private String mchid;

    @Schema(description = "由于package为java保留关键字，因此改为packageValue. 前端使用时记得要更改为package")
    private String packageValue;

    @Schema(description = "时间戳")
    private String timeStamp;

    @Schema(description = "随机字符串")
    private String nonceStr;

    @Schema(description = "签名方式")
    private String signType;

    @Schema(description = "签名")
    private String paySign;

    @Schema(description = "mweb_url H5支付跳转链接")
    private String mwebUrl;

    @Schema(description = "trade_type为NATIVE时有返回，用于生成二维码，展示给用户进行扫码支付")
    private String codeUrl;

    @Schema(description = "h5支付链接，有效期5分钟")
    private String h5Url;
}
