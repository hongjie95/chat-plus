package com.chatplus.application.client.pay.domain.request.wechat;

import lombok.Data;

/**
 * 支付宝支付需要到的参数封装
 *
 * @author Angus
 * @date 2022-04-22 09:42
 **/
@Data
public class WeChatOrderPayRequest {

    /**
     * 金额：单位分
     */
    private Long money;
    /**
     * 渠道交易ID
     */
    private String payTransactionId;
    /**
     * 微信小程序时候必传，对应openid
     */
    private String userId;
    /**
     * 订单标题
     */
    private String subject;
    /**
     * 支付完成跳转地址
     */
    private String returnUrl;
    /**
     * 支付回调通知地址
     */
    private String notifyUrl;
    /**
     * 商户号
     */
    private String mchId;
    // 微信h5支付需要以下三个字段
    /**
     * 用户的客户端IP，支持IPv4和IPv6两种格式的IP地址。
     * 示例值：14.23.150.211
     */
    private String payerClientIp;
    /**
     * 场景类型 示例值：iOS, Android, Wap
     */
    private String sceneType;
    /**
     * 为空则使用系统默认过期时间（30分钟），格式 yyyy-MM-dd HH:mm:ss
     */
    private String expireTime;

}
