package com.chatplus.application.client.pay.domain.response;

import com.chatplus.application.enumeration.PayStatusEnum;
import java.time.Instant;

/**
 * 支付查询结果返回
 *
 * @author liexuan
 * @date 2022-08-17 11:35
 **/
public class InternalPayQueryResponse {


    /**
     * 支付ID
     */
    private Long payRequestId;
    /**
     * 订单状态
     */
    private PayStatusEnum payStatusEnum;
    /**
     * 支付成功时间
     */
    private Instant paySuccessAt;
    /**
     * 付款方信息 支付账户信息
     */
    private String payerInfo;

    private String tradeTransactionId;

    private String targetSys;

    public String getTargetSys() {
        return targetSys;
    }

    public void setTargetSys(String targetSys) {
        this.targetSys = targetSys;
    }

    public Long getPayRequestId() {
        return payRequestId;
    }

    public void setPayRequestId(Long payRequestId) {
        this.payRequestId = payRequestId;
    }
    public Instant getPaySuccessAt() {
        return paySuccessAt;
    }

    public void setPaySuccessAt(Instant paySuccessAt) {
        this.paySuccessAt = paySuccessAt;
    }

    public String getPayerInfo() {
        return payerInfo;
    }

    public void setPayerInfo(String payerInfo) {
        this.payerInfo = payerInfo;
    }

    public String getTradeTransactionId() {
        return tradeTransactionId;
    }

    public void setTradeTransactionId(String tradeTransactionId) {
        this.tradeTransactionId = tradeTransactionId;
    }

    public PayStatusEnum getPayStatusEnum() {
        return payStatusEnum;
    }

    public void setPayStatusEnum(PayStatusEnum payStatusEnum) {
        this.payStatusEnum = payStatusEnum;
    }
}
