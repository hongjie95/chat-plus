package com.chatplus.application.client.pay.domain.response.alipay;

import java.io.Serializable;
import java.util.Map;

/**
 * 支付宝生成支付订单信息返回值
 * * @date 23:08 - 2021/2/23.
 */
public class AlipayOrderPayResponse implements Serializable {
    private static final long serialVersionUID = -9034801130735437902L;

    private String responseCode;

    private String responseMsg;

    private String actionUrl;

    private String appId;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public String getActionUrl() {
        return actionUrl;
    }

    public void setActionUrl(String actionUrl) {
        this.actionUrl = actionUrl;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }
}
