package com.chatplus.application.client.pay.domain.response.wechat;

import com.github.binarywang.wxpay.bean.result.WxPayRefundV3Result;

public class TurnRightWxPayRefundV3Result extends WxPayRefundV3Result {

    private String errCodeDes;

    public String getErrCodeDes() {
        return errCodeDes;
    }

    public void setErrCodeDes(String errCodeDes) {
        this.errCodeDes = errCodeDes;
    }
}
