package com.chatplus.application.client.pay.domain.request.alipay;
/**
 * 支付宝支付需要到的参数封装
 *
 * @author liexuan
 * @date 2022-04-22 09:42
 **/
public class AlipayOrderPayRequest {
    /**
     * 金额：单位分
     */
    private Long money;
    /**
     * 渠道交易ID
     */
    private String payTransactionId;
    /**
     * 订单标题
     */
    private String subject;
    /**
     * 支付完成跳转地址
     */
    private String returnUrl;
    /**
     * 备注可选 （但是业务需要）
     */
    private String remark;
    private AlipayOrderPayRequest() {
    }

    /**
     * 参数构造
     *
     * @return ParamBuild
     */
    public static AlipayOrderPayRequest build() {
        return new AlipayOrderPayRequest();
    }

    /**
     * 构建必填参数
     */
    public AlipayOrderPayRequest setRequired(Long money,
                                             String channelTransactionId,
                                             String subject) {
        this.money = money;
        this.payTransactionId = channelTransactionId;
        this.subject = subject;
        return this;
    }

    public Long getMoney() {
        return money;
    }

    public void setMoney(Long money) {
        this.money = money;
    }

    public String getPayTransactionId() {
        return payTransactionId;
    }

    public void setPayTransactionId(String payTransactionId) {
        this.payTransactionId = payTransactionId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
