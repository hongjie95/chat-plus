package com.chatplus.application.client.pay.domain.response;

import lombok.Data;

import java.io.Serializable;

/**
 *  通用
 *
 * @author chj
 * @date 2024/1/15
 **/
@Data
public class InternalPayResponse implements Serializable {
    /**
     * 二维码跳转链接
     */
    private String actionUrl;

    //////////////////  微信JSAPI 支付参数 start //////////////////

    private String appId;
    /**
     * 由于package为java保留关键字，因此改为packageValue. 前端使用时记得要更改为package
     */
    private String packageValue;

    /**
     * 时间戳
     */
    private String timeStamp;

    /**
     * 随机字符串
     */
    private String nonceStr;

    /**
     * 签名方式
     */
    private String signType;

    /**
     * 签名
     */
    private String paySign;

    //////////////////  微信JSAPI 支付参数 end //////////////////
}
