package com.chatplus.application.client.pay.domain.request.alipay;

/**
 * 支付宝退款需要到的参数封装
 **/
public class AlipayRefundRequest {

    /**
     * 金额：单位分
     */
    private Long refundMoney;
    /**
     * appId
     */
    private String appId;
    /**
     * 退款备注
     */
    private String refundRemark;
    /**
     * 退款流水号，收银台这边生成的
     */
    private String refundApplySeqNo;

    /**
     *  支付流水号，收银台生成的
     */
    private String payTransactionId;


    public Long getRefundMoney() {
        return refundMoney;
    }

    public void setRefundMoney(Long refundMoney) {
        this.refundMoney = refundMoney;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getRefundRemark() {
        return refundRemark;
    }

    public void setRefundRemark(String refundRemark) {
        this.refundRemark = refundRemark;
    }

    public String getRefundApplySeqNo() {
        return refundApplySeqNo;
    }

    public void setRefundApplySeqNo(String refundApplySeqNo) {
        this.refundApplySeqNo = refundApplySeqNo;
    }

    public String getPayTransactionId() {
        return payTransactionId;
    }

    public void setPayTransactionId(String payTransactionId) {
        this.payTransactionId = payTransactionId;
    }
}
