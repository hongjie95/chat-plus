package com.chatplus.application.config.extend;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * 主要实现配置变更事件发布于监听
 *
 * @Author Angus
 * @Date 2024/3/31
 */
@Component
public class ConfigChangeListener implements ApplicationListener<ConfigChangeListener.ConfigChangeEvent> {

    @Override
    public void onApplicationEvent(@NotNull ConfigChangeEvent configChangeEvent) {
        // 收到通知的处理逻辑
    }

    public static void publishConfigChangeEvent(String key) {
        SpringConfigUtil.getApplicationContext().publishEvent(new ConfigChangeEvent(Thread.currentThread().getStackTrace()[0], key));
    }

    @Getter
    public static class ConfigChangeEvent extends ApplicationEvent {
        private final String key;

        public ConfigChangeEvent(Object source, String key) {
            super(source);
            this.key = key;
        }
    }
}
