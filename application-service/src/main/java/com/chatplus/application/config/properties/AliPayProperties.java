package com.chatplus.application.config.properties;


import com.chatplus.application.config.pay.PayConfig;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Alipay 配置
 *
 * @Author Angus
 * @Date 2024/3/31
 */
@Data
@Component
@ConfigurationProperties(PayConfig.CONFIG_KEY_ROOTS + ".alipay")
public class AliPayProperties {


    private boolean enable = false;
    /**
     * 通信协议，通常填写https
     */
    private String protocol = "https";

    /**
     * 网关域名
     * 线上为：openapi.alipay.com
     * 沙箱为：openapi.alipaydev.com
     */
    private String gatewayHost = "openapi.alipay.com";

    /**
     * AppId
     */
    private String appId;

    /**
     * 签名类型，Alipay Easy SDK只推荐使用RSA2，估此处固定填写RSA2
     */
    private String signType = "RSA2";

    /**
     * 支付宝公钥
     */
    private String alipayPublicKey;

    /**
     * 应用私钥
     */
    private String merchantPrivateKey;

    /**
     * 应用公钥证书文件路径
     */
    private String merchantCertPath;

    /**
     * 支付宝公钥证书文件路径
     */
    private String alipayCertPath;

    /**
     * 支付宝根证书文件路径
     */
    private String alipayRootCertPath;

    /**
     * 异步通知回调地址（可选）
     */
    private String notifyUrl;

    /**
     * AES密钥（可选）
     */
    private String encryptKey;

    /**
     * 签名提供方的名称(可选)，例：Aliyun KMS签名，signProvider = "AliyunKMS"
     */
    private String signProvider;

    /**
     * 支付宝授权地址
     */
    private String authorizeUrl = "https://openauth.alipay.com/oauth2/publicAppAuthorize.htm";

    /**
     * 授权回调
     */
    private String authorizeCallback;

    /**
     * 签约的支付宝账号对应的支付宝唯一用户号，以 2088 开头的 16 位纯数字组成。
     */
    private String pid;

    /**
     * 是否支持沙箱环境(注: 正式环境不用配置,默认即可)
     */
    private boolean supportSandbox = true;
}
