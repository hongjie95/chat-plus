package com.chatplus.application.config.runner;

import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.service.basedata.ApiKeyService;
import com.chatplus.application.service.basedata.ConfigService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 初始化对应业务数据
 */
@Component
@Order(2)
public class SystemApplicationRunner implements ApplicationRunner {

    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(SystemApplicationRunner.class);

    private final ApiKeyService apiKeyService;
    private final ConfigService configService;

    public SystemApplicationRunner(ApiKeyService apiKeyService, ConfigService configService) {
        this.apiKeyService = apiKeyService;
        this.configService = configService;
    }

    @Override
    public void run(ApplicationArguments args) {
        LOGGER.message("初始化OSS配置成功").info();
        configService.init();
        LOGGER.message("初始化系统配置成功").info();
        apiKeyService.init();
        LOGGER.message("初始化APIKEY配置成功").info();
    }

}
