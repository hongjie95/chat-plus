package com.chatplus.application.config.properties;


import com.chatplus.application.config.pay.PayConfig;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * WeChat支付 配置
 *
 * @Author Angus
 * @Date 2024/3/31
 */
@Data
@Component
@ConfigurationProperties(PayConfig.CONFIG_KEY_ROOTS + ".wechat")
public class WechatPayProperties {
    private boolean enable = false;
    /**
     * 账号适用于那个终端付款如：APP，PC
     */
    private String useFor;
    /**
     * 设置微信公众号或者小程序等的appid
     */
    private String appId;

    /**
     * 微信支付商户号
     */
    private String mchId;

    /**
     * 微信支付商户密钥
     */
    private String mchKey;

    private String apiV3Key;

    /**
     * apiclient_key.p12文件的绝对路径，或者如果放在项目中，请以classpath:开头指定
     */
    private String privateKeyPath;
    /**
     * apiclient_cert.p12文件的绝对路径，或者如果放在项目中，请以classpath:开头指定
     */
    private String privateCertPath;

    /**
     * 业务回调地址
     */
    private String notifyUrl;

    /**
     * 是否进行分账
     */
    private boolean profitSharing;

    /**
     * 分账接收账号
     */
    private String subAccount;

    /**
     * 分账比率
     */
    private String proportion;

    /**
     * 手续费
     */
    private String poundageRate;
}
