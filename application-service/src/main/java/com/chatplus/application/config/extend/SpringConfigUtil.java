package com.chatplus.application.config.extend;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author Angus
 * @Date 2024/3/31
 */
@Component
public class SpringConfigUtil implements ApplicationContextAware, EnvironmentAware {
    @Getter
    private static ApplicationContext applicationContext;
    @Getter
    private static Environment environment;

    @Getter
    private static Binder binder;

    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
        SpringConfigUtil.applicationContext = applicationContext;
    }

    @Override
    public void setEnvironment(@NotNull Environment environment) {
        SpringConfigUtil.environment = environment;
        binder = Binder.get(environment);
    }

}
