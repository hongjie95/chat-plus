package com.chatplus.application.config.pay;

import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 支付配置
 */
@Configuration
@EnableScheduling
@EnableConfigurationProperties
@EnableAspectJAutoProxy()
public class PayConfig {

    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(PayConfig.class);
    public static final String CONFIG_KEY_ROOTS = "chatplus.pay";
    @Bean
    @ConfigurationProperties(CONFIG_KEY_ROOTS)
    public Properties appProperties() {
        return new Properties();
    }
    @Data
    public static class Properties {
        private Long maxPayMoney = Long.MAX_VALUE;
        private String certFolder;
    }
}
