package com.chatplus.application.config.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * WeChat公众号 配置
 *
 * @Author Angus
 * @Date 2024/3/31
 */
@Data
@Component
@ConfigurationProperties("chatplus.wechat.mp")
public class WechatMpProperties {

    private boolean enable = false;
    /**
     * 设置微信公众号或者小程序等的appid
     */
    private String appId;
    /**
     * 设置微信公众号secret
     */
    private String secret;

    /**
     * 设置微信公众号token
     */
    private String token;
    /**
     * 设置微信公众号消息加解密密钥
     */
    private String aesKey;
    /**
     * 回调地址
     */
    private String notifyUrl;

}
