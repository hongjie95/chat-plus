package com.chatplus.application.config.extend;

import lombok.Getter;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 自定义一个属性配置源
 * 直接基于内存的ConcurrentHashMap来进行模拟，内部提供了一个配置更新的方法，当配置刷新之后，还会对外广播一个配置变更事件
 *
 * @Author Angus
 * @Date 2024/3/31
 */
public class ExtendConfigContext {
    private ExtendConfigContext() {
    }

    @Getter
    private static volatile ExtendConfigContext instance = new ExtendConfigContext();
    @Getter
    private Map<String, Object> cache = new ConcurrentHashMap<>();
    /**
     * 更新配置
     *
     * @param key 配置key
     * @param val 配置值
     */
    public void updateConfig(String key, Object val) {
        if (Objects.isNull(val) || (cache.containsKey(key) && cache.get(key).equals(val))) {
            return;
        }
        cache.put(key, val);
        ConfigChangeListener.publishConfigChangeEvent(key);
    }

}
