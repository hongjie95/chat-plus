package com.chatplus.application.aiprocessor.constant;

import com.chatplus.application.enumeration.AiPlatformEnum;

/**
 * AI常量
 * @Author Angus
 */
public class AIConstants {

    private AIConstants() {
    }
    public static final String ERROR_MSG = "抱歉，AI 助手开小差了，请稍后再试。";
    public static final String SYSTEM_CONFIG_REDIS_KEY = "ai_system_config";
    public static final String SYSTEM_FILE_REDIS_KEY = "ai_system_file";

    public static final String SYSTEM_NOTICE_REDIS_KEY = "ai_system_notice";

    public static final String SYSTEM_EXTEND_REDIS_KEY = "ai_extend_config";
    public static final String SYSTEM_DEVOPS_REDIS_KEY = "ai_devops_config";
    /**
     * 以下参数不要随便改，如果自定义有url的话也要参考以下参数
     */
    public static class  DefaultBaseUrl{
        private DefaultBaseUrl() {
        }
        public static final String AZURE = "https://chat-bot-api.openai.azure.com/openai/deployments/{model}/chat/completions?api-version=2023-05-15";
        public static final String BAI_DU = "https://aip.baidubce.com";
        public static final String CHAT_GLM = "https://open.bigmodel.cn";
        public static final String OPEN_AI = "https://api.openai.com";
        public static final String XUN_FEI = "https://spark-api.xf-yun.com";
        public static final String ALI_YUN_Q_WEN_TEXT_GENERATION = "https://dashscope.aliyuncs.com";


        public static String getDefaultBaseUrlByChannel(AiPlatformEnum channel) {
            return switch (channel) {
                case AZURE -> AZURE;
                case BAI_DU -> BAI_DU;
                case CHAT_GLM -> CHAT_GLM;
                case OPEN_AI, DALL -> OPEN_AI;
                case XUN_FEI -> XUN_FEI;
                case ALI_YUN_Q_WEN -> ALI_YUN_Q_WEN_TEXT_GENERATION;
                case SD_LOCAL -> "";
                case SD_STABILITY -> "https://api.stability.ai";
                case MJ -> "https://api.ablai.top";
            };
        }
    }
}
