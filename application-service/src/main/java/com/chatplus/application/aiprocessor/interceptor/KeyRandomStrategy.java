package com.chatplus.application.aiprocessor.interceptor;

import cn.bugstack.openai.executor.interceptor.KeyStrategyFunction;
import cn.hutool.core.util.RandomUtil;
import com.chatplus.application.domain.dto.ApiKeyDto;

import java.util.List;

/**
 * 随机策略
 *
 * @author chj
 * @date 2024/3/28
 **/
public class KeyRandomStrategy implements KeyStrategyFunction<List<ApiKeyDto>, ApiKeyDto> {

    @Override
    public ApiKeyDto apply(List<ApiKeyDto> apiKeys) {
        return RandomUtil.randomEle(apiKeys);
    }
}
