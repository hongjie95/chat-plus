package com.chatplus.application.aiprocessor.util;

import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * WebSocketSession的管理器
 *
 * @author chj
 * @date 2024/2/1
 **/
public class WebSocketManager {

    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(WebSocketManager.class);


    private WebSocketManager() {
    }

    // 保存 session 连接的地方
    private static final ConcurrentMap<String, WebSocketSession> SESSION_POOL = new ConcurrentHashMap<>();
    // 保存 当前session的OkHttpClient
    private static final ConcurrentMap<String, OkHttpClient> OKHTTP_SESSION_POOL = new ConcurrentHashMap<>();

    /**
     * 添加 session
     */
    public static void add(String key, WebSocketSession session) {
        SESSION_POOL.put(key, session);
    }

    /**
     * 添加 session
     */
    public static void addSeeClient(String key, OkHttpClient client) {
        if (StringUtils.isEmpty(key) || client == null) {
            return;
        }
        OKHTTP_SESSION_POOL.put(key, client);
    }

    /**
     * 删除并返回 session
     */
    public static WebSocketSession remove(String key) {
        if (StringUtils.isEmpty(key)) {
            return null;
        }
        return SESSION_POOL.remove(key);
    }

    /**
     * 删除并返回 session
     */
    public static OkHttpClient removeSeeClient(String key) {
        if (StringUtils.isEmpty(key)) {
            return null;
        }
        return OKHTTP_SESSION_POOL.remove(key);
    }

    /**
     * 关闭连接的操作
     */
    public static void removeAndClose(String key) throws IOException {
        if (StringUtils.isEmpty(key)) {
            return;
        }
        WebSocketSession session = remove(key);
        if (session != null && session.isOpen()) {
            session.close();
        }
    }

    /**
     * 关闭连接的操作
     */
    public static void stopSeeClient(String key) {
        if (StringUtils.isEmpty(key)) {
            return;
        }
        try {
            OkHttpClient okHttpClient = removeSeeClient(key);
            if (okHttpClient != null) {
                for (Call call : okHttpClient.dispatcher().queuedCalls()) {
                    Object callTag = call.request().tag();
                    if (callTag != null && callTag.toString().contains(key)) {
                        call.cancel();
                        okHttpClient.dispatcher().executorService().shutdown();
                    }
                }
                for (Call call : okHttpClient.dispatcher().runningCalls()) {
                    Object callTag = call.request().tag();
                    if (callTag != null && callTag.toString().contains(key)) {
                        call.cancel();
                        okHttpClient.dispatcher().executorService().shutdown();
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.message("停止会话SeeClient失败").exception(e).error();
        }
    }

    /**
     * 获得 session
     */
    public static WebSocketSession get(String key) {
        return SESSION_POOL.get(key);
    }
}
