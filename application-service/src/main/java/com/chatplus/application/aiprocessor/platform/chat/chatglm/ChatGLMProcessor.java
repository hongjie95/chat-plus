package com.chatplus.application.aiprocessor.platform.chat.chatglm;

import cn.bugstack.openai.executor.model.chatglm.config.ChatGLMConfig;
import cn.bugstack.openai.executor.parameter.CompletionRequest;
import cn.bugstack.openai.session.Configuration;
import cn.bugstack.openai.session.OpenAiSession;
import cn.bugstack.openai.session.defaults.DefaultOpenAiSessionFactory;
import com.chatplus.application.aiprocessor.interceptor.DynamicKeyHandleInterceptor;
import com.chatplus.application.aiprocessor.listener.CommonEventSourceListener;
import com.chatplus.application.aiprocessor.platform.chat.ChatProcessorService;
import com.chatplus.application.aiprocessor.provider.ChatAiProcessorServiceProvider;
import com.chatplus.application.aiprocessor.util.WebSocketManager;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.enumeration.AiPlatformEnum;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.stereotype.Service;


/**
 * ChatGLM 机器人处理器
 *
 * @author chj
 * @date 2023/12/27
 **/
@Service(value = ChatAiProcessorServiceProvider.SERVICE_NAME_PRE + "ChatGLM")
public class ChatGLMProcessor extends ChatProcessorService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(ChatGLMProcessor.class);

    @Override
    public void processStream() throws Exception {
        CompletionRequest request = getCompletionRequest();
        OpenAiSession openAiSession = getSessionFactory();
        if (openAiSession != null) {
            openAiSession.completions(request, new CommonEventSourceListener(chatRequest));
        }
    }

    @Override
    public String processSync() {
        return null;
    }

    @Override
    public AiPlatformEnum getPlatform() {
        return AiPlatformEnum.CHAT_GLM;
    }

    @Override
    public synchronized OpenAiSession getSessionFactory() {
        instance();
        // 单独设置链接
        openApiKey.forEach(item -> {
            if (!item.getUrl().contains(ChatGLMConfig.V4_COMPLETIONS)) {
                item.setUrl(item.getUrl().concat(ChatGLMConfig.V4_COMPLETIONS));
            }
        });
        // 2. 配置文件
        Configuration configuration = new Configuration();
        configuration.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        // 3. 会话工厂
        DefaultOpenAiSessionFactory factory = new DefaultOpenAiSessionFactory(configuration);
        factory.setInterceptor(new DynamicKeyHandleInterceptor(openApiKey, getPlatform()));
        OpenAiSession openAiSession = factory.openSession();
        WebSocketManager.addSeeClient(chatRequest.getSessionId(), openAiSession.getClient());
        return openAiSession;
    }
}