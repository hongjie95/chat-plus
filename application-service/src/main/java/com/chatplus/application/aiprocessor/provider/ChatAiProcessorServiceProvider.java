package com.chatplus.application.aiprocessor.provider;

import com.chatplus.application.aiprocessor.platform.chat.ChatProcessorService;
import com.chatplus.application.domain.request.ws.ChatWebSocketRequest;
import com.chatplus.application.enumeration.AiPlatformEnum;
import org.springframework.stereotype.Service;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * AI聊天渠道处理器提供者
 **/
@Service
public class ChatAiProcessorServiceProvider {
    public static final String SERVICE_NAME_PRE = "chatAiChannelService";

    private final Map<AiPlatformEnum, ChatProcessorService> aiChannelServiceMap = new EnumMap<>(AiPlatformEnum.class);

    public ChatAiProcessorServiceProvider(List<ChatProcessorService> chatProcessorServiceList) {
        for (ChatProcessorService aiChannelService : chatProcessorServiceList) {
            aiChannelServiceMap.put(aiChannelService.getPlatform(), aiChannelService);
        }
    }

    /**
     * 获取AI渠道处理器
     */
    public ChatProcessorService getAiProcessorServiceByRequest(ChatWebSocketRequest request) {
        AiPlatformEnum platform = request.getPlatform();
        ChatProcessorService aiProcessorService = getAiProcessorServiceByPlatform(platform);
        aiProcessorService.setChatRequest(request);
        return aiProcessorService;
    }
    /**
     * 获取AI渠道处理器
     */
    public ChatProcessorService getAiProcessorServiceByPlatform(AiPlatformEnum platform) {
        return aiChannelServiceMap.get(platform);
    }
}
