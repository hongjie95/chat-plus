package com.chatplus.application.aiprocessor.provider;

import com.chatplus.application.aiprocessor.function.TriggerFunctionService;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * AI函数处理器提供者
 **/
@Service
public class TriggerFunctionServiceProvider {
    public static final String SERVICE_NAME_PRE = "functionChannelService";

    private final Map<String, TriggerFunctionService> triggerFunctionServiceMap = new HashMap<>();

    public TriggerFunctionServiceProvider(List<TriggerFunctionService> triggerFunctionServiceList) {
        for (TriggerFunctionService triggerFunctionService : triggerFunctionServiceList) {
            triggerFunctionServiceMap.put(triggerFunctionService.getFunctionChannel(), triggerFunctionService);
        }
    }

    /**
     * 获取函数渠道处理器
     * @param channel 支付渠道
     * @return PayChannelService
     */
    public TriggerFunctionService getTriggerFunctionService(String channel) {
        return triggerFunctionServiceMap.get(channel);
    }

}
