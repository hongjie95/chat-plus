package com.chatplus.application.aiprocessor.function.impl;

import com.chatplus.application.aiprocessor.function.TriggerFunctionService;
import com.chatplus.application.aiprocessor.provider.TriggerFunctionServiceProvider;
import com.chatplus.application.common.domain.response.TurnRightResponse;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.domain.dto.extend.WeiBoHotDto;
import com.chatplus.application.domain.entity.functions.FunctionEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 微博热搜
 */
@Service(value = TriggerFunctionServiceProvider.SERVICE_NAME_PRE + WeiBoHotServiceImpl.WEIBO_HOT)
public class WeiBoHotServiceImpl extends TriggerFunctionService {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(WeiBoHotServiceImpl.class);

    private final ObjectMapper objectMapper;

    public WeiBoHotServiceImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public static final String WEIBO_HOT = "weibo";

    @Override
    public String executeReturnMarkdown(FunctionEntity functionEntity, String arguments) {
        String result = "";
        try {
            result = commonExecute(functionEntity, arguments);
            LOGGER.message("微博热搜结果").context("result", result).info();
            TurnRightResponse turnRightResponse = objectMapper.readValue(result, TurnRightResponse.class);
            if (Objects.equals(turnRightResponse.getCode(), TurnRightResponse.SUCCESS)
                    && Objects.nonNull(turnRightResponse.getData())) {
                CollectionType listType =
                        objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, WeiBoHotDto.class);
                List<WeiBoHotDto> weiBoHotDtoList =
                        objectMapper.readValue(objectMapper.writeValueAsString(turnRightResponse.getData()), listType);
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < weiBoHotDtoList.size(); i++) {
                    WeiBoHotDto weiBoHotDto = weiBoHotDtoList.get(i);
                    // 1. [包上恩](https://s.weibo.com/weibo?q=%23包上恩%23) [热度：1479727]
                    stringBuilder.append(i + 1).append("、 [").append(weiBoHotDto.getNote()).append("](")
                            .append(weiBoHotDto.getUrl()).append(") [热度：").append(weiBoHotDto.getNum()).append("]")
                            .append("<br/><br/>");
                }
                return stringBuilder.toString();
            }
        } catch (Exception e) {
            LOGGER.message("微博热搜结果报错").context("result", result).exception(e).error();
        }
        return "微博热搜结果异常，等待管理员处理";
    }

    @Override
    public String getFunctionChannel() {
        return WEIBO_HOT;
    }
}
