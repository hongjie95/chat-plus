package com.chatplus.application.aiprocessor.provider;

import com.chatplus.application.aiprocessor.platform.image.ImgAiProcessorService;
import com.chatplus.application.enumeration.AiPlatformEnum;
import org.springframework.stereotype.Service;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * AI图片渠道处理器提供者
 **/
@Service
public class ImgAiProcessorServiceProvider {
    public static final String SERVICE_NAME_PRE = "imgAiChannelService";

    private final Map<AiPlatformEnum, ImgAiProcessorService> aiChannelServiceMap = new EnumMap<>(AiPlatformEnum.class);

    public ImgAiProcessorServiceProvider(List<ImgAiProcessorService> chatAiProcessorServiceList) {
        for (ImgAiProcessorService aiChannelService : chatAiProcessorServiceList) {
            aiChannelServiceMap.put(aiChannelService.getChannel(), aiChannelService);
        }
    }

    /**
     * 获取AI渠道处理器
     *
     * @param channel 支付渠道
     * @return PayChannelService
     */
    public ImgAiProcessorService getAiProcessorService(AiPlatformEnum channel) {
        return aiChannelServiceMap.get(channel);
    }

}
