# 作者最近忙其他项目，本项目暂时不更新，大概6月份会恢复更新，有新需求或者问题请加群讨论
## 项目介绍
本项目是重构 [chatgpt-plus](https://gitee.com/blackfox/chatgpt-plus) 的后端Java重构版

## 体验地址

> 体验地址：https://ai.anshangba.com 体验账号：`18888888888/12345678`
>
> 文档地址：https://doc.anshangba.com/ 文档没那么全，等待后续补充
>
> 镜像仓库地址：`docker.io/chj9/chat-plus-java:latest`
>
> 本项目前端地址 https://gitee.com/hongjie95/chatgpt-plus 请使用 `chat-plus-java` 分支打包编译前端包，main分支保持和主仓库代码一致

执行以下命令打包前端项目

```
npm run build
```

并结合nginx把dist的文件目录放到前端服务器上即可

## 项目讨论

如有问题加群讨论
> 或者加作者微信: **tulingai88** 请备注：**chat-plus**

![img.png](https://cdn.anshangba.com/images/wxqun.jpg)

## 项目技术
- Springboot 3
- JDK 21
- Mybatis
- MySQL
- Redis
- Websocket
- ElasticSearch（已接入，默认关闭，未实际使用，后续看功能使用）
### 项目结构

接口文档地址：
http://127.0.0.1:8080/swagger-ui/index.html
```yaml
├─application-api # 业务数据模型模块
├─application-app # 业务接口Controller模块
├─application-dao # 业务DAO模块
├─application-openai-sdk # 各家的机器人SDK对接模块，统一入参出参
├─application-service # 业务服务层模块
│    └─aiprocessor # 核心模块，对接open ai sdk 的方式，以及发消息的处理
├─core # 核心模块
│  ├─common # 公共模块
│  ├─datasource # 数据源模块
│  ├─elasticsearch # es配置模块
│  ├─job # 定时任务模块
│  ├─file # 文件模块
│  ├─json # json序列化配置模块
│  └─web # web配置，和拦截器过滤器相关模块
└─tools # 代码生成工具模块
    └─code-generator
```
## 已完成功能
### 用户首页
- [x] 用户登录、注册、修改密码
- [x] 用户信息修改、用户邀请码功能
- [x] 图形码人机验证、短信验证码
- [x] ChatGPT机器人聊天 && 函数调用
- [x] 文心一言机器人聊天 && 函数调用
- [x] 讯飞星火机器人聊天 && 函数调用
- [x] ChatGLM机器人聊天
- [x] 阿里通义千问机器人聊天
- [x] Stable Diffusion绘画、Midjourney绘画
- [x] 邀请码注册
- [x] 新增敏感词库，对用户输入敏感词检测过滤
- [x] 接口审计日志
- [x] `支付宝`、`微信支付`原生支付
- [x] 订单下单
- [x] 用户扣费逻辑
- [x] 函数模块：函数DEMO，另外新增了：今日早报，微博热搜，dalle3画图
- [x] 系统配置中心: 短信、邮件、微信支付宝支付配置、百度翻译配置、微信公众号配置、审计日志配置、告警配置

### 管理员后台
- [x] 仪表盘
- [x] 用户管理
- [x] 角色管理
- [x] API-KEY
- [x] 语言模型
- [x] 充值产品
- [x] 充值订单
- [x] 函数管理
- [x] 系统设置
- [x] 用户登录日志
## OPEN AI SDK 架构

![img.png](doc/image/img.png)

详细介绍
https://bugstack.cn/md/zsxq/project/openai-sdk-java.html
我这边代码复制过来，做了大量的修改和兼容，可能和原项目有所不同

## 注意事项
1. 本项目使用的是`Java 21`，如果你的Java版本不是21，请自行兼容修改
2. 本项目使用的是`Gradle 8`构建，如果你不会使用`Gradle`，请自行学习
3. 本项目修改和新增了`原项目的数据库表名，和部分字段名和类型`，如果你想使用原项目的数据库，请自行修改
## 本地开发
1. 克隆本项目到本地
    ```angular2html
    git clone https://gitee.com/hongjie95/chat-plus.git
    ```
2. 使用IDEA打开本项目
3. 执行SQL文件`/doc/chat-plus.sql`，创建数据库和表,初始化数据，管理员账户：`18888888888/12345678`
4. 修改`application.yml`中的`spring.profiles`
    ```yaml
    spring:
      profiles: 
        group:
          default:
            - dev
          local:
            - local
          prod:
            - prod
          production:
            - production
        active: default
    ```
   根据选择的环境，修改`spring.profiles.active`的值，如`dev`、`local`、`prod`、`production`
5. 修改`application-dev.yml`中的数据库配置,Redis配置
## 部署方式

> 相关文件都在`doc`文件夹下面了
> fonts包主要是中文字体，生成图片验证码的时候用到
1. 打包jar包
   > 如果本地没有环境，推荐切个release分支，然后开通Gitee的ci流水线，ci脚本在`.workflow/release-pipeline.yml`目录下,
   > 代码提交了会自动编译打jar包，下载制品即可

   本地打包命令
    ```angular2html
    ./gradlew --build-cache clean -Dorg.gradle.console=plain :application-app:bootJar -PbootJarArchiveId=app
    ```
   jar包打包完成后，会在`application-app/build/libs`目录下生成`app.jar`文件
2. 创建Dockerfile文件,这里我打包的配置文件后缀为`prod`,所以线上我的配置文件为`application-prod.yml`
3. 构建Docker镜像
    ```angular2html
    docker build -t chj9/chat-plus-java:latest .
    ```
4. 创建docker-compose.yml文件
5. 执行数据库脚本，数据库脚本在doc文件夹下，默认数据库名为`chat-plus`，如果你的数据库名不是`chat-plus`，请自行修改
   默认管理员账号：`15302789406/123456`，由表`t_user`的字段`admin`控制，1为管理员，0为普通用户

6. 部署XXL-JOB
   以下是我的部署方法，如果你已经部署有了请忽略此步骤
   ```angular2html
   version: "3"
   services:
     xxl_job:
       image: xuxueli/xxl-job-admin:2.4.0
       container_name: xxl-job
       restart: on-failure:3
       volumes:
         - /etc/localtime:/etc/localtime:ro
       ports:
         - "8099:8080"
       environment:
         - PARAMS=--spring.datasource.url=jdbc:mysql://127.0.0.1:3306/xxl_job?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=Asia/Shanghai --spring.datasource.username=root --spring.datasource.password=xxxx --xxl.job.accessToken=xxxx
         - JAVA_TOOL_OPTIONS=-XX:InitialRAMPercentage=40.0 -XX:MinRAMPercentage=20.0 -XX:MaxRAMPercentage=80.0
       deploy:
        resources:
           limits:
              memory: 1024m
           reservations:
              memory: 512m
   ```
   注意修改数据库地址和`--xxl.job.accessToken`的值
7. 配置XXL-JOB定时任务
    1. 登录XXL-JOB后台
    2. 配置执行器![img_1.png](doc/image/img.png) ![img_1.png](doc/image/img_1.png)
    4. 配置任务执行器![img_1.png](doc/image/img_2.png)
    5. 配置任务调度（目前暂时只有三个定时任务）
    ```
    任务描述: 用户订阅定时任务	
    调度周期: 0 0/2 * * * ?
    运行模式：UserSubsTaskJob#subsJob
    ```
    ```
    任务描述: 支付结果定时任务		
    调度周期: 0 0/1 * * * ?
    运行模式：PayTaskJob#payResultJob
    ```
    ```
    任务描述: 订单结果定时任务		
    调度周期: 0 0/1 * * * ?
    运行模式：OrderTaskJob#orderResultJob
    ```
    6. 分别每个点击执行一次，应用日志正常会打印以下日志![img_1.png](doc/image/img_3.png)
## 使用须知
本项目基于 MIT 协议，免费开放全部源代码，可以作为个人学习使用或者商用。
如需商用必须保留版权信息，请自觉遵守。确保合法合规使用，在运营过程中产生的一切任何后果自负，与作者无关。