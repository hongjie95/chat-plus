package com.chatplus.application.dao.basedata;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.basedata.MenuEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 前端菜单表数据访问层
 *
 * <p>Table: t_menu - 前端菜单表</p>
 *
 * @author developer
 * @see MenuEntity
 */
@Mapper
@Repository
public interface MenuDao extends BaseMapper<MenuEntity> {

}
