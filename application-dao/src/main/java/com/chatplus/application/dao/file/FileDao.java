package com.chatplus.application.dao.file;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.file.FileEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * OSS对象存储表数据访问层
 *
 * <p>Table: t_file - OSS对象存储表</p>
 *
 * @author developer
 * @see FileEntity
 */
@Mapper
@Repository
public interface FileDao extends BaseMapper<FileEntity> {

}
