package com.chatplus.application.dao.account;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.account.InviteCodeEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 用户邀请码数据访问层
 *
 * <p>Table: t_invite_code - 用户邀请码</p>
 *
 * @author developer
 * @see InviteCodeEntity
 */
@Mapper
@Repository
public interface InviteCodeDao extends BaseMapper<InviteCodeEntity> {

}
