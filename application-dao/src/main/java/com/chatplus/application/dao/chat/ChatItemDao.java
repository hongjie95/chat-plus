package com.chatplus.application.dao.chat;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.chat.ChatItemEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 用户会话列表数据访问层
 *
 * <p>Table: t_chat_item - 用户会话列表</p>
 *
 * @author developer
 * @see ChatItemEntity
 */
@Mapper
@Repository
public interface ChatItemDao extends BaseMapper<ChatItemEntity> {

}
