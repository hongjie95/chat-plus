package com.chatplus.application.dao.basedata;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.basedata.PowerLogsEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 用户算力消费日志数据访问层
 *
 * <p>Table: t_power_logs - 用户算力消费日志</p>
 *
 * @author developer
 * @see PowerLogsEntity
 */
@Mapper
@Repository
public interface PowerLogsDao extends BaseMapper<PowerLogsEntity> {

}
