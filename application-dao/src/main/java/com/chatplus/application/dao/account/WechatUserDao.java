package com.chatplus.application.dao.account;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.account.WechatUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 微信小程序用户数据访问层
 *
 * <p>Table: t_wechat_user - 微信小程序用户</p>
 *
 * @author developer
 * @see WechatUserEntity
 */
@Mapper
@Repository
public interface WechatUserDao extends BaseMapper<WechatUserEntity> {

}
