package com.chatplus.application.dao.orders;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.orders.RewardEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 用户打赏数据访问层
 *
 * <p>Table: t_reward - 用户打赏</p>
 *
 * @author developer
 * @see RewardEntity
 */
@Mapper
@Repository
public interface RewardDao extends BaseMapper<RewardEntity> {

}
