package com.chatplus.application.dao.pay;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.pay.RefundRequestEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * 退款请求表数据访问层
 *
 * <p>Table: t_refund_request - 退款请求表</p>
 *
 * @author developer
 * @see RefundRequestEntity
 */
@Mapper
@Repository
public interface RefundRequestDao extends BaseMapper<RefundRequestEntity> {

    @Select("select ifnull(sum(refund_money),0) from t_refund_request where pay_request_id=#{payRequestId} and refund_status in (0,1)")
    long sumRefundedMoney(@Param("payRequestId") long payRequestId);

}
