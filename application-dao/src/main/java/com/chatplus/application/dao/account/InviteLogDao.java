package com.chatplus.application.dao.account;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.account.InviteLogEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 邀请注册日志数据访问层
 *
 * <p>Table: t_invite_log - 邀请注册日志</p>
 *
 * @author developer
 * @see InviteLogEntity
 */
@Mapper
@Repository
public interface InviteLogDao extends BaseMapper<InviteLogEntity> {

}
