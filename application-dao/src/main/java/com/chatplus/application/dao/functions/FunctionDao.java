package com.chatplus.application.dao.functions;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.functions.FunctionEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 函数插件表数据访问层
 *
 * <p>Table: t_function - 函数插件表</p>
 *
 * @author developer
 * @see FunctionEntity
 */
@Mapper
@Repository
public interface FunctionDao extends BaseMapper<FunctionEntity> {

}
