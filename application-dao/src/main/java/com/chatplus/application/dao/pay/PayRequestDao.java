package com.chatplus.application.dao.pay;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.pay.PayRequestEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 支付请求记录（账单）数据访问层
 *
 * <p>Table: t_pay_request - 支付请求记录（账单）</p>
 *
 * @author developer
 * @see PayRequestEntity
 */
@Mapper
@Repository
public interface PayRequestDao extends BaseMapper<PayRequestEntity> {

}
