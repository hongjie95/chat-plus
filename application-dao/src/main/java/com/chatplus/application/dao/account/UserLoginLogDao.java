package com.chatplus.application.dao.account;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.account.UserLoginLogEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 用户登录日志数据访问层
 *
 * <p>Table: t_user_login_log - 用户登录日志</p>
 *
 * @author developer
 * @see UserLoginLogEntity
 */
@Mapper
@Repository
public interface UserLoginLogDao extends BaseMapper<UserLoginLogEntity> {

}
