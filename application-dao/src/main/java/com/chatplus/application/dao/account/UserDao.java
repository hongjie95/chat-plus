package com.chatplus.application.dao.account;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.account.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 用户表数据访问层
 *
 * <p>Table: t_users - 用户表</p>
 *
 * @author developer
 * @see UserEntity
 */
@Mapper
@Repository
public interface UserDao extends BaseMapper<UserEntity> {

}
