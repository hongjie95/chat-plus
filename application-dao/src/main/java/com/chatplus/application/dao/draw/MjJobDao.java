package com.chatplus.application.dao.draw;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.draw.MjJobEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * MidJourney 任务表数据访问层
 *
 * <p>Table: t_mj_job - MidJourney 任务表</p>
 *
 * @author developer
 * @see MjJobEntity
 */
@Mapper
@Repository
public interface MjJobDao extends BaseMapper<MjJobEntity> {

}
