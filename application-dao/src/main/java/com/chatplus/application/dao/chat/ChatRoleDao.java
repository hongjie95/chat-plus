package com.chatplus.application.dao.chat;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.chat.ChatRoleEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 聊天角色表数据访问层
 *
 * <p>Table: t_chat_role - 聊天角色表</p>
 *
 * @author developer
 * @see ChatRoleEntity
 */
@Mapper
@Repository
public interface ChatRoleDao extends BaseMapper<ChatRoleEntity> {

}
