package com.chatplus.application.dao.chat;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.chat.ChatModelEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * AI 模型表数据访问层
 *
 * <p>Table: t_chat_model - AI 模型表</p>
 *
 * @author developer
 * @see ChatModelEntity
 */
@Mapper
@Repository
public interface ChatModelDao extends BaseMapper<ChatModelEntity> {

}
