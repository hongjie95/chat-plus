package com.chatplus.application.dao.orders;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.orders.OrderEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 充值订单表数据访问层
 *
 * <p>Table: t_order - 充值订单表</p>
 *
 * @author developer
 * @see OrderEntity
 */
@Mapper
@Repository
public interface OrderDao extends BaseMapper<OrderEntity> {

}
