package com.chatplus.application.dao.framework;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.framework.AuditLogRuleEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 审计日志规则表数据访问层
 *
 * <p>Table: t_audit_log_rule - 审计日志规则表</p>
 *
 * @author developer
 * @see AuditLogRuleEntity
 */
@Mapper
@Repository
public interface AuditLogRuleDao extends BaseMapper<AuditLogRuleEntity> {

}
