package com.chatplus.application.dao.basedata;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.basedata.SensitiveWordEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 敏感词表数据访问层
 *
 * <p>Table: t_sensitive_word - 敏感词表</p>
 *
 * @author developer
 * @see SensitiveWordEntity
 */
@Mapper
@Repository
public interface SensitiveWordDao extends BaseMapper<SensitiveWordEntity> {

}
