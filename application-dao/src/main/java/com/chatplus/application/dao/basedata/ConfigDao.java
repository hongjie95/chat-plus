package com.chatplus.application.dao.basedata;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.basedata.ConfigEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 第三方AI配置表数据访问层
 *
 * <p>Table: t_config - 第三方AI配置表</p>
 *
 * @author developer
 * @see ConfigEntity
 */
@Mapper
@Repository
public interface ConfigDao extends BaseMapper<ConfigEntity> {

}
