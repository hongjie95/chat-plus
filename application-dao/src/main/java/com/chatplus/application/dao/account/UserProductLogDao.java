package com.chatplus.application.dao.account;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.account.UserProductLogEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 用户套餐记录数据访问层
 *
 * <p>Table: t_user_product_log - 用户套餐记录</p>
 *
 * @author developer
 * @see UserProductLogEntity
 */
@Mapper
@Repository
public interface UserProductLogDao extends BaseMapper<UserProductLogEntity> {

}
