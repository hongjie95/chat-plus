package com.chatplus.application.dao.chat;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.chat.ChatHistoryEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 聊天历史记录数据访问层
 *
 * <p>Table: t_chat_history - 聊天历史记录</p>
 *
 * @author developer
 * @see ChatHistoryEntity
 */
@Mapper
@Repository
public interface ChatHistoryDao extends BaseMapper<ChatHistoryEntity> {

}
