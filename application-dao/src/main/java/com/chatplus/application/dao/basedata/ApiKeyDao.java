package com.chatplus.application.dao.basedata;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.basedata.ApiKeyEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * OpenAI API 数据访问层
 *
 * <p>Table: t_api_key - OpenAI API </p>
 *
 * @author developer
 * @see ApiKeyEntity
 */
@Mapper
@Repository
public interface ApiKeyDao extends BaseMapper<ApiKeyEntity> {

}
