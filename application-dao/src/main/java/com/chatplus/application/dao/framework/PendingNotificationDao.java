package com.chatplus.application.dao.framework;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.framework.PendingNotificationEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 待执行队列通知数据访问层
 *
 * <p>Table: t_pending_notification - 待执行队列通知</p>
 *
 * @author developer
 * @see PendingNotificationEntity
 */
@Mapper
@Repository
public interface PendingNotificationDao extends BaseMapper<PendingNotificationEntity> {

}
