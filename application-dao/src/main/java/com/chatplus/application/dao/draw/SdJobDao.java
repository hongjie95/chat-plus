package com.chatplus.application.dao.draw;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.draw.SdJobEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * Stable Diffusion 任务表数据访问层
 *
 * <p>Table: t_sd_job - Stable Diffusion 任务表</p>
 *
 * @author developer
 * @see SdJobEntity
 */
@Mapper
@Repository
public interface SdJobDao extends BaseMapper<SdJobEntity> {

}
