package com.chatplus.application.dao.basedata;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chatplus.application.domain.entity.basedata.ProductEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 会员套餐表数据访问层
 *
 * <p>Table: t_product - 会员套餐表</p>
 *
 * @author developer
 * @see ProductEntity
 */
@Mapper
@Repository
public interface ProductDao extends BaseMapper<ProductEntity> {

}
