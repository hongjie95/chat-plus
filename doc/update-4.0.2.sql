ALTER TABLE t_api_key
    ADD COLUMN `channel` VARCHAR(255) DEFAULT '' AFTER platform;
UPDATE t_api_key
SET `channel` = `platform`;

ALTER TABLE t_chat_model
    ADD COLUMN `channel` VARCHAR(255) DEFAULT '' AFTER platform;
UPDATE t_chat_model
SET `channel` = `platform`;

CREATE TABLE `t_menu`
(
    `id`             bigint                                                 NOT NULL AUTO_INCREMENT,
    `name`           varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '菜单名称',
    `icon`           varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单图标',
    `url`            varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '地址',
    `sort_num`       smallint                                               NOT NULL COMMENT '排序',
    `enabled`        tinyint(1) NOT NULL COMMENT '是否启用',
    `created_at`     datetime NULL DEFAULT NULL,
    `updated_at`     datetime NULL DEFAULT NULL,
    `scheme_version` int NULL DEFAULT NULL COMMENT '数据版本号',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '前端菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu`
VALUES (1, '对话聊天', '/images/menu/chat.png', '/chat', 0, 1, '2024-04-13 19:49:30', '2024-04-13 19:49:33', 1);
INSERT INTO `t_menu`
VALUES (5, 'MJ 绘画', '/images/menu/mj.png', '/mj', 1, 1, '2024-04-13 19:50:13', '2024-04-13 19:50:11', 1);
INSERT INTO `t_menu`
VALUES (6, 'SD 绘画', '/images/menu/sd.png', '/sd', 2, 1, '2024-04-13 19:50:07', '2024-04-13 19:50:09', 1);
INSERT INTO `t_menu`
VALUES (7, '算力日志', '/images/menu/log.png', '/powerLog', 5, 1, '2024-04-13 19:50:04', '2024-04-13 19:50:03', 1);
INSERT INTO `t_menu`
VALUES (8, '应用中心', '/images/menu/app.png', '/apps', 3, 1, '2024-04-13 19:49:58', '2024-04-13 19:50:00', 1);
INSERT INTO `t_menu`
VALUES (9, '作品展示', '/images/menu/img-wall.png', '/images-wall', 4, 1, '2024-04-13 19:49:56', '2024-04-13 19:49:53',
        1);
INSERT INTO `t_menu`
VALUES (10, '会员计划', '/images/menu/member.png', '/member', 6, 1, '2024-04-13 19:49:48', '2024-04-13 19:49:50', 1);
INSERT INTO `t_menu`
VALUES (11, '分享计划', '/images/menu/share.png', '/invite', 7, 1, '2024-04-13 19:49:44', '2024-04-13 19:49:41', 1);