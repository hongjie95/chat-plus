-- 添加新列 `power`
ALTER TABLE `t_user_product_log`
    ADD COLUMN `power` int NOT NULL DEFAULT 0 COMMENT '算力' AFTER `product_type`;

-- 删除旧列 `chat_calls` 和 `img_calls`
ALTER TABLE `t_user_product_log` DROP COLUMN `chat_calls`, DROP COLUMN `img_calls`;

-- 重命名weight为 `power`
ALTER TABLE t_chat_model CHANGE COLUMN weight power INT;

-- 添加新列 `max_tokens`
ALTER TABLE `t_chat_model`
    ADD COLUMN `max_tokens` int NULL COMMENT '最大响应长度' AFTER `scheme_version`;

-- 添加新列 `max_context`
ALTER TABLE `t_chat_model`
    ADD COLUMN `max_context` int NULL COMMENT '最大上下文长度' AFTER `max_tokens`;

-- 添加新列 `temperature`
ALTER TABLE `t_chat_model`
    ADD COLUMN `temperature` float NULL COMMENT '创意度' AFTER `max_context`;

-- 添加新列 `enabled_function`
ALTER TABLE `t_chat_model`
    ADD COLUMN `enabled_function` tinyint(1) NULL COMMENT '是否启用函数' AFTER `temperature`;

-- 添加新列 `user_id`
ALTER TABLE `t_file`
    ADD COLUMN `user_id` bigint NULL DEFAULT NULL AFTER `file_path`;

-- 添加新列 `power` 和 `vip_day_power`
ALTER TABLE `t_product`
    ADD COLUMN `power` int NOT NULL DEFAULT 0 AFTER `days`,
ADD COLUMN `vip_day_power` int NULL DEFAULT NULL AFTER `power`;
-- 删除旧列 `chat_calls` 和 `img_calls`
ALTER TABLE `t_product` DROP COLUMN `chat_calls`, DROP COLUMN `img_calls`;

ALTER TABLE `t_user`
    ADD COLUMN `mj_power` int NULL DEFAULT NULL COMMENT '给用户自定义的算力' AFTER `scheme_version`,
ADD COLUMN `sd_power` int NULL DEFAULT NULL COMMENT '给用户自定义的算力' AFTER `mj_power`,
ADD COLUMN `dall_power` int NULL DEFAULT NULL COMMENT '给用户自定义的算力' AFTER `sd_power`;


CREATE TABLE `t_power_logs`
(
    `id`             bigint                                                 NOT NULL AUTO_INCREMENT,
    `platform`       varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '平台',
    `user_id`        bigint                                                 NOT NULL COMMENT '用户ID',
    `username`       varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin  NOT NULL COMMENT '用户名',
    `type`           int                                                    NOT NULL COMMENT '类型（1：充值，2：消费，3：退费）',
    `amount`         int NULL DEFAULT NULL COMMENT '算力数值',
    `balance`        int                                                    NOT NULL COMMENT '余额',
    `model`          varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '模型',
    `remark`         varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '备注',
    `mark`           int                                                    NOT NULL COMMENT '资金类型（0：支出，1：收入）',
    `created_at`     datetime                                               NOT NULL COMMENT '创建时间',
    `updated_at`     datetime NULL DEFAULT NULL,
    `scheme_version` int NULL DEFAULT NULL COMMENT '数据版本号',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1774424111959134211 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户算力消费日志' ROW_FORMAT = Dynamic;


CREATE TABLE `t_wechat_user`
(
    `id`             bigint                                                 NOT NULL COMMENT '主键',
    `app_id`         varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
    `user_id`        bigint NULL DEFAULT NULL COMMENT '绑定的用户ID',
    `open_id`        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '微信openid',
    `nick_name`      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '微信昵称',
    `avatar_url`     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '微信头像',
    `status`         varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '状态',
    `created_at`     datetime                                               NOT NULL COMMENT '创建时间',
    `updated_at`     datetime                                               NOT NULL COMMENT '更新时间',
    `scheme_version` int NULL DEFAULT NULL COMMENT '数据版本号',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '微信用户' ROW_FORMAT = Dynamic;


