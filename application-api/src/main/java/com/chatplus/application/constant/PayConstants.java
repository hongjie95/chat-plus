package com.chatplus.application.constant;

/**
 * 支付模块常用常量：无特殊情况不允许更改
 */
public class PayConstants {
    /**
     * 未付款
     */
    public static final Integer UNPAID = 0;

    /**
     * 已付款
     */
    public static final Integer PAID = 1;

    /**
     * 已取消
     */
    public static final Integer CANCELLED = 2;

    /**
     * 未消费
     */
    public static final Integer NOT_CONSUMED = 3;

    /**
     * 已消费
     */
    public static final Integer CONSUMED = 4;

    private PayConstants() {
    }

    /**
     * 微信相关的支付状态
     */
    public static class Wechat {
        /**
         * 支付的订单不存在
         */
        public static final String ORDER_NOT_EXIST = "ORDER_NOT_EXIST";
        public static final String ORDERNOTEXIST = "ORDERNOTEXIST";
        /**
         * 退款单查询失败
         */
        public static final String RESOURCE_NOT_EXISTS = "RESOURCE_NOT_EXISTS";
        /**
         * 通知类型为退款
         */
        public static final String EVENT_TYPE_REFUND = "REFUND";
        /**
         * 通知类型为交易
         */
        public static final String EVENT_TYPE_TRANSACTION = "TRANSACTION";

        /**
         * SUCCESS—支付成功
         * 交易状态，枚举值：
         * SUCCESS：支付成功
         * REFUND：转入退款
         * NOTPAY：未支付
         * CLOSED：已关闭
         * REVOKED：已撤销（付款码支付）
         * USERPAYING：用户支付中（付款码支付）
         * PAYERROR：支付失败(其他原因，如银行返回失败)
         */
        public static final String TRADE_SUCCESS = "SUCCESS";
        /**
         * REFUND—转入退款
         */
        public static final String TRADE_REFUND = "REFUND";
        /**
         * NOTPAY—未支付
         */
        public static final String TRADE_NOTPAY = "NOTPAY";

        /**
         * CLOSED—已关闭
         */
        public static final String TRADE_CLOSED = "CLOSED";

        /**
         * PAYERROR--支付失败(其他原因，如银行返回失败)
         */
        public static final String TRADE_PAYERROR = "PAYERROR";


        //---------------------退款类型--------------------------
        /**
         * 退款关闭
         */
        public static final String REFUND_REFUNDCLOSE = "REFUNDCLOSE";
        /**
         * 退款处理中
         */
        public static final String REFUND_PROCESSING = "PROCESSING";
        /**
         * 退款异常，退款到银行发现用户的卡作废或者冻结了，导致原路退款银行卡失败，可前往商户平台（pay.weixin.qq.com）-交易中心，手动处理此笔退款。
         */
        public static final String REFUND_ABNORMAL = "ABNORMAL";

    }

    public static class Alipay {


        public static final String Success = "Success";
        /**
         * 交易创建，等待买家付款
         */
        public static final String WAIT_BUYER_PAY = "WAIT_BUYER_PAY";

        /**
         * 未付款交易超时关闭，或支付完成后全额退款
         */
        public static final String TRADE_CLOSED = "TRADE_CLOSED";

        /**
         * 交易支付成功
         */
        public static final String TRADE_SUCCESS = "TRADE_SUCCESS";

        /**
         * 交易结束，不可退款
         */
        public static final String TRADE_FINISHED = "TRADE_FINISHED";

        //-------------------------退款类型--------------------------
        /**
         * 退款成功
         */
        public static final String REFUND_SUCCESS = "REFUND_SUCCESS";
        /**
         * 查询退款的交易不存在
         */
        public static final String TRADE_NOT_EXIST = "TRADE_NOT_EXIST";
        /**
         * 支付宝业务处理失败
         */
        public static final String BUSINESS_FAIL_CODE = "40004";

        /**
         * 支付宝查询的交易不存在
         */
        public static final String ACQ_TRADE_NOT_EXIST = "ACQ.TRADE_NOT_EXIST";
        public static final String NOTIFY_SUCCESS = "success";
        public static final String NOTIFY_FAILURE = "failure";
        public static final String FUND_CHANGE_YES = "Y";
    }
}
