package com.chatplus.application.constant;

/**
 * @author Administrator
 */
public class Constants {

    private Constants() {
    }

    /**
     * 模型版本号加1通用语句
     */
    public static final String SCHEME_VERSION_EQ_SCHEME_VERSION_ADD_1 = "scheme_version = scheme_version + 1";
    public static final String CLIENT_ID_HEADER = "X-CLIENT-ID";

    public static final String CLIENT_APP_VERSION = "X-APP-VERSION";

    public static final String SUCCESS = "Success";

    /**
     * 三方认证 redis key
     */
    public static final String SOCIAL_AUTH_CODE_KEY = "global:social_auth_codes:";


}
