package com.chatplus.application.event.pay.dto;

import lombok.Data;

import java.io.Serializable;
@Data
public class RefundRequestEvent implements Serializable {

    private Long orderId;
}
