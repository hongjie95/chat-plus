package com.chatplus.application.event.pay;

import com.chatplus.application.event.GenericEvent;
import com.chatplus.application.event.pay.dto.RefundRequestEvent;

public class RefundFailEvent extends GenericEvent<RefundRequestEvent> {
    public RefundFailEvent(String appBusinessCode, RefundRequestEvent data) {
        super(appBusinessCode, data);
    }
}
