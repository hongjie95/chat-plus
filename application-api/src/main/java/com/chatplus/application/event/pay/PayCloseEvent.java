package com.chatplus.application.event.pay;

import com.chatplus.application.event.GenericEvent;
import com.chatplus.application.event.pay.dto.PayRequestEvent;

public class PayCloseEvent extends GenericEvent<PayRequestEvent> {
    public PayCloseEvent(String appBusinessCode, PayRequestEvent data) {
        super(appBusinessCode, data);
    }
    private String closedDesc;
    public String getClosedDesc() {
        return closedDesc;
    }
    public void setClosedDesc(String closedDesc) {
        this.closedDesc = closedDesc;
    }
}
