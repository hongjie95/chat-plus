package com.chatplus.application.event.pay;

import com.chatplus.application.event.GenericEvent;
import com.chatplus.application.event.pay.dto.RefundRequestEvent;

public class RefundSuccessEvent extends GenericEvent<RefundRequestEvent> {
    public RefundSuccessEvent(String appBusinessCode, RefundRequestEvent data) {
        super(appBusinessCode, data);
    }
}
