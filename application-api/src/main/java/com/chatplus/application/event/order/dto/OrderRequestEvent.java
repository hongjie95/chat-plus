package com.chatplus.application.event.order.dto;

import com.chatplus.application.enumeration.OrderStatusEnum;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.Instant;

/**
 * 订单请求事件
 *
 * @author chj
 * @date 2024/2/4
 **/
@Data
@Builder
public class OrderRequestEvent implements Serializable {
    /**
     * 订单id（必传）
     */
    private Long orderId;
    /**
     * 渠道交易ID;第三方支付渠道的流水号 （选传）
     */
    private String tradeTransactionId;
    /**
     * 成功时间 （选传）
     */
    private Instant successAt;

    private OrderStatusEnum status;
}
