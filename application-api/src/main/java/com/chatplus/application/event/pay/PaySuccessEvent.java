package com.chatplus.application.event.pay;

import com.chatplus.application.event.GenericEvent;
import com.chatplus.application.event.pay.dto.PayRequestEvent;

public class PaySuccessEvent extends GenericEvent<PayRequestEvent> {
    public PaySuccessEvent(String appBusinessCode, PayRequestEvent data) {
        super(appBusinessCode, data);
    }
}
