package com.chatplus.application.event.order;

import com.chatplus.application.event.GenericEvent;
import com.chatplus.application.event.order.dto.OrderRequestEvent;

/**
 * 订单更新时间
 */
public class OrderUpdateEvent extends GenericEvent<OrderRequestEvent> {
    public OrderUpdateEvent(OrderRequestEvent data) {
        super(null, data);
    }
}
