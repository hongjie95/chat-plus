package com.chatplus.application.event.pay;

import com.chatplus.application.event.GenericEvent;
import com.chatplus.application.event.pay.dto.PayRequestEvent;

public class PayFailEvent extends GenericEvent<PayRequestEvent> {
    public PayFailEvent(String appBusinessCode, PayRequestEvent data) {
        super(appBusinessCode, data);
    }
}
