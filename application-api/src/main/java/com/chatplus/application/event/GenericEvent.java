package com.chatplus.application.event;


import lombok.Data;

import java.io.Serializable;

@Data
public class GenericEvent<T> implements Serializable {
    /**
     * 业务类型
     */
    private String bizCode;
    private T data;

    public GenericEvent(String bizCode, T data) {
        this.bizCode = bizCode;
        this.data = data;
    }

}
