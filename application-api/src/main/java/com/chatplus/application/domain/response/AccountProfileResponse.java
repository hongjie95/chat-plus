package com.chatplus.application.domain.response;

import com.chatplus.application.domain.dto.UserCustomChatConfigDto;
import com.chatplus.application.json.Int64AsString;
import com.chatplus.application.json.PhoneJsonSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * 账户信息响应
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "账户信息响应")
public class AccountProfileResponse implements Serializable {

    @JsonProperty("id")
    @Schema(description = "账号Id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;

    @JsonProperty("nickname")
    @Schema(description = "昵称")
    private String nickname;

    @JsonProperty("username")
    @JsonSerialize(using = PhoneJsonSerializer.class)
    @Schema(description = "登录账号")
    private String username;

    @JsonProperty("avatar")
    @Schema(description = "头像")
    private String avatar;

    @JsonProperty("chat_config")
    @Schema(description = "自定义的聊天配置")
    private UserCustomChatConfigDto chatConfig;

    @JsonProperty("power")
    @Schema(description = "算力余额")
    private Integer power;

    @JsonProperty("total_token")
    @JsonSerialize(using = Int64AsString.class)
    @Schema(description = "总消耗算力")
    private Long totalToken;

    @JsonProperty("token")
    @JsonSerialize(using = Int64AsString.class)
    @Schema(description = "当月消耗算力")
    private Long token;

    @JsonProperty("expired_time")
    @JsonSerialize(using = Int64AsString.class)
    @Schema(description = "VIP过期时间")
    private Long expiredTime;

    @JsonProperty("vip")
    @Schema(description = "是否是VIP")
    private Boolean vip;

}
