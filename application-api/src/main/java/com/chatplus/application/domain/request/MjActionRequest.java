package com.chatplus.application.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * MJ放大重绘
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Schema(description = "MJ放大重绘")
public class MjActionRequest implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    @NotNull(message = "index不能为空")
    @Schema(description = "index")
    private Integer index;

    @JsonProperty("task_id")
    @NotEmpty(message = "message_id不能为空")
    @Schema(description = "任务ID")
    private String taskId;

    @Schema(description = "prompt")
    private String prompt;

    @JsonProperty("customId")
    @Schema(description = "customId")
    private String customId;

}
