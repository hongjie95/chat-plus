package com.chatplus.application.domain.request;

import com.chatplus.application.enumeration.PayChannelEnum;
import com.chatplus.application.enumeration.SubModeEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 *  支付请求
 *
 * @author chj
 * @date 2024/1/15
 **/
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "支付请求")
public class QrCodePayRequest implements Serializable {
    /**
     * pay_way : alipay
     * product_id : 1
     * user_id : 100
     */
    @JsonProperty("pay_way")
    @Schema(description = "支付方式")
    private PayChannelEnum payWay;

    @JsonProperty("product_id")
    @Schema(description = "产品ID")
    private Long productId;

    @JsonProperty("user_id")
    @Schema(description = "用户ID")
    private Long userId;

    @JsonProperty("sub_mode")
    @Schema(description = "子支付方式")
    private SubModeEnum subMode;
}
