package com.chatplus.application.domain.entity.basedata;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.id.IdEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;


/**
 * <p>Table: t_menu - 前端菜单表</p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Alias("menu")
@TableName(value = "t_menu", autoResultMap = true)
public class MenuEntity extends IdEntity {

    /**
     * 菜单名称
     */
    private String name;
    /**
     * 菜单图标
     */
    private String icon;
    /**
     * 地址
     */
    private String url;
    /**
     * 排序
     */
    private Integer sortNum;
    /**
     * 是否启用
     */
    private Boolean enabled;

}
