package com.chatplus.application.domain.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.io.Serializable;

/**
 * 人机验证请求
 */
@Schema(description = "人机验证请求")
@Data
public class BotCheckCaptchaRequest implements Serializable {

    @NotEmpty(message = "请进行人机验证再操作")
    @Schema(description = "人机验证的坐标")
    private String dots;

    @NotEmpty(message = "参数错误")
    @Schema(description = "人机验证的唯一key")
    private String key;

}
