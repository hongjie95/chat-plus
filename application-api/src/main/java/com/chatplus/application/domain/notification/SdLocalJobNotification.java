package com.chatplus.application.domain.notification;

import com.chatplus.application.domain.request.SdLocalImageRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 绘画任务JOB
 **/
@EqualsAndHashCode(callSuper = true)
@Data
public class SdLocalJobNotification extends SdJobNotification implements Serializable {

    private SdLocalImageRequest request;

}
