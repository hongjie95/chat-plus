package com.chatplus.application.domain.entity.pay;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.id.LogicDeleteEntity;
import com.chatplus.application.enumeration.RefundStatusEnum;

import java.time.Instant;
/**
 *
 * <p>Table: t_refund_request - 退款请求对象 </p>
 */
@TableName("t_refund_request")
public class RefundRequestEntity extends LogicDeleteEntity {

    private static final long serialVersionUID=1L;
    /**
     * 应用ID;
     */
    private String appId;
    /**
     * 业务系统的用户ID;例如商家ID、师傅ID
     */
    private Long minUserId;

    private Long payRequestId;
    /**
     * 第三方交易ID;微信/支付宝生成的支付流水号
     */
    private String tradeTransactionId;
    /**
     * 提款申请金额
     */
    private Long refundMoney;
    /**
     * 退款原因
     */
    private String refundDesc;
    /**
     * 申请退款时间
     */
    private Instant applyAt;
    /**
     * 退款请求流水号;收银台生成的退款流水号
     */
    private String refundApplyNo;
    /**
     * 退款请求返回的流水号;微信/支付宝返回的流水号
     */
    private String refundResponseNo;
    /**
     * 成功时间
     */
    private Instant successAt;
    /**
     * 充值退款状态
     */
    private RefundStatusEnum refundStatus;
    /**
     * 平台在渠道系统中的交易账号;公司的支付宝、微信的账户
     */
    private String platformChannelAccount;
    /**
     * 错误码
     */
    private String errCode;
    /**
     * 错误描述
     */
    private String errCodeDes;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public Long getMinUserId() {
        return minUserId;
    }

    public void setMinUserId(Long minUserId) {
        this.minUserId = minUserId;
    }

    public Long getPayRequestId() {
        return payRequestId;
    }

    public void setPayRequestId(Long payRequestId) {
        this.payRequestId = payRequestId;
    }

    public String getTradeTransactionId() {
        return tradeTransactionId;
    }

    public void setTradeTransactionId(String tradeTransactionId) {
        this.tradeTransactionId = tradeTransactionId;
    }

    public Long getRefundMoney() {
        return refundMoney;
    }

    public void setRefundMoney(Long refundMoney) {
        this.refundMoney = refundMoney;
    }

    public String getRefundDesc() {
        return refundDesc;
    }

    public void setRefundDesc(String refundDesc) {
        this.refundDesc = refundDesc;
    }

    public Instant getApplyAt() {
        return applyAt;
    }

    public void setApplyAt(Instant applyAt) {
        this.applyAt = applyAt;
    }

    public String getRefundApplyNo() {
        return refundApplyNo;
    }

    public void setRefundApplyNo(String refundApplyNo) {
        this.refundApplyNo = refundApplyNo;
    }

    public String getRefundResponseNo() {
        return refundResponseNo;
    }

    public void setRefundResponseNo(String refundResponseNo) {
        this.refundResponseNo = refundResponseNo;
    }

    public Instant getSuccessAt() {
        return successAt;
    }

    public void setSuccessAt(Instant successAt) {
        this.successAt = successAt;
    }

    public RefundStatusEnum getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(RefundStatusEnum refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getPlatformChannelAccount() {
        return platformChannelAccount;
    }

    public void setPlatformChannelAccount(String platformChannelAccount) {
        this.platformChannelAccount = platformChannelAccount;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public String getErrCodeDes() {
        return errCodeDes;
    }

    public void setErrCodeDes(String errCodeDes) {
        this.errCodeDes = errCodeDes;
    }
}
