package com.chatplus.application.domain.notification;

import com.chatplus.application.enumeration.AiPlatformEnum;
import com.chatplus.application.enumeration.FundTypeEnum;
import com.chatplus.application.enumeration.PowerLogTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户消费记录通知
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PowerLogNotification implements Serializable {
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 类型（1：充值，2：消费，3：退费）
     */
    private PowerLogTypeEnum type;
    /**
     * 算力数值
     */
    private Integer amount;
    /**
     * 历史表的itemId
     */
    private Long historyItemId;
    /**
     * 资金类型（0：支出，1：收入）
     */
    private FundTypeEnum mark;

    private AiPlatformEnum platform;

    private String remark;
}
