package com.chatplus.application.domain.request.admin;

import com.chatplus.application.domain.request.PlusPageRequest;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 后台获取订单列表
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "后台获取订单列表")
public class OrderAdminApiRequest extends PlusPageRequest implements Serializable {

    @JsonProperty("order_no")
    @Schema(description = "订单号")
    private Long orderId;

    @JsonProperty("pay_time")
    @Schema(description = "支付时间", example = "[\"2024-03-01\",\"2024-03-16\"]")
    private List<String> payTime;

}
