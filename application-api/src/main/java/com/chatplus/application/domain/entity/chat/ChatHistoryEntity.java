package com.chatplus.application.domain.entity.chat;

import com.baomidou.mybatisplus.annotation.*;
import com.chatplus.application.datasource.id.IdEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;


/**
 * <p>Table: t_chat_history - 聊天历史记录</p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Alias("chatHistory")
@TableName(value = "t_chat_history", autoResultMap = true)
@Data
public class ChatHistoryEntity extends IdEntity {

    /**
     * 用户 ID
     */
    private Long userId;
    /**
     * 会话 ID
     */
    private String chatId;
    /**
     * 类型：prompt|reply
     */
    private String type;
    /**
     * 角色 ID
     */
    private Long roleId;
    /**
     * 聊天内容
     */
    private String content;
    /**
     * 耗费 token 数量
     */
    private Long tokens;
    /**
     * 是否允许作为上下文语料
     */
    private Boolean useContext = Boolean.TRUE;
    /**
     * 每次保存 reply prompt 生成当前问题和答复的 item id，用于关联
     */
    private Long itemId;
}
