package com.chatplus.application.domain.entity.basedata;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.id.IdEntity;
import com.chatplus.application.enumeration.AiPlatformEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

import java.time.Instant;


/**
 * <p>Table: t_api_keys- OpenAI API </p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Alias("apiKey")
@TableName(value = "t_api_key", autoResultMap = true)
public class ApiKeyEntity extends IdEntity {
    /**
     * 平台协议
     */
    private AiPlatformEnum platform;
    /**
     * 所属渠道
     */
    private String channel;
    /**
     * API KEY value
     */
    private String value;
    /**
     * 用途（chat=>聊天，image=>图片，internal => 内部key）
     */
    private String type;
    /**
     * 最后使用时间
     */
    private Instant lastUsedAt;

    private Boolean useProxy;

    private Boolean enabled;

    private String apiUrl;
    /**
     * 代理地址
     * 参考代理URL格式：【http://username:password@host:port、https://username:password@host:port、http://host:port、https://host:port】
     */
    private String proxyUrl;
    /**
     * 预留字段
     */
    private String reserved;
    /**
     * 通知地址
     */
    private String notifyUrl;
}
