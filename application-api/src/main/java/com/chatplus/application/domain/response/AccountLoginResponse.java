package com.chatplus.application.domain.response;

import com.chatplus.application.common.enumeration.UserStatusEnum;
import com.chatplus.application.common.enumeration.UserTypeEnum;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
/**
 * 账号登录相关响应信息
 */
@Data
@Schema(description = "账号登录相关响应信息")
public class AccountLoginResponse implements Serializable {
    @Serial
    private static final long serialVersionUID = 6429344185772985945L;
    /**
     * 账号Id
     */
    @JsonSerialize(using = Int64AsString.class)
    @Schema(description = "账号Id")
    private Long id;
    /**
     * 登录账号
     */
    @Schema(description = "登录账号")
    private String username;
    /**
     * 授权token
     */
    @Schema(description = "授权token")
    private String authorizationToken;
    /**
     * 当前状态
     */
    @Schema(description = "当前状态")
    private UserStatusEnum status;
    /**
     * 用户类型
     */
    @Schema(description = "用户类型")
    private UserTypeEnum userType;
}
