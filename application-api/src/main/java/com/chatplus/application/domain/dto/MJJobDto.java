package com.chatplus.application.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 绘画任务JOB
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class MJJobDto implements Serializable {
    /**
     * base64Array : ["string"]
     * notifyHook : string
     * prompt : Cat
     * state : string
     */
    @JsonProperty("notifyHook")
    @Schema(description = "回调地址, 为空时使用全局notifyHook")
    private String notifyHook;

    @JsonProperty("prompt")
    @Schema(description = "提示词")
    private String prompt;

    @JsonProperty("state")
    @Schema(description = "自定义参数")
    private String state;

    @JsonProperty("base64Array")
    @Schema(description = "垫图base64数组,融图必传")
    private List<String> base64Array;

    @Schema(description = "图生图选传的参数，比例: PORTRAIT(2:3); SQUARE(1:1); LANDSCAPE(3:2)")
    private String dimensions;

    // 换脸需要的参数
    @JsonProperty("sourceBase64")
    @Schema(description = "人脸源图片base64")
    private String sourceBase64;

    @JsonProperty("targetBase64")
    @Schema(description = "目标图片base64")
    private String targetBase64;

    // 所有的关联按钮动作UPSCALE; VARIATION; REROLL; ZOOM等)
    @JsonProperty("customId")
    @Schema(description = "放大重绘的索引")
    private String customId;

    @JsonProperty("taskId")
    @Schema(description = "任务ID")
    private String taskId;
}
