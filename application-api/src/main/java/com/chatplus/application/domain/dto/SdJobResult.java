package com.chatplus.application.domain.dto;

import com.chatplus.application.enumeration.AiPlatformEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;

import java.io.Serializable;
import java.util.List;

/**
 * @Description SD图片绘图任务结果
 * @Author Angus
 * @Date 2024/2/24
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SdJobResult implements Serializable {
    // --------- 这是本地SD绘图的返回
    private List<String> images;

    private String info;

    // --------- 这是stability API的返回
    @JsonProperty("artifacts")
    private List<ArtifactsBean> artifacts;

    @Data
    public static class ArtifactsBean implements Serializable {
        /**
         * base64 : ...very long string...
         * finishReason : SUCCESS
         * seed : 1050625087
         */
        @JsonProperty("base64")
        private String base64;
        // CONTENT_FILTERED ERROR SUCCESS
        @JsonProperty("finishReason")
        private String finishReason;
    }

    public boolean isSuccess(AiPlatformEnum platform) {
        return switch (platform) {
            case SD_LOCAL -> CollectionUtils.isNotEmpty(getImages());
            case SD_STABILITY -> CollectionUtils.isNotEmpty(getArtifacts());
            default -> false;
        };
    }
}
