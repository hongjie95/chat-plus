package com.chatplus.application.domain.response;

import lombok.Data;

import java.io.Serializable;

/**
 * 人机验证响应
 */
@Data
public class BotCaptchaResponse implements Serializable {

    private String image;

    private String key;

    private String thumb;

}
