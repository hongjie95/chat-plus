package com.chatplus.application.domain.request;

import com.chatplus.application.enumeration.ProductTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 产品保存更新请求参数
 */
@Schema(description = "产品保存更新请求参数")
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductSaveRequest implements Serializable {
    @JsonProperty("id")
    @Schema(description = "产品ID，更新时候必传")
    private Long id;

    @JsonProperty("name")
    @Schema(description = "产品名称")
    private String name;

    @JsonProperty("price")
    @Schema(description = "价格，单位元")
    private BigDecimal price;

    @JsonProperty("discount")
    @Schema(description = "折扣，单位元")
    private BigDecimal discount;

    @JsonProperty("days")
    @Schema(description = "有效天数")
    private int days;
    /**
     * 算力
     */
    @JsonProperty("power")
    @Schema(description = "算力")
    private Integer power;
    /**
     * 如果是订阅卡的话，每日额度
     */
    @JsonProperty("vip_day_power")
    @Schema(description = "VIP每日额度")
    private Integer vipDayPower;

    @JsonProperty("enabled")
    @Schema(description = "是否启用")
    private boolean enabled;

    @JsonProperty("sales")
    @Schema(description = "销量")
    private int sales;

    @JsonProperty("sort_num")
    @Schema(description = "排序")
    private int sortNum;

    @JsonProperty("product_type")
    @Schema(description = "产品类型，分为天卡、次卡")
    private ProductTypeEnum productType;
}
