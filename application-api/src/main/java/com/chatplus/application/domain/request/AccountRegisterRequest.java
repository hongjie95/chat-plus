package com.chatplus.application.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.io.Serializable;

/**
 * 注册请求
 *
 * @author chj
 * @date 2023/6/8
 **/
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "注册请求参数")
public class AccountRegisterRequest implements Serializable {

    @JsonProperty("username")
    @NotEmpty(message = "用户名不能为空")
    @Schema(description = "用户名")
    private String username;

    @JsonProperty("password")
    @NotEmpty(message = "密码不能为空")
    @Schema(description = "密码")
    private String password;

    @JsonProperty("code")
    @NotEmpty(message = "验证码不能为空")
    @Schema(description = "验证码")
    private String code;

    @JsonProperty("repass")
    @NotEmpty(message = "密码不能为空")
    @Schema(description = "确认密码")
    private String repass;

    @JsonProperty("invite_code")
    @Schema(description = "邀请码")
    private String inviteCode;

}
