package com.chatplus.application.domain.entity.account;

import com.baomidou.mybatisplus.annotation.*;
import com.chatplus.application.datasource.id.IdEntity;
import org.apache.ibatis.type.Alias;

/**
 * <p>Table: t_invite_code - 用户邀请码</p>
 *
 * @author developer
 */
@Alias("inviteCode")
@TableName(value = "t_invite_code", autoResultMap = true)
public class InviteCodeEntity extends IdEntity {

    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 邀请码
     */
    private String code;
    /**
     * 点击次数
     */
    private Integer hits;
    /**
     * 注册数量
     */
    private Integer regNum;

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId =  userId;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code =  code;
    }

    public Integer getHits() {
        return this.hits;
    }

    public void setHits(Integer hits) {
        this.hits =  hits;
    }

    public Integer getRegNum() {
        return this.regNum;
    }

    public void setRegNum(Integer regNum) {
        this.regNum =  regNum;
    }

}
