package com.chatplus.application.domain.dto.ws;

import cn.hutool.json.JSONObject;
import com.chatplus.application.enumeration.MessageTypeEnum;
import lombok.Data;

import java.io.Serializable;

/**
 * WebSocket消息体
 */
@Data
public class WsChatMessage implements Serializable {
    /**
     * 消息类型 分为：start、middle、end、mj、heartbeat
     */
    private MessageTypeEnum type;
    /**
     * 消息内容
     */
    private String content;

    public WsChatMessage(MessageTypeEnum type, String content) {
        this.type = type;
        this.content = content;
    }
    public WsChatMessage() {
    }

    public byte[] toByte() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.putOpt("type", type.getValue());
        jsonObject.putOpt("content", content);
        return jsonObject.toString().getBytes();
    }
}
