package com.chatplus.application.domain.entity.orders;

import com.baomidou.mybatisplus.annotation.*;
import com.chatplus.application.datasource.id.IdEntity;
import org.apache.ibatis.type.Alias;

import java.math.BigDecimal;

/**
 * <p>Table: t_reward - 用户打赏</p>
 *
 * @author developer
 */
@Alias("reward")
@TableName(value = "t_reward", autoResultMap = true)
public class RewardEntity extends IdEntity {

    /**
     * 用户 ID
     */
    private Long userId;
    /**
     * 交易 ID
     */
    private String txId;
    /**
     * 打赏金额
     */
    private BigDecimal amount;
    /**
     * 备注
     */
    private String remark;
    /**
     * 核销状态，0：未核销，1：已核销
     */
    private Boolean status;

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId =  userId;
    }

    public String getTxId() {
        return this.txId;
    }

    public void setTxId(String txId) {
        this.txId =  txId;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount =  amount;
    }

    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String remark) {
        this.remark =  remark;
    }

    public Boolean getStatus() {
        return this.status;
    }

    public void setStatus(Boolean status) {
        this.status =  status;
    }

}
