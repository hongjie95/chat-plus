package com.chatplus.application.domain.response;

import com.chatplus.application.domain.entity.basedata.PowerLogsEntity;
import com.chatplus.application.enumeration.FundTypeEnum;
import com.chatplus.application.enumeration.PowerLogTypeEnum;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;

/**
 * 消费日志响应
 *
 * @Author Angus
 * @Date 2024/3/30
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PowerLogsResponse implements Serializable {


    /**
     * id : 5
     * user_id : 1
     * username : 15302789406
     * type : 2
     * type_str : 消费
     * amount : 1
     * mark : 0
     * balance : 9971
     * model : gpt-3.5-turbo-16k
     * remark : 模型名称：GPT-3.5, 提问长度：31，回复长度：1090
     * created_at : 1711523142
     */

    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;
    @JsonProperty("user_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long userId;
    @JsonProperty("username")
    private String username;
    @JsonProperty("type")
    private PowerLogTypeEnum type;
    @JsonProperty("type_str")
    private String typeStr;
    @JsonProperty("amount")
    private Integer amount;
    @JsonProperty("mark")
    private FundTypeEnum mark;
    @JsonProperty("balance")
    private Integer balance;
    @JsonProperty("model")
    private String model;
    @JsonProperty("remark")
    private String remark;
    @JsonProperty("created_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long createdAt;

    public static PowerLogsResponse build(PowerLogsEntity entity) {
        PowerLogsResponse response = new PowerLogsResponse();
        response.setId(entity.getId());
        response.setUserId(entity.getUserId());
        response.setUsername(entity.getUsername());
        response.setType(entity.getType());
        response.setTypeStr(entity.getType() != null ? entity.getType().getName() : "未知");
        response.setAmount(entity.getAmount());
        response.setMark(entity.getMark());
        response.setBalance(entity.getBalance());
        response.setModel(entity.getModel());
        response.setRemark(entity.getRemark());
        response.setCreatedAt(entity.getCreatedAt().getEpochSecond());
        return response;
    }
}
