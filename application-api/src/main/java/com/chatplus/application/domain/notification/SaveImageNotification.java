package com.chatplus.application.domain.notification;

import com.chatplus.application.enumeration.AiPlatformEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

/**
 * 异步保存图片
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SaveImageNotification implements Serializable {

    /**
     * 业务类型：MJ，SD
     */
    private AiPlatformEnum bizType;
    /**
     * 业务ID
     */
    private Long bizId;
    /**
     * 图片地址
     */
    private String netImgUrl;
    /**
     * 图片地址
     */
    private String base64ImgUrl;
}
