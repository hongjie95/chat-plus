package com.chatplus.application.domain.response;

import com.chatplus.application.domain.dto.OrderRemarkDto;
import com.chatplus.application.domain.entity.orders.OrderEntity;
import com.chatplus.application.enumeration.OrderStatusEnum;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 订单详情响应
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDetailApiResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * id : 2
     * created_at : 1706166779
     * updated_at : 1706166824
     * user_id : 109
     * product_id : 1
     * username : 15302789406
     * order_no : 202401257156182146653093888
     * trade_no : 2024012522001437130502086982
     * subject : 会员1个月
     * amount : 9.9
     * status : 2
     * pay_time : 1706166823
     * pay_way : 支付宝
     * remark : {"days":30,"calls":0,"img_calls":0,"name":"会员1个月","price":19.9,"discount":10}
     */

    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;
    @JsonProperty("created_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long createdAt;
    @JsonProperty("updated_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long updatedAt;
    @JsonProperty("user_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long userId;
    @JsonProperty("product_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long productId;
    @JsonProperty("username")
    private String username;
    @JsonProperty("order_no")
    @JsonSerialize(using = Int64AsString.class)
    private Long orderNo;
    @JsonProperty("trade_no")
    private String tradeNo;
    @JsonProperty("subject")
    private String subject;
    @JsonProperty("amount")
    private BigDecimal amount;
    @JsonProperty("status")
    private OrderStatusEnum status;
    @JsonProperty("pay_time")
    @JsonSerialize(using = Int64AsString.class)
    private Long payTime;
    @JsonProperty("pay_way")
    private String payWay;
    @JsonProperty("remark")
    private OrderRemarkDto remark;

    public static OrderDetailApiResponse build(OrderEntity orderEntity) {
        OrderDetailApiResponse orderApiResponse = new OrderDetailApiResponse();
        orderApiResponse.setId(orderEntity.getId());
        orderApiResponse.setCreatedAt(orderEntity.getCreatedAt().getEpochSecond());
        orderApiResponse.setUpdatedAt(orderEntity.getUpdatedAt().getEpochSecond());
        orderApiResponse.setUserId(orderEntity.getUserId());
        orderApiResponse.setProductId(orderEntity.getProductId());
        orderApiResponse.setUsername(orderEntity.getUsername());
        orderApiResponse.setOrderNo(orderEntity.getId());
        orderApiResponse.setTradeNo(orderEntity.getTradeNo());
        orderApiResponse.setSubject(orderEntity.getSubject());
        orderApiResponse.setAmount(orderEntity.getAmount());
        orderApiResponse.setStatus(orderEntity.getStatus());
        orderApiResponse.setPayTime(orderEntity.getPayTime() != null ? orderEntity.getPayTime().getEpochSecond() : null);
        orderApiResponse.setPayWay(orderEntity.getPayWay().getName());
        orderApiResponse.setRemark(orderEntity.getRemark());
        return orderApiResponse;
    }
}
