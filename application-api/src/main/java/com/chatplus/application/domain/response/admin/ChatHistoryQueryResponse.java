package com.chatplus.application.domain.response.admin;

import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;

/**
 * 后台用户会话历史查询响应
 *
 * @author chj
 * @date 2024/3/1
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ChatHistoryQueryResponse implements Serializable {

    /**
     * id : 154
     * user_id : 109
     * username : 15302789406
     * content : 调用函数工具出错：获取绘图 API KEY 失败: record not found
     * type : reply
     * model : gpt-3.5-turbo-16k
     * token : 603
     * icon : /images/avatar/gpt.png
     * created_at : 1709200015
     */
    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;
    @JsonProperty("user_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long userId;
    @JsonProperty("username")
    private String username;
    @JsonProperty("content")
    private String content;
    @JsonProperty("type")
    private String type;
    @JsonProperty("model")
    private String model;
    @JsonProperty("token")
    private Long token;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("created_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long createdAt;
}
