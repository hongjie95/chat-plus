package com.chatplus.application.domain.request.admin;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * 配置更新请求
 */
@Schema(description = "配置更新请求")
@Data
public class ConfigUpdateAdminApiRequest  implements Serializable {

    @JsonProperty("config")
    @Schema(description = "配置JSON字符串")
    private Object config;

    @JsonProperty("key")
    @Schema(description = "配置key")
    private String key;

}
