package com.chatplus.application.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统运维相关配置
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class DevopsConfigDto implements Serializable {


    @JsonProperty("enable_audit_logger")
    private Boolean enableAuditLogger;

    @JsonProperty("enable_alert")
    private Boolean enableAlert;

    @JsonProperty("alert_interval")
    private Integer alertInterval;

    @JsonProperty("alert_mail")
    private String alertMail;

    @JsonProperty("alert_webhook")
    private String alertWebhook;
}
