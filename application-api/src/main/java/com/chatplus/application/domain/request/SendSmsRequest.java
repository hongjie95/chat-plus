package com.chatplus.application.domain.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.io.Serializable;

/**
 * 发送短信请求
 */
@Schema(description = "发送短信请求")
@Data
public class SendSmsRequest implements Serializable {

    @NotEmpty(message = "用户名不能为空")
    @Schema(description = "用户名")
    private String receiver;

    @Schema(description = "人机校验的key")
    private String key;

}
