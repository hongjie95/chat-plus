package com.chatplus.application.domain.request;

import com.chatplus.application.domain.dto.TextPromptsDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * SD stability 参数
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SdStabilityImageRequest implements Serializable {


    /**
     * cfg_scale : 7
     * clip_guidance_preset : FAST_BLUE
     * height : 512
     * width : 512
     * sampler : K_DPM_2_ANCESTRAL
     * samples : 1
     * steps : 30
     * text_prompts : [{"text":"A lighthouse on a cliff","weight":1}]
     */
    @Schema(description = "SD v1.6: must be between 320x320 and 1536x1536")
    @JsonProperty("height")
    @Min(value = 320, message = "图片尺寸高度最小值为320")
    @Max(value = 1536, message = "图片尺寸高度最大值为1536")
    private int height;

    @Schema(description = "SD v1.6: must be between 320x320 and 1536x1536")
    @JsonProperty("width")
    @Min(value = 320, message = "图片尺寸宽度最小值为320")
    @Max(value = 1536, message = "图片尺寸宽度最大值为1536")
    private int width;

    @JsonProperty("text_prompts")
    private List<TextPromptsDto> textPrompts;

    private String prompt;

    @JsonProperty("negative_prompt")
    private String negativePrompt;

    @JsonProperty("cfg_scale")
    @Schema(description = "扩散过程遵循提示文本的严格程度,值越高，图像越接近提示", defaultValue = "7", minimum = "0", maximum = "35")
    @Min(value = 0, message = "引导系数最小值为0")
    @Max(value = 35, message = "引导系数最大值为35")
    private int cfgScale = 7;

    @JsonProperty("clip_guidance_preset")
    @Schema(description = "枚举： FAST_BLUE FAST_GREEN NONE SIMPLE SLOW SLOWER SLOWEST")
    private String clipGuidancePreset;

    @JsonProperty("sampler")
    @Schema(description = "采样方法。DDIM DDPM K_DMPPP_2M K_DMPPP_2S_ANCESTRAL K_DPM_2 K_DPM_2_ANCESTRAL K_EULER K_EULER_ANCESTRAL K_HEUN K_LMS")
    private String sampler;

    @JsonProperty("seed")
    @Schema(description = "随机因子", defaultValue = "0", minimum = "0", maximum = "4294967295")
    @Min(value = -1, message = "随机因子最小值为-1")
    @Max(value = 4294967295L, message = "随机因子最大值为4294967295")
    private int seed;

    @JsonProperty("samples")
    @Schema(description = "生成图片张数", defaultValue = "1", minimum = "1", maximum = "10")
    private int samples = 1;

    @Schema(description = "迭代步数", defaultValue = "30")
    @JsonProperty("steps")
    @Min(value = 1, message = "迭代步数最小值为1")
    @Max(value = 50, message = "迭代步数最大值为35")
    private int steps = 30;

    @Schema(description = "3d-model analog-film anime cinematic comic-book digital-art enhance fantasy-art isometric line-art low-poly modeling-compound neon-punk origami photographic pixel-art tile-texture", defaultValue = "0", minimum = "0", maximum = "4294967295")
    @JsonProperty("style_preset")
    private String stylePreset;

    @Schema(description = "SD Job ID")
    private Long sdJobId;

    @Schema(description = "用户ID")
    private Long userId;

    @Schema(description = "渠道")
    private String channel;

}
