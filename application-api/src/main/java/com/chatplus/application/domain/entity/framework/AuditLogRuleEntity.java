package com.chatplus.application.domain.entity.framework;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.id.IdEntity;
import com.chatplus.application.enumeration.AuditLogRuleTypeEnum;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;

/**
 * <p>Table: t_audit_log_rule - 审计日志规则表</p>
 *
 * @author developer
 */
@Alias("auditLogRule")
@TableName(value = "t_audit_log_rule", autoResultMap = true)
public class AuditLogRuleEntity extends IdEntity {
    /**
     * 规则url
     */
    private String url;
    /**
     * 类型;1:白名单,2:黑名单
     */
    private AuditLogRuleTypeEnum type;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public AuditLogRuleTypeEnum getType() {
        return this.type;
    }

    public void setType(AuditLogRuleTypeEnum type) {
        this.type = type;
    }

}
