package com.chatplus.application.domain.entity.basedata;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.id.IdEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>Table: t_config - 第三方AI配置表</p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Alias("config")
@TableName(value = "t_config", autoResultMap = true)
public class ConfigEntity extends IdEntity {

    /**
     * 标识
     */
    private String marker;

    private String config;

}
