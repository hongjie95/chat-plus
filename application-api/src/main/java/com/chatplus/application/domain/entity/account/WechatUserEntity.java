package com.chatplus.application.domain.entity.account;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.id.IdEntity;
import com.chatplus.application.enumeration.WechatUserStatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>Table: t_wechat_user - 微信小程序用户</p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Alias("wechatUser")
@TableName(value = "t_wechat_user", autoResultMap = true)
@Data
public class WechatUserEntity extends IdEntity {

    /**
     * appId
     */
    private String appId;
    /**
     * 绑定的用户ID
     */
    private Long userId;
    /**
     * 微信openid
     */
    private String openId;
    /**
     * 微信unionid
     */
    private String unionId;
    /**
     * 微信昵称
     */
    private String nickName;
    /**
     * 微信头像
     */
    private String avatarUrl;
    /**
     * 状态
     */
    private WechatUserStatusEnum status;
}
