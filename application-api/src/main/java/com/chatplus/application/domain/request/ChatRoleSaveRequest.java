package com.chatplus.application.domain.request;

import com.chatplus.application.domain.dto.RoleContextDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 聊天角色保存更新请求
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description ="聊天角色保存更新请求参数")
public class ChatRoleSaveRequest implements Serializable {


    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * context : [{"role":"User","content":"xxxxxxx"}]
     * name : 测试
     * key : 测试111
     * icon : /xxx/xxx
     * hello_msg : 你好 叼毛
     * enable : true
     */
    @Schema(description = "角色ID,更新时候必传")
    private Long id;
    /**
     * 角色名称
     */
    @JsonProperty("name")
    @Schema(description = "角色名称")
    private String name;
    @JsonProperty("key")
    @Schema(description = "角色标志")
    private String key;
    @JsonProperty("icon")
    @Schema(description = "角色图标")
    private String icon;
    @JsonProperty("hello_msg")
    @Schema(description = "打招呼信息")
    private String helloMsg;
    @JsonProperty("enable")
    @Schema(description = "启用状态")
    private boolean enable;
    @JsonProperty("context")
    @Schema(description = "上下文信息")
    private List<RoleContextDto> context;

}
