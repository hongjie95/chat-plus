package com.chatplus.application.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.io.Serializable;

/**
 * SD本地绘图任务请求
 */
@Data
public class SdLocalImageRequest implements Serializable {

    @JsonProperty("width")
    @Schema(description = "图片尺寸宽度")
    @Min(value = 100, message = "图片尺寸宽度最小值为100")
    @Max(value = 1024, message = "图片尺寸宽度最大值为1024")
    private int width;

    @JsonProperty("height")
    @Schema(description = "图片尺寸高度")
    @Min(value = 100, message = "图片尺寸高度最小值为100")
    @Max(value = 1024, message = "图片尺寸高度最大值为1024")
    private int height;

    @JsonProperty("sampler_name")
    @Schema(description = "采样方法")
    private String samplerName = "Euler a";

    @JsonProperty("seed")
    @Schema(description = "随机因子")
    private int seed = -1;

    @JsonProperty("steps")
    @Schema(description = "迭代步骤")
    @Min(value = 1, message = "迭代步骤最小值为1")
    @Max(value = 50, message = "迭代步骤最大值为50")
    private int steps;

    @JsonProperty("cfg_scale")
    @Schema(description = "引导系数")
    @Min(value = 1, message = "引导系数最小值为1")
    @Max(value = 10, message = "引导系数最大值为10")
    private int cfgScale;

    @JsonProperty("enable_hr")
    @Schema(description = "高清修复")
    private boolean enableHr;

    @JsonProperty("denoising_strength")
    @Schema(description = "重绘幅度")
    private double denoisingStrength;

    @JsonProperty("hr_scale")
    @Schema(description = "放大倍数")
    private int hrScale;

    @JsonProperty("hr_upscaler")
    @Schema(description = "放大算法")
    private String hrUpscaler;

    @JsonProperty("hr_second_pass_steps")
    @Schema(description = "迭代步数")
    private int hrSecondPassSteps;

    @JsonProperty("prompt")
    @Schema(description = "提示词")
    @NotEmpty(message = "提示词不能为空")
    private String prompt;

    @JsonProperty("negative_prompt")
    @Schema(description = "反向提示词")
    private String negativePrompt;

    @JsonProperty("session_id")
    @Schema(description = "当前任务会话ID")
    private String sessionId;

    @Schema(description = "SD Job ID")
    private Long sdJobId;

    @JsonProperty("task_id")
    private String taskId;

    @Schema(description = "用户ID")
    private Long userId;

    @Schema(description = "渠道")
    private String channel;

}
