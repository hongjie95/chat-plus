package com.chatplus.application.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 数据库字段更新请求公共参数
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description ="数据库字段更新请求公共参数")
public class FiledUpdateRequest implements Serializable {


    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * id : 39
     * filed : enable
     * value : false
     */

    @JsonProperty("id")
    @Schema(description = "业务ID")
    private String id;
    @JsonProperty("filed")
    @Schema(description = "字段名")
    private String filed;
    @JsonProperty("value")
    @Schema(description = "修改的内容")
    private String value;

}
