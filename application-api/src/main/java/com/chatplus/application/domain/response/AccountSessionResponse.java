package com.chatplus.application.domain.response;

import com.chatplus.application.domain.dto.UserCustomChatConfigDto;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 账号详细信息
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Schema(description = "账号详细信息")
public class AccountSessionResponse implements Serializable {
    @Serial
    private static final long serialVersionUID = 6429344185772985945L;

    /**
     * id : 100
     * created_at : 12321
     * updated_at : 123123
     * username : 15302789406
     * nickname : 极客学长@281216
     * avatar : /images/avatar/user.png
     * salt : bh94d1rv
     * total_tokens : 30466
     * calls : 9566
     * img_calls : 5
     * chat_config : {"api_keys":{"Azure":"","ChatGLM":"","OpenAI":""}}
     * chat_roles : ["teacher","psychiatrist"]
     * chat_models : ["eb-instant","completions_pro","generalv2","general","chatglm_pro","gpt-3.5-turbo-16k","chatglm_lite","chatglm_std"]
     * expired_time : 0
     * status : true
     * last_login_at : 1705627592
     * last_login_ip : 116.1.249.206
     * vip : false
     * token : 30466
     */

    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    @Schema(description = "账号Id")
    private Long id;

    @JsonProperty("created_at")
    @JsonSerialize(using = Int64AsString.class)
    @Schema(description = "创建时间")
    private Long createdAt;

    @JsonProperty("updated_at")
    @JsonSerialize(using = Int64AsString.class)
    @Schema(description = "更新时间")
    private Long updatedAt;

    @JsonProperty("username")
    @Schema(description = "登录账号")
    private String username;

    @JsonProperty("nickname")
    @Schema(description = "昵称")
    private String nickname;

    @JsonProperty("avatar")
    @Schema(description = "头像")
    private String avatar;

    @JsonProperty("total_token")
    @JsonSerialize(using = Int64AsString.class)
    @Schema(description = "总消耗算力")
    private Long totalToken;

    @JsonProperty("power")
    @Schema(description = "算力余额")
    private Integer power;

    @JsonProperty("chat_config")
    @Schema(description = "自定义的聊天配置")
    private UserCustomChatConfigDto chatConfig;

    @JsonProperty("expired_time")
    @JsonSerialize(using = Int64AsString.class)
    @Schema(description = "VIP过期时间")
    private Long expiredTime;

    @JsonProperty("status")
    @Schema(description = "用户状态")
    private boolean status;

    @JsonProperty("last_login_at")
    @JsonSerialize(using = Int64AsString.class)
    @Schema(description = "最后登录时间")
    private Long lastLoginAt;

    @JsonProperty("last_login_ip")
    @Schema(description = "最后登录IP")
    private String lastLoginIp;

    @JsonProperty("vip")
    @Schema(description = "是否是VIP")
    private boolean vip;

    @JsonProperty("token")
    @JsonSerialize(using = Int64AsString.class)
    @Schema(description = "当月消耗算力")
    private Long token;

    @JsonProperty("chat_roles")
    @Schema(description = "可用聊天角色")
    private List<String> chatRoles;

    @JsonProperty("chat_models")
    @Schema(description = "可用聊天模型")
    private List<String> chatModels;

    @JsonProperty("admin")
    @Schema(description = "是否是管理员")
    private Boolean admin;
}
