package com.chatplus.application.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
/**
 * 获取Token请求参数
 */
@Data
@Schema(description = "获取Token请求参数")
public class ChatTokenRequest implements Serializable {
    /**
     * text :
     * ws : gpt-3.5-turbo-16k
     */
    @JsonProperty("chat_id")
    @Schema(description = "会话ID")
    private String chatId;

    @Schema(description = "文本内容")
    private String text;

    @Schema(description = "模型")
    private String model;

}
