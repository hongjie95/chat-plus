package com.chatplus.application.domain.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * 微信二维码响应
 *
 * @Author Angus
 * @Date 2024/4/4
 */
@Data
@Schema(description = "微信二维码响应")
public class WechatLoginQrResponse implements Serializable {

    @Schema(description = "ticket")
    private String ticket;

    @Schema(description = "二维码地址")
    private String codeUrl;
}
