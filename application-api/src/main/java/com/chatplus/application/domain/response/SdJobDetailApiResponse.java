package com.chatplus.application.domain.response;

import com.chatplus.application.common.util.PlusJsonUtils;
import com.chatplus.application.domain.entity.draw.SdJobEntity;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 * Sd任务详情响应
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Schema(description = "Sd任务详情响应")
public class SdJobDetailApiResponse implements Serializable {

    /**
     * id : 23
     * type : image
     * user_id : 109
     * task_id : task(aruwc3ba5n3rlqr)
     * img_url : /static/upload/2024/2/1708761716271387.png
     * params : {"task_id":"task(aruwc3ba5n3rlqr)","prompt":"Create a striking portrait of Xi Jinping, capturing the essence of his leadership and wisdom. Use bold brushstrokes to depict his strong and determined expression, conveying his authoritative presence. Render his distinctive features with precision, showcasing his iconic hairstyle and friendly smile. Utilize a color palette of deep reds and rich golds to symbolize power and prosperity. Add subtle highlights and shadows to enhance depth and dimension, giving the portrait a lifelike quality. Imagine Xi Jinping in a dignified and regal pose, exuding confidence and charisma. Illuminate the picture with soft, warm lighting to create a sense of warmth and approachability. Bring this portrait to life with attention to detail and a careful balance of realism and artistic interpretation.","negative_prompt":"nsfw, paintings,low quality,easynegative,ng_deepnegative ,lowres,bad anatomy,bad hands,bad feet","steps":30,"sampler":"Euler a","face_fix":false,"cfg_scale":7,"seed":647598606,"height":1024,"width":1024,"hd_fix":false,"hd_redraw_rate":0.5,"hd_scale":2,"hd_scale_alg":"Latent","hd_steps":15}
     * progress : 100
     * prompt : Create a striking portrait of Xi Jinping, capturing the essence of his leadership and wisdom. Use bold brushstrokes to depict his strong and determined expression, conveying his authoritative presence. Render his distinctive features with precision, showcasing his iconic hairstyle and friendly smile. Utilize a color palette of deep reds and rich golds to symbolize power and prosperity. Add subtle highlights and shadows to enhance depth and dimension, giving the portrait a lifelike quality. Imagine Xi Jinping in a dignified and regal pose, exuding confidence and charisma. Illuminate the picture with soft, warm lighting to create a sense of warmth and approachability. Bring this portrait to life with attention to detail and a careful balance of realism and artistic interpretation.
     * publish : false
     * created_at : 2024-02-24T16:01:10+08:00
     */
    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    @Schema(description = "任务Id")
    private Long id;

    @JsonProperty("type")
    @Schema(description = "任务类型")
    private String type;

    @JsonProperty("user_id")
    @JsonSerialize(using = Int64AsString.class)
    @Schema(description = "用户Id")
    private Long userId;

    @JsonProperty("task_id")
    @Schema(description = "任务Id")
    private String taskId;

    @JsonProperty("img_url")
    @Schema(description = "图片地址")
    private String imgUrl;

    @JsonProperty("params")
    @Schema(description = "参数")
    private Map<String, Object> params;

    @JsonProperty("progress")
    @Schema(description = "进度")
    private Integer progress;

    @JsonProperty("prompt")
    @Schema(description = "提示")
    private String prompt;

    @JsonProperty("publish")
    @Schema(description = "是否发布")
    private Boolean publish;

    @JsonProperty("created_at")
    @Schema(description = "创建时间")
    private Instant createdAt;

    public static SdJobDetailApiResponse build(SdJobEntity sdJobEntity) {
        SdJobDetailApiResponse sdJobDetailApiResponse = new SdJobDetailApiResponse();
        sdJobDetailApiResponse.setId(sdJobEntity.getId());
        sdJobDetailApiResponse.setType(sdJobEntity.getType());
        sdJobDetailApiResponse.setUserId(sdJobEntity.getUserId());
        sdJobDetailApiResponse.setTaskId(sdJobEntity.getId() + "");
        sdJobDetailApiResponse.setImgUrl(sdJobEntity.getImgUrl());
        sdJobDetailApiResponse.setParams(PlusJsonUtils.parseObject(sdJobEntity.getParams(), HashMap.class));
        sdJobDetailApiResponse.setProgress(sdJobEntity.getProgress());
        sdJobDetailApiResponse.setPrompt(sdJobEntity.getPrompt());
        sdJobDetailApiResponse.setPublish(sdJobEntity.getPublish());
        sdJobDetailApiResponse.setCreatedAt(sdJobEntity.getCreatedAt());
        return sdJobDetailApiResponse;
    }
}
