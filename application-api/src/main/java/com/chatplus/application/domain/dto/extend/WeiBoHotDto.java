package com.chatplus.application.domain.dto.extend;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
public class WeiBoHotDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 热搜名字
     */
    private String note;
    /**
     * 热搜类型
     */
    private String category;
    /**
     * 热搜热度
     */
    private Long num;
    /**
     * 热搜链接
     */
    private String url;

}
