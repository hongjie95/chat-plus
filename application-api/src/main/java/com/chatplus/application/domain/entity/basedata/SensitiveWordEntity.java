package com.chatplus.application.domain.entity.basedata;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.id.LogicDeleteEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>Table: t_sensitive_word - 敏感词表</p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Alias("sensitiveWord")
@TableName(value = "t_sensitive_word", autoResultMap = true)
public class SensitiveWordEntity extends LogicDeleteEntity {

    /**
     * 敏感词
     */
    private String word;
    /**
     * 备注
     */
    private String remark;

}
