package com.chatplus.application.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class NoticeConfigDto implements Serializable {
    /**
     * content : 注意：当前站点仅为开源项目
     * updated : true
     */
    @JsonProperty("content")
    private String content;
    @JsonProperty("updated")
    private boolean updated;

    public NoticeConfigDto(String content,boolean updated){
        this.content = content;
        this.updated = updated;
    }

    public NoticeConfigDto(){

    }
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }
}
