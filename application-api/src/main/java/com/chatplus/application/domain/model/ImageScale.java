package com.chatplus.application.domain.model;

import lombok.Data;

/**
 * 如果要略缩图，需要传入的参数
 *
 * @author chj
 * @date 2024/1/24
 **/
@Data
public class ImageScale {
    private int width;
    private int height;

}
