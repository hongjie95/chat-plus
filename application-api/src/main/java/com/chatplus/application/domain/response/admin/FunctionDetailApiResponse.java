package com.chatplus.application.domain.response.admin;

import com.chatplus.application.domain.dto.FunctionParametersDto;
import com.chatplus.application.domain.entity.functions.FunctionEntity;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FunctionDetailApiResponse implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * id : 1
     * name : weibo
     * label : 微博热搜
     * description : 新浪微博热搜榜，微博当日热搜榜单
     * parameters : {"type":"object","properties":{}}
     * action : http://localhost:5678/api/function/weibo
     * token : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcmVkIjowLCJ1c2VyX2lkIjowfQ.tLAGkF8XWh_G-oQzevpIodsswtPByBLoAZDz_eWuBg
     * enabled : true
     */

    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("label")
    private String label;
    @JsonProperty("description")
    private String description;
    @JsonProperty("parameters")
    private FunctionParametersDto parameters;
    @JsonProperty("action")
    private String action;
    @JsonProperty("token")
    private String token;
    @JsonProperty("enabled")
    private boolean enabled;
    /**
     * 函数请求方法
     */
    @JsonProperty("request_method")
    private String requestMethod;
    public static FunctionDetailApiResponse build(FunctionEntity functionEntity) {
        FunctionDetailApiResponse functionDetailApiResponse = new FunctionDetailApiResponse();
        functionDetailApiResponse.setId(functionEntity.getId());
        functionDetailApiResponse.setName(functionEntity.getName());
        functionDetailApiResponse.setLabel(functionEntity.getLabel());
        functionDetailApiResponse.setDescription(functionEntity.getDescription());
        functionDetailApiResponse.setAction(functionEntity.getAction());
        functionDetailApiResponse.setEnabled(functionEntity.getEnabled());
        functionDetailApiResponse.setToken(functionEntity.getToken());
        functionDetailApiResponse.setParameters(functionEntity.getParameters());
        functionDetailApiResponse.setRequestMethod(functionEntity.getRequestMethod());
        return functionDetailApiResponse;
    }
}
