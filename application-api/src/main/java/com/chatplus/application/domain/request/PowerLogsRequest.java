package com.chatplus.application.domain.request;

import com.chatplus.application.enumeration.PowerLogTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 *  用户消费日志查询请求
 * @Author Angus
 * @Date 2024/3/30
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "用户消费日志查询请求")
public class PowerLogsRequest extends PlusPageRequest implements Serializable {

    // 日期范围，格式：yyyy-MM-dd
    @JsonProperty("date")
    @Schema(description = "创建时间", example = "[\"2024-03-01\",\"2024-03-16\"]")
    private List<String> date;

    @Schema(description = "模型值")
    private String model;

    @Schema(description = "算力类型")
    private PowerLogTypeEnum type;

}
