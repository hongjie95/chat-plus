package com.chatplus.application.domain.response;

import com.chatplus.application.domain.entity.basedata.SensitiveWordEntity;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p>敏感词表</p>
 *
 */
@Data
@Schema(description = "敏感词表")
public class SensitiveWordResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 对象存储主键
     */
    @JsonSerialize(using = Int64AsString.class)
    private Long id;
    /**
     * 敏感词
     */
    @Schema(description = "敏感词")
    private String word;
    /**
     * 备注
     */
    @Schema(description = "备注")
    private String remark;

    public static SensitiveWordResponse build(SensitiveWordEntity sensitiveWordEntity) {
        if (sensitiveWordEntity == null) {
            return null;
        }
        SensitiveWordResponse sensitiveWordResponse = new SensitiveWordResponse();
        sensitiveWordResponse.setId(sensitiveWordEntity.getId());
        sensitiveWordResponse.setWord(sensitiveWordEntity.getWord());
        sensitiveWordResponse.setRemark(sensitiveWordEntity.getRemark());
        return sensitiveWordResponse;
    }

}
