package com.chatplus.application.domain.request;

import com.chatplus.application.enumeration.AiPlatformEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 聊天模型保存更新请求参数
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "聊天模型保存更新请求参数")
public class ChatModelSaveRequest implements Serializable {


    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * enabled : true
     * weight : 1
     * platform : XunFei
     * name : 123
     * value : 123
     * open : true
     */
    @Schema(description = "模型ID,更新时候必传")
    private Long id;
    @JsonProperty("enabled")
    @Schema(description = "启用状态")
    private boolean enabled;
    @JsonProperty("power")
    @Schema(description = "对话权重")
    private int power;
    @JsonProperty("platform")
    @Schema(description = "所属平台")
    private AiPlatformEnum platform;
    @JsonProperty("channel")
    @Schema(description = "所属渠道")
    private String channel;
    @JsonProperty("name")
    @Schema(description = "模型名称")
    private String name;
    @JsonProperty("value")
    @Schema(description = "模型值")
    private String value;
    @JsonProperty("open")
    @Schema(description = "开放状态")
    private boolean open;
    @JsonProperty("description")
    @Schema(description = "模型描述")
    private String description;
    /**
     * 是否启用函数功能
     */
    @JsonProperty("enabled_function")
    @Schema(description = "是否启用函数功能")
    private Boolean enabledFunction;
    /**
     * 最大响应长度
     */
    @JsonProperty("max_tokens")
    @Schema(description = "最大响应长度")
    private Integer maxTokens;
    /**
     * 最大上下文长度
     */
    @JsonProperty("max_context")
    @Schema(description = "最大上下文长度")
    private Integer maxContext;
    /**
     * 创意度
     */
    @JsonProperty("temperature")
    @Schema(description = "创意度")
    private Float temperature;
}
