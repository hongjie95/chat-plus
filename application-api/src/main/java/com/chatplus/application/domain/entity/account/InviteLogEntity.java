package com.chatplus.application.domain.entity.account;

import com.baomidou.mybatisplus.annotation.*;
import com.chatplus.application.datasource.handler.JacksonJsonTypeHandler;
import com.chatplus.application.datasource.id.IdEntity;
import com.chatplus.application.domain.dto.RewardDto;
import org.apache.ibatis.type.Alias;

/**
 * <p>Table: t_invite_log - 邀请注册日志</p>
 *
 * @author developer
 */
@Alias("inviteLog")
@TableName(value = "t_invite_log", autoResultMap = true)
public class InviteLogEntity extends IdEntity {

    /**
     * 邀请人ID
     */
    private Long inviterId;
    /**
     * 注册用户ID
     */
    private Long userId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 邀请码
     */
    private String inviteCode;
    /**
     * 邀请奖励
     */
    @TableField(typeHandler = JacksonJsonTypeHandler.class)
    private RewardDto reward;

    public Long getInviterId() {
        return this.inviterId;
    }

    public void setInviterId(Long inviterId) {
        this.inviterId =  inviterId;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId =  userId;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username =  username;
    }

    public String getInviteCode() {
        return this.inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode =  inviteCode;
    }

    public RewardDto getReward() {
        return reward;
    }

    public void setReward(RewardDto reward) {
        this.reward = reward;
    }
}
