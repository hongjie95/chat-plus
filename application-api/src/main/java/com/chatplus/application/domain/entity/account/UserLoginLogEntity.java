package com.chatplus.application.domain.entity.account;

import com.baomidou.mybatisplus.annotation.*;
import com.chatplus.application.datasource.id.IdEntity;
import org.apache.ibatis.type.Alias;


/**
 * <p>Table: t_user_login_log - 用户登录日志</p>
 *
 * @author developer
 */
@Alias("userLoginLog")
@TableName(value = "t_user_login_log", autoResultMap = true)
public class UserLoginLogEntity extends IdEntity {

    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 登录IP
     */
    private String loginIp;
    /**
     * 登录地址
     */
    private String loginAddress;

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId =  userId;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username =  username;
    }

    public String getLoginIp() {
        return this.loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp =  loginIp;
    }

    public String getLoginAddress() {
        return this.loginAddress;
    }

    public void setLoginAddress(String loginAddress) {
        this.loginAddress =  loginAddress;
    }

}
