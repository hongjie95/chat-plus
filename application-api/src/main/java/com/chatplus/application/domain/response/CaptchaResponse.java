package com.chatplus.application.domain.response;

import lombok.Data;

import java.io.Serializable;

/**
 * 验证码信息
 */
@Data
public class CaptchaResponse implements Serializable {

    /**
     * 是否开启验证码
     */
    private Boolean captchaEnabled = true;

    private String uuid;

    /**
     * 验证码图片
     */
    private String img;
}
