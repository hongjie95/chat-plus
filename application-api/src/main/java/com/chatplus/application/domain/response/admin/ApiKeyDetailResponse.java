package com.chatplus.application.domain.response.admin;

import com.chatplus.application.domain.entity.basedata.ApiKeyEntity;
import com.chatplus.application.enumeration.AiPlatformEnum;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiKeyDetailResponse implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * id : 1
     * created_at : 1704776943
     * updated_at : 1704776943
     * platform : XunFei
     * name : 星火讯飞
     * type : chat
     * value : 64cd8af8|36c39920a2c48739d9ced11c9bae4e52|ODc4NTRkZjNhZDA3ZjI1MTZlZTk2ZGY5
     * api_url : wss://spark-api.xf-yun.com/{version}/chat
     * enabled : true
     * use_proxy : false
     * last_used_at : 1705933602
     */

    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;
    @JsonProperty("created_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long createdAt;
    @JsonProperty("updated_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long updatedAt;
    @JsonProperty("platform")
    private AiPlatformEnum platform;
    @JsonProperty("channel")
    private String channel;
    @JsonProperty("type")
    private String type;
    @JsonProperty("value")
    private String value;
    @JsonProperty("api_url")
    private String apiUrl;
    @JsonProperty("enabled")
    private boolean enabled;
    @JsonProperty("use_proxy")
    private boolean useProxy;

    @JsonProperty("proxy_url")
    private String proxyUrl;

    @JsonProperty("last_used_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long lastUsedAt;
    // 预留字段
    @JsonProperty("reserved")
    private String reserved;
    @JsonProperty("notify_url")
    private String notifyUrl;
    public static ApiKeyDetailResponse build(ApiKeyEntity apiKeyEntity) {
        ApiKeyDetailResponse apiKeyDetailResponse = new ApiKeyDetailResponse();
        apiKeyDetailResponse.setId(apiKeyEntity.getId());
        apiKeyDetailResponse.setCreatedAt(apiKeyEntity.getCreatedAt().getEpochSecond());
        apiKeyDetailResponse.setUpdatedAt(apiKeyEntity.getUpdatedAt().getEpochSecond());
        apiKeyDetailResponse.setPlatform(apiKeyEntity.getPlatform());
        apiKeyDetailResponse.setChannel(apiKeyEntity.getChannel());
        apiKeyDetailResponse.setType(apiKeyEntity.getType());
        apiKeyDetailResponse.setValue(apiKeyEntity.getValue());
        apiKeyDetailResponse.setApiUrl(apiKeyEntity.getApiUrl() != null ? apiKeyEntity.getApiUrl() : "");
        apiKeyDetailResponse.setEnabled(apiKeyEntity.getEnabled());
        apiKeyDetailResponse.setUseProxy(apiKeyEntity.getUseProxy());
        apiKeyDetailResponse.setReserved(apiKeyEntity.getReserved());
        apiKeyDetailResponse.setNotifyUrl(apiKeyEntity.getNotifyUrl());
        apiKeyDetailResponse.setLastUsedAt(apiKeyEntity.getLastUsedAt() != null ? apiKeyEntity.getLastUsedAt().getEpochSecond() : null);
        apiKeyDetailResponse.setProxyUrl(apiKeyEntity.getProxyUrl());
        return apiKeyDetailResponse;

    }
}
