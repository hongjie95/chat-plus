package com.chatplus.application.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.io.Serializable;

/**
 * 忘记密码请求参数
 *
 * @author chj
 * @date 2023/6/8
 **/
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "重置密码请求参数")
public class AccountResetPassRequest implements Serializable {

    @JsonProperty("username")
    @NotEmpty(message = "用户名不能为空")
    @Schema(description = "用户名")
    private String username;

    @JsonProperty("code")
    @NotEmpty(message = "验证码不能为空")
    @Schema(description = "验证码")
    private String code;

    @JsonProperty("password")
    @NotEmpty(message = "密码不能为空")
    @Schema(description = "密码")
    private String password;

    @JsonProperty("repass")
    @NotEmpty(message = "密码不能为空")
    @Schema(description = "确认密码")
    private String repass;

}
