package com.chatplus.application.domain.response;

import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;

/**
 * 会话详情响应
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatItemDetailResponse implements Serializable {


    /**
     * id : 26
     * created_at : 0
     * updated_at : 0
     * user_id : 101
     * icon : /images/avatar/gpt.png
     * role_id : 1
     * chat_id : 6d3916d1-0311-487b-af4a-c28a86eab71d
     * model_id : 10
     * title : 帮我优化以下这段代码
     */

    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;
    @JsonProperty("created_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long createdAt;
    @JsonProperty("updated_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long updatedAt;
    @JsonProperty("user_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long userId;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("role_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long roleId;
    @JsonProperty("chat_id")
    private String chatId;
    @JsonProperty("model_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long modelId;
    @JsonProperty("title")
    private String title;

}
