package com.chatplus.application.domain.entity.draw;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.annotation.JsonCollectionGenericType;
import com.chatplus.application.datasource.handler.JacksonJsonTypeHandler;
import com.chatplus.application.datasource.id.IdEntity;
import com.chatplus.application.domain.dto.MJButtonDto;
import com.chatplus.application.enumeration.ImageTaskTypeEnum;
import com.chatplus.application.enumeration.MjJobActionEnum;
import com.chatplus.application.enumeration.MjJobStatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

import java.util.List;

/**
 * <p>Table: t_mj_job - MidJourney 任务表</p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Alias("mjJob")
@TableName(value = "t_mj_job", autoResultMap = true)
public class MjJobEntity extends IdEntity {

    /**
     * 用户 ID
     */
    private Long userId;
    /**
     * 任务 ID
     */
    private String taskId;
    /**
     * 任务类别
     */
    private ImageTaskTypeEnum type;
    /**
     * 任务类型
     */
    private MjJobActionEnum action;
    /**
     * 任务状态
     */
    private MjJobStatusEnum status;
    /**
     * 频道ID
     */
    private String channelId;
    /**
     * 引用消息 ID
     */
    private Long referenceId;
    /**
     * 会话提示词
     */
    private String prompt;
    /**
     * 图片URL
     */
    private String imgUrl;
    /**
     * 原始图片地址
     */
    @TableField(typeHandler = JacksonJsonTypeHandler.class)
    @JsonCollectionGenericType(String.class)
    private List<String> orgUrl;
    /**
     * message hash
     */
    private String hash;
    /**
     * 任务进度
     */
    private Integer progress;

    private Boolean publish;

    private String errMsg;

    private String orgMjUrl;

    @TableField(typeHandler = JacksonJsonTypeHandler.class)
    @JsonCollectionGenericType(MJButtonDto.class)
    private List<MJButtonDto> buttons;

}
