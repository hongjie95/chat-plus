package com.chatplus.application.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 奖励DTO
 *
 * @author chj
 * @date 2024/1/9
 **/
@Data
public class RewardDto implements Serializable {
    /**
     * chat_calls : 0
     * img_calls : 0
     */

    @JsonProperty("power")
    private int power;
}
