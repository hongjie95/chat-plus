package com.chatplus.application.domain.request;

import com.chatplus.application.enumeration.ImageTaskTypeEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * MJ图片绘图任务请求
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Schema(description = "MJ图片绘图任务请求")
public class MjImageJobRequest implements Serializable {


    /**
     * rate : 1:1
     * model :  --v 6
     * chaos : 0
     * stylize : 100
     * seed : 0
     * raw : false
     * img :
     * weight : 0.25
     * prompt : fine art painting, still life, portrait, landscape, abstract, surrealism, impressionism, modern art, realism, watercolor, acrylic, oil painting, mixed media, artist's easel, palette, brush strokes, art studio
     * neg_prompt : 2222
     * tile : false
     * quality : 0
     * session_id : pfr6ulz0rehcqbqhmov1amyxol65svyqtepk48zgbc
     */
    @JsonProperty("task_type")
    @Schema(description = "任务类型,image：文生图，blend：融图，swapFace：换脸")
    private ImageTaskTypeEnum taskType;

    @JsonProperty("rate")
    @Schema(description = "图片比例 --ar")
    private String rate;

    @JsonProperty("model")
    @Schema(description = "--v/--niji 模式。MJ：偏真实通用模型，NIJI：偏动漫通用模型")
    private String model;

    @JsonProperty("chaos")
    @Schema(description = "创意度，参数用法：--chaos 或--c，取值范围: 0-100，取值越高结果越发散，反之则稳定收敛，默认值0最为精准稳定")
    private Integer chaos;

    @JsonProperty("stylize")
    @Schema(description = "风格化，风格化：--stylize 或 --s，范围 1-1000，默认值100，高取值会产生非常艺术化但与提示关联性较低的图像")
    private Integer stylize;

    @JsonProperty("seed")
    @Schema(description = "随机种子,随机种子：--seed，默认值0表示随机产生使用相同的种子参数和描述将产生相似的图像")
    private Integer seed;
    @JsonProperty("raw")
    @Schema(description = "--style raw 原始模式，启用新的RAW模式，呈现的人物写实感更加逼真，人物细节、光源、流畅度也更加接近原始作品。同时也意味着您需要添加更长的提示。")
    private Boolean raw;

    @Schema(description = "垫图：以某张图片为底稿参考来创作绘画支持 PNG 和 JPG 格式图片")
    private String img;

    @JsonProperty("img_arr")
    @Schema(description = " data.Img prompt垫图：以某张图片为底稿参考来创作绘画支持 PNG 和 JPG 格式图片（融图和换脸需要用到多张图片）")
    private List<String> imgArr;

    @JsonProperty("weight")
    @Schema(description = "--iw 图像所占用的权重，范围 0-1，默认值0.25，权重越高，图像越接近提示内容，权重越低，图像越接近原始图像。使用图像权重参数--iw来调整图像 URL 与文本的重要性\n" +
            "权重较高时意味着图像提示将对完成的作业产生更大的影响")
    private Double weight;

    @JsonProperty("prompt")
    @Schema(description = "提示词，输入想要的内容，用逗号分割")
    private String prompt;

    @JsonProperty("neg_prompt")
    @Schema(description = "不希望出现的内容  --no 忽略 --no car 图中不出现车")
    private String negPrompt;

    @JsonProperty("tile")
    @Schema(description = "重复平铺，重复：--tile，参数释义：生成可用作重复平铺的图像，以创建无缝图案。")
    private Boolean tile;

    @JsonProperty("quality")
    @Schema(description = "图片画质 --q，0:默认,0.25:普通,0.5:清晰,1:高清")
    private Double quality;

    @Schema(description = "角色一致性，图片URL")
    private String cref;

    @Schema(description = "风格一致性，图片URL")
    private String sref;

    @Schema(description = "参考权重，取值范围 0-100")
    @Min(value = 0, message = "参考权重最小值为0")
    @Max(value = 100, message = "参考权重最大值为100")
    private Integer cw;

    @JsonProperty("session_id")
    @Schema(description = "会话ID")
    private String sessionId;

    @Schema(description = "Mj Job ID")
    private Long mjJobId;

    @Schema(description = "用户ID")
    private Long userId;

    @Schema(description = "action 索引")
    private Integer index;

    private String messageHash;

    private String handleTaskId;
    /**
     * 引用消息 ID
     */
    private Long referenceId;

    private String customId;
}
