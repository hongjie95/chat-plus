package com.chatplus.application.domain.request.extend;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
@Schema(description = "Dalle3请求")
public class Dalle3Request implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "提示信息")
    private String prompt;

}
