package com.chatplus.application.domain.notification;

import com.chatplus.application.domain.dto.ApiKeyDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

/**
 * 绘画任务JOB
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SdJobNotification implements Serializable {

    private Long sdJobId;

    private ApiKeyDto apiKeyDto;
}
