package com.chatplus.application.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 提示词翻译请求
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description ="提示词翻译请求")
public class PromptApiRequest implements Serializable {


    @Serial
    private static final long serialVersionUID = 1L;
    @Schema(description = "提示词")
    private String prompt;

}
