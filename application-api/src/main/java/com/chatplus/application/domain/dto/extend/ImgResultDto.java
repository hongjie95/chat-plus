package com.chatplus.application.domain.dto.extend;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
public class ImgResultDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 图片地址
     */
    private String url;
    /**
     * 描述
     */
    private String revisedPrompt;

}
