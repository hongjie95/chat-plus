package com.chatplus.application.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 *  图片发布
 * @Author Angus
 * @Date 2024/2/24
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Schema(description = "图片发布请求")
public class ImagePublishRequest implements Serializable {

    /**
     * id : 24
     * action : true
     */

    @JsonProperty("id")
    @Schema(description = "MJ/SD ID")
    private Long id;

    @JsonProperty("action")
    @Schema(description = "发布状态")
    private boolean action;
}
