package com.chatplus.application.domain.response;

import com.chatplus.application.domain.entity.file.FileEntity;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;

/**
 * 产品详情响应
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class FileDetailApiResponse implements Serializable {

    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;
    @JsonProperty("user_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long userId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("obj_key")
    private String objKey;
    @JsonProperty("url")
    private String url;
    @JsonProperty("ext")
    private String ext;
    @JsonProperty("created_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long createdAt;

    public static FileDetailApiResponse build(FileEntity fileEntity) {
        FileDetailApiResponse response = new FileDetailApiResponse();
        response.setId(fileEntity.getId());
        response.setUserId(fileEntity.getUserId());
        response.setName(fileEntity.getOriginalName());
        response.setObjKey(fileEntity.getHashId());
        response.setUrl(fileEntity.getUrl());
        response.setExt(fileEntity.getFileSuffix());
        response.setCreatedAt(fileEntity.getCreatedAt().getEpochSecond());
        return response;
    }
}
