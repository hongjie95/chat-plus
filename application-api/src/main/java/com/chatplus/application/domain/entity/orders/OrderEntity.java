package com.chatplus.application.domain.entity.orders;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.handler.JacksonJsonTypeHandler;
import com.chatplus.application.datasource.id.LogicDeleteEntity;
import com.chatplus.application.domain.dto.OrderRemarkDto;
import com.chatplus.application.enumeration.OrderStatusEnum;
import com.chatplus.application.enumeration.PayChannelEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

import java.math.BigDecimal;
import java.time.Instant;

/**
 * <p>Table: t_order - 充值订单表</p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Alias("order")
@TableName(value = "t_order", autoResultMap = true)
public class OrderEntity extends LogicDeleteEntity {

    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 第三方平台交易流水号
     */
    private String tradeNo;
    /**
     * 产品ID
     */
    private Long productId;
    /**
     * 订单产品
     */
    private String subject;
    /**
     * 订单金额
     */
    private BigDecimal amount;
    /**
     * 订单状态（0：待支付，1：已扫码，2：支付失败）
     */
    private OrderStatusEnum status;
    /**
     * 备注
     */
    @TableField(typeHandler = JacksonJsonTypeHandler.class)
    private  OrderRemarkDto remark;
    /**
     * 支付时间
     */
    private Instant payTime;
    /**
     * 支付方式
     */
    private PayChannelEnum payWay;

}
