package com.chatplus.application.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 *  支付查询请求
 *
 * @author chj
 * @date 2024/1/15
 **/
@Schema(description = "支付查询请求")
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentQueryRequest implements Serializable {

    /**
     * order_no : 202401157152491329413447680
     */

    @JsonProperty("order_no")
    @Schema(description = "订单号")
    private String orderNo;

}
