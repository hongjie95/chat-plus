package com.chatplus.application.domain.entity.chat;

import com.baomidou.mybatisplus.annotation.*;
import com.chatplus.application.datasource.annotation.JsonCollectionGenericType;
import com.chatplus.application.datasource.handler.JacksonJsonTypeHandler;
import com.chatplus.application.datasource.id.IdEntity;
import com.chatplus.application.domain.dto.RoleContextDto;
import org.apache.ibatis.type.Alias;

import java.util.List;


/**
 * <p>Table: t_chat_role - 聊天角色表</p>
 *
 * @author developer
 */
@Alias("chatRole")
@TableName(value = "t_chat_role", autoResultMap = true)
public class ChatRoleEntity extends IdEntity {

    /**
     * 角色名称
     */
    private String name;
    /**
     * 角色标识
     */
    private String marker;
    /**
     * 角色语料 json
     */
    @JsonCollectionGenericType(RoleContextDto.class)
    @TableField(typeHandler = JacksonJsonTypeHandler.class)
    private List<RoleContextDto> context;
    /**
     * 打招呼信息
     */
    private String helloMsg;
    /**
     * 角色图标
     */
    private String icon;
    /**
     * 是否被启用
     */
    private Boolean enable;
    /**
     * 角色排序
     */
    private Integer sortNum;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name =  name;
    }

    public String getMarker() {
        return this.marker;
    }

    public void setMarker(String marker) {
        this.marker =  marker;
    }

    public List<RoleContextDto> getContext() {
        return context;
    }

    public void setContext(List<RoleContextDto> context) {
        this.context = context;
    }

    public String getHelloMsg() {
        return this.helloMsg;
    }

    public void setHelloMsg(String helloMsg) {
        this.helloMsg =  helloMsg;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon) {
        this.icon =  icon;
    }

    public Boolean getEnable() {
        return this.enable;
    }

    public void setEnable(Boolean enable) {
        this.enable =  enable;
    }

    public Integer getSortNum() {
        return this.sortNum;
    }

    public void setSortNum(Integer sortNum) {
        this.sortNum =  sortNum;
    }

}
