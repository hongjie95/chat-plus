package com.chatplus.application.domain.entity.file;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.id.IdEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>Table: t_sys_oss - OSS对象存储表</p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Alias("file")
@TableName(value = "t_file", autoResultMap = true)
public class FileEntity extends IdEntity {
    /**
     * 文件名
     */
    private String filePath;
    /**
     * 原名
     */
    private String originalName;
    /**
     * 文件后缀名
     */
    private String fileSuffix;
    /**
     * 文件类型
     */
    private String contentType;
    /**
     * URL地址
     */
    private String url;
    /**
     * 服务商
     */
    private String service;
    /**
     * 文件hash值，用于去重
     */
    private String hashId;
    /**
     * 用户Id
     * 用户主动传的才设置用户ID
     */
    private Long userId;

}
