package com.chatplus.application.domain.response;

import com.chatplus.application.enumeration.OrderStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 支付查询响应
 **/
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentQueryResponse implements Serializable {

    /**
     * status : 0：未支付 1：已扫码，2：已支付
     */
    @JsonProperty("status")
    private OrderStatusEnum status;

}
