package com.chatplus.application.domain.entity.pay;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.id.LogicDeleteEntity;
import com.chatplus.application.enumeration.AppBusinessCodeEnum;
import com.chatplus.application.enumeration.PayChannelEnum;
import com.chatplus.application.enumeration.PayStatusEnum;
import com.chatplus.application.enumeration.SubModeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.Instant;

/**
 * <p>Table: t_pay_request - 支付请求记录（账单）</p>
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value = "t_pay_request", autoResultMap = true)
public class PayRequestEntity extends LogicDeleteEntity {
    /**
     * 应用ID;
     */
    private String appId;
    /**
     * 业务系统的用户ID
     */
    private Long userId;
    /**
     * 业务系统的业务流水号;记录业务系统的交易流水号
     */
    private Long bizId;
    /**
     * 业务系统的业务类型;记录本次支付业务的业务类型，例如下单支付、补款支付、充值等
     */
    private AppBusinessCodeEnum bizCode;
    /**
     * 账单总金额
     */
    private Long totalMoney;
    /**
     * 渠道交易ID;收银台生成的提交给第三方支付渠道的流水号
     */
    private Long payTransactionId;
    /**
     * 第三方交易ID;微信/支付宝生成的支付流水号
     */
    private String tradeTransactionId;
    /**
     * 平台在渠道系统中的交易账号;公司的支付宝、微信的账户
     */
    private String platformChannelAccount;
    /**
     * 支付渠道
     */
    private PayChannelEnum payChannel;
    /**
     * 支付方式
     */
    private SubModeEnum subMode;
    /**
     * 用户的客户端IP，支持IPv4和IPv6两种格式的IP地址。
     * 示例值：14.23.150.211
     */
    private String clientIp;
    /**
     * 场景类型 示例值：iOS, Android, Wap
     */
    private String sceneType;
    /**
     * 支付账户信息，例如付款人微信openid、支付宝buyer_id
     */
    private String payerInfo;
    /**
     * 支付状态
     */
    private PayStatusEnum payStatus;
    /**
     * 已支付总金额
     */
    private Long paidMoney;
    /**
     * 已退款总金额
     */
    private Long refundedMoney;
    /**
     * 请求时间
     */
    private Instant requestAt;
    /**
     * 成功时间
     */
    private Instant successAt;
    /**
     * 关闭时间
     */
    private Instant closedAt;
    /**
     * 错误码
     */
    private String errCode;
    /**
     * 错误描述
     */
    private String errCodeDes;


}
