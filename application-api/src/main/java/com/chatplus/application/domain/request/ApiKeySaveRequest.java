package com.chatplus.application.domain.request;

import com.chatplus.application.enumeration.AiPlatformEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * ApiKey保存更新请求参数
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "ApiKey保存更新请求参数")
public class ApiKeySaveRequest implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    @JsonProperty("id")
    @Schema(description = "主键，更新时必填")
    private Long id;

    @JsonProperty("platform")
    @Schema(description = "平台")
    private AiPlatformEnum platform;

    @JsonProperty("channel")
    @Schema(description = "渠道")
    private String channel;

    @JsonProperty("type")
    @Schema(description = "key类型：分为chat和image两种")
    private String type;

    @JsonProperty("value")
    @Schema(description = "值")
    private String value;

    @JsonProperty("api_url")
    @Schema(description = "api地址")
    private String apiUrl;

    @JsonProperty("enabled")
    @Schema(description = "是否启用")
    private boolean enabled;

    @JsonProperty("use_proxy")
    @Schema(description = "是否使用代理")
    private boolean useProxy;

    @JsonProperty("proxy_url")
    @Schema(description = "代理地址")
    private String proxyUrl;

    @JsonProperty("reserved")
    @Schema(description = "保留字段")
    private String reserved;

    @JsonProperty("notify_url")
    @Schema(description = "回调通知地址，比如MJ绘图就需要填这里")
    private String notifyUrl;

}
