package com.chatplus.application.domain.request.admin;

import com.chatplus.application.domain.request.PlusPageRequest;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 用户会话查询
 *
 * @author chj
 * @date 2024/3/1
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "用户会话查询")
public class ChatItemQueryRequest extends PlusPageRequest implements Serializable {
    /**
     * title : 22
     * created_at : ["2024-03-01","2024-03-16"]
     * page : 1
     * page_size : 15
     * user_id : 22
     */
    @JsonProperty("title")
    @Schema(description = "标题")
    private String title;

    @JsonProperty("user_id")
    @Schema(description = "用户ID")
    private Long userId;
    // 日期范围，格式：yyyy-MM-dd
    @JsonProperty("created_at")
    @Schema(description = "创建时间", example = "[\"2024-03-01\",\"2024-03-16\"]")
    private List<String> createdAt;
}
