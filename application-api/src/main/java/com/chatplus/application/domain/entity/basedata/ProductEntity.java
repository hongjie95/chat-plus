package com.chatplus.application.domain.entity.basedata;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.id.IdEntity;
import com.chatplus.application.enumeration.ProductTypeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

import java.math.BigDecimal;

/**
 * <p>Table: t_product - 会员套餐表</p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Alias("product")
@TableName(value = "t_product", autoResultMap = true)
public class ProductEntity extends IdEntity {
    /**
     * 名称
     */
    private String name;
    /**
     * 1:月卡，2:次卡
     */
    private ProductTypeEnum productType;
    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 优惠金额
     */
    private BigDecimal discount;
    /**
     * 有效天数
     * <p>0表示长期有效</p>
     */
    private Integer days;
    /**
     * 算力
     */
    private Integer power;
    /**
     * 如果是订阅卡的话，每日额度(备用字段，暂时未使用到)
     */
    private Integer vipDayPower;
    /**
     * 是否启动
     */
    private Boolean enabled;
    /**
     * 销量
     */
    private Integer sales;
    /**
     * 排序
     */
    private Integer sortNum;

}
