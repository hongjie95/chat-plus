package com.chatplus.application.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * 会话更新请求
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "会话更新请求")
public class ChatItemUpdateRequest implements Serializable {

    /**
     * chat_id : ba1c1202-796e-4a96-9ccf-62f6ad7a82e9
     * title : 测试
     */
    @JsonProperty("chat_id")
    @Schema(description = "会话ID")
    private String chatId;

    @JsonProperty("title")
    @Schema(description = "会话标题")
    private String title;

}
