package com.chatplus.application.domain.request;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;

/**
 * 用户保存更新请求参数
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description ="用户保存更新请求参数")
public class AccountSaveRequest  implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    @Schema(description ="用户ID，更新时候必传")
    private Long id;
    @JsonProperty("username")
    @Schema(description ="用户登录名，可以为邮箱或者手机号")
    private String username;
    @JsonProperty("password")
    @Schema(description ="用户登录密码")
    private String password;
    @JsonProperty("power")
    @Schema(description = "算力")
    private Integer power;
    @JsonProperty("expired_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @Schema(description ="有效期")
    private Instant expiredTime;
    @JsonProperty("status")
    @Schema(description = "启用状态", defaultValue = "false")
    private boolean status = false;
    @JsonProperty("admin")
    @Schema(description = "是否管理员", defaultValue = "false")
    private boolean admin = false;
    @JsonProperty("chat_roles")
    @Schema(description ="聊天角色")
    private List<String> chatRoles;
    @JsonProperty("chat_models")
    @Schema(description ="模型权限")
    private List<String> chatModels;

}
