package com.chatplus.application.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserCustomChatConfigDto implements Serializable {


    /**
     * api_keys : {"Azure":"","OpenAI":"","ChatGLM":""}
     */

    @JsonProperty("api_keys")
    private ApiKeysBean apiKeys = new ApiKeysBean("","","");

    public ApiKeysBean getApiKeys() {
        return apiKeys;
    }

    public void setApiKeys(ApiKeysBean apiKeys) {
        this.apiKeys = apiKeys;
    }
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ApiKeysBean implements Serializable{

        public ApiKeysBean() {
        }
        public ApiKeysBean(String azure, String openAI, String chatGLM) {
            this.azure = azure;
            this.openAI = openAI;
            this.chatGLM = chatGLM;
        }
        /**
         * Azure :
         * OpenAI :
         * ChatGLM :
         */
        @JsonProperty("Azure")
        private String azure;
        @JsonProperty("OpenAI")
        private String openAI;
        @JsonProperty("ChatGLM")
        private String chatGLM;

        public String getAzure() {
            return azure;
        }

        public void setAzure(String azure) {
            this.azure = azure;
        }

        public String getOpenAI() {
            return openAI;
        }

        public void setOpenAI(String openAI) {
            this.openAI = openAI;
        }

        public String getChatGLM() {
            return chatGLM;
        }

        public void setChatGLM(String chatGLM) {
            this.chatGLM = chatGLM;
        }
    }
}
