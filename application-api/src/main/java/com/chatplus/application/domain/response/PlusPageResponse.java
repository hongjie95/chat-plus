package com.chatplus.application.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 自定义分页请求参数
 *
 * @author chj
 * @date 2024/1/9
 **/
@Data
public class PlusPageResponse<T> implements Serializable {


    /**
     * items : []
     * page : 1
     * page_size : 10
     * total : 1
     * total_page : 1
     */
    @JsonProperty("page")
    private Long page;
    @JsonProperty("page_size")
    private Long pageSize;
    @JsonProperty("total")
    private Long total;
    @JsonProperty("total_page")
    private Long totalPage;
    @JsonProperty("items")
    private List<T> items;

    public PlusPageResponse(long currentPageNum, long currentPageSize, long totalPages, long totalElements, List<T> elementList) {

        this.page = currentPageNum;
        this.pageSize = currentPageSize;
        this.total = totalElements;
        this.totalPage = totalPages;
        this.items = elementList;
    }

}
