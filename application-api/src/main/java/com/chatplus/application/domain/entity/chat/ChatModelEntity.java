package com.chatplus.application.domain.entity.chat;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.id.IdEntity;
import com.chatplus.application.enumeration.AiPlatformEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;


/**
 * <p>Table: t_chat_model - AI 模型表</p>
 *
 * @Author Angus
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Alias("chatModel")
@TableName(value = "t_chat_model", autoResultMap = true)
public class ChatModelEntity extends IdEntity {

    /**
     * 模型平台
     */
    private AiPlatformEnum platform;
    /**
     * 渠道
     */
    private String channel;
    /**
     * 模型名称
     */
    private String name;
    /**
     * 模型值
     */
    private String value;
    /**
     * 模型描述
     */
    private String description;
    /**
     * 排序数字
     */
    private Integer sortNum;
    /**
     * 是否启用模型
     */
    private Boolean enabled;
    /**
     * 对话权重，每次对话扣减多少次对话额度
     */
    private Integer power;
    /**
     * 是否开放模型
     */
    private Boolean open;
    /**
     * 是否启用函数功能
     */
    private Boolean enabledFunction;
    /**
     * 最大响应长度
     */
    private Integer maxTokens;
    /**
     * 最大上下文长度
     */
    private Integer maxContext;
    /**
     * 创意度
     */
    private Float temperature;

}
