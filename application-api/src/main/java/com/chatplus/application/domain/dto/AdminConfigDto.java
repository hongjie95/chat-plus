package com.chatplus.application.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 系统基础配置
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class AdminConfigDto implements Serializable {

    // 控制台标题
    @JsonProperty("admin_title")
    private String adminTitle;
    // 后端基础地址
    @JsonProperty("base_backend_url")
    private String baseBackendUrl;
    @JsonProperty("base_frontend_url")
    private String baseFrontendUrl;
    // 前端网页标题
    private String logo;

    private String title;

    @JsonProperty("context_deep")
    private Integer contextDeep;

    @JsonProperty("enable_context")
    private boolean enableContext;

    @JsonProperty("enabled_register")
    private boolean enabledRegister;

    @JsonProperty("enabled_reward")
    private boolean enabledReward;

    @JsonProperty("enabled_bot_validate")
    private boolean enabledBotValidate;

    @JsonProperty("power_price")
    private double powerPrice;

    @JsonProperty("mj_power")
    private Integer mjPower;

    @JsonProperty("mj_action_power")
    private Integer mjActionPower;

    @JsonProperty("sd_power")
    private Integer sdPower;

    @JsonProperty("dall_power")
    private Integer dallPower;

    // 初始用户调用次数
    @JsonProperty("init_power")
    private Integer initPower;
    // 邀请用户奖励次数
    @JsonProperty("invite_power")
    private Integer invitePower;
    // 每日赠送
    @JsonProperty("daily_power")
    private Integer dailyPower;

    // 会员月卡调用次数
    @JsonProperty("vip_default_day_power")
    private Integer vipDefaultDayPower;

    @JsonProperty("order_pay_timeout")
    private Long orderPayTimeout;

    @JsonProperty("reward_img")
    private String rewardImg;

    @JsonProperty("wechat_card_url")
    private String wechatCardUrl;

    @JsonProperty("default_models")
    private List<String> defaultModels;
    // wechat  mobile email
    @JsonProperty("register_ways")
    private List<String> registerWays;

    @JsonProperty("enabled_sensitive_word")
    private boolean enabledSensitiveWord;

    @JsonProperty("vip_info_text")
    private String vipInfoText;
}
