package com.chatplus.application.domain.entity.functions;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.handler.JacksonJsonTypeHandler;
import com.chatplus.application.datasource.id.IdEntity;
import com.chatplus.application.domain.dto.FunctionParametersDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>Table: t_function - 函数插件表</p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Alias("function")
@TableName(value = "t_function", autoResultMap = true)
public class FunctionEntity extends IdEntity {

    /**
     * 函数名称
     */
    private String name;
    /**
     * 函数标签
     */
    private String label;
    /**
     * 函数描述
     */
    private String description;
    /**
     * 函数参数（JSON）
     */
    @TableField(typeHandler = JacksonJsonTypeHandler.class)
    private FunctionParametersDto parameters;
    /**
     * 函数处理 API
     */
    private String action;
    /**
     * 函数请求方法
     */
    private String requestMethod;
    /**
     * 是否启用
     */
    private Boolean enabled;
    /**
     * API授权token
     */
    private String token;

}
