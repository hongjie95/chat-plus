package com.chatplus.application.domain.request;

import com.chatplus.application.common.crypto.CryptoValue;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 登录请求信息
 */
@Data
@Schema(description = "登录请求参数")
public class AccountLoginRequest implements  Serializable {
    @Serial
    private static final long serialVersionUID = -3081352005273150641L;
    /**
     * 登录号
     */
    @NotBlank(message = "登录号不能为空")
    @Schema(description = "登录号")
    private String username;
    /**
     * 短信验证码
     */
    @Schema(description = "短信验证码")
    private String verifyCode;
    /**
     * 登录密码（预留，当前未使用）
     */
    @Schema(description = "登录密码")
    private String password;

    /**
     * 加密密码串（预留，当前未使用）
     */
    @Schema(description = "加密密码串")
    private CryptoValue cryptoPassword;

    /**
     * 验证码
     */
    @Schema(description = "验证码")
    private String code;
    /**
     * 验证码唯一标识
     */
    @Schema(description = "验证码唯一标识")
    private String uuid;

    /**
     * 是否记住我（用来决定登录cookie失效时间）
     */
    @Schema(description = "是否记住我")
    private Boolean rememberMe;

}
