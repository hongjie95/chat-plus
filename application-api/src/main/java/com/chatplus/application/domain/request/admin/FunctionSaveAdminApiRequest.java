package com.chatplus.application.domain.request.admin;

import com.chatplus.application.domain.dto.FunctionParametersDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 函数保存请求
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "函数保存请求")
public class FunctionSaveAdminApiRequest implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * id : 1
     * name : weibo
     * label : 微博热搜
     * description : 新浪微博热搜榜，微博当日热搜榜单
     * parameters : {"type":"object","properties":{}}
     * action : http://localhost:5678/api/function/weibo
     * token : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcmVkIjowLCJ1c2VyX2lkIjowfQ.tLAGkF8XWh_G-oQzevpIodsswtPByBLoAZDz_eWuBg
     * enabled : true
     */

    @JsonProperty("id")
    @Schema(description = "函数ID，新增时不传，更新时传")
    private Long id;

    @JsonProperty("name")
    @Schema(description = "函数名称")
    private String name;

    @JsonProperty("label")
    @Schema(description = "函数标签")
    private String label;

    @JsonProperty("description")
    @Schema(description = "函数描述")
    private String description;

    @JsonProperty("parameters")
    @Schema(description = "函数参数")
    private FunctionParametersDto parameters;

    @JsonProperty("action")
    @Schema(description = "函数请求地址")
    private String action;

    @JsonProperty("token")
    @Schema(description = "函数请求token")
    private String token;

    @JsonProperty("enabled")
    @Schema(description = "函数是否启用")
    private boolean enabled;

    @JsonProperty("request_method")
    @Schema(description = "函数请求方法", example = "GET")
    private String requestMethod;
}
