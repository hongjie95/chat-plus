package com.chatplus.application.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @Description
 * @Author Angus
 * @Date 2024/3/12
 */
@Data
public class TextPromptsDto implements Serializable {

    /**
     * text : A lighthouse on a cliff
     * weight : 1
     */

    @JsonProperty("text")
    private String text;
    @JsonProperty("weight")
    private int weight;
}
