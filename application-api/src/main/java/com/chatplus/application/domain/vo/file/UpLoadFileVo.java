package com.chatplus.application.domain.vo.file;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

/**
 * 文件上传响应
 *
 * @author Administrator
 */
@Data
public class UpLoadFileVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 3219394916639164853L;

    public UpLoadFileVo(Boolean success, String id, String originalName,String hashId, String url, String contentType, String fileSuffix) {
        this.success = success;
        this.id = id;
        this.originalName = originalName;
        this.hashId = hashId;
        this.url = url;
        this.contentType = contentType;
        this.fileSuffix = fileSuffix;
    }

    public UpLoadFileVo(Boolean success) {
        this.success = success;
    }

    /**
     * 文件上传是否成功
     */
    private Boolean success;

    /**
     * 文件上传成功后的 文件标识
     */
    private String id;

    /**
     * 原名
     */
    private String originalName;

    /**
     * 文件上传成功后的 文件标识
     */
    private String hashId;

    /**
     * 文件上传成功后的访问你地址
     */
    private String url;

    /**
     * 文件类型标识
     */
    private String contentType;

    /**
     * 文件后缀名
     */
    private String fileSuffix;

    /**
     * 文件上传时间
     */
    private Date uploadedAt;


}
