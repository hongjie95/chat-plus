package com.chatplus.application.domain.response;

import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * 支付响应请求
 **/
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "支付响应请求")
public class QrCodePayResponse implements Serializable {

    @JsonProperty("image")
    @Schema(description = "base64图片")
    private String image;

    @JsonProperty("order_no")
    @JsonSerialize(using = Int64AsString.class)
    @Schema(description = "订单ID")
    private Long orderNo;

    @JsonProperty("url")
    @Schema(description = "扫码请求链接")
    private String url;

    //////////////////  微信JSAPI 支付参数 start //////////////////
    private String appId;
    /**
     * 由于package为java保留关键字，因此改为packageValue. 前端使用时记得要更改为package
     */
    @Schema(description = "由于package为java保留关键字，因此改为packageValue. 前端使用时记得要更改为package")
    private String packageValue;

    /**
     * 时间戳
     */
    @Schema(description = "时间戳")
    private String timeStamp;

    /**
     * 随机字符串
     */
    @Schema(description = "随机字符串")
    private String nonceStr;

    /**
     * 签名方式
     */
    @Schema(description = "签名方式")
    private String signType;

    /**
     * 签名
     */
    @Schema(description = "签名")
    private String paySign;
    //////////////////  微信JSAPI 支付参数 end //////////////////

}
