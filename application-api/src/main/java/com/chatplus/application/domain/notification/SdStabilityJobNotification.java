package com.chatplus.application.domain.notification;

import com.chatplus.application.domain.request.SdStabilityImageRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 绘画任务JOB
 **/
@EqualsAndHashCode(callSuper = true)
@Data
public class SdStabilityJobNotification extends SdJobNotification implements Serializable {

    private SdStabilityImageRequest request;

}
