package com.chatplus.application.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.io.Serializable;

/**
 * 重置密码请求参数
 *
 * @author chj
 * @date 2023/6/8
 **/
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "重置密码请求参数")
public class AccountUpdatePassRequest implements Serializable {

    @JsonProperty("old_pass")
    @NotEmpty(message = "旧密码不能为空")
    @Schema(description = "旧密码")
    private String oldPass;

    @JsonProperty("password")
    @NotEmpty(message = "密码不能为空")
    @Schema(description = "新密码")
    private String password;

    @JsonProperty("repass")
    @NotEmpty(message = "密码不能为空")
    @Schema(description = "确认密码")
    private String repass;

}
