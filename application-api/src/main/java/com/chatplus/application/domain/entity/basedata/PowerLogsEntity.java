package com.chatplus.application.domain.entity.basedata;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.id.IdEntity;
import com.chatplus.application.enumeration.AiPlatformEnum;
import com.chatplus.application.enumeration.FundTypeEnum;
import com.chatplus.application.enumeration.PowerLogTypeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>Table: t_power_logs - 用户算力消费日志</p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Alias("powerLogs")
@TableName(value = "t_power_logs", autoResultMap = true)
public class PowerLogsEntity extends IdEntity {

    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 类型（1：充值，2：消费，3：退费）
     */
    private PowerLogTypeEnum type;
    /**
     * 算力数值
     */
    private Integer amount;
    /**
     * 余额
     */
    private Integer balance;
    /**
     * 模型
     */
    private String model;
    /**
     * 备注
     */
    private String remark;
    /**
     * 资金类型（0：支出，1：收入）
     */
    private FundTypeEnum mark;
    /**
     * 平台
     */
    private AiPlatformEnum platform;

}
