package com.chatplus.application.domain.request.admin;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 用户会话历史查询
 *
 * @author chj
 * @date 2024/3/1
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "用户会话历史查询")
public class ChatHistoryQueryRequest extends ChatItemQueryRequest implements Serializable {
    /**
     * title :
     * created_at : ["2024-03-01","2024-04-14"]
     * page : 1
     * page_size : 15
     * user_id : 11
     * content : 11
     * model : 11
     */
    @Schema(description = "会话内容")
    @JsonProperty("content")
    private String content;
    @Schema(description = "模型值")
    @JsonProperty("model")
    private String model;
}
