package com.chatplus.application.domain.notification;

import com.chatplus.application.domain.dto.ApiKeyDto;
import com.chatplus.application.domain.dto.MJJobDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

/**
 * 绘画任务JOB
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class MJJobNotification implements Serializable {


    private Long mjJobId;

    private ApiKeyDto apiKeyDto;

    private MJJobDto mjJobDto;
}
