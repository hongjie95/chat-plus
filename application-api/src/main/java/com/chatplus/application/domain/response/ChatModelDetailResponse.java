package com.chatplus.application.domain.response;

import com.chatplus.application.enumeration.AiPlatformEnum;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;

/**
 * 模型详情响应
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatModelDetailResponse implements Serializable {

    /**
     * id : 1
     * created_at : 1692763596
     * updated_at : 1703482677
     * platform : OpenAI
     * name : gpt-3.5-turbo-16k
     * value : gpt-3.5-turbo-16k
     * enabled : true
     * sort_num : 0
     * weight : 3
     * open : true
     */

    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;
    @JsonProperty("created_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long createdAt;
    @JsonProperty("updated_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long updatedAt;
    @JsonProperty("platform")
    private AiPlatformEnum platform;
    @JsonProperty("channel")
    private String channel;
    @JsonProperty("name")
    private String name;
    @JsonProperty("value")
    private String value;
    @JsonProperty("enabled")
    private boolean enabled;
    @JsonProperty("sort_num")
    private int sortNum;
    @JsonProperty("power")
    private int power;
    @JsonProperty("open")
    private boolean open;

    @JsonProperty("description")
    private String description;
    /**
     * 是否启用函数功能
     */
    @JsonProperty("enabled_function")
    private Boolean enabledFunction;
    /**
     * 最大响应长度
     */
    @JsonProperty("max_tokens")
    private Integer maxTokens;
    /**
     * 最大上下文长度
     */
    @JsonProperty("max_context")
    private Integer maxContext;
    /**
     * 创意度
     */
    @JsonProperty("temperature")
    private Float temperature;
}
