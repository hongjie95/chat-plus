package com.chatplus.application.domain.request;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 用户保存更新请求参数
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "用户保存更新请求参数")
public class AccountUpdateRequest extends AccountSaveRequest implements Serializable {

    @Schema(description = "用户头像")
    private String avatar;

    @Schema(description = "用户昵称")
    private String nickname;

    @Schema(description = "验证码")
    private String code;

}
