package com.chatplus.application.domain.response;

import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;

/**
 * 邀请码响应
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class InviteCodeDetailResponse implements Serializable {


    /**
     * id : 1
     * user_id : 101
     * code : 58BJX7Y4
     * hits : 2
     * reg_num : 0
     * created_at : 0
     */

    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;
    @JsonProperty("user_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long userId;
    @JsonProperty("code")
    private String code;
    @JsonProperty("hits")
    private int hits;
    @JsonProperty("reg_num")
    private int regNum;

}
