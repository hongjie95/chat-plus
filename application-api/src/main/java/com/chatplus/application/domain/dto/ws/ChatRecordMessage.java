package com.chatplus.application.domain.dto.ws;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * Chat记录消息体
 */
@Data
@Builder
public class ChatRecordMessage implements Serializable {
    private String prompt;
    /**
     * 消息内容,最新的一条是没有reply的
     */
    private String reply;

    private String system;
}
