package com.chatplus.application.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 任务完成后的可执行按钮
 *
 * @author chj
 * @date 2024/3/7
 **/
@Schema(description = "任务完成后的可执行按钮")
@Data
public class MJButtonDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "动作标识", example = "MJ::JOB::upsample::1::85a4b4c1-8835-46c5-a15c-aea34fad1862")
    @JsonProperty("customId")
    private String customId;

    @Schema(description = "图标")
    private String emoji;

    @Schema(description = "文本",example = "Make Variations")
    private String label;

    @Schema(description = "类型，系统内部使用")
    private String style;

    @Schema(description = "样式: 2（Primary）、3（Green）")
    private String type;
}
