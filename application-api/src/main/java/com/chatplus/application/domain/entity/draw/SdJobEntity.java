package com.chatplus.application.domain.entity.draw;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.id.IdEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;


/**
 * <p>Table: t_sd_job - Stable Diffusion 任务表</p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Alias("sdJob")
@TableName(value = "t_sd_job", autoResultMap = true)
public class SdJobEntity extends IdEntity {

    private String taskId;
    /**
     * 用户 ID
     */
    private Long userId;
    /**
     * 任务类别
     */
    private String type;
    /**
     * 会话提示词
     */
    private String prompt;
    /**
     * 图片URL
     */
    private String imgUrl;
    /**
     * 绘画参数json
     */
    private String params;
    /**
     * 任务进度
     */
    private Integer progress;

    private Boolean publish;
}
