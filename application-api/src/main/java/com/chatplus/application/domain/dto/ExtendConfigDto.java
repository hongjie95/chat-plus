package com.chatplus.application.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 第三方api配置
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class ExtendConfigDto implements Serializable {
    // 百度翻译配置
    @JsonProperty("enable_baidu_trans")
    private Boolean enableBaiduTrans;

    @JsonProperty("baidu_trans_app_id")
    private String baiduTransAppId;

    @JsonProperty("baidu_trans_security_key")
    private String baiduTransSecurityKey;

    // 短信配置
    @JsonProperty("enable_sms_captcha")
    private Boolean enableSms;

    @JsonProperty("sms_mock")
    private Boolean smsMock;

    @JsonProperty("sms_supplier")
    private String smsSupplier;

    @JsonProperty("sms_request_url")
    private String smsRequestUrl;

    @JsonProperty("sms_access_key_id")
    private String smsAccessKeyId;

    @JsonProperty("sms_access_key_secret")
    private String smsAccessKeySecret;

    @JsonProperty("sms_signature")
    private String smsSignature;

    @JsonProperty("sms_template_id")
    private String smsTemplateId;

    @JsonProperty("sms_sdk_app_id")
    private String smsSdkAppId;

    @JsonProperty("sms_territory")
    private String smsTerritory;

    // 邮箱配置
    @JsonProperty("enable_mail")
    private Boolean enableMail;

    @JsonProperty("mail_mock")
    private Boolean mailMock;

    @JsonProperty("mail_host")
    private String mailHost;

    @JsonProperty("mail_port")
    private Integer mailPort;

    @JsonProperty("mail_enable_auth")
    private Boolean mailEnableAuth;

    @JsonProperty("mail_user")
    private String mailUser;

    @JsonProperty("mail_password")
    private String mailPassword;

    @JsonProperty("mail_from")
    private String mailFrom;

    @JsonProperty("mail_enable_starttls")
    private Boolean mailEnableStarttls;

    @JsonProperty("mail_enable_ssl")
    private Boolean mailEnableSsl;

    @JsonProperty("mail_timeout")
    private Integer mailTimeout;

    @JsonProperty("mail_connection_timeout")
    private Integer mailConnectionTimeout;
    // 支付宝配置
    @JsonProperty("enable_alipay")
    private Boolean enableAlipay;

    @JsonProperty("alipay_protocol")
    private String alipayProtocol;

    @JsonProperty("alipay_gateway_host")
    private String alipayGatewayHost;

    @JsonProperty("alipay_app_id")
    private String alipayAppId;

    @JsonProperty("alipay_sign_type")
    private String alipaySignType;

    @JsonProperty("alipay_merchant_private_key")
    private String alipayMerchantPrivateKey;

    @JsonProperty("alipay_merchant_cert_path")
    private String alipayMerchantCertPath;

    @JsonProperty("alipay_alipay_cert_path")
    private String alipayAlipayCertPath;

    @JsonProperty("alipay_alipay_root_cert_path")
    private String alipayAlipayRootCertPath;

    @JsonProperty("alipay_notify_url")
    private String alipayNotifyUrl;

    @JsonProperty("alipay_encrypt_key")
    private String alipayEncryptKey;

    @JsonProperty("alipay_sign_provider")
    private String alipaySignProvider;
    // 微信支付配置
    @JsonProperty("enable_wechat_pay")
    private Boolean enableWechatPay;

    @JsonProperty("wechat_pay_app_id")
    private String wechatPayAppId;

    @JsonProperty("wechat_pay_mch_id")
    private String wechatPayMchId;

    @JsonProperty("wechat_pay_mch_key")
    private String wechatPayMchKey;

    @JsonProperty("wechat_pay_api_v3_key")
    private String wechatPayApiV3Key;

    @JsonProperty("wechat_pay_private_key")
    private String wechatPayPrivateKey;

    @JsonProperty("wechat_pay_private_cert")
    private String wechatPayPrivateCert;

    @JsonProperty("wechat_pay_notify_url")
    private String wechatPayNotifyUrl;

    // 微信公众号配置
    @JsonProperty("enable_wechat_mp")
    private Boolean enableWechatMp;

    @JsonProperty("wechat_mp_app_id")
    private String wechatMpAppId;

    @JsonProperty("wechat_mp_app_secret")
    private String wechatMpAppSecret;

    @JsonProperty("wechat_mp_token")
    private String wechatMpToken;

    @JsonProperty("wechat_mp_aes_key")
    private String wechatMpAesKey;

    @JsonProperty("wechat_mp_notify_url")
    private String wechatMpNotifyUrl;
    
}
