package com.chatplus.application.domain.request.admin;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 菜单排序请求
 *
 * @author chj
 * @date 2024/4/10
 **/
@Schema(description = "菜单排序请求")
@Data
public class MenuSortRequest implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @NotEmpty(message = "ids不能为空")
    @Schema(description = "菜单ID列表")
    private List<Long> ids;

    @NotEmpty(message = "sorts不能为空")
    @Schema(description = "排序列表")
    private List<Integer> sorts;
}
