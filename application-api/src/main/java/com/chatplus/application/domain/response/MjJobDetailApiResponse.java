package com.chatplus.application.domain.response;

import com.chatplus.application.domain.dto.MJButtonDto;
import com.chatplus.application.domain.entity.draw.MjJobEntity;
import com.chatplus.application.enumeration.ImageTaskTypeEnum;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import org.apache.commons.collections4.CollectionUtils;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

/**
 * Mj任务详情响应
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class MjJobDetailApiResponse implements Serializable {
    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;

    @JsonProperty("type")
    private ImageTaskTypeEnum type;

    @JsonProperty("user_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long userId;

    @JsonProperty("task_id")
    private String taskId;

    @JsonProperty("message_id")
    private String messageId;

    @JsonProperty("message_hash")
    private String messageHash;

    @JsonProperty("img_url")
    private String imgUrl;

    @JsonProperty("progress")
    private Integer progress;

    @JsonProperty("prompt")
    private String prompt;

    @JsonProperty("err_msg")
    private String errMsg;

    @JsonProperty("publish")
    private Boolean publish;

    @JsonProperty("created_at")
    private Instant createdAt;

    @JsonProperty("can_opt")
    private Boolean canOpt = false;

    @JsonProperty("buttons")
    private List<MJButtonDto> buttons;

    public static MjJobDetailApiResponse build(MjJobEntity mjJobEntity) {
        MjJobDetailApiResponse sdJobDetailApiResponse = new MjJobDetailApiResponse();
        sdJobDetailApiResponse.setId(mjJobEntity.getId());
        sdJobDetailApiResponse.setType(mjJobEntity.getType());
        sdJobDetailApiResponse.setUserId(mjJobEntity.getUserId());
        sdJobDetailApiResponse.setTaskId(mjJobEntity.getTaskId());
        sdJobDetailApiResponse.setImgUrl(mjJobEntity.getImgUrl());
        sdJobDetailApiResponse.setProgress(mjJobEntity.getProgress());
        sdJobDetailApiResponse.setPrompt(mjJobEntity.getPrompt());
        sdJobDetailApiResponse.setPublish(mjJobEntity.getPublish());
        sdJobDetailApiResponse.setCreatedAt(mjJobEntity.getCreatedAt());
        sdJobDetailApiResponse.setMessageHash(mjJobEntity.getHash());
        sdJobDetailApiResponse.setErrMsg(mjJobEntity.getErrMsg());
        List<MJButtonDto> buttonDtoList = mjJobEntity.getButtons();
        if (CollectionUtils.isNotEmpty(buttonDtoList)) {
            buttonDtoList.stream().filter(item -> {
                String customId = item.getCustomId();
                return customId.startsWith("MJ::JOB::variation::") || customId.startsWith("MJ::JOB::upsample::");
            }).findFirst().ifPresent(mjButtonDto -> sdJobDetailApiResponse.setCanOpt(true));
        }
        //sdJobDetailApiResponse.setButtons(mjJobEntity.getButtons());
        return sdJobDetailApiResponse;
    }
}
