package com.chatplus.application.domain.response;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * 三方登录对象
 *
 * @author Lion Li
 */

@Data
public class SocialLoginBody {

    /**
     * 第三方登录平台
     */
    @NotBlank(message = "{social.source.not.blank}")
    private String source;

    /**
     * 第三方登录code
     */
    @NotBlank(message = "{social.code.not.blank}")
    private String socialCode;

    /**
     * 第三方登录socialState
     */
    @NotBlank(message = "{social.state.not.blank}")
    private String socialState;

}
