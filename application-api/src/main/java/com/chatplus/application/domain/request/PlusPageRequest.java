package com.chatplus.application.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;

/**
 * 自定义分页请求参数
 *
 * @author chj
 * @date 2024/1/9
 **/
@Schema(description = "自定义分页请求参数")
@Data
public class PlusPageRequest implements Serializable {
    /**
     * page : 1
     * page_size : 10
     */

    @JsonProperty("page")
    @Schema(description = "页码")
    private int page;

    @JsonProperty("page_size")
    @Schema(description = "每页数量")
    private int pageSize;

}
