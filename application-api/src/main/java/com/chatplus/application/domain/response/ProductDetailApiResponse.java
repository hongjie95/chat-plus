package com.chatplus.application.domain.response;

import com.chatplus.application.domain.entity.basedata.ProductEntity;
import com.chatplus.application.enumeration.ProductTypeEnum;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 产品详情响应
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDetailApiResponse implements Serializable {


    /**
     * id : 1
     * created_at : 1693190937
     * updated_at : 1699435327
     * name : 会员1个月
     * price : 19.9
     * discount : 10
     * days : 30
     * calls : 0
     * img_calls : 0
     * enabled : true
     * sales : 3
     * sort_num : 0
     */
    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;
    @JsonProperty("created_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long createdAt;
    @JsonProperty("updated_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long updatedAt;
    @JsonProperty("name")
    private String name;
    @JsonProperty("price")
    private BigDecimal price;
    @JsonProperty("discount")
    private BigDecimal discount;
    @JsonProperty("days")
    private int days;
    @JsonProperty("power")
    private Integer power;

    @JsonProperty("vip_day_power")
    private Integer vipDayPower;

    @JsonProperty("enabled")
    private boolean enabled;
    @JsonProperty("sales")
    private int sales;
    @JsonProperty("sort_num")
    private int sortNum;
    @JsonProperty("product_type")
    private ProductTypeEnum productType;
    public static ProductDetailApiResponse build(ProductEntity product) {
        ProductDetailApiResponse response = new ProductDetailApiResponse();
        response.setId(product.getId());
        response.setName(product.getName());
        response.setPrice(product.getPrice());
        response.setDiscount(product.getDiscount());
        response.setDays(product.getDays());
        response.setPower(product.getPower());
        response.setVipDayPower(product.getVipDayPower());
        response.setSales(product.getSales());
        response.setCreatedAt(product.getCreatedAt().getEpochSecond());
        response.setUpdatedAt(product.getUpdatedAt().getEpochSecond());
        response.setProductType(product.getProductType());
        response.setEnabled(product.getEnabled());
        return response;
    }
}
