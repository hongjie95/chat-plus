package com.chatplus.application.domain.response;

import com.chatplus.application.domain.dto.RewardDto;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;

/**
 * 邀请码分页请求返回的数据
 *
 * @author chj
 * @date 2024/1/9
 **/
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class InviteCodeListResponse implements Serializable {

    /**
     * id : 1
     * inviter_id : 100
     * user_id : 101
     * username : 杰哥
     * invite_code : 7RIOG62L
     * reward : {"chat_calls":0,"img_calls":0}
     * created_at : 1704793001
     */
    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;
    @JsonProperty("inviter_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long inviterId;
    @JsonProperty("user_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long userId;
    @JsonProperty("username")
    private String username;
    @JsonProperty("invite_code")
    private String inviteCode;
    @JsonProperty("reward")
    private RewardDto reward;
    @JsonProperty("created_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long createdAt;

}
