package com.chatplus.application.domain.response;

import com.chatplus.application.domain.dto.RoleContextDto;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 角色详情响应
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatRoleDetailResponse implements Serializable {


    /**
     * id : 1
     * created_at : 0
     * updated_at : 0
     * key : gpt
     * name : 通用AI助手
     * context : null
     * hello_msg : 您好，我是您的AI智能助手，我会尽力回答您的问题或提供有用的建议。
     * icon : /images/avatar/gpt.png
     * enable : true
     * sort : 0
     */
    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;
    @JsonProperty("created_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long createdAt;
    @JsonProperty("updated_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long updatedAt;
    @JsonProperty("key")
    private String key;
    @JsonProperty("name")
    private String name;
    @JsonProperty("context")
    private List<RoleContextDto> context;
    @JsonProperty("hello_msg")
    private String helloMsg;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("enable")
    private boolean enable;
    @JsonProperty("sort")
    private int sort;

}
