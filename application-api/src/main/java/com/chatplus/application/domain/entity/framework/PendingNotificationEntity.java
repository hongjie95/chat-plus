package com.chatplus.application.domain.entity.framework;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.id.IdEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

/**
 * <p>Table: t_pending_notification - 待执行队列通知</p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Alias("pendingNotification")
@TableName(value = "t_pending_notification", autoResultMap = true)
public class PendingNotificationEntity extends IdEntity {

    /**
     * 交换机名称
     */
    private String messageId;
    /**
     * 队列名称
     */
    private String streamKey;
    /**
     * 消息主题
     */
    private String body;
    /**
     * 异常信息
     */
    private String cause;
    /**
     * 重试次数
     */
    private Integer retryNum;
}
