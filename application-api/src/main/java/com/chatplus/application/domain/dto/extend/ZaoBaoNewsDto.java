package com.chatplus.application.domain.dto.extend;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ZaoBaoNewsDto implements Serializable {
    @JsonProperty("date")
    private String date;
    @JsonProperty("head_image")
    private String headImage;
    @JsonProperty("weiyu")
    private String weiYu;
    @JsonProperty("news")
    private List<String> news;

}
