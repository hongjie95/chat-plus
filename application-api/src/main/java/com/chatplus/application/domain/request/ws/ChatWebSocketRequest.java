package com.chatplus.application.domain.request.ws;

import com.chatplus.application.domain.dto.ws.ChatRecordMessage;
import com.chatplus.application.domain.entity.chat.ChatModelEntity;
import com.chatplus.application.enumeration.AiPlatformEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 会话请求参数
 */
@Data
@Builder
@Schema(description = "会话请求参数")
public class ChatWebSocketRequest implements Serializable {
    @Serial
    private static final long serialVersionUID = -3081352005273150641L;

    public static final String WS_URL_PATH = "/api/chat/new";

    public static final String MJ_URL_PATH = "/api/mj/client";

    public static final String SD_URL_PATH = "/api/sd/client";
    /**
     * session_id: mqfiinjwivpu7fpm8plqkthgekmxjqn8jtwf7ygupm
     * role_id: 1
     * chat_id: 601f23ec-0d2d-42ea-8369-9522c05f9e4d
     * model_id: 1
     * token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcmVkIjoxNzA2ODYxODA1LCJ1c2VyX2lkIjoxMDl9._awG0_f-hm20KdnP7PaRsayA-7M9DfSg1hN8VTCG4B4
     */
    @Schema(description = "当前会话ID")
    private String sessionId;
    @Schema(description = "角色ID")
    private Long roleId;
    @Schema(description = "会话ID")
    private String chatId;
    @Schema(description = "模型ID")
    private Long modelId;
    @Schema(description = "登录的token")
    private String token;
    // 以下为传入后握手成功后设置的参数
    @Schema(description = "用户ID")
    private Long userId;
    @Schema(description = "当前模型需要消耗的算力")
    private Integer power;
    @Schema(description = "当前模型")
    private ChatModelEntity chatModel;
    @Schema(description = "平台")
    private AiPlatformEnum platform;

    @Schema(description = "渠道")
    private String channel;
    // 以下是发消息来时候设置的
    /**
     * 上下文会话列表
     */
    @Schema(description = "上下文会话列表")
    private List<ChatRecordMessage> chatContextList;
    /**
     * 当前发送来的消息
     */
    @Schema(description = "当前发送来的消息")
    private String currentPrompt;
    /**
     * 回复
     */
    @Schema(description = "回复")
    private String reply;
    /**
     * 会话token
     */
    @Schema(description = "会话token")
    private Long replyToken;
    /**
     * 是否允许作为上下文语料
     */
    @Schema(description = "是否允许作为上下文语料")
    private Boolean useContext;
    /**
     * 返回给扣分那里用到
     */
    @Schema(description = "返回给扣分那里用到")
    private Long historyItemId;
}
