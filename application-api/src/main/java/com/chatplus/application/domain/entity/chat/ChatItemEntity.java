package com.chatplus.application.domain.entity.chat;

import com.baomidou.mybatisplus.annotation.*;
import com.chatplus.application.datasource.id.IdEntity;
import org.apache.ibatis.type.Alias;

/**
 * <p>Table: t_chat_item - 用户会话列表</p>
 *
 * @author developer
 */
@Alias("chatItem")
@TableName(value = "t_chat_item", autoResultMap = true)
public class ChatItemEntity extends IdEntity {

    /**
     * 会话 ID
     */
    private String chatId;
    /**
     * 用户 ID
     */
    private Long userId;
    /**
     * 角色 ID
     */
    private Long roleId;
    /**
     * 会话标题
     */
    private String title;
    /**
     * 模型 ID
     */
    private Long modelId;

    public String getChatId() {
        return this.chatId;
    }

    public void setChatId(String chatId) {
        this.chatId =  chatId;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId =  userId;
    }

    public Long getRoleId() {
        return this.roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId =  roleId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title =  title;
    }

    public Long getModelId() {
        return this.modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId =  modelId;
    }

}
