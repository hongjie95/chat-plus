package com.chatplus.application.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 任务的扩展属性，系统内部使用
 *
 * @author chj
 * @date 2024/3/7
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "任务的扩展属性，系统内部使用")
@Data
public class MJPropertiesDto implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "消息内容提取出的prompt",example = "Cat")
    @JsonProperty("finalPrompt")
    private String finalPrompt;

    @Schema(description = "bot类型: MID_JOURNEY,NIJI_JOURNEY,INSIGHT_FACE",example = "NIJI_JOURNEY")
    @JsonProperty("botType")
    private String botType;

    @Schema(description = "执行该任务的实例ID(频道ID)",example = "1118138338562560102")
    @JsonProperty("discordInstanceId")
    private String discordInstanceId;

    @Schema(description = "消息ID",example = "1174910863984033903")
    @JsonProperty("messageId")
    private String messageId;

    @Schema(description = "消息Hash", example = "1174910863984033903")
    @JsonProperty("messageHash")
    private String messageHash;

    @Schema(description = "消息内容",example = "**Cat** - Image #1 <@590422081204912129>")
    @JsonProperty("messageContent")
    private String messageContent;

    @JsonProperty("cancelComponent")
    private String cancelComponent;

    @JsonProperty("customId")
    private String customId;

    @JsonProperty("discordChannelId")
    private String discordChannelId;

    @JsonProperty("flags")
    private Integer flags;

    @JsonProperty("needModel")
    private Boolean needModel;

    @JsonProperty("nonce")
    private String nonce;

    @JsonProperty("notifyHook")
    private String notifyHook;

    @JsonProperty("progressMessageId")
    private String progressMessageId;
}
