package com.chatplus.application.domain.response.admin;

import com.chatplus.application.domain.entity.account.UserLoginLogEntity;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 账号登录日志相关响应信息
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountLoginLogDetailResponse implements Serializable {
    @Serial
    private static final long serialVersionUID = 6429344185772985945L;

    /**
     * id : 1
     * created_at : 1706166373
     * updated_at : 0
     * user_id : 109
     * username : 15302789406
     * login_ip : 116.1.249.206
     * login_address : 中国-广西-南宁市
     */

    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;
    @JsonProperty("created_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long createdAt;
    @JsonProperty("updated_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long updatedAt;
    @JsonProperty("user_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long userId;
    @JsonProperty("username")
    private String username;
    @JsonProperty("login_ip")
    private String loginIp;
    @JsonProperty("login_address")
    private String loginAddress;

    public static AccountLoginLogDetailResponse build(UserLoginLogEntity userLoginLogEntity) {
        AccountLoginLogDetailResponse accountLoginLogDetailResponse = new AccountLoginLogDetailResponse();
        accountLoginLogDetailResponse.setId(userLoginLogEntity.getId());
        accountLoginLogDetailResponse.setCreatedAt(userLoginLogEntity.getCreatedAt().getEpochSecond());
        accountLoginLogDetailResponse.setUpdatedAt(userLoginLogEntity.getUpdatedAt().getEpochSecond());
        accountLoginLogDetailResponse.setUserId(userLoginLogEntity.getUserId());
        accountLoginLogDetailResponse.setUsername(userLoginLogEntity.getUsername());
        accountLoginLogDetailResponse.setLoginIp(userLoginLogEntity.getLoginIp());
        accountLoginLogDetailResponse.setLoginAddress(userLoginLogEntity.getLoginAddress());
        return accountLoginLogDetailResponse;
    }
}
