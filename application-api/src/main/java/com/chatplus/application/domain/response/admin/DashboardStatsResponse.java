package com.chatplus.application.domain.response.admin;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardStatsResponse implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    public DashboardStatsResponse(long users, long chats, long tokens, BigDecimal income) {
        this.users = users;
        this.chats = chats;
        this.tokens = tokens;
        this.income = income;
    }
    /**
     * users : 0
     * chats : 0
     * tokens : 0
     * income : 0
     */
    /**
     * 今日新增用户
     */
    @Schema(description = "今日新增用户", example = "1")
    @JsonProperty("users")
    private long users;
    /**
     * 今日新增对话
     */
    @Schema(description = "今日新增对话", example = "1")
    @JsonProperty("chats")
    private long chats;
    /**
     * 今日消耗 Tokens
     */
    @Schema(description = "今日消耗", example = "1")
    @JsonProperty("tokens")
    private long tokens;
    /**
     * 今日入账
     */
    @Schema(description = "今日入账", example = "1")
    @JsonProperty("income")
    private BigDecimal income;

    @Schema(description = "曲线图:key有，historyMessage，orders，users", example = "{'historyMessage':{'2023-03-02':1},'orders':{'2023-03-02':1},'users':{'2023-03-02':1}")
    private ChartDto chart = new ChartDto();

    @Data
    public static class ChartDto implements Serializable {
        //"2024-03-21": 0
        private Map<String, Integer> historyMessage = new HashMap<>();
        private Map<String, Integer> orders = new HashMap<>();
        private Map<String, Integer> users = new HashMap<>();
    }


}
