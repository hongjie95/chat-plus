package com.chatplus.application.domain.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * <p>敏感词表</p>
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "敏感词请求")
public class SensitiveWordRequest extends PlusPageRequest implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 敏感词
     */
    @Schema(description = "敏感词")
    private String word;
    /**
     * 备注
     */
    @Schema(description = "备注")
    private String remark;

}
