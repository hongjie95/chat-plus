package com.chatplus.application.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 聊天角色保存更新请求
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description ="用户更新角色列表")
public class UserChatRoleUpdateRequest implements Serializable {


    @Serial
    private static final long serialVersionUID = 1L;
    @Schema(description = "角色key")
    private List<String> keys;

}
