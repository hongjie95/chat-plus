package com.chatplus.application.domain.request;

import com.chatplus.application.domain.dto.MJButtonDto;
import com.chatplus.application.domain.dto.MJPropertiesDto;
import com.chatplus.application.enumeration.MjJobActionEnum;
import com.chatplus.application.enumeration.MjJobStatusEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * MJ回调请求
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Schema(description = "MJ回调请求")
public class MjCallbackNotifyRequest implements Serializable {


    /**
     * id : 14001929738841620
     * action : IMAGINE
     * status : SUCCESS
     * prompt : 猫猫
     * promptEn : Cat
     * description : /imagine 猫猫
     * submitTime : 1689231405854
     * startTime : 1689231442755
     * finishTime : 1689231544312
     * progress : 100%
     * imageUrl : https://cdn.discordapp.com/attachments/xxx/xxx/xxxx.png
     * failReason : null
     * properties : {"finalPrompt":"Cat"}
     * buttons : []
     */
    @Schema(description = "任务ID")
    @JsonProperty("id")
    private String id;

    @Schema(description = "任务类型: IMAGINE（绘图）、UPSCALE（放大）、VARIATION（变化）、ZOOM（图片变焦）、PAN（焦点移动）、DESCRIBE（图生文）、BLEND（图片混合）、SHORTEN（prompt分析）、SWAP_FACE（人脸替换）")
    @JsonProperty("action")
    private MjJobActionEnum action;

    @Schema(description = "任务状态: NOT_START（未启动）、SUBMITTED（已提交处理）、MODAL（窗口等待）、IN_PROGRESS（执行中）、FAILURE（失败）、SUCCESS（成功）、CANCEL（已取消）")
    @JsonProperty("status")
    private MjJobStatusEnum status;

    @Schema(description = "提示词")
    @JsonProperty("prompt")
    private String prompt;

    @Schema(description = "英文提示词")
    @JsonProperty("promptEn")
    private String promptEn;

    @Schema(description = "任务描述")
    @JsonProperty("description")
    private String description;

    @Schema(description = "自定义参数")
    @JsonProperty("state")
    private String state;

    @Schema(description = "提交时间")
    @JsonProperty("submitTime")
    private Long submitTime;

    @Schema(description = "开始执行时间")
    @JsonProperty("startTime")
    private Long startTime;

    @Schema(description = "结束时间")
    @JsonProperty("finishTime")
    private Long finishTime;

    @Schema(description = "任务进度")
    @JsonProperty("progress")
    private String progress;

    @Schema(description = "生成图片的url, 成功或执行中时有值，可能为png或webp")
    @JsonProperty("imageUrl")
    private String imageUrl;

    @Schema(description = "失败原因, 失败时有值")
    @JsonProperty("failReason")
    private String failReason;

    @Schema(description = "任务的扩展属性，系统内部使用")
    @JsonProperty("properties")
    private MJPropertiesDto properties;

    @Schema(description = "任务完成后的可执行按钮")
    @JsonProperty("buttons")
    private List<MJButtonDto> buttons;

}
