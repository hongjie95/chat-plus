package com.chatplus.application.domain.response;

import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;

/**
 * 会话历史详情响应
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatHistoryDetailResponse implements Serializable {
    /**
     * id : 0
     * created_at : 1704027791
     * updated_at : 1704027791
     * chat_id : 27534b98-59fd-4fce-8828-b8057961c664
     * user_id : 101
     * role_id : 1
     * type : prompt
     * icon : /images/avatar/user.png
     * tokens : 7
     * content : objectMapper把字符串转对象
     * use_context : true
     */

    @JsonProperty("id")
    @JsonSerialize(using = Int64AsString.class)
    private Long id;
    @JsonProperty("created_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long createdAt;
    @JsonProperty("updated_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long updatedAt;
    @JsonProperty("chat_id")
    private String chatId;
    @JsonProperty("user_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long userId;
    @JsonProperty("role_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long roleId;
    @JsonProperty("type")
    private String type;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("tokens")
    private Integer tokens;
    @JsonProperty("content")
    private String content;
    @JsonProperty("use_context")
    private boolean useContext;

}
