package com.chatplus.application.domain.entity.account;

import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.datasource.id.IdEntity;
import com.chatplus.application.enumeration.ProductTypeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

import java.time.Instant;

/**
 * <p>Table: t_user_product_log - 用户套餐记录</p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Alias("userProductLog")
@TableName(value = "t_user_product_log", autoResultMap = true)
public class UserProductLogEntity extends IdEntity {

    /**
     * 关联的用户ID
     */
    private Long userId;
    /**
     * 关联的产品ID
     */
    private Long productId;
    /**
     * 1:月卡，2:次卡
     */
    private ProductTypeEnum productType;
    /**
     * 关联的订单ID
     */
    private Long orderId;
    /**
     * 算力
     */
    private Integer power;
    /**
     * 订阅开始时间
     */
    private Instant subsStartTime;
    /**
     * 订阅结束时间
     */
    private Instant subsEndTime;
}
