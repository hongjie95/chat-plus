package com.chatplus.application.domain.response;

import com.chatplus.application.domain.entity.basedata.MenuEntity;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;

/**
 * 菜单响应
 *
 * @Author Angus
 * @Date 2024/4/8
 */
@Data
public class MenuDetailResponse implements Serializable {
    @JsonSerialize(using = Int64AsString.class)
    private long id;

    private String name;

    private String icon;

    private String url;

    @JsonProperty("sort_num")
    private int sortNum;

    private boolean enabled;
    @JsonProperty("external_link")
    private boolean externalLink;

    public static MenuDetailResponse build(MenuEntity menu) {
        MenuDetailResponse response = new MenuDetailResponse();
        response.setId(menu.getId());
        response.setName(menu.getName());
        response.setIcon(menu.getIcon());
        response.setUrl(menu.getUrl());
        response.setSortNum(menu.getSortNum());
        response.setEnabled(menu.getEnabled());
        //判断url是否是链接
        response.setExternalLink(menu.getUrl().startsWith("http"));
        return response;
    }

}
