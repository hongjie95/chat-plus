package com.chatplus.application.domain.response.admin;

import com.chatplus.application.domain.response.ChatRoleDetailResponse;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户会话查询响应
 *
 * @author chj
 * @date 2024/3/1
 **/
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatItemQueryResponse implements Serializable {

    @JsonProperty("username")
    private String username;

    @JsonProperty("user_id")
    @JsonSerialize(using = Int64AsString.class)
    private Long userId;

    @JsonProperty("chat_id")
    private String chatId;

    @JsonProperty("title")
    private String title;

    @JsonProperty("model")
    private String model;

    @JsonProperty("token")
    private Long token;

    @JsonProperty("created_at")
    @JsonSerialize(using = Int64AsString.class)
    private Long createdAt;

    @JsonProperty("msg_num")
    private int msgNum;

    @JsonProperty("role")
    private ChatRoleDetailResponse role;

}
