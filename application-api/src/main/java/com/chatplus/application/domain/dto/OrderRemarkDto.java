package com.chatplus.application.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 订单备注
 *
 * @author chj
 * @date 2024/1/16
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class OrderRemarkDto implements Serializable {


    /**
     * days : 30
     * calls : 0
     * img_calls : 0
     * name : 会员1个月
     * price : 19.9
     * discount : 10
     */

    @JsonProperty("days")
    private int days;
    @JsonProperty("power")
    private int power;
    @JsonProperty("name")
    private String name;
    @JsonProperty("price")
    private BigDecimal price;
    @JsonProperty("discount")
    private BigDecimal discount;
}
