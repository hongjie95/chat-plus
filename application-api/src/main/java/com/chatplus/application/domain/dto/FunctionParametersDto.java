package com.chatplus.application.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Schema(description = "函数参数")
public class FunctionParametersDto implements Serializable {

    /**
     * type : object
     * required : ["11","444"]
     * properties : {"11":{"type":"string","description":"2222"},"333":{"type":"number","description":"222444"},"444":{"type":"string","description":"2222"}}
     */

    @JsonProperty("type")
    @Schema(description = "类型")
    private String type;

    @JsonProperty("properties")
    @Schema(description = "属性")
    private Map<String, FunctionFieldDetailBean> properties;

    @JsonProperty("required")
    @Schema(description = "必填字段")
    private List<String> required;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    @Schema(description = "函数参数字段详情")
    public static class FunctionFieldDetailBean implements Serializable {
        /**
         * type : string
         * description : 2222
         */
        @JsonProperty("type")
        @Schema(description = "类型")
        private String type;

        @JsonProperty("description")
        @Schema(description = "描述")
        private String description;
    }
}
