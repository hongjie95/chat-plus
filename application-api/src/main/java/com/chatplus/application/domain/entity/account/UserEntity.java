package com.chatplus.application.domain.entity.account;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.chatplus.application.common.enumeration.UserStatusEnum;
import com.chatplus.application.datasource.annotation.JsonCollectionGenericType;
import com.chatplus.application.datasource.handler.JacksonJsonTypeHandler;
import com.chatplus.application.datasource.id.IdEntity;
import com.chatplus.application.domain.dto.UserCustomChatConfigDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.Alias;

import java.time.Instant;
import java.util.List;

/**
 * <p>Table: t_user - 用户表</p>
 *
 * @author developer
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Alias("user")
@TableName(value = "t_user", autoResultMap = true)
public class UserEntity extends IdEntity {

    /**
     * 昵称
     */
    private String nickname;
    /**
     * 登录账号
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 头像
     */
    private String avatar;
    /**
     * VIP过期时间，如果为空则表示不是VIP
     */
    private Instant vipExpiredTime;
    /**
     * VIP重置时间，格式为yyyy-MM-dd
     */
    private String vipResetDate;
    /**
     * 当前状态
     */
    private UserStatusEnum status;
    /**
     * 聊天配置json
     */
    @TableField(typeHandler = JacksonJsonTypeHandler.class)
    private UserCustomChatConfigDto chatConfig;
    /**
     * 聊天角色 json
     */
    @JsonCollectionGenericType(String.class)
    @TableField(typeHandler = JacksonJsonTypeHandler.class)
    private List<String> chatRoles;
    /**
     * AI模型 json
     */
    @JsonCollectionGenericType(String.class)
    @TableField(typeHandler = JacksonJsonTypeHandler.class)
    private List<String> chatModels;
    /**
     * 最后登录时间
     */
    private Instant lastLoginAt;
    /**
     * 最后登录 IP
     */
    private String lastLoginIp;
    /**
     * 是否是管理员
     */
    private Boolean admin;
    /**
     * 是否自定义画图的价格
     */
    private Integer mjPower;

    private Integer sdPower;

    private Integer dallPower;

}
