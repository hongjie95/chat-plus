package com.chatplus.application.domain.dto;


import lombok.Data;

import java.io.Serializable;
@Data
public class RoleContextDto implements Serializable {

    private String role;
    private String content;
}
