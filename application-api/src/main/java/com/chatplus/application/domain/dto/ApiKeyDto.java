package com.chatplus.application.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiKeyDto implements Serializable {
    private String apiKey;
    private String url;
    private String proxyUrl;
    private String notifyUrl;
}
