package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 微信用户状态
 **/
public enum WechatUserStatusEnum implements NameValueEnum<Integer> {
    /**
     * 未支付
     */
    UNFOLLOW(0, "未关注"),

    FOLLOWED(1, "已关注"),

    DISABLE(999, "被系统拉黑");

    WechatUserStatusEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    private final Integer value;
    private final String name;

    @Override
    @JsonValue
    public Integer getValue() {
        return value;
    }

    @Override
    public String getName() {
        return name;
    }

    @JsonCreator
    public static WechatUserStatusEnum get(int value) {
        try {
            return ValueEnumUtils.parse(value, WechatUserStatusEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
