package com.chatplus.application.enumeration;

public enum ImgOperateType {
    /**
     * 添加水印
     */
    WATER_MARK,
    /**
     * 缩略图
     */
    THUMB,
}
