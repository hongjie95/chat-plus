package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
/**
 * 产品类型枚举
 */
public enum ProductTypeEnum implements NameValueEnum<Integer> {
    SUBSCRIPTION_CARD(1, "天卡"),
    TIME_CARD(2, "次卡");

    ProductTypeEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    private final Integer value;
    private final String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @JsonCreator
    public static ProductTypeEnum get(int value) {
        try {
            return ValueEnumUtils.parse(value, ProductTypeEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
