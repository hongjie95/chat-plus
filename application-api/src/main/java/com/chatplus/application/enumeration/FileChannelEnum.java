package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;

public enum FileChannelEnum implements NameValueEnum<String> {
    OSS("OSS", "oss上传"),
    LOCAL("LOCAL", "本地上传");

    FileChannelEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    private final String value;
    private final String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return value;
    }

    @JsonCreator
    public static FileChannelEnum get(String value) {
        try {
            return ValueEnumUtils.parse(value, FileChannelEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
