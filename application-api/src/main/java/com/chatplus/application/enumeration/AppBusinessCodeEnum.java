package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 业务类型
 **/
public enum AppBusinessCodeEnum implements NameValueEnum<Integer> {

    CHAT(1, "Chat");


    AppBusinessCodeEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    private final Integer value;
    private final String name;

    @Override
    @JsonValue
    public Integer getValue() {
        return value;
    }

    @Override
    public String getName() {
        return name;
    }

    @JsonCreator
    public static AppBusinessCodeEnum get(int value) {
        try {
            return ValueEnumUtils.parse(value, AppBusinessCodeEnum.class);
        }
        catch (Exception e) {
            return null;
        }
    }
}
