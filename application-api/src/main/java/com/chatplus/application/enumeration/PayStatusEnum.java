package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 支付状态
 **/
public enum PayStatusEnum implements NameValueEnum<Integer> {
    /**
     * 未支付
     */
    NOT(0, "未支付"),

    SUCCESS(1, "已支付"),

    FAIL(2, "支付失败"),

    CLOSE(999,"支付关闭");


    PayStatusEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    private final Integer value;
    private final String name;

    @Override
    @JsonValue
    public Integer getValue() {
        return value;
    }

    @Override
    public String getName() {
        return name;
    }

    @JsonCreator
    public static PayStatusEnum get(int value) {
        try {
            return ValueEnumUtils.parse(value, PayStatusEnum.class);
        }
        catch (Exception e) {
            return null;
        }
    }
}
