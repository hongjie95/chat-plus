package com.chatplus.application.enumeration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;

public enum GenderEnum implements NameValueEnum<Integer> {
    UNKNOWN(0, "未知"),
    MALE(1, "男性"),
    FEMALE(2, "女性");

    GenderEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    private final Integer value;
    private final String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @JsonCreator
    public static GenderEnum get(int value) {
        try {
            return ValueEnumUtils.parse(value, GenderEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
