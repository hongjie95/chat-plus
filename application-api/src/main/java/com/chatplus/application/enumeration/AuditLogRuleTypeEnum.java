package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;

/**
 * 审计日志规则类型
 *
 * @author chj
 * @date 2022/6/13
 **/
public enum AuditLogRuleTypeEnum implements NameValueEnum<Integer> {

    WHITE_LIST(1, "白名单"),

    BLACK_LIST(2, "黑名单");

    AuditLogRuleTypeEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    private final Integer value;
    private final String name;

    @Override
    @JsonValue
    public Integer getValue() {
        return value;
    }

    @Override
    public String getName() {
        return name;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static AuditLogRuleTypeEnum get(int value) {
        try {
            return ValueEnumUtils.parse(value, AuditLogRuleTypeEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
