package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * AI平台常量
 * @Author Angus
 */
public enum AiPlatformEnum implements NameValueEnum<String> {


    BAI_DU("【百度】文心一言", "Baidu"),
    XUN_FEI("【讯飞】星火大模型", "XunFei"),
    CHAT_GLM("【清华智普】ChatGLM", "ChatGLM"),
    OPEN_AI("【OpenAI】ChatGPT", "OpenAI"),
    AZURE("【微软】Azure", "Azure"),
    ALI_YUN_Q_WEN("【阿里】通义千问", "QWen"),
    SD_LOCAL("【SD】绘画", "SD"),

    SD_STABILITY("【SD】原厂stability", "SD_STABILITY"),

    MJ("【MJ】绘画", "MJ"),
    DALL("【DALL】绘画", "DALL");
    private final String name;
    private final String value;

    AiPlatformEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    @JsonValue
    public String getValue() {
        return value;
    }

    @JsonCreator
    public static AiPlatformEnum get(String value) {
        try {
            return ValueEnumUtils.parse(value, AiPlatformEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
