package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 附加支付方式
 *
 * @author liexuan
 * @date 2022-08-15 17:03
 **/
public enum SubModeEnum implements NameValueEnum<String> {

    /**
     * 微信扫码 二维码
     */
    WECHAT_NATIVE("WeixinNative", "微信web支付"),
    WECHAT_H5("WeixinH5", "原生微信H5支付"),
    WECHAT_JSAPI("WeixinJsapi", "微信公众号（非原生），使用微信支付时，是微信小程序支付"),
    /**
     * 支付宝扫码 二维码
     */
    ALIPAY_NATIVE("AlipayNative", "支付宝主扫");

    private final String value;
    private final String name;

    SubModeEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    @JsonValue
    public String getValue() {
        return value;
    }


    @JsonCreator
    public static SubModeEnum get(String value) {
        try {
            return ValueEnumUtils.parse(value, SubModeEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
