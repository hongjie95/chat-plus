package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;

public enum MessageTypeEnum implements NameValueEnum<String> {
    WS_START("start", "会话输出开始标致"),
    WS_MIDDLE("middle", "会话输出实际内容"),
    WS_END("end", "会话输出结束标志"),
    WS_MJ_IMG("mj", "mj"),
    CHAT("chat", "聊天"),
    HEARTBEAT("heartbeat", "心跳");

    MessageTypeEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    private final String value;
    private final String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return value;
    }

    @JsonCreator
    public static MessageTypeEnum get(String value) {
        try {
            return ValueEnumUtils.parse(value, MessageTypeEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
