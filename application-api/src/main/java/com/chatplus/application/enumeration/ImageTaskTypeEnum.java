package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 绘画任务类型
 * @Author Angus
 */
public enum ImageTaskTypeEnum implements NameValueEnum<String> {


    TEXT_2_IMG("文生图", "image"),
    BLEND("融图", "blend"),
    SWAP_FACE("换脸", "swapFace"),
    VARIATION("变换", "variation"),
    UPSCALE("放大", "upscale"),
    ACTION("修改动作", "action");
    private final String name;
    private final String value;

    ImageTaskTypeEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    @JsonValue
    public String getValue() {
        return value;
    }

    @JsonCreator
    public static ImageTaskTypeEnum get(String value) {
        try {
            return ValueEnumUtils.parse(value, ImageTaskTypeEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
