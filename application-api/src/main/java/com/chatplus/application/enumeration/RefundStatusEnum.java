package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
/**
 * 收银台款项支付状态
 */
public enum RefundStatusEnum implements NameValueEnum<Integer> {

    HANDLING(0, "处理中"),
    SUCCESS(1, "已退款"),
    FAIL(2, "退款失败"),
    AUDITING(11, "审核中"),
    AUDIT_FAIL(12, "审核失败"),
    CLOSED(999, "交易关闭");

    private final Integer value;

    private final String name;

    RefundStatusEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    @JsonValue
    public Integer getValue() {
        return value;
    }


    @JsonCreator
    public static RefundStatusEnum get(int value) {
        try {
            return ValueEnumUtils.parse(value, RefundStatusEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
