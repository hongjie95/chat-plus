package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 订单状态
 **/
public enum OrderStatusEnum implements NameValueEnum<Integer> {
    /**
     * 未支付
     */
    NOT(0, "未支付"),

    SCANNED(1, "已扫码"),

    SUCCESS(2, "支付成功");


    OrderStatusEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    private final Integer value;
    private final String name;

    @Override
    @JsonValue
    public Integer getValue() {
        return value;
    }

    @Override
    public String getName() {
        return name;
    }

    @JsonCreator
    public static OrderStatusEnum get(int value) {
        try {
            return ValueEnumUtils.parse(value, OrderStatusEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
