package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;

public enum MjJobStatusEnum implements NameValueEnum<String> {
    NOT_START("NOT_START", "未启动"),
    SUBMITTED("SUBMITTED", "已提交处理"),
    MODAL("MODAL", "窗口等待"),
    IN_PROGRESS("IN_PROGRESS", "执行中"),
    FAILURE("FAILURE", "失败"),
    SUCCESS("SUCCESS", "成功"),
    CANCEL("CANCEL", "已取消");

    MjJobStatusEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    private final String value;
    private final String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return value;
    }

    @JsonCreator
    public static MjJobStatusEnum get(String value) {
        try {
            return ValueEnumUtils.parse(value, MjJobStatusEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
