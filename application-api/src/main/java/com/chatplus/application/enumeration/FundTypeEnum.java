package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 资金类型
 */
public enum FundTypeEnum implements NameValueEnum<Integer> {
    EXPEND(0, "支出"),
    INCOME(1, "收入");

    FundTypeEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    private final Integer value;
    private final String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @JsonCreator
    public static FundTypeEnum get(int value) {
        try {
            return ValueEnumUtils.parse(value, FundTypeEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
