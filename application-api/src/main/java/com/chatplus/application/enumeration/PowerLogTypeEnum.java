package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 算力日志类型
 */
public enum PowerLogTypeEnum implements NameValueEnum<Integer> {
    PAY(1, "充值"),
    EXPEND(2, "消费"),
    REFUND(3, "退费");

    PowerLogTypeEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    private final Integer value;
    private final String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @JsonCreator
    public static PowerLogTypeEnum get(int value) {
        try {
            return ValueEnumUtils.parse(value, PowerLogTypeEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
