package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;

public enum MjJobActionEnum implements NameValueEnum<String> {
    IMAGINE("IMAGINE", "绘图"),
    UPSCALE("UPSCALE", "放大"),
    VARIATION("VARIATION", "变化"),
    ZOOM("ZOOM", "图片变焦"),
    PAN("PAN", "焦点移动"),
    DESCRIBE("DESCRIBE", "图生文"),
    BLEND("BLEND", "图片混合"),
    SHORTEN("SHORTEN", "prompt分析"),
    SWAP_FACE("SWAP_FACE", "人脸替换");

    MjJobActionEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    private final String value;
    private final String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return value;
    }

    @JsonCreator
    public static MjJobActionEnum get(String value) {
        try {
            return ValueEnumUtils.parse(value, MjJobActionEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
