package com.chatplus.application.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 支付渠道
 */
public enum PayChannelEnum implements NameValueEnum<String> {
    ALIPAY("alipay", "支付宝支付"),
    WEI_XIN("wechat", "微信支付");

    private final String value;

    private final String name;

    PayChannelEnum(String value, String name) {
        this.value = value;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    @JsonValue
    public String getValue() {
        return value;
    }


    @JsonCreator
    public static PayChannelEnum get(String value) {
        try {
            return ValueEnumUtils.parse(value, PayChannelEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
