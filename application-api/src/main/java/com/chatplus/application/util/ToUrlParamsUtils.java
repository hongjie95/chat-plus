package com.chatplus.application.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @Description 实体类对象转换为url参数
 */
@Slf4j
public class ToUrlParamsUtils {

    /**
     * url后追加参数
     * 如果url包含当前参数，则输出原始url
     *
     * @param url
     * @param param
     * @return
     */
    public static String addParamToUrl(String url, String param, String paramValue) {
        if (StringUtils.isBlank(url)) {
            return StringUtils.EMPTY;
        }
        if (url.contains("?")) {
            if (isContainParam(url, param)) {
                return url;
            }
            return url.contains("#") && url.indexOf("#") > url.indexOf("?") ? url.replaceFirst("#", "&" + param + "=" + paramValue + "#") : url + "&" + param + "=" + paramValue;
        } else {
            return url.contains("#") ? url.replaceFirst("#", "?" + param + "=" + paramValue + "#") : url + "?" + param + "=" + paramValue;
        }
    }

    /**
     * 解析出url请求的路径，包括页面
     *
     * @param strURL url地址
     * @return url路径
     */
    public static String urlPage(String strURL) {
        String strPage = null;
        String[] arrSplit;
        strURL = strURL.trim().toLowerCase();
        arrSplit = strURL.split("[?]");
        if (strURL.length() > 0) {
            if (arrSplit.length > 1) {
                if (arrSplit[0] != null) {
                    strPage = arrSplit[0];
                }
            }
        }
        return strPage;
    }

    /**
     * 去掉url中的路径，留下请求参数部分
     *
     * @param strURL url地址
     * @return url请求参数部分
     */
    private static String truncateUrlPage(String strURL) {
        String strAllParam = null;
        String[] arrSplit;

        strURL = strURL.trim().toLowerCase();

        arrSplit = strURL.split("[?]");
        if (strURL.length() > 1) {
            if (arrSplit.length > 1) {
                if (arrSplit[1] != null) {
                    strAllParam = arrSplit[1];
                }
            }
        }
        return strAllParam;
    }

    /**
     * 解析出url参数中的键值对
     * 如 "index.jsp?Action=del&id=123"，解析出Action:del,id:123存入map中
     *
     * @param url url地址
     * @return url请求参数部分
     */
    public static Map<String, String> urlRequest(String url) {
        Map<String, String> mapRequest = new HashMap<>();
        String[] arrSplit;

        String strUrlParam = truncateUrlPage(url);
        if (strUrlParam == null) {
            return mapRequest;
        }
        // 每个键值为一组
        arrSplit = strUrlParam.split("[&]");
        return putKVInMap(mapRequest, arrSplit);
    }

    public static Map<String, String> strToMap(String splitStr, String delimiter) {
        Map<String, String> map = new HashMap<>();

        if (StringUtils.isBlank(splitStr)) {
            return map;
        }
        // 以指定分隔符切分
        String[] arrSplit = splitStr.split("[" + delimiter + "]");
        return putKVInMap(map, arrSplit);
    }

    private static Map<String, String> putKVInMap(Map<String, String> map, String[] arrSplit) {
        for (String strSplit : arrSplit) {
            String[] arrSplitEqual = strSplit.split("[=]");

            //解析出键值
            if (arrSplitEqual.length > 1) {
                //正确解析
                map.put(arrSplitEqual[0], arrSplitEqual[1]);
            } else {
                if (!arrSplitEqual[0].equals("")) {
                    //只有参数没有值，不加入
                    map.put(arrSplitEqual[0], "");
                }
            }
        }
        return map;
    }

    /**
     * 请求参数中是否包含参数
     *
     * @param url
     * @param param
     * @return
     */
    public static boolean isContainParam(String url, String param) {
        return urlRequest(url).containsKey(param);
    }

    /**
     * @param clazz 参数实体类
     * @return String
     * @Description: 将实体类clazz的属性转换为url参数
     */
    public static String getParams(Object clazz) {
        // 遍历属性类、属性值
        Field[] fields = clazz.getClass().getDeclaredFields();

        StringBuilder requestURL = new StringBuilder();
        try {
            boolean flag = true;
            String property, value;
            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                // 允许访问私有变量
                field.setAccessible(true);

                // 属性名
                property = field.getName();
                // 属性值
                value = Optional.ofNullable(String.valueOf(field.get(clazz))).orElse("");

                String params = property + "=" + value;
                if (flag) {
                    requestURL.append(params);
                    flag = false;
                } else {
                    requestURL.append("&" + params);
                }
            }
        } catch (Exception e) {
            log.error("转换URL参数异常：", e);
        }
        return requestURL.toString();
    }

}

