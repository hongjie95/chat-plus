package com.chatplus.application.web.idempotent.concurrent;

import com.chatplus.application.common.exception.SystemException;
import com.chatplus.application.web.idempotent.LockType;
import com.chatplus.application.web.idempotent.annotation.Concurrent;
import com.chatplus.application.web.idempotent.generator.ValueGenerator;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.Method;
import java.util.Objects;

/**
 * aop 形式的并发拦截器实现
 *
 * @author Angus
 */
public class ConcurrentInterceptor implements MethodInterceptor {

    /**
     * redisson 客户端
     */
    private final RedissonClient redissonClient;

    /**
     * key 生成器
     */
    private final ValueGenerator valueGenerator;

    public ConcurrentInterceptor(RedissonClient redissonClient, ValueGenerator valueGenerator) {
        this.redissonClient = redissonClient;
        this.valueGenerator = valueGenerator;
    }

    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {

        Concurrent concurrent = AnnotationUtils.findAnnotation(invocation.getMethod(), Concurrent.class);

        if (Objects.isNull(concurrent)) {
            return invocation.proceed();
        }

        String key = concurrent.value();

        if (StringUtils.isBlank(key)) {
            Method method = invocation.getMethod();
            key = method.getDeclaringClass().getName() + "." + method.getName();
        }

        Object concurrentKey = valueGenerator.generate(key, invocation.getMethod(), invocation.getArguments());

        RLock lock;

        if (LockType.FairLock.equals(concurrent.type())) {
            lock = redissonClient.getFairLock(concurrentKey.toString());
        } else if (LockType.Lock.equals(concurrent.type())) {
            lock = redissonClient.getLock(concurrentKey.toString());
        } else {
            throw new SystemException("找不到对 [" + concurrent.type() + "] 的所类型支持");
        }

        boolean tryLock;

        long waitTime = concurrent.waitTime().unit().toMillis(concurrent.waitTime().value());

        if (waitTime <= 0) {
            tryLock = lock.tryLock();
        } else {
            tryLock = lock.tryLock(waitTime, concurrent.leaseTime().value(), concurrent.leaseTime().unit());
        }

        if (tryLock) {
            try {
                return invocation.proceed();
            } finally {
                if (lock.isLocked() && lock.isHeldByCurrentThread() && !concurrent.lockToExpire()) {
                    lock.unlock();
                }
            }
        }

        throw new SystemException(concurrent.exception());

    }
}
