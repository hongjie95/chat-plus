package com.chatplus.application.web.interceptor;


/**
 * 读取了inputStream之后，再将数据写回去
 *
 * @author chj
 * @date 2022/7/18
 **/
public interface IContentCachingRequestWrapper {

    byte[] getBody();

}
