package com.chatplus.application.web.notification;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 声明消息的来源，用于构造Exchange、Queue的名字。
 *
 * <p>组成格式"$prefix.$messageSource"</p>
 */
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface MessageSource {
    @AliasFor("source")
    String value() default "";

    @AliasFor("value")
    String source() default "";
}
