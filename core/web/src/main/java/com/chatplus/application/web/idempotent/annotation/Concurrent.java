package com.chatplus.application.web.idempotent.annotation;

import com.chatplus.application.web.idempotent.LockType;
import com.chatplus.application.web.idempotent.Time;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 并发处理注解
 *
 * @author Angus
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Concurrent {

    /**
     * 并发的 key
     * <p>spring el 规范: 启用 spring el 时，通过中括号([])识别启用</p>
     * <p>如:</p>
     * <p>@Concurrent(value="[#vo.fieldName]")</p>
     * <p>public void save(Vo vo);</p>
     *
     * @return key 名称
     */
    String value() default "";

    /**
     * 异常信息
     *
     * @return 信息
     */
    String exception() default "请不要重复操作";

    /**
     * 锁等待时间
     *
     * @return 锁等待时间
     */
    Time waitTime() default @Time(1500);

    /**
     * 锁生存/释放时间
     *
     * @return 锁生存/释放时间
     */
    Time leaseTime() default @Time(4500);

    /**
     * 锁类型
     *
     * @return 所类型
     */
    LockType type() default LockType.Lock;

    /**
     * 锁定足够 leaseTime的时间才解锁。默认是方法执行完就解锁，只有当需要锁定足够的设定时间时才需要设置值
     *
     * @return 是否需要锁定足够设置的过期时间
     */
    boolean lockToExpire() default false;

}
