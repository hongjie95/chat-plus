package com.chatplus.application.web;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 验证码 自动配置。
 */
@EnableConfigurationProperties
@Configuration
public class VerificationConfig {
    public static final String PREFIX_VERIFICATION_CONFIG = "chatplus.verification";
}
