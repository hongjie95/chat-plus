package com.chatplus.application.web.auditlogger.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 审计日志配置
 *
 * @Author Angus
 * @Date 2024/3/31
 */
@Data
@Component
@ConfigurationProperties("chatplus.audit-logger")
public class AuditLoggerProperties {

    private boolean enable = false;

}
