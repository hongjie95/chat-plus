package com.chatplus.application.web.basecontroller;
import com.chatplus.application.web.satoken.helper.LoginHelper;
import com.chatplus.application.web.satoken.model.LoginUser;

/**
 * web层通用数据处理
 */
public class BaseController {
    /**
     * 获取用户缓存信息
     */
    public LoginUser getLoginUser() {
        return LoginHelper.getLoginUser();
    }

    /**
     * 获取登录用户id
     */
    public Long getUserId() {
        return LoginHelper.getUserId();
    }

    /**
     * 获取登录用户名
     */
    public String getUsername() {
        return LoginHelper.getUsername();
    }
}
