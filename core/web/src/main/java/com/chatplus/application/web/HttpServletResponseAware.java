package com.chatplus.application.web;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


@SuppressWarnings("unused")
public interface HttpServletResponseAware {
    @SuppressWarnings("ConstantConditions")
    default HttpServletResponse getHttpServletResponse() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
    }
}
