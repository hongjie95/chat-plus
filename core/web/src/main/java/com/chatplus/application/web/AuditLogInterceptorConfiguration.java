package com.chatplus.application.web;

import com.chatplus.application.web.interceptor.AuditLoggerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AuditLogInterceptorConfiguration implements WebMvcConfigurer {


    @Bean
    public AuditLoggerInterceptor auditLoggerInterceptor() {
        return new AuditLoggerInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //addPathPatterns拦截所有请求包括静态资源
        registry.addInterceptor(auditLoggerInterceptor())
            .addPathPatterns("/**");
    }
}
