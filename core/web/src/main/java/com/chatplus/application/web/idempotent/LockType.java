package com.chatplus.application.web.idempotent;

/**
 * 锁类型
 *
 * @author Angus
 */
public enum LockType {

    /**
     * 安全锁
     */
    Lock,
    /**
     * 公平锁
     */
    FairLock
}
