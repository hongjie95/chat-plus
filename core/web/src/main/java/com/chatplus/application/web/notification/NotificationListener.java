package com.chatplus.application.web.notification;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 订阅通知。
 */
@Target({ElementType.METHOD})
@Retention(RUNTIME)
@Inherited
@Documented
@Repeatable(NotificationListener.List.class)
public @interface NotificationListener {
    /**
     * 要监听的通知类。
     */
    Class<?> notification();
    @SuppressWarnings("unused")
    @Target({ElementType.METHOD})
    @Retention(RUNTIME)
    @Inherited
    @Documented
    @interface List {
        NotificationListener[] value();
    }
}
