package com.chatplus.application.web.advice;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.NotPermissionException;
import cn.dev33.satoken.exception.NotRoleException;
import com.chatplus.application.common.domain.response.TurnRightResponse;
import com.chatplus.application.common.exception.*;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import com.chatplus.application.json.JsonReader;
import com.chatplus.application.web.util.IpUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolationException;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.*;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestControllerAdvice
public class BaseExceptionAdvice extends ResponseEntityExceptionHandler {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(BaseExceptionAdvice.class);

    private JsonReader jsonReader;

    @Autowired
    public void setJsonReader(JsonReader jsonReader) {
        this.jsonReader = jsonReader;
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(TurnRightException.class)
    public TurnRightResponse turnRightExceptionHandler(TurnRightException e) {
        return new TurnRightResponse(e);
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(NotDevelopedException.class)
    public TurnRightResponse notDevelopedExceptionHandler(NotDevelopedException e) {
        return new TurnRightResponse(e);
    }

    //    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(BadRequestException.class)
    public TurnRightResponse badRequestExceptionHandler(BadRequestException e) {
        return new TurnRightResponse(e);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ExceptionHandler(NoContentException.class)
    public void noContentException() {
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(ForbiddenException.class)
    public TurnRightResponse forbiddenException(ForbiddenException e) {
        return new TurnRightResponse(e);
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler({UnauthorizedException.class})
    public void unauthorizedException() {
    }

    @ResponseStatus(HttpStatus.UPGRADE_REQUIRED)
    @ExceptionHandler(UpgradeRequiredException.class)
    public void upgradeRequiredException() {
    }

    private static final List<String> ignoreRequestURI = List.of("/api/user/session");
    @ExceptionHandler({NotLoginException.class})
    public TurnRightResponse authException(NotLoginException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        String clientIP = IpUtil.getRemoteIp(request);
        if (!ignoreRequestURI.contains(requestURI)) {
            LOGGER.message("认证失败,无法访问系统资源")
                    .context("requestURI", requestURI)
                    .context("clientIP", clientIP)
                    .error();
        }
        TurnRightResponse turnRightResponse = new TurnRightResponse();
        turnRightResponse.setMessage("认证失败，禁止访问!");
        turnRightResponse.setCode(HttpStatus.UNAUTHORIZED.value());
        return turnRightResponse;
    }
    @ExceptionHandler(NotPermissionException.class)
    public TurnRightResponse handleNotPermissionException(NotPermissionException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        String clientIP = IpUtil.getRemoteIp(request);
        LOGGER.message("请求地址,权限码校验失败")
                .context("requestURI", requestURI)
                .context("clientIP", clientIP)
                .error();
        TurnRightResponse turnRightResponse = new TurnRightResponse();
        turnRightResponse.setMessage("没有访问权限，请联系管理员授权");
        turnRightResponse.setCode(HttpStatus.FORBIDDEN.value());
        return turnRightResponse;
    }

    /**
     * 角色权限异常
     */
    @ExceptionHandler(NotRoleException.class)
    public TurnRightResponse handleNotRoleException(NotRoleException e, HttpServletRequest request) {
        String requestURI = request.getRequestURI();
        LOGGER.message("请求地址,角色权限校验失败").context("requestURI",requestURI).error();
        TurnRightResponse turnRightResponse = new TurnRightResponse();
        turnRightResponse.setMessage("没有访问权限，请联系管理员授权");
        turnRightResponse.setCode(HttpStatus.FORBIDDEN.value());
        return turnRightResponse;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ConstraintViolationException.class)
    public void constraintViolationException(ConstraintViolationException exception, HttpServletRequest request) {
        LOGGER.message("属性校验失败")
                .context("RequestURI", request.getRequestURI())
                .context("violations", exception.getConstraintViolations().stream()
                        .map(v -> v.getPropertyPath().toString() + "=" + v.getMessage())
                        .sorted()
                        .reduce((prev, next) -> prev + "," + next)
                        .map(result -> "[" + result + "]")
                        .orElse("[]"))
                .context("message", exception.getMessage())
                .warn();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(OptimisticLockingFailureException.class)
    public void optimisticLockingFailureException(OptimisticLockingFailureException e, HttpServletRequest request) {
        LOGGER.message("乐观锁并发冲突")
                .context("RequestURI", request.getRequestURI())
                .context("Message", e.getMessage())
                .error();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(InternalErrorException.class)
    public TurnRightResponse internalErrorExceptionHandler(InternalErrorException e) {
        TurnRightResponse turnRightResponse = new TurnRightResponse();
        turnRightResponse.setMessage(e.getMessage());
        turnRightResponse.setCode(400);
        turnRightResponse.setData(e.getPayload());
        return turnRightResponse;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = {
            NameEnumNotFoundException.class,
            ServiceException.class,
            SystemException.class,
            ValueEnumNotFoundException.class})
    public TurnRightResponse internalSystemErrorExceptionHandler(RuntimeException e, HttpServletRequest request) {
        LOGGER.message("系统内部错误")
                .context("RequestURI", request.getRequestURI())
                .context("Message", e.getMessage())
                .context("e", e.toString())
                .error();
        TurnRightResponse turnRightResponse = new TurnRightResponse();
        turnRightResponse.setMessage(e.getMessage());
        turnRightResponse.setCode(400);
        return turnRightResponse;
    }

    /**
     * 处理 @valid 验证失败异常信息返回
     *
     * @param exception methodArgumentNotValidException
     * @param headers   headers
     * @param status    status
     * @param request   domain
     * @return responseEntity
     */
    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, @NotNull HttpHeaders headers, @NotNull HttpStatusCode status, @NotNull WebRequest request) {
        BindingResult result = exception.getBindingResult();
        Map<String, String> data = new HashMap<>();
        String errorMsg = "参数校验失败";
        if (result.hasErrors()) {
            List<FieldError> fieldErrors = result.getFieldErrors();
            fieldErrors.forEach(error -> data.put(error.getField(), error.getDefaultMessage()));
            //修复errorMsg不是取payload第一条错误消息的问题 by weiride （fieldErrors底层返回的FieldError是乱序的）
            errorMsg = data.values().stream().findFirst().orElse(errorMsg);
        }
        TurnRightResponse response = new TurnRightResponse(new BadRequestException(errorMsg));
        response.setData(data);
        return new ResponseEntity<>(response, headers, 200);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(@NotNull TypeMismatchException ex, HttpHeaders headers, @NotNull HttpStatusCode status, @NotNull WebRequest request) {
        headers.setContentType(MediaType.APPLICATION_JSON);
        TurnRightResponse response = new TurnRightResponse(new BadRequestException("参数不合法"));
        return new ResponseEntity<>(response, headers, status);
    }
}
