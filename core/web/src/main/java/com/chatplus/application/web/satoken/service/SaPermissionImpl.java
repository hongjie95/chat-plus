package com.chatplus.application.web.satoken.service;

import cn.dev33.satoken.stp.StpInterface;
import com.chatplus.application.common.enumeration.UserTypeEnum;
import com.chatplus.application.web.satoken.helper.LoginHelper;
import com.chatplus.application.web.satoken.model.LoginUser;

import java.util.ArrayList;
import java.util.List;

/**
 * sa-token 权限管理实现类
 *
 * @author Lion Li
 */
public class SaPermissionImpl implements StpInterface {

    /**
     * 获取菜单权限列表
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        LoginUser loginUser = LoginHelper.getLoginUser();
        assert loginUser != null;
        UserTypeEnum userType = UserTypeEnum.getUserType(loginUser.getUserType());
        if (userType == UserTypeEnum.NORMAL_USER) {
            // return new ArrayList<>(loginUser.getMenuPermission());
            return List.of(UserTypeEnum.NORMAL_USER.getUserType());
        } else if (userType == UserTypeEnum.ADMIN_USER) {
            return List.of(UserTypeEnum.ADMIN_USER.getUserType());
        }
        return new ArrayList<>();
    }
    /**
     * 获取角色权限列表
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        LoginUser loginUser = LoginHelper.getLoginUser();
        assert loginUser != null;
        UserTypeEnum userType = UserTypeEnum.getUserType(loginUser.getUserType());
        // 对用户类型进行判断
        if (userType == UserTypeEnum.NORMAL_USER) {
            // return new ArrayList<>(loginUser.getRolePermission());
            return List.of(UserTypeEnum.NORMAL_USER.getUserType());
        } else if (userType == UserTypeEnum.ADMIN_USER) {
            // 其他端 自行根据业务编写
            return List.of(UserTypeEnum.ADMIN_USER.getUserType());
        }
        return new ArrayList<>();
    }
}
