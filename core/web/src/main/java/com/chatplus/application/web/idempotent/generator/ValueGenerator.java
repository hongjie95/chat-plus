package com.chatplus.application.web.idempotent.generator;

import java.lang.reflect.Method;

/**
 * 值生成器
 *
 * @author Angus
 */
public interface ValueGenerator {

    /**
     * 生成值
     *
     * @param key    当前 key
     * @param method 被调用的方法
     * @param args   参数值
     * @return key 实际值
     */
    Object generate(String key, Method method, Object... args);
}
