package com.chatplus.application.web.idempotent.concurrent;

import com.chatplus.application.web.idempotent.annotation.Concurrent;
import org.aopalliance.aop.Advice;
import org.jetbrains.annotations.NotNull;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.StaticMethodMatcherPointcut;

import java.io.Serial;
import java.lang.reflect.Method;

/**
 * aop 的并发切面处理实现
 *
 * @author Angus
 */
public class ConcurrentPointcutAdvisor extends AbstractPointcutAdvisor {

    @Serial
    private static final long serialVersionUID = -2973618152809395856L;

    private final ConcurrentInterceptor concurrentInterceptor;

    public ConcurrentPointcutAdvisor(ConcurrentInterceptor concurrentInterceptor) {
        this.concurrentInterceptor = concurrentInterceptor;
    }

    @NotNull
    @Override
    public Pointcut getPointcut() {
        return new StaticMethodMatcherPointcut() {
            @Override
            public boolean matches(@NotNull Method method, @NotNull Class<?> targetClass) {
                return method.isAnnotationPresent(Concurrent.class);
            }

        };
    }

    @NotNull
    @Override
    public Advice getAdvice() {
        return concurrentInterceptor;
    }
}
