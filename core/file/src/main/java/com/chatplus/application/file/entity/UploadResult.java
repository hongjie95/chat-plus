package com.chatplus.application.file.entity;


import java.io.Serializable;

/**
 * 上传返回体
 *
 * @author Lion Li
 */
public class UploadResult implements Serializable {

    /**
     * 文件路径
     */
    private String url;

    /**
     * 文件名
     */
    private String filename;

    public static UploadResult builder(String url, String filename) {
        UploadResult uploadResult = new UploadResult();
        uploadResult.setUrl(url);
        uploadResult.setFilename(filename);
        return uploadResult;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
