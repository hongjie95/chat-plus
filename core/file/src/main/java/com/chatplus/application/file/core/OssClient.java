package com.chatplus.application.file.core;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.IdUtil;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.HttpMethod;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.chatplus.application.file.dto.FileConfigDto;
import com.chatplus.application.file.entity.UploadResult;
import com.chatplus.application.file.enumd.AccessPolicyType;
import com.chatplus.application.file.enumd.PolicyType;
import com.chatplus.application.file.exception.OssException;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.Date;

/**
 * S3 存储协议 所有兼容S3协议的云厂商均支持
 * 阿里云 腾讯云 七牛云 minio
 *
 * @author Lion Li
 */
public class OssClient {
    
    private static final String FILE_SEPARATOR = "/";
    private final FileConfigDto fileConfigDto;
    private final AmazonS3 client;
    private AmazonS3 minioExtranetClient = null;
    private static final String[] CLOUD_SERVICE = new String[]{"aliyun", "qcloud", "qiniu", "obs"};


    public OssClient(FileConfigDto fileConfigDto) {
        this.fileConfigDto = fileConfigDto;
        try {
            Boolean isHttps = fileConfigDto.getHttps();
            this.client = builder(fileConfigDto.getEndpoint(), isHttps).build();
            createBucket();
            // minio 单独处理
            if (StringUtils.isNotBlank(fileConfigDto.getDomain())) {
                URI url = new URI(fileConfigDto.getDomain());
                String protocol = url.getScheme();
                String host = url.getHost();
                this.minioExtranetClient = builder(host, "https".equals(protocol)).build();
            }
        } catch (OssException ossException) {
            throw ossException;
        } catch (Exception e) {
            throw new OssException("配置错误! 请检查系统配置:[" + e.getMessage() + "]");
        }
    }

    private AmazonS3ClientBuilder builder(String endpoint, Boolean isHttps) {
        AwsClientBuilder.EndpointConfiguration endpointConfig =
                new AwsClientBuilder.EndpointConfiguration(endpoint, fileConfigDto.getRegion());

        AWSCredentials credentials = new BasicAWSCredentials(fileConfigDto.getAccessKey(), fileConfigDto.getSecretKey());
        AWSCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(credentials);
        ClientConfiguration clientConfig = new ClientConfiguration();
        if (Boolean.TRUE.equals(isHttps)) {
            clientConfig.setProtocol(Protocol.HTTPS);
        } else {
            clientConfig.setProtocol(Protocol.HTTP);
        }
        clientConfig.setMaxConnections(100);
        AmazonS3ClientBuilder build = AmazonS3Client.builder()
                .withEndpointConfiguration(endpointConfig)
                .withClientConfiguration(clientConfig)
                .withCredentials(credentialsProvider)
                .disableChunkedEncoding();
        if (!StringUtils.containsAny(endpoint, CLOUD_SERVICE)) {
            // minio 使用https限制使用域名访问 需要此配置 站点填域名
            build.enablePathStyleAccess();
        }
        return build;
    }

    public void createBucket() {
        try {
            String bucketName = fileConfigDto.getBucketName();
            if (client.doesBucketExistV2(bucketName)) {
                return;
            }
            CreateBucketRequest createBucketRequest = new CreateBucketRequest(bucketName);
            AccessPolicyType accessPolicy = getAccessPolicy();
            createBucketRequest.setCannedAcl(accessPolicy.getAcl());
            client.createBucket(createBucketRequest);
            client.setBucketPolicy(bucketName, getPolicy(bucketName, accessPolicy.getPolicyType()));
        } catch (Exception e) {
            throw new OssException("创建Bucket失败, 请核对配置信息:[" + e.getMessage() + "]");
        }
    }

    public UploadResult upload(byte[] data, String path, String contentType) {
        return upload(new ByteArrayInputStream(data), path, contentType);
    }

    public UploadResult upload(InputStream inputStream, String path, String contentType) {
        if (!(inputStream instanceof ByteArrayInputStream)) {
            inputStream = new ByteArrayInputStream(IoUtil.readBytes(inputStream));
        }
        try {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(contentType);
            metadata.setContentLength(inputStream.available());
            PutObjectRequest putObjectRequest = new PutObjectRequest(fileConfigDto.getBucketName(), path, inputStream, metadata);
            // 设置上传对象的 Acl 为公共读
            putObjectRequest.setCannedAcl(getAccessPolicy().getAcl());
            client.putObject(putObjectRequest);
        } catch (Exception e) {
            throw new OssException("上传文件失败，请检查配置信息:[" + e.getMessage() + "]");
        }
        return UploadResult.builder(getUrl() + FILE_SEPARATOR + path, path);
    }

    public UploadResult upload(File file, String path) {
        try {
            PutObjectRequest putObjectRequest = new PutObjectRequest(fileConfigDto.getBucketName(), path, file);
            // 设置上传对象的 Acl 为公共读
            putObjectRequest.setCannedAcl(getAccessPolicy().getAcl());
            client.putObject(putObjectRequest);
        } catch (Exception e) {
            throw new OssException("上传文件失败，请检查配置信息:[" + e.getMessage() + "]");
        }
        return UploadResult.builder(getUrl() + FILE_SEPARATOR + path, path);
    }

    public void delete(String path) {
        path = path.replace(getUrl() + FILE_SEPARATOR, "");
        try {
            client.deleteObject(fileConfigDto.getBucketName(), path);
        } catch (Exception e) {
            throw new OssException("删除文件失败，请检查配置信息:[" + e.getMessage() + "]");
        }
    }

    public UploadResult uploadSuffix(byte[] data, String suffix, String contentType) {
        return upload(data, getPath(fileConfigDto.getPrefix(), suffix), contentType);
    }

    public UploadResult uploadSuffix(InputStream inputStream, String suffix, String contentType) {
        return upload(inputStream, getPath(fileConfigDto.getPrefix(), suffix), contentType);
    }

    public UploadResult uploadSuffix(File file, String suffix) {
        return upload(file, getPath(fileConfigDto.getPrefix(), suffix));
    }

    /**
     * 获取文件元数据
     *
     * @param path 完整文件路径
     */
    public ObjectMetadata getObjectMetadata(String path) {
        path = path.replace(getUrl() + FILE_SEPARATOR, "");
        S3Object object = client.getObject(fileConfigDto.getBucketName(), path);
        return object.getObjectMetadata();
    }

    public InputStream getObjectContent(String path) {
        path = path.replace(getUrl() + FILE_SEPARATOR, "");
        S3Object object = client.getObject(fileConfigDto.getBucketName(), path);
        return object.getObjectContent();
    }

    public String getUrl() {
        String domain = fileConfigDto.getDomain();
        String endpoint = fileConfigDto.getEndpoint();
        String header = Boolean.TRUE.equals(fileConfigDto.getHttps()) ? "https://" : "http://";
        // 云服务商直接返回
        if (StringUtils.containsAny(endpoint, CLOUD_SERVICE)) {
            if (StringUtils.isNotBlank(domain)) {
                return domain;
            }
            return header + fileConfigDto.getBucketName() + "." + endpoint;
        }
        // minio 单独处理
        if (StringUtils.isNotBlank(domain)) {
            return domain + FILE_SEPARATOR + fileConfigDto.getBucketName();
        }
        return header + endpoint + FILE_SEPARATOR + fileConfigDto.getBucketName();
    }

    public String getPath(String prefix, String suffix) {
        // 生成uuid
        String uuid = IdUtil.fastSimpleUUID();
        // 文件路径
        String path = DateUtil.format(new Date(), DatePattern.PURE_DATE_PATTERN) + FILE_SEPARATOR + uuid;
        if (StringUtils.isNotBlank(prefix)) {
            path = prefix + FILE_SEPARATOR + path;
        }
        return path + suffix;
    }
    /**
     * 获取私有URL链接
     *
     * @param objectKey 对象KEY
     * @param second    授权时间
     */
    public String getPrivateUrl(String objectKey, Integer second) {
        GeneratePresignedUrlRequest generatePresignedUrlRequest =
                new GeneratePresignedUrlRequest(fileConfigDto.getBucketName(), objectKey)
                        .withMethod(HttpMethod.GET)
                        .withExpiration(new Date(System.currentTimeMillis() + 1000L * second));
        URL url;
        if (minioExtranetClient != null) {
            url = minioExtranetClient.generatePresignedUrl(generatePresignedUrlRequest);
        } else {
            url = client.generatePresignedUrl(generatePresignedUrlRequest);
        }
        return url.toString();
    }

    public boolean checkFileExist(String path) {
        path = path.replace(getUrl() + FILE_SEPARATOR, "");
        return client.doesObjectExist(fileConfigDto.getBucketName(), path);
    }
    /**
     * 获取当前桶权限类型
     *
     * @return 当前桶权限类型code
     */
    public AccessPolicyType getAccessPolicy() {
        return AccessPolicyType.getByType(fileConfigDto.getAccessPolicy());
    }

    private static String getPolicy(String bucketName, PolicyType policyType) {
        StringBuilder builder = new StringBuilder();
        builder.append("{\n\"Statement\": [\n{\n\"Action\": [\n");
        if (policyType == PolicyType.WRITE) {
            builder.append("\"s3:GetBucketLocation\",\n\"s3:ListBucketMultipartUploads\"\n");
        } else if (policyType == PolicyType.READ_WRITE) {
            builder.append("\"s3:GetBucketLocation\",\n\"s3:ListBucket\",\n\"s3:ListBucketMultipartUploads\"\n");
        } else {
            builder.append("\"s3:GetBucketLocation\"\n");
        }
        builder.append("],\n\"Effect\": \"Allow\",\n\"Principal\": \"*\",\n\"Resource\": \"arn:aws:s3:::");
        builder.append(bucketName);
        builder.append("\"\n},\n");
        if (policyType == PolicyType.READ) {
            builder.append("{\n\"Action\": [\n\"s3:ListBucket\"\n],\n\"Effect\": \"Deny\",\n\"Principal\": \"*\",\n\"Resource\": \"arn:aws:s3:::");
            builder.append(bucketName);
            builder.append("\"\n},\n");
        }
        builder.append("{\n\"Action\": ");
        switch (policyType) {
            case WRITE:
                builder.append("[\n\"s3:AbortMultipartUpload\",\n\"s3:DeleteObject\",\n\"s3:ListMultipartUploadParts\",\n\"s3:PutObject\"\n],\n");
                break;
            case READ_WRITE:
                builder.append("[\n\"s3:AbortMultipartUpload\",\n\"s3:DeleteObject\",\n\"s3:GetObject\",\n\"s3:ListMultipartUploadParts\",\n\"s3:PutObject\"\n],\n");
                break;
            default:
                builder.append("\"s3:GetObject\",\n");
                break;
        }
        builder.append("\"Effect\": \"Allow\",\n\"Principal\": \"*\",\n\"Resource\": \"arn:aws:s3:::");
        builder.append(bucketName);
        builder.append("/*\"\n}\n],\n\"Version\": \"2012-10-17\"\n}\n");
        return builder.toString();
    }

}
