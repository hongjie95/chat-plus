package com.chatplus.application.file.enumd;


/**
 * minio策略配置
 */
public enum PolicyType {
    /**
     * 只读
     */
    READ("read-only"),

    /**
     * 只写
     */
    WRITE("write-only"),

    /**
     * 读写
     */
    READ_WRITE("read-write");

    PolicyType(String type) {
        this.type = type;
    }
    /**
     * 类型
     */
    private final String type;

    public String getType() {
        return type;
    }
}
