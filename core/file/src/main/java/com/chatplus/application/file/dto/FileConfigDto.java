package com.chatplus.application.file.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * 存储配置
 * minio配置
 */
public class FileConfigDto implements Serializable {
    /**
     * 配置key，oss 或者 local
     */
    @JsonProperty("config_key")
    private String configKey;
    /**
     * 存储类型为local时，本地路径
     */
    @JsonProperty("local_path")
    private String localPath;
    /**
     * 文件访问地址，如果设置oss设置为私有的话，这里不能为空
     */
    @JsonProperty("access_base_url")
    private String accessBaseUrl;
    /**
     * accessKey
     */
    @JsonProperty("access_key")
    private String accessKey;
    /**
     * 秘钥
     */
    @JsonProperty("secret_key")
    private String secretKey;
    /**
     * 桶名称
     */
    @JsonProperty("bucket_name")
    private String bucketName;
    /**
     * 前缀
     */
    private String prefix;
    /**
     * 访问站点，如果是minio，则是minio的9000端口
     */
    private String endpoint;
    /**
     * 自定义的CDN域名
     */
    private String domain;
    /**
     * 域
     */
    private String region;
    /**
     * 桶权限类型(0=private 1=public 2=custom)
     */
    @JsonProperty("access_policy")
    private String accessPolicy;

    @JsonProperty("https")
    private Boolean https;

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public String getAccessBaseUrl() {
        return accessBaseUrl;
    }

    public void setAccessBaseUrl(String accessBaseUrl) {
        this.accessBaseUrl = accessBaseUrl;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAccessPolicy() {
        return accessPolicy;
    }

    public void setAccessPolicy(String accessPolicy) {
        this.accessPolicy = accessPolicy;
    }

    public Boolean getHttps() {
        return https;
    }

    public void setHttps(Boolean https) {
        this.https = https;
    }
}
