package com.chatplus.application.datasource.filter.generator.mybatis.wildcard;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chatplus.application.common.exception.SystemException;
import com.chatplus.application.datasource.filter.Property;
import com.chatplus.application.datasource.filter.generator.WildcardParser;

import java.util.ArrayList;
import java.util.List;

/**
 * 范围查询通配符实现
 *
 * @author chenxiaobo
 */
public class BetweenWildcardParser implements WildcardParser {

    private final static String DEFAULT_WILDCARD_NAME = "between";

    @Override
    public void structure(Property property, QueryWrapper<?> queryWrapper) {

        if (!Iterable.class.isAssignableFrom(property.getValue().getClass())) {
            throw new SystemException("Between 查询的参数值的数组必须大于 1 位");
        }

        Iterable<?> iterable = (Iterable<?>) property.getValue();
        List<Object> values = new ArrayList<>();

        iterable.forEach(values::add);

        if (values.size() < 2) {
            throw new SystemException("Between 查询必须参数值的数组大小为 2 位");
        }

        queryWrapper.between(property.getPropertyName(), values.iterator().next(), values.get(values.size() - 1));
    }

    @Override
    public boolean isSupport(String condition) {
        return DEFAULT_WILDCARD_NAME.equals(condition);
    }
}
