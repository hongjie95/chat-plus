package com.chatplus.application.datasource.annotation;


import com.chatplus.application.datasource.json.support.JacksonJsonCollectionPostInterceptor;

import java.lang.annotation.*;

/**
 * json 集合泛型类型声明注解，用于持久化实体带有 json 数组形式的字段时，告知 objectMapper 该字段的泛型类型时什么。
 *
 * @author maurice.chen
 * @see JacksonJsonCollectionPostInterceptor
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.FIELD})
public @interface JsonCollectionGenericType {

    /**
     * 泛型类型
     *
     * @return 泛型类型 class
     */
    Class<?> value();

}
