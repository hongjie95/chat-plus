package com.chatplus.application.datasource.filter.condition;


import com.chatplus.application.common.util.enumeration.NameEnum;

/**
 * 条件类型
 *
 * @author chenxiaobo
 */
public enum ConditionType implements NameEnum {

    /**
     * 并且条件
     */
    And("并且条件"),
    /**
     * 或者条件
     */
    Or("或者条件");

    ConditionType(String name) {
        this.name = name;
    }

    private final String name;

    @Override
    public String getName() {
        return name;
    }
}
