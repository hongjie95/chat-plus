package com.chatplus.application.datasource.id;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.time.Instant;

/**
 * 逻辑删除实体
 *
 * @author chenxiaobo
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class LogicDeleteEntity extends IdEntity {

    @Serial
    private static final long serialVersionUID = 2906056237154364425L;

    /**
     * 更新时间字段名称
     */
    public static final String DELETED_AT_FIELD_NAME = "deletedAt";

    private Instant deletedAt;
}
