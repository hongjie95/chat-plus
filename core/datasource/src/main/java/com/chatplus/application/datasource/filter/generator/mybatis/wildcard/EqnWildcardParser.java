package com.chatplus.application.datasource.filter.generator.mybatis.wildcard;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chatplus.application.datasource.filter.Property;
import com.chatplus.application.datasource.filter.generator.WildcardParser;
import org.apache.commons.lang3.BooleanUtils;

/**
 * 等于 null 的通配符实现
 *
 * @author chenxiaobo
 */
public class EqnWildcardParser implements WildcardParser {

    private final static String DEFAULT_WILDCARD_NAME = "eqn";

    @Override
    public void structure(Property property, QueryWrapper<?> queryWrapper) {
        if (BooleanUtils.toBoolean(property.getValue().toString())) {
            queryWrapper.isNull(property.getPropertyName());
        }
    }

    @Override
    public boolean isSupport(String condition) {
        return DEFAULT_WILDCARD_NAME.equals(condition);
    }
}
