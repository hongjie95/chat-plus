package com.chatplus.application.datasource.id;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
public class BasicIdEntity<T> extends Model implements Serializable {

    @Serial
    private static final long serialVersionUID = -2579649132362949467L;

    private T id;
}
