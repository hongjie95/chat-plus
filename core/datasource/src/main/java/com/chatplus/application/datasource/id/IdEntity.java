package com.chatplus.application.datasource.id;

import com.baomidou.mybatisplus.annotation.*;
import com.chatplus.application.json.Int64AsString;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.chatplus.application.common.exception.SystemException;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.time.Instant;

/**
 * 主键实体
 *
 * @author chenxiaobo
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class IdEntity extends BasicIdEntity<Long> {

    @Serial
    private static final long serialVersionUID = -1430010950223063423L;

    /**
     * id 字段名称
     */
    public static final String ID_FIELD_NAME = "id";

    /**
     * 创建时间字段名称
     */
    public static final String CREATED_AT_FIELD_NAME = "createdAt";

    /**
     * 更新时间字段名称
     */
    public static final String UPDATED_AT_FIELD_NAME = "updatedAt";

    /**
     * 主键 id
     */
    @JsonSerialize(using = Int64AsString.class)
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Instant createdAt;

    /**
     * 最后更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Instant updatedAt;

    /**
     * 版本号
     */
    @Version
    private Integer schemeVersion = 0;

    /**
     * 创建一个新的对象，并把 idEntity 的 id 复制到新对象中
     *
     * @param <T> 主键实体子类
     * @return 新的主键实体
     */
    public <T extends IdEntity> T ofNew() {
        T result;
        try {
            //noinspection unchecked
            result = (T) this.getClass().getConstructor().newInstance();
            result.setCreatedAt(null);
            result.setId(getId());
            return result;
        } catch (Exception e) {
            throw new SystemException(e);
        }
    }
}
