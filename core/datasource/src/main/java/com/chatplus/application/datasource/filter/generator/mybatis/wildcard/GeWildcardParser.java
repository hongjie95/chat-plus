package com.chatplus.application.datasource.filter.generator.mybatis.wildcard;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chatplus.application.datasource.filter.Property;
import com.chatplus.application.datasource.filter.generator.WildcardParser;

/**
 * 大于等于查询通配符实现
 *
 * @author chenxiaobo
 */
public class GeWildcardParser implements WildcardParser {

    private final static String DEFAULT_WILDCARD_NAME = "ge";

    @Override
    public void structure(Property property, QueryWrapper<?> queryWrapper) {
        queryWrapper.ge(property.getPropertyName(), property.getValue());
    }

    @Override
    public boolean isSupport(String condition) {
        return DEFAULT_WILDCARD_NAME.equals(condition);
    }
}
