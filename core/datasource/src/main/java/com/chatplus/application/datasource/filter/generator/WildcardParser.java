package com.chatplus.application.datasource.filter.generator;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chatplus.application.datasource.filter.Property;

/**
 * 通配符解析器
 *
 * @author chenxiaobo
 */
public interface WildcardParser {

    /**
     * 构造 queryWrapper
     *
     * @param property     属性信息
     * @param queryWrapper 当前 queryWrapper
     */
    void structure(Property property, QueryWrapper<?> queryWrapper);

    /**
     * 是否支持条件
     *
     * @param condition 条件，识别符
     * @return true 是，否则 false
     */
    boolean isSupport(String condition);
}
