package com.chatplus.application.datasource.json.support;

import com.chatplus.application.common.util.enumeration.NameEnum;
import com.chatplus.application.common.util.enumeration.ValueEnum;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.chatplus.application.common.exception.SystemException;
import com.chatplus.application.common.util.Casts;
import com.chatplus.application.common.util.ReflectionUtils;
import com.chatplus.application.datasource.annotation.JsonCollectionGenericType;
import com.chatplus.application.datasource.handler.NameValueEnumTypeHandler;
import com.chatplus.application.datasource.json.AbstractJsonCollectionPostInterceptor;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * jackson json 实现的 json 集合后续映射拦截器
 *
 * @author maurice.chen
 */
@Intercepts(
        @Signature(
                type = Executor.class,
                method = "query",
                args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}
        )
)
public class JacksonJsonCollectionPostInterceptor extends AbstractJsonCollectionPostInterceptor {

    @Override
    protected Object doMappingResult(Object result, Class<?> type, List<PropertyDescriptor> propertyDescriptors) {
        // 循环目标类型的属性去，通过 get 方法获取返回值类型，
        // 如果改类型是 Collection 父类 做具体转型操作。
        for (PropertyDescriptor pd : propertyDescriptors) {
            Class<?> collectionClass = null;

            Field field = ReflectionUtils.findFiled(result, pd.getName());
            if (Objects.nonNull(field)) {
                collectionClass = field.getType();
            }

            Method method = pd.getReadMethod();
            if (Objects.nonNull(method)) {
                collectionClass = pd.getReadMethod().getReturnType();
            }

            if (Objects.isNull(collectionClass) || !Collection.class.isAssignableFrom(collectionClass)) {
                continue;
            }

            Object value = ReflectionUtils.getFieldValue(result, pd.getName());
            if (Objects.isNull(value)) {
                continue;
            }

            JsonCollectionGenericType ann = getJsonCollectionGenericType(pd, type);
            Class<?> targetClass = ann.value();

            Object newValue;
            // 如果为枚举特殊类的泛型，将数据转换成具体的枚举类型， 否则通过 TypeFactory 构造一个新的泛型值
            if (ValueEnum.class.isAssignableFrom(targetClass) || NameEnum.class.isAssignableFrom(targetClass)) {
                Collection<?> collection = Casts.cast(value);
                Stream<Object> stream = collection
                        .stream()
                        .map(o -> getEnumValue(o, targetClass)).filter(Objects::nonNull);
                if (List.class.equals(collectionClass)) {
                    newValue = stream.collect(Collectors.toList());
                } else if (Set.class.equals(collectionClass)) {
                    newValue = stream.collect(Collectors.toSet());
                } else {
                    throw new SystemException("找不到对应类型为 [" + collectionClass + "] 的集合处理方式");
                }

            } else {
                CollectionType collectionType = TypeFactory
                        .defaultInstance()
                        .constructCollectionType(Casts.cast(collectionClass), targetClass);
                newValue = Casts.convertValue(value, collectionType);
            }
            // 如果存在 set 方法调用 set 方法进行赋值，否则通过字段名称进行赋值。
            if (Objects.nonNull(pd.getWriteMethod())) {
                ReflectionUtils.invokeMethod(result, pd.getWriteMethod(), List.of(newValue));
            } else {
                ReflectionUtils.setFieldValue(result, pd.getName(), newValue);
            }
        }
        return result;
    }

    private Object getEnumValue(Object o, Class<?> targetClass) {
        Object result = NameValueEnumTypeHandler.getValue(o, targetClass);

        if (Objects.isNull(result)) {
            result = Enum.valueOf(Casts.cast(targetClass), o.toString());
        }

        return result;
    }
}
