package com.chatplus.application.json;


import cn.hutool.core.collection.CollUtil;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public interface JsonReader {
    /**
     * 把字节流转换为指定的JavaBean。
     */
    <T> T read(InputStream inputStream, Class<T> cls);

    default <T> T read(String json, Class<T> cls) {
        return read(new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8)), cls);
    }

    default <T> T read(byte[] bytes, Class<T> cls) {
        return read(new ByteArrayInputStream(bytes), cls);
    }

    /**
     * 把对象转换为指定的JavaBean。
     */
    <T> T convert(Object object, Class<T> cls);

    /**
     * 把一个map list组合的对象转换为指定的JavaBean列表。
     */
    default <T> List<T> convertToList(List<?> list, Class<T> cls) {
        if (CollUtil.isEmpty(list)) {
            return Collections.emptyList();
        }

        return list.stream()
            .map(item -> convert(item, cls))
            .collect(Collectors.toList());
    }
}
