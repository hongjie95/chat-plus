package com.chatplus.application.json;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;

public class TurnRightJsonReader implements JsonReader {

    private final ObjectMapper objectMapper;

    public TurnRightJsonReader(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public <T> T read(InputStream inputStream, Class<T> cls) {
        try {
            return objectMapper.readValue(inputStream, cls);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> T convert(Object object, Class<T> cls) {
        return objectMapper.convertValue(object, cls);
    }

}
