package com.chatplus.application.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.List;

public class Int64ArrayAsStringArray extends JsonSerializer<List<Long>> {
    @Override
    public void serialize(List<Long> list, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        if (null == list) {
            gen.writeNull();
        } else {
            String[] array = list.stream().map(l -> l == null ? null : l.toString()).toArray(String[]::new);
            gen.writeArray(array, 0, array.length);
        }
    }
}
