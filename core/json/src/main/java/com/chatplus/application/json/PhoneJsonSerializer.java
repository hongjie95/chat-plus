package com.chatplus.application.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Objects;

/**
 * 手机号脱敏
 */
public class PhoneJsonSerializer extends JsonSerializer {
    @Override
    public void serialize(Object value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        if (value instanceof String){
            if (!Objects.equals(value,"") && value.toString().length() > 5){
                String phone = value.toString();
                phone = phone.substring(0, 3) + "******" + phone.substring(phone.length() - 2);
                gen.writeString(phone);
            } else {
                gen.writeObject(value);
            }
        } else {
            gen.writeObject(value);
        }
    }
}
