package com.chatplus.application.common.util;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;

class InMemoryCachesTest {

    @Test
    void testRunWithCache() {
        AtomicInteger runCount = new AtomicInteger();
        AtomicInteger loadCount = new AtomicInteger();
        String key = "key";
        int totalCount = 10;
        InMemoryCaches.runWithCache(wrapperCache -> {
            for (int i = 0; i < totalCount; i++) {
                runCount.incrementAndGet();
                final int value = i;
                Object v = wrapperCache.get(key, () -> {
                    loadCount.incrementAndGet();
                    return value;
                });
                Assert.assertEquals(0, v);
            }
        });

        Assert.assertEquals(1, loadCount.get());
        Assert.assertEquals(totalCount, runCount.get());
    }
}
