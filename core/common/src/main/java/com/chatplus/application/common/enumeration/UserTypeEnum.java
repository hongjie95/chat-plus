package com.chatplus.application.common.enumeration;

import org.apache.commons.lang3.StringUtils;

/**
 * 设备类型
 * 针对多套 用户体系
 *
 * @author Lion Li
 */
public enum UserTypeEnum {
    /**
     * 管理员
     */
    ADMIN_USER("admin_user"),

    /**
     * 普通用户
     */
    NORMAL_USER("normal_user");

    private final String userType;

    public String getUserType() {
        return userType;
    }
    UserTypeEnum(String userType) {
        this.userType = userType;
    }
    public static UserTypeEnum getUserType(String str) {
        for (UserTypeEnum value : values()) {
            if (StringUtils.contains(str, value.getUserType())) {
                return value;
            }
        }
        throw new RuntimeException("'UserTypeEnum' not found By " + str);
    }
}
