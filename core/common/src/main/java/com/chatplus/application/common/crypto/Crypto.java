package com.chatplus.application.common.crypto;

import java.lang.annotation.*;

/**
 * 加密标记注解，标记该注解的请求参数或字段被解密为明文；
 *
 * @author Angus
 */
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Crypto {
    /**
     * 参数名称
     *
     * @return 参数名称
     */
    String name() default "";
}
