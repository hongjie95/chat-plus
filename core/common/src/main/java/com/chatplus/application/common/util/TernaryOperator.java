package com.chatplus.application.common.util;

import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

/**
 * @Author liexuan
 * @Description: 三目运算封装
 * @ClassName: com.chatplus.api.utils.TernaryOperatorUtil
 * @Date 18:40 - 2020/10/26.
 */
public class TernaryOperator {

    private TernaryOperator() {
    }


    /**
     * 自定义处理三目运算
     *
     * @param bool       条件
     * @param trueValue  true 的时候返回
     * @param falseValue false 的时候返回
     * @param <T>
     * @return
     */
    public static <T> T getHopeObj(boolean bool, T trueValue, T falseValue) {
        if (bool) {
            return trueValue;
        }
        return falseValue;
    }

    /**
     * 根据条件决定返回值
     *
     * @param flagHandle 自定义的判断条件
     * @param handle     自定义的返回值
     * @param defaultObj 条件不成立时返回
     * @param <T>
     * @return
     */
    public static <T> T defaultIfFalse(BooleanSupplier flagHandle, Supplier<T> handle, T defaultObj) {
        if (flagHandle.getAsBoolean()) {
            return handle.get();
        }
        return defaultObj;
    }

    /**
     * 根据条件决定返回值
     *
     * @param bool       条件
     * @param handle     自定义的返回值
     * @param defaultObj 条件不成立时返回
     * @param <T>
     * @return
     */
    public static <T> T defaultIfFalse(boolean bool, Supplier<T> handle, T defaultObj) {
        if (bool) {
            return handle.get();
        }
        return defaultObj;
    }

}
