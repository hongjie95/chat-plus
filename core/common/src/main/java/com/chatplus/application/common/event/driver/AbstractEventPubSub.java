package com.chatplus.application.common.event.driver;

import com.chatplus.application.common.event.EventPubSub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class AbstractEventPubSub<E> implements EventPubSub<E> {
    private final ApplicationContext applicationContext;

    @Autowired
    public AbstractEventPubSub(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void publish(E event) {
        applicationContext.publishEvent(event);
    }
}
