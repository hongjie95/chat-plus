package com.chatplus.application.common.exception;


import com.chatplus.application.common.domain.response.TurnRightResponse;
import jakarta.validation.ConstraintViolationException;

import java.util.HashMap;
import java.util.function.Supplier;

/**
 * @author
 */
@SuppressWarnings("unused")
public class BadRequestException extends RuntimeException {
    private final static long serialVersionUID = 2744600180962486567L;

    private Object data;

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Object data) {
        super(message);
        this.data = data;
    }

    public BadRequestException(TurnRightResponse response) {
        super(response.getMessage());
        this.data = response.getData();
    }

    @SuppressWarnings("unused")
    public BadRequestException(ConstraintViolationException exception) {
        super(exception.getMessage());

        HashMap<String, String> data = new HashMap<>();
        exception.getConstraintViolations().forEach(violation ->
                data.put(violation.getPropertyPath().toString(), violation.getMessage())
        );
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static void throwIfTrue(boolean condition, String msg) {
        if (condition) {
            throw new BadRequestException(msg);
        }
    }

    public static Supplier<BadRequestException> supplierOf(String message) {
        return () -> new BadRequestException(message);
    }

    public static Supplier<BadRequestException> supplierOf(String message, Object payload) {
        return () -> new BadRequestException(message, payload);
    }
}
