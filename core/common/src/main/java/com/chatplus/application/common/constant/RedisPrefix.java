package com.chatplus.application.common.constant;

/**
 * redis key 前缀常量
 */
public class RedisPrefix {
    private RedisPrefix() {
    }

    /**
     * qbzg.merchant 项目固定前缀
     */
    public static final String TURN_RIGHT_REDIS_PREFIX = "chat-plus:";

    public static final String EXTEND_REDIS_PREFIX_TEST = TURN_RIGHT_REDIS_PREFIX + "extend:";
    /**
     * SESSION
     */
    public static final String SESSION_PREFIX = TURN_RIGHT_REDIS_PREFIX + "session:";
    /**
     * ROLE
     */
    public static final String AI_ROLE_PREFIX = TURN_RIGHT_REDIS_PREFIX + "ai_role:";
    /**
     * 注册登录模块key前缀
     */
    public static final String USER_LOGIN_PREFIX = TURN_RIGHT_REDIS_PREFIX + "login:";
    public static final String PAY_STATUS_PREFIX = TURN_RIGHT_REDIS_PREFIX + "pay:status:";
    /**
     * 注册登录模块短信验证码key前缀
     */
    public static final String USER_LOGIN_SMS_CAPTCHA_PREFIX = USER_LOGIN_PREFIX + "sms-captcha:";
    /**
     * 防重提交 redis key
     */
    public static final String REPEAT_SUBMIT_KEY = "repeat_submit:";
    /**
     * 限流 redis key
     */
    public static final String RATE_LIMIT_KEY = "rate_limit:";

}
