package com.chatplus.application.common.event;


/**
 * 发布及广播事件。
 * 广播指发布到当前ApplicationContext之外。
 */
public interface EventPubSub<E> {
    void publish(E event);
}
