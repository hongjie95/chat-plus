package com.chatplus.application.common.domain.response;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * 敏感词过滤结果对象
 */
@Data
@Schema(description = "敏感词过滤结果对象")
public class SensitiveWordFilterResultResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = -4237275481915866511L;
    @Schema(description = "输入内容")
    private String input;
    @Schema(description = "去敏感词后的内容")
    private String output;
    @Schema(description = "是否包含敏感词")
    private boolean containsSensitiveWord;
    @Schema(description = "触发的敏感词列表")
    private List<String> sensitiveWordList = Collections.emptyList();

}
