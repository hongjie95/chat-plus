package com.chatplus.application.common.util;

import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import org.springframework.beans.BeanUtils;

public class ModelCopyUtils {

    private ModelCopyUtils() {
    }
    private static final SouthernQuietLogger log = SouthernQuietLoggerFactory.getLogger(ModelCopyUtils.class);

    public static <T> T copy(Object src, Class<T> clazz, String... ignoreProperties) {
        if (src == null) {
            return null;
        }
        try {
            T t = clazz.getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(src, t, ignoreProperties);
            return t;
        } catch (Exception e) {
            log.message("ModelCopyUtils creates newInstance error")
                .exception(e)
                .error();
            return null;
        }
    }

}
