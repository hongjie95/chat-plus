package com.chatplus.application.common.crypto;

/**
 * 加密请求Body的包装类，请求体类型可继承该类
 *
 * @author Angus
 */
public class CryptoBodyWrapper implements CryptoBody {
    private String cipherBody;

    public String getCipherBody() {
        return cipherBody;
    }

    public void setCipherBody(String cipherBody) {
        this.cipherBody = cipherBody;
    }
}
