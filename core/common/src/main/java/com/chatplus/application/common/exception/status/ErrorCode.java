package com.chatplus.application.common.exception.status;

import java.util.Objects;

/**
 * description: 错误码
 *
 * @author liexuan
 * @date 2021-11-05 14:53
 **/
public enum ErrorCode implements Code {
    INVALID_PARAMETER(1001, "参数不合法"),
    INVALID_PARAMETER_TYPE(1002, "参数类型不匹配"),
    SYSTEM_ERROR_TRY_AGAIN(1006, "系统错误，请稍后重试"),
    SYSTEM_VERIFY_SIGN_ERROR(1008, "签名不正确，验签失败"),
    TOO_MANY_REQUEST(100, "接口重复提交"),
    PAY_ORDER_DISABLED(10001, "抱歉，服务暂不可用，请稍后重试"),
    ;


    private final Integer code;
    private final String msg;

    ErrorCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getMsg() {
        return this.msg;
    }


    public static ErrorCode getEnum(Integer code) {
        for (ErrorCode ele : ErrorCode.values()) {
            if (Objects.equals(code, ele.getCode())) {
                return ele;
            }
        }
        return null;
    }
}
