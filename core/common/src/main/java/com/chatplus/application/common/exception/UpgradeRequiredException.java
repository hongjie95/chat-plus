package com.chatplus.application.common.exception;


/**
 * @author Angus
 */
public class UpgradeRequiredException extends RuntimeException {
    private static final long serialVersionUID = 2691753330848651876L;

    public UpgradeRequiredException() {
    }

    public UpgradeRequiredException(String message) {
        super(message);
    }
}
