package com.chatplus.application.common.enumeration;

public enum PhoneNumberSection {
    /**
     * 联通
     */
    ChinaUnicom,
    /**
     * 移动
     */
    ChinaMobile,
    /**
     * 电信
     */
    ChinaTelecom,
    /**
     * 中国广电
     */
    ChinaBroadcastingNetwork,
    /**
     * 中国交通运输通信
     */
    ChinaTransportTelecommunication,
    /**
     * 未知
     */
    Unknown
}
