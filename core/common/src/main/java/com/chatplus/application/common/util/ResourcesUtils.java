package com.chatplus.application.common.util;

import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class ResourcesUtils {

    private ResourcesUtils() {
    }

    public static final SouthernQuietLogger log = SouthernQuietLoggerFactory.getLogger(ResourcesUtils.class);

    public static String getResourceContentString(String filePath) {
        ClassPathResource resource = new ClassPathResource(filePath);
        String source;
        try (InputStream resourceInputStream = resource.getInputStream()) {
            source = StreamUtils.copyToString(resourceInputStream, StandardCharsets.UTF_8);
        } catch (IOException e) {
            log.message("getResourceContentString error")
                .context("filePath", filePath)
                .exception(e)
                .error();
            source = null;
        }
        return source;
    }
    public static InputStream getResourceInputStream(String filePath) throws IOException {
        ClassPathResource resource = new ClassPathResource(filePath);
        return resource.getInputStream();
    }

}
