package com.chatplus.application.common.util;

/**
 * @Author liexuan
 * @Description: 只提供执行体，不需要参数也没有返回值
 * @ClassName: com.chatplus.api.utils.Void
 * @Date 14:03 - 2020/11/4.
 */
@FunctionalInterface
public interface VoidConsumer {


    /**
     * 执行体
     */
    void consume();
}
