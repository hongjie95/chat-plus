package com.chatplus.application.common.constant;

/**
 * 缓存组名称常量
 * <p>
 * key 格式为 cacheNames#ttl#maxIdleTime#maxSize
 * <p>
 * ttl 过期时间 如果设置为0则不过期 默认为0
 * maxIdleTime 最大空闲时间 根据LRU算法清理空闲数据 如果设置为0则不检测 默认为0
 * maxSize 组最大长度 根据LRU算法清理溢出数据 如果设置为0则无限长 默认为0
 * <p>
 * 例子: test#60s、test#0#60s、test#0#1m#1000、test#1h#0#500
 */
public class GroupCacheNames {
    private GroupCacheNames() {
    }
    /**
     * OSS配置
     */
    public static final String SYS_OSS_CONFIG = "sys_oss_config";
    /**
     * AI调参参数
     */
    public static final String SYS_AI_SETTING = "sys_ai_setting";
    /**
     * 对话AI key
     */
    public static final String SYS_AI_CHAT_API_KEY = "sys_ai_chat_api_key";

    public static final String SYS_AI_IMAGE_API_KEY = "sys_ai_image_api_key";

}
