package com.chatplus.application.common.domain.response;


/**
 * @author Angus
 */
public class PayloadOnlyResponse {

    private Object payload;

    public PayloadOnlyResponse(Object payload) {
        this.payload = payload;
    }

    public static PayloadOnlyResponse of(Object payload) {
        return new PayloadOnlyResponse(payload);
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }
}
