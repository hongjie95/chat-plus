package com.chatplus.application.common.validation;

import com.chatplus.application.common.domain.response.SensitiveWordFilterResultResponse;

/**
 * 敏感词验证服务
 */
public interface SensitiveWordValidationService {
    SensitiveWordFilterResultResponse filter(String value);
}
