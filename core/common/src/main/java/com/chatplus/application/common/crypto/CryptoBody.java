package com.chatplus.application.common.crypto;

/**
 * 加密请求Body，请求体类型实现此接口；
 * 用法：<pre>{@code
 * public class LoginRequestBody implements CryptoBody {
 * }}</pre>
 *
 * @author Angus
 * @see CryptoBodyWrapper
 */
public interface CryptoBody {
    /**
     * 获取原始请求体密文
     *
     * @return 原始请求体密文
     */
    String getCipherBody();

    /**
     * 设置请求体密文
     *
     * @param cipherBody 请求体密文
     */
    void setCipherBody(String cipherBody);
}
