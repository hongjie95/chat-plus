package com.chatplus.application.common.crypto;

import java.io.Serializable;

/**
 * 请求加密值类型，可作为请求参数或请求体的字段；
 * 用法：
 * <pre>{@code
 *     @PostMapping("/login")
 *     public String login(CryptoValue password) {
 *     }
 * }</pre>
 * 或
 * <pre>{@code
 *     public class LoginRequest {
 *         private String username;
 *         private CryptoValue password;
 *         ...
 *         setter
 *         getter
 *     }
 * }</pre>
 *
 * @author Angus
 */
public class CryptoValue implements Serializable {

    private static final long serialVersionUID = 1905122041950251207L;

    /**
     * 密文
     */
    private String cipherText;
    /**
     * 明文
     */
    private String rawText;

    public CryptoValue(String cipherText) {
        this.cipherText = cipherText;
    }

    public CryptoValue(String cipherText, String rawText) {
        this.cipherText = cipherText;
        this.rawText = rawText;
    }

    public String getCipherText() {
        return cipherText;
    }

    public void setCipherText(String cipherText) {
        this.cipherText = cipherText;
    }

    public String getRawText() {
        return rawText;
    }

    public void setRawText(String rawText) {
        this.rawText = rawText;
    }
}
