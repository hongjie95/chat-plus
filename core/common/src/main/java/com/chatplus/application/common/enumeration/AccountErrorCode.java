package com.chatplus.application.common.enumeration;


import com.chatplus.application.common.exception.BadRequestException;
import com.chatplus.application.common.exception.ServiceException;
import com.chatplus.application.common.exception.status.Code;

import java.util.Objects;

/**
 * 账号相关-错误码定义
 */
public enum AccountErrorCode implements Code {
    ACCOUNT_NOT_EXIST(400, "账号不存在"),
    ACCOUNT_HAS_LOCKED(400, "账号已被锁定"),
    ACCOUNT_HAS_EXIST(400, "手机号已注册"),
    CAPTCHA_IS_INVALID(400, "短信验证码错误"),
    ACCOUNT_OR_PASSWORD_INVALID(400,"账号不存在或密码错误，请输入正确的账号，密码登录！"),
    VERIFY_CODE_REQUIRED(400, "请输入验证码"),
    PHONE_NUMBER_USED_BY_ANOTHER(400, "该手机号码已经被注册，请更换其他手机号。"),
    PHONE_NUMBER_IS_SAME(400, "新手机号码和旧手机号码不能一致"),
    VERIFY_CODE_OR_PASSWORD_REQUIRED(400, "请输入短信验证码或密码"),
    PHONE_INVALID(400, "请输入正确的电话号码"),
    ACCOUNT_INVALID(400, "请输入正确的登录账号，邮箱或者手机号！"),
    FORCE_INVITE(400, "当前系统设定必须使用邀请码才能注册"),
    INVALID_INVITE_CODE(400, "邀请码无效"),
    NEW_PASSWORD_REQUIRED(400,  "请输入新密码"),
    OLD_PASSWORD_INVALID(400,  "旧密码错误"),
    NEW_PASSWORD_AND_CONFIRM_PASSWORD_NOT_EQUAL(400,"两次输入密码不一致"),
    NEW_PASSWORD_INVALID(400, "请输入8~16位的密码"),
    PASSWORD_REQUIRED(400,"请输入账号密码"),
    ACCOUNT_NO_PERMISSION(400,"账号无权限");
    private final Integer code;
    private final String message;

    AccountErrorCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public RuntimeException get() {
        if (Objects.equals(400, getCode())) {
            return new BadRequestException(getMsg(), null);
        }
        return new ServiceException(getMsg(), getCode());
    }

    public RuntimeException toException() {
        return get();
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return message;
    }
}
