package com.chatplus.application.common.validation;

import com.chatplus.application.common.domain.response.SensitiveWordFilterResultResponse;
import com.chatplus.application.common.logging.SouthernQuietLogger;
import com.chatplus.application.common.logging.SouthernQuietLoggerFactory;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 敏感词验证器
 */
public class SensitiveWordCheckValidator implements ConstraintValidator<SensitiveWordCheck, String> {
    private static final SouthernQuietLogger LOGGER = SouthernQuietLoggerFactory.getLogger(SensitiveWordCheckValidator.class);

    private SensitiveWordValidationService sensitiveWordService;

    @Autowired
    public void setSensitiveWordService(SensitiveWordValidationService sensitiveWordVerifyService) {
        this.sensitiveWordService = sensitiveWordVerifyService;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        //不检测空字符串
        if (StringUtils.isBlank(value)) {
            return true;
        }

        SensitiveWordFilterResultResponse sensitiveWordFilterResultVo = sensitiveWordService.filter(value);

        LOGGER.message("敏感词验证").context("sensitiveWordFilterResult", sensitiveWordFilterResultVo).debug();

        return CollectionUtils.isEmpty(sensitiveWordFilterResultVo.getSensitiveWordList());
    }
}
