package com.chatplus.application.common.exception;

import com.chatplus.application.common.domain.response.TurnRightResponse;

public class UnauthorizedException extends RuntimeException {
    private final static long serialVersionUID = 5992021193625844491L;

    private Integer code;
    private Object data;

    public UnauthorizedException() {
    }

    public UnauthorizedException(String message) {
        super(message);
    }

    public UnauthorizedException(Integer code, String message, Object data) {
        super(message);
        this.code = code;
        this.data = data;
    }

    public UnauthorizedException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public UnauthorizedException(TurnRightResponse response) {
        super(response.getMessage());
        this.code = response.getCode();
        this.data = response.getData();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
