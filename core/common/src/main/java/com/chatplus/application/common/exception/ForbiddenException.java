package com.chatplus.application.common.exception;

import com.chatplus.application.common.domain.response.TurnRightResponse;

public class ForbiddenException extends RuntimeException {
    private static final long serialVersionUID = 5992021193625844491L;

    private Integer code;
    private Object data;

    public ForbiddenException() {
    }

    public ForbiddenException(String message) {
        super(message);
    }

    public ForbiddenException(Integer code, String message, Object data) {
        super(message);
        this.code = code;
        this.data = data;
    }

    public ForbiddenException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public ForbiddenException(TurnRightResponse response) {
        super(response.getMessage());
        this.code = response.getCode();
        this.data = response.getData();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
