package com.chatplus.application.common.util.enumeration;

/**
 * 带有名称的枚举接口
 *
 * @author
 */
public interface NameEnum {

    /**
     * 获取名称
     * @return 名称
     */
    String getName();

}
