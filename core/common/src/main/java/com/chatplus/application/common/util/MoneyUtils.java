package com.chatplus.application.common.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MoneyUtils {

    private MoneyUtils() {
    }

    /**
     * 多商品折扣基数10
     */
    private static final String DISCOUNT_CARDINAL_NUMBER = "10";

    public static Long convertYuanToCent(String yuan, long defaultVal) {
        try {
            return convertYuanToCent(Double.parseDouble(yuan));
        } catch (Exception e) {
            return defaultVal;
        }
    }

    public static Long convertYuanToCent(Double yuan, long defaultVal) {
        if (yuan == null) {
            return defaultVal;
        }
        return convertYuanToCent(yuan);
    }

    public static Long convertYuanToCent(double yuan) {
        return BigDecimal.valueOf(yuan).setScale(2, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).longValue();
    }

    public static double convertCentToYuanDouble(double cent) {
        return BigDecimal.valueOf(cent)
            .divide(new BigDecimal(100))
            .setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static String convertCentToYuan(long cent) {
        return convertCentToYuanBigDecimal(cent).toString();
    }

    /**
     * @param cent     金额
     * @param newScale 保留几位小数点
     * @return
     */
    public static String convertCentToYuan(long cent, int newScale) {
        return convertCentToYuanBigDecimal(cent, newScale).toString();
    }

    public static double convertCentToYuanDouble(long cent) {
        return BigDecimal.valueOf(cent)
            .divide(new BigDecimal(100))
            .setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static BigDecimal convertCentToYuanBigDecimal(long cent) {
        return BigDecimal.valueOf(cent)
            .divide(new BigDecimal(100))
            .setScale(2, RoundingMode.HALF_UP);
    }

    public static BigDecimal convertCentToYuanBigDecimal(long cent, int newScale) {
        return BigDecimal.valueOf(cent)
            .divide(new BigDecimal(100))
            .setScale(newScale, RoundingMode.HALF_UP);
    }

    public static double convertCentToYuanDouble(Long cent, double defaultVal) {
        if (cent == null) {
            return defaultVal;
        }
        return convertCentToYuanDouble(cent);
    }

    public static Long convertYuanToCent(BigDecimal bigDecimal, long defaultVal) {
        if (bigDecimal == null) {
            return defaultVal;
        }
        return convertYuanToCent(bigDecimal.doubleValue(), defaultVal);
    }

    public static long convertCentToYuanLong(long cent) {
        return convertCentToYuanBigDecimal(cent).longValue();
    }

    /**
     * 分转万元
     *
     * @param cent 分
     * @return 万元
     */
    public static long convertCentToTenThousandLong(long cent) {
        return BigDecimal.valueOf(cent)
            .divide(new BigDecimal(1000000))
            .setScale(2, RoundingMode.HALF_UP).longValue();
    }

    /**
     * 计算折扣金额向上取整,折扣是按10折计算的
     *
     * @param originalPrice 原价分
     * @param discount      折扣6.5折8折之类的
     * @return
     */
    public static long getDiscountMoney(Long originalPrice, Double discount) {
        if (null == originalPrice || null == discount) {
            return 0;
        }
        if (discount > 10) {
            return 0;
        }
        BigDecimal bigDecimal = new BigDecimal(discount + "").divide(new BigDecimal(DISCOUNT_CARDINAL_NUMBER));
        BigDecimal multiply = bigDecimal.multiply(new BigDecimal(originalPrice));
        return originalPrice - multiply.setScale(0, RoundingMode.DOWN).longValue();
    }

}
