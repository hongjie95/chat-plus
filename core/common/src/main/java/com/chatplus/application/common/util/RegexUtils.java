package com.chatplus.application.common.util;

import org.apache.commons.lang3.StringUtils;

public class RegexUtils {

    private static final String[] REGEX_SPECIAL_WORDS = {"\\", "$", "(", ")", "*", "+", ".", "[", "]", "?", "^", "{", "}", "|"};

    /**
     * 转义正则特殊字符 （$()*+.[]?\^{},|）
     */
    public static String escapeRegexSpecialWord(String keyword) {
        if (StringUtils.isBlank(keyword)) {
            return keyword;
        }
        for (String key : REGEX_SPECIAL_WORDS) {
            if (keyword.contains(key)) {
                keyword = keyword.replace(key, "\\" + key);
            }
        }
        return keyword;
    }

}
