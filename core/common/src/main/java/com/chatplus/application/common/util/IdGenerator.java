package com.chatplus.application.common.util;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;

/**
 * ID生成器
 *
 * @author chj
 * @date 2024/1/15
 **/
public class IdGenerator {

    private IdGenerator() {
    }
    private static final Snowflake SNOWFLAKE_ID_WORKER = IdUtil.getSnowflake(1, 1) ;

    public static String generateId() {
        return SNOWFLAKE_ID_WORKER.nextIdStr();
    }
    public static Long generateLongId() {
        return SNOWFLAKE_ID_WORKER.nextId();
    }
}
