package com.chatplus.application.common.util.enumeration;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * 带有值得枚举接口
 *
 * @param <V> 值类型
 * @author
 */
public interface ValueEnum<V> {

    String METHOD_NAME = "getValue";

    @JsonValue
    V getValue();

}
