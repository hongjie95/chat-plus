package com.chatplus.application.common.util;

import cn.hutool.core.util.StrUtil;
import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class StringFilterUtil {

    private static final String regEx = "[`~!#$%^&*{}|\\\\';:?/,\\u0020\\u3000\\[\\]]";
    private static final Pattern pattern = Pattern.compile(regEx);
    private static final Pattern PHONE_NUMBER_REGULAR = Pattern.compile("[零一二三四五六七八九十壹贰叁肆伍陆柒捌玖拾0-9０１２３４５６７８９]+");
    private static final String[] NUMBER_LIST_1 = new String[]{"零", "一", "二", "三", "四", "五", "六", "七", "八", "九"};
    private static final String[] NUMBER_LIST_2 = new String[]{"零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"};
    private static final String[] NUMBER_LIST_3 = new String[]{"０", "１", "２", "３", "４", "５", "６", "７", "８", "９"};
    private static final String[] NUMBER_LIST = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
    private static final String PHONE_NUMBER_REG_EXP = "^[1]\\d{10}$";

    // 清除掉指定# % ^ & * { } | \的特殊字符
    public static String stringFilterSpecialCharacters(String str) throws PatternSyntaxException {
        Matcher matcher = pattern.matcher(str);
        return matcher.replaceAll("").trim();
    }

    public static String htmlEscape(String text) {
        if (StringUtils.isEmpty(text)) {
            return text;
        }
        return HtmlUtils.htmlEscape(text.trim());
    }

    public static String stringEscape(String str) {
        return htmlEscape(stringEscape(str, false));
    }

    /**
     * 处理转义字符（多\转成单\）
     * https://blog.csdn.net/wojiushiwo945you/article/details/89334747
     *
     * @param str
     * @param compressSpace 是否压缩空格
     * @return
     */
    public static String stringEscape(String str, boolean compressSpace) {
        if (StrUtil.isNotBlank(str)) {
            //需要替换成普通空格的字符
            List<String> escapeParameter = List.of(" ", "&nbsp;", "\t");
            //处理多个连续\转成一个\\
            str = str.replaceAll("(\\\\)\\1+", "$1");
            str = str.replace("\\n", "\n");
            str = str.replace("\\t", "\t");
            str = str.replace("\\r", "\r");
            for (String s : escapeParameter) {
                str = str.replace(s, " ");
            }
            if (compressSpace) {
                str = str.replaceAll("(" + " " + ")\\1+", "$1");
            }
        }
        return str;
    }

    /**
     * 提取文本中的手机号码
     */
    public static List<String> extractPhoneNumber(String str) {
        if (StrUtil.isBlank(str)) {
            return Collections.emptyList();
        }
        List<String> list = new ArrayList<>();
        Matcher matcher = PHONE_NUMBER_REGULAR.matcher(str);
        while (matcher.find()) {
            list.add(matcher.group());
        }
        List<String> phoneNumberList = new ArrayList<>();
        list.forEach(list2 -> {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < list2.length(); i++) {
                stringBuilder.append(matchString(Character.toString(list2.charAt(i))));
            }
            phoneNumberList.add(stringBuilder.toString());
        });
        return phoneNumberList;
    }

    public static String matchString(String str) {
        switch (str) {
            case "零":
                return "0";
            case "一":
                return "1";
            case "二":
                return "2";
            case "三":
                return "3";
            case "四":
                return "4";
            case "五":
                return "5";
            case "六":
                return "6";
            case "七":
                return "7";
            case "八":
                return "8";
            case "九":
                return "9";
            case "壹":
                return "1";
            case "贰":
                return "2";
            case "叁":
                return "3";
            case "肆":
                return "4";
            case "伍":
                return "5";
            case "陆":
                return "6";
            case "柒":
                return "7";
            case "捌":
                return "8";
            case "玖":
                return "9";
            case "０":
                return "0";
            case "１":
                return "1";
            case "２":
                return "2";
            case "３":
                return "3";
            case "４":
                return "4";
            case "５":
                return "5";
            case "６":
                return "6";
            case "７":
                return "7";
            case "８":
                return "8";
            case "９":
                return "9";
            case "0":
                return "0";
            case "1":
                return "1";
            case "2":
                return "2";
            case "3":
                return "3";
            case "4":
                return "4";
            case "5":
                return "5";
            case "6":
                return "6";
            case "7":
                return "7";
            case "8":
                return "8";
            case "9":
                return "9";
            default:
                return "";
        }
    }

    public static String convertPhoneNumber(String phoneNumber) {

        if (Pattern.matches(PHONE_NUMBER_REG_EXP, phoneNumber)) {
            return phoneNumber;
        }
        //零一二三四五六七八九十壹贰叁肆伍陆柒捌玖拾0-9０１２３４５６７８９
        if (phoneNumber.length() == 22) {
            phoneNumber = phoneNumber.substring(0, 11);
            if (Pattern.matches(PHONE_NUMBER_REG_EXP, phoneNumber)) {
                return phoneNumber;
            }
        }
        for (int i = 0; i < NUMBER_LIST_1.length; i++) {
            phoneNumber = phoneNumber.replace(NUMBER_LIST_1[i], NUMBER_LIST[i]);
        }
        for (int i = 0; i < NUMBER_LIST_2.length; i++) {
            phoneNumber = phoneNumber.replace(NUMBER_LIST_2[i], NUMBER_LIST[i]);
        }
        for (int i = 0; i < NUMBER_LIST_3.length; i++) {
            phoneNumber = phoneNumber.replace(NUMBER_LIST_3[i], NUMBER_LIST[i]);
        }
        return phoneNumber;
    }

    public static String filterConsecutiveDigits(String str) {
        if (StrUtil.isBlank(str)) {
            return "";
        }
        return str.replaceAll("\\d+\\.\\d+", "*");
    }

    public static void main(String[] args) {
        List<String> list = extractPhoneNumber("一3a866521458");
        System.out.println(list);
    }
}
