package com.chatplus.application.common.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 敏感词验证注解
 *
 */
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = SensitiveWordCheckValidator.class)
public @interface SensitiveWordCheck {
    String message() default "内容包含敏感词汇，请修改后重新提交";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
