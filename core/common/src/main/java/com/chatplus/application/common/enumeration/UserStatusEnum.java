package com.chatplus.application.common.enumeration;

import com.chatplus.application.common.util.enumeration.NameValueEnum;
import com.chatplus.application.common.util.enumeration.ValueEnumUtils;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 用户状态
 *
 * @author ruoyi
 */
public enum UserStatusEnum implements NameValueEnum<Integer> {
    DISABLE(0, "停用"), OK(1, "正常"), DELETED(2, "删除");

    private final Integer value;
    private final String name;

    UserStatusEnum(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @JsonCreator
    public static UserStatusEnum get(int value) {
        try {
            return ValueEnumUtils.parse(value, UserStatusEnum.class);
        } catch (Exception e) {
            return null;
        }
    }
}
