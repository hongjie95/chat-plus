package com.chatplus.application.common.domain.response;


import com.chatplus.application.common.exception.BadRequestException;
import com.chatplus.application.common.exception.ForbiddenException;
import com.chatplus.application.common.exception.NotDevelopedException;
import com.chatplus.application.common.exception.TurnRightException;
import lombok.Data;

import java.time.Instant;

/**
 * @author Angus
 */
@Data
public class TurnRightResponse {
    public static final Integer SUCCESS = 0;
    public static final Integer REDIRECT = 302;

    private Integer code = 0;
    private String message = "success";
    private Object data;
    private Instant responseAt = Instant.now();

    public <E extends TurnRightException> TurnRightResponse(E exception) {
        this.code = exception.getCode();
        this.message = exception.getMessage();
        this.data = exception.getPayload();
    }

    public TurnRightResponse(BadRequestException exception) {
        this.code = 400;
        this.message = exception.getMessage();
        this.data = exception.getData();
    }

    public TurnRightResponse(ForbiddenException exception) {
        this.code = exception.getCode();
        this.message = exception.getMessage();
        this.data = exception.getData();
    }

    public TurnRightResponse(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public TurnRightResponse(String message, Object data) {
        this.code = SUCCESS;
        this.message = message;
        this.data = data;
    }

    public TurnRightResponse(Object data) {
        this.code = SUCCESS;
        this.data = data;
    }

    public TurnRightResponse(NotDevelopedException exception) {
        this.message = exception.getMessage();
    }

    @SuppressWarnings("unused")
    public TurnRightResponse() {
        this.code = SUCCESS;
    }

    public static TurnRightResponse ofSuccess() {
        return new TurnRightResponse();
    }

    @Override
    public String toString() {
        return "TurnRightResponse{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
