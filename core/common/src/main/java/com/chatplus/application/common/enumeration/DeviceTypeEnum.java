package com.chatplus.application.common.enumeration;

import org.apache.commons.lang3.StringUtils;

/**
 * 设备类型
 * 针对一套 用户体系
 *
 * @author Lion Li
 */
public enum DeviceTypeEnum {
    /**
     * pc端
     */
    PC("pc"),

    /**
     * app端
     */
    APP("app"),

    /**
     * 小程序端
     */
    XCX("xcx");

    DeviceTypeEnum(String device) {
        this.device = device;
    }
    private final String device;

    public String getDevice() {
        return device;
    }

    public static DeviceTypeEnum findDevice(String device) {
        if (StringUtils.isBlank(device)) {
            return null;
        }
        for (DeviceTypeEnum type : values()) {
            if (type.getDevice().equals(device)) {
                return type;
            }
        }
        return null;
    }
}
