package com.chatplus.application.common.enumeration;

/**
 * 文件大小单位
 *
 * @author chj
 * @date 2023/6/14
 **/
public enum FileSizeUnit {
    BIT,
    KB,
    MB,
    GB,
    TB;
}
