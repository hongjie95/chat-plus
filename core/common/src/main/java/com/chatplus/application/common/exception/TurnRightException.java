package com.chatplus.application.common.exception;


import com.chatplus.application.common.domain.response.TurnRightResponse;
import com.chatplus.application.common.exception.status.Code;
import com.chatplus.application.common.exception.status.ErrorCode;
import com.chatplus.application.common.util.CommonUtils;

/**
 * @author Angus
 */
public class TurnRightException extends RuntimeException implements Code {
    private static final long serialVersionUID = 8918971145316349533L;

    private Code code;
    private String detail;
    private Object payload;

    public TurnRightException(Code code, String detail) {
        super(CommonUtils.isEmpty(detail) ? code.getMsg() : code.getMsg() + ":" + detail + ";");
        this.code = code;
        this.detail = detail;
    }

    public TurnRightException(Code code, String detail, Object payload) {
        super(CommonUtils.isEmpty(detail) ? code.getMsg() : code.getMsg() + ":" + detail + ";" + payload);
        this.code = code;
        this.detail = detail;
        this.payload = payload;
    }

    public TurnRightException(TurnRightResponse message) {
        super(message.getMessage());
        this.code = ErrorCode.getEnum(message.getCode());
        this.payload = message.getData();
    }

    public TurnRightException(Code code) {
        super(code.getMsg());
        this.code = code;
    }


    @Override
    public Integer getCode() {
        return code.getCode();
    }

    @Override
    public String getMsg() {
        return null;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }
}
