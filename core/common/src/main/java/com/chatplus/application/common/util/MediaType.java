package com.chatplus.application.common.util;

import lombok.Getter;

@Getter
public enum MediaType {

    MULTIPART_FORM_DATA("multipart/form-data"),
    APPLICATION_JSON("application/json"),
    APPLICATION_JSON_UTF8("application/json;charset=UTF-8"),
    APPLICATION_OCTET_STREAM("application/octet-stream");

    private final String value;

    MediaType(String value) {
        this.value = value;
    }

}
