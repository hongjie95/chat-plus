package com.chatplus.application.common.crypto;

import java.io.InputStream;

public interface RequestMessageDecryptor {
    byte[] decrypt(byte[] bytes);

    byte[] decrypt(InputStream data);

    String decrypt(String data);

}
