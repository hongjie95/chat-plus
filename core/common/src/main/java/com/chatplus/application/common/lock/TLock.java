package com.chatplus.application.common.lock;

import org.redisson.api.RLock;

public class TLock implements AutoCloseable {

    private final RLock lock;

    public TLock(RLock lock) {
        this.lock = lock;
    }

    public void unlock() {
        if (lock == null) {
            return;
        }
        try {
            lock.unlock();
        } catch (Exception ignored) {
        }
    }

    @Override
    public void close() {
        unlock();
    }
}
